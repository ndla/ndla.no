<?php
global $base_url, $base_path;
$url = $base_url . $base_path . 'ndlatimeline/' . $nid . '/json';
?>

<div id="page">
  <script>
    $(document).ready(function() {
      createStoryJS({
        type:   'timeline',
        width:    '632',
        height:   '632',
        source:   '<?php print $url; ?>',
        embed_id: 'my-timeline'
      });
    });
  </script>
  <div id="my-timeline"></div>
</div>