utdanning_rdf_admin_move_count = 0;
utdanning_rdf_admin_move_total = 0;

Drupal.behaviors.utdanning_rdf_admin_move = function () {
  $('#edit-submit').click(function(){
    var from = $('#edit-from').val();
    var to = $('#edit-to').val();
    if(from == to) {
      alert(Drupal.t("From and to can't be equal"));
      return false;
    }
    $('#edit-from, #edit-to').attr('disabled', 'diabled');
    $('div#submit, div#result').toggle();
    
    
    $.getJSON(Drupal.settings.basePath + "admin/settings/rdf/utdanning_rdf_move/count/?from=" + encodeURIComponent(from), function(data){
      utdanning_rdf_admin_move_total = data['count'];
      if(utdanning_rdf_admin_move_total > 0) {
        percent = Math.round((utdanning_rdf_admin_move_count / utdanning_rdf_admin_move_total) * 100) + "% (" + utdanning_rdf_admin_move_count + " av " + utdanning_rdf_admin_move_total + ")";
        utdanning_rdf_admin_move(from, to);
      } else {
        percent = Drupal.t('No nodes found');
        $('div#result img').hide();
      }
      $('span#percent').html(percent);
    });
    
    return false;
  });
}

utdanning_rdf_admin_move = function(from, to) {
  $.getJSON(Drupal.settings.basePath + "admin/settings/rdf/utdanning_rdf_move/process/?from=" + encodeURIComponent(from) + "&to=" + encodeURIComponent(to), function(data){
    if(data.count == 0) {
      utdanning_rdf_admin_move_count = utdanning_rdf_admin_move_total;
    } else {
      utdanning_rdf_admin_move_count += data.count;
    }
    percent = Math.round((utdanning_rdf_admin_move_count / utdanning_rdf_admin_move_total) * 100) + "% (" + utdanning_rdf_admin_move_count + " av " + utdanning_rdf_admin_move_total + ")";
    $('span#percent').html(percent);
    $('div#list div.form-item').append(data.nodes);
    if(utdanning_rdf_admin_move_count < utdanning_rdf_admin_move_total) {
       utdanning_rdf_admin_move(from, to);
    } else {
      $('div#result img').hide();
    }
  });
}