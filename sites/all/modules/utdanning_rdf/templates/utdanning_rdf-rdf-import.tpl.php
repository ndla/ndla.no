@prefix ndla: <<?php print $params['uri']; ?>#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .

#Create the normal relation
<<?php print $params['uri_relation']; ?>>
	<<?php print $params['uri_relation']; ?>> "<?php print $params['rel_name_nb']; ?>"@nb;
	<<?php print $params['uri_relation']; ?>> "<?php print $params['rel_name_nn']; ?>"@nn;
	<<?php print $params['uri_relation']; ?>> "<?php print $params['rel_name_en']; ?>"@en.

#Add the inverse relation
<<?php print $params['uri_inverse_relation']; ?>>
  <<?php print $params['uri_inverse_relation']; ?>> "<?php print $params['inverse_rel_name_nb']; ?>"@nb;
  <<?php print $params['uri_inverse_relation']; ?>> "<?php print $params['inverse_rel_name_nn']; ?>"@nn;
  <<?php print $params['uri_inverse_relation']; ?>> "<?php print $params['inverse_rel_name_en']; ?>"@en.
