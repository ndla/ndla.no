<?php

/* Form for nodemenu node. */
function ndla_menu_nodemenu_form(&$form, &$form_state) {
  /* Menu fieldset container and json post field */
  $form['ndla_menu_type'] = array(
    '#type' => 'hidden',
    '#value' => (arg(3) == 'new') ? 1 : 0,
  );
  
  if(isset($form['#node']->ndla_menu_type)) {
    $form['ndla_menu_type'] = array(
      '#type' => 'hidden',
      '#value' => $form['#node']->ndla_menu_type,
    );
  }
  
  $form['ndla_menu'] = array(
    '#tree' => TRUE,
    '#title' => 'NDLA Menu',
    '#type' => 'fieldset',
    'json' => array(
      '#type' => 'hidden',
    ),
  );
  
  /* Edit fieldset and common fields */
  $terms = ndla_menu_get_terms();
  $languages = ndla_menu_get_languages();
  $form['ndla_menu']['item'] = array(
    '#prefix' => '<div id="ndla-menu-item-form">',
    '#suffix' => '<ul class="item-controls"><li><a class="save-item" href="#"><span class="icon"></span>' . t('Save item') . '</a></li><li><a class="remove-item" href="#"><span class="icon"></span>' . t('Remove item') . '</a></li><li><a class="cancel-item" href="#"><span class="icon"></span>' . t('Cancel') . '</a></li></ul></div>',
    'language' => array(
      '#type' => 'select',
      '#options' => $languages,
    ),
    'id' => array(
      '#type' => 'hidden',
    ),
  );

  /* Edit fields for languages */
  foreach($languages as $lang => $language) {
    $extra = ($form['ndla_menu_type']['#value'] == 1) ? 'boombasticfagforsider/' : '';
    $ac_path = 'ndla-menu/autocomplete/path/' . $extra . $lang;
    $form['ndla_menu']['item'][$lang] = array(
      'title' => array(
        '#prefix' => "<div class='menu-item-edit-lanugage $lang'>",
        '#title' => t('Title'),
        '#type' => 'textfield',
      ),
      'path' => array(
        '#type' => 'textfield',
        '#title' => t('Path'),
        '#description' => t('Specify the internal link path, e.g. %example_1 or %example_2.', array('%example_1' => 'node/1337', '%example_2' => 'treeoflife')),
        '#autocomplete_path' => $ac_path,
        '#attributes' => array(
          'onchange' => 'ndla_menu_set_title(this);',
        ),
      ),
      'published' => array(
        '#type' => 'checkbox',
        '#title' => t('Published'),
        '#description' => t('Make this menu item viewable to the public.'),
      ),
      'terms' => array(
        '#type' => 'checkboxes',
        '#title' => t('Year'),
        '#options' => $terms,
        '#description' => t('Associate this menu item with a school year.'),
        '#attributes' => array('class' => 'ndla-menu-terms ndla-menu-terms-' . $lang)
      ),
      'image_upload' => array(
        '#suffix' => "<img src='' class='ndla-menu-image-preview $lang'><br><a class='ndla-menu-image-remove $lang' href='javascript:void(0);' onclick='NdlaMenu.remove_image(this);'>" . t('Remove image') . "</a>",
        '#type' => 'textfield',
        '#title' => t('Image'),
        '#autocomplete_path' => 'ndla-menu/autocomplete/image',
      ),
      'image_path' => array(
        '#type' => 'hidden',
      ),
      'image_id' => array(
        '#type' => 'hidden',
      ),
      'image_height' => array(
        '#type' => 'hidden',
      ),
      'image_width' => array(
        '#type' => 'hidden',
      ),
      'expanded' => array(
        '#type' => 'checkbox',
        '#title' => t('Expanded'),
        '#description' => t('Make this menu item expanded by default. This will only work if the menu item has children.'),
      ),
      'description' => array(
        '#type' => 'textarea',
        '#title' => t('Description'),
        '#description' => t('A short description(one paragraph).'),
      ),
      'include_text' => array(
        '#type' => 'checkbox',
        '#title' => t('Include text'),
        '#description' => t('Display the topic description on the page we are linking to.'),
      ),
      'has_content' => array(
        '#type' => 'radios',
        '#title' => t('Sub menus'),
        '#options' => array(t('Default behaviour'), t('Load in left menu'), t('List with description and images')),
        '#description' => t('Decide how to display child menu items.'),
        '#suffix' => "</div>"
      ),
    );
  }
  $form['#after_build'][] = 'ndla_menu_after_build';
  $form['#validate'][] = 'ndla_menu_validate';
}

/* Autocomplete for path field */
function ndla_menu_autocomplete_path($lang, $search = '') {
  if(!empty($lang) && in_array($lang, array_keys(language_list()))) {
    $extra = " AND language IN ('', '$lang')";
  }

  $node_paths = array();
  $result = db_query("SELECT n.nid AS id, n.title, n.language, n.type FROM {node} n WHERE (n.type NOT IN ('emneside', 'fagressurs', 'emneartikkel')) AND (n.title LIKE '%s%%' OR n.nid = %d) $extra", $search, $search);
  while ($node = db_fetch_object($result)) {
    $node_paths['node/' . $node->id] = $node->title . ($node->language ? ' (' . $node->language . ')' : NULL) . ' (' . $node->type . ')';
  }
  drupal_json($node_paths);
}

/* Autocomplete for path field */
function ndla_menu_autocomplete_path_new($lang, $search = '') {
  if(!empty($lang) && in_array($lang, array_keys(language_list()))) {
    $extra = " AND language IN ('', '$lang')";
  }

  $node_paths = array();
  $result = db_query("SELECT n.nid AS id, n.title, n.language, n.type FROM {node} n WHERE (n.type IN('emneartikkel', 'fagressurs')) AND (n.title LIKE '%s%%' OR n.nid = %d) $extra", $search, $search);
  while ($node = db_fetch_object($result)) {
    $node_paths['node/' . $node->id] = $node->title . ($node->language ? ' (' . $node->language . ')' : NULL) . ' (' . $node->type . ')';
  }
  drupal_json($node_paths);
}

/* Autocomplete for path field */
function ndla_menu_autocomplete_image($search = '') {
  $node_paths = array();
  $result = db_query("SELECT n.nid AS id, n.title, n.language, n.type FROM {node} n WHERE (n.title LIKE '%s%%' OR n.nid = %d) AND n.type = 'image'", $search, $search);
  while ($node = db_fetch_object($result)) {
    $node_paths[$node->title . ' [nid:' . $node->id . ']'] = $node->title . ' [nid:' . $node->id . ']';
  }
  drupal_json($node_paths);
}

function ndla_menu_get_states(&$items) {
  $languages = ndla_menu_get_languages();
  foreach($items as $item) {
    foreach($languages as $language => $name) {
      if(!empty($item->$language->path)) {
        $status = ndla_menu_get_status($item->$language->path);
        $item->$language->status = $status;
      }
    }
  }
}

function ndla_menu_get_status($path) {
  $states = ndla_publish_workflow_get_states();
  if(preg_match('/^node\/([\d]*)$/', strtolower($path), $matches)) {
    $nid = $matches[1];
    $result = db_query('SELECT status FROM ndla_publish_workflow_status WHERE nid = %d ORDER BY timestamp DESC LIMIT 1', $nid);
    while ($data = db_fetch_object($result)) {
      return $states[$data->status];
    }
  }
  return '';
}

function ndla_menu_status() {
  $status = ndla_menu_get_status($_GET['path']);
  echo drupal_json(array('status' => $status));
}

function ndla_menu_menu_edit($menu) {
  /* Setup empty item for new items */
  $item = new stdClass();
  $item->id = 'new';
  $item->parent_id = 0;
  $menu[] = $item;
  
  /* Build menu */
  $menu_tree = ndla_menu_edit($menu);
  $menu = theme('ndla_menu_menu_edit', $menu_tree);
  
  return $menu;
}

function ndla_menu_edit($items) {
  $html = '';
  $languages = ndla_menu_get_languages();
  foreach($items as $item) {
    if(!empty($item->children)) {
      $item->children = ndla_menu_edit($item->children);
    }
    $item->li_classes = 'menu-item-' . $item->id;
    $item->classes = 'item';
    $item_valid = FALSE;
    $language_valid = TRUE;
    foreach($languages as $language => $name) {
      if(empty($item->$language->title)) {
        $language_valid = $language_valid && ndla_menu_is_empty_array($item->$language);
      } else {
        $item_valid = TRUE;
      }
    }
    if((!$item_valid || !$language_valid) && $item->id != 'new') {
      $item->classes .= ' menu-item-error';
    }
    if (!empty($item->children)) {
      $item->classes .= ' children';
      if (!empty($item->expanded)) {
        $item->li_classes .= ' open';
      }
    }
    $html .= theme('ndla_menu_item_edit', $item);
  }
  return $html;
}

function ndla_menu_upload() {
  $source = '';
  foreach($_FILES['files']['error'] as $name => $value) {
    if($value == 0) {
      $source = $name;
      $parts = explode('_', $name);
      $language = end($parts);
    }
  }
  $file = file_save_upload($source, array('ndla_menu_image_validate' => array()));
  $error = '';
  if ($file) {
    $path = file_directory_path() . '/ndla-menu/';
    if (!is_dir($path)) {
      mkdir($path);
    }
    if (file_move($file->filepath, $path)) {
      db_query("UPDATE {files} SET filename = '%s', filepath = '%s', status = 1 WHERE fid = %d", $file->filename, $file->filepath, $file->fid);
      $size = @getimagesize($file->filepath);
      $image_file_id = $file->fid;
      $image_width = $size[0];
      $image_height = $size[1];
    }
  } else {
    drupal_get_messages('error');
    $error = t('The file must be an image with a width of 210px.');
  }
  
  $result = array(
    'id' => $file->fid,
    'width' => $size[0],
    'height' => $size[1],
    'path' => $file->filepath,
    'language' => $language,
    'error' => $error,
  );
  
  echo json_encode($result);
  exit();
}

function ndla_menu_image_validate($file) {
  $errors = array();
  $size = @getimagesize($file->filepath);
  if ($size) {
    if ($size[0] != 210) {
      $errors[] = t("Image must have a width of 210px.");
    }
  } else {
    $errors[] = t("File must be an image.");
  }
  return $errors;
}