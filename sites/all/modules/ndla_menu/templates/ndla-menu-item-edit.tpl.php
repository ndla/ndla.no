<?php $languages = ndla_menu_get_languages(TRUE); ?>
<li class="<?php print $menu_item->li_classes ?>">
  <div class="<?php print $menu_item->classes ?>">
    <?php if ($menu_item->children != ''): ?>
      <a href="javascript:void(0);" class="arrow"></a>
    <?php else: ?>
      <span class="arrow"></span>
    <?php endif ?>
    <a class="edit" href="javascript:void(0);" onclick="NdlaMenu.edit(this);"><?php print t('Edit') ?></a>
    <div class="name">
      <span class="title">
      <?php
      foreach($languages as $language => $name) {
        if(!empty($menu_item->$language->title)) {
          print check_plain($menu_item->$language->title);
          break;
        }
      }
      ?></span>
        <span class="translations">
          <?php foreach ($languages as $language => $name): ?>
            <span class="<?php print check_plain($language); if(!$menu_item->$language->published) print ' unpublished'; ?>"<?php if(empty($menu_item->$language->title)) print ' style="display: none;"'; ?>>
              <?php print check_plain($language) ?>
            </span>
            <span class='node-status'>
              <?php if(!empty($menu_item->$language->status)): ?>
              <?php print check_plain($menu_item->$language->status) ?>
              <?php endif; ?>
            </span>
          <?php endforeach ?>
        </span>
      
    </div>
  </div>
  <?php if ($menu_item->children != ''): ?>
    <ul>
      <?php print $menu_item->children ?>
    </ul>
  <?php endif ?>
</li>