<?php

/**
 * Remove duplicate leaf nodes in ndla menus.
 */
function ndla_menu_remove_dups() {
  // Find all menus
  $query = "SELECT id FROM {ndla_menu_menus}";
  $result = db_query($query);

  $item_list = array();
  $dup_count = 0;
  while ($menu = db_fetch_object($result)) {
    // Find all leaf nodes in menu
    // Note: The reason for doing this per menu is that we then utilize the existing index
    $query = "SELECT i1.*, t.title, t.language, t.path
              FROM {ndla_menu_items} i1
              INNER JOIN {ndla_menu_translations} t ON i1.id = t.menu_item_id
              WHERE menu_id=%d
                AND i1.parent_id > 0
                AND (SELECT COUNT(*)
                     FROM {ndla_menu_items} i2
                     WHERE menu_id=%d
                       AND i2.parent_id=i1.id
                    ) = 0
              ORDER BY i1.parent_id, i1.id";
    $result2 = db_query($query, $menu->id, $menu->id);
    while ($menu_item = db_fetch_object($result2)) {
      // Items with same parent, title and language are considered duplicates
      $hash = md5($menu_item->parent_id . $menu_item->title . $menu_item->language . $menu_item->path);

      if ($menu_item->language == '') {
        // We do not want to 
        continue;
      }
      if (isset($item_list[$hash])) {
        db_query("DELETE i, t FROM {ndla_menu_items} i JOIN {ndla_menu_translations} t ON i.id = t.menu_item_id WHERE i.id = %d", $menu_item->id);
        $dup_count++;
      }
      else {
        $item_list[$hash] = TRUE;
      }
    }
  }

  print "Removed {$dup_count} duplicates";
}

function ndla_menu_trimma_moped(&$form_state) {
  if(!empty($form_state['post'])) {
    db_query("UPDATE {ndla_menu_translations} SET title = TRIM(CHAR(9) FROM TRIM(title))");
    drupal_set_message(t('Removed all whitespaces from the beginning and the end of the menu link title'));
  }
  
  $query = 'SELECT COUNT(*) AS num FROM {ndla_menu_translations} WHERE title LIKE "\t%" OR title LIKE " %"';
  $num = db_fetch_object(db_query($query))->num;
  
  $form['text'] = array(
    '#type' => 'markup',
    '#value' => '<p>' .t('There are @num rows in the database which begin with whitespaces', array('@num' => $num)) . '</p>',
  );
  
  if($num > 0) {
    $form['button'] = array(
      '#type' => 'submit',
      '#value' => t('Remove white spaces from menu links'),
    );
  }
  else {
    $form['text']['#value'] = '<p>' . t('Nothing to do. The data looks OK.') . '</p>';
  }

  return $form;
}

function ndla_menu_show_whitespace_nodes(&$form_state) {
  $result = db_query("
    SELECT mt.title as menu_item_link, n.title as node_title, n.nid FROM {ndla_menu_translations} mt
    INNER JOIN {ndla_menu_items} items ON items.id = mt.menu_item_id
    INNER JOIN {ndla_menu_menus} menus ON menus.id = items.menu_id
    INNER JOIN {node} n ON n.nid = menus.`nid`
    WHERE mt.title LIKE \"\t%\" OR mt.title LIKE \" %\" 
    GROUP BY mt.path,mt.title
    ORDER by n.title, mt.title");
  $form['nodes'] = array('#type' => 'markup', '#value' => 'No items contain whitespaces, nothing to show you.');
  $items = array();
  while($row = db_fetch_object($result)) {
    $items[] = l($row->menu_item_link, 'node/' . $row->nid) . " (in node " . $row->node_title . " # " . $row->nid . ")";
  }

  if(!empty($items)) {
    $form['nodes']['#value'] = theme('item_list', $items);
  }

  return $form;
}