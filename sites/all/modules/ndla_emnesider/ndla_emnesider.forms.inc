<?php



/**
 * Implementation of hook_form_alter()
 */
function ndla_emnesider_form_alter(&$form, &$form_state, $form_id) {
  if($form_id == 'contentbrowser_search_form') {
    $ref = !empty($_REQUEST['nodereference']) ? $_REQUEST['nodereference'] : NULL;
    if($ref) {
      $strip_if_field = array(
        'field-visual-element',
        'field-banner-image',
        'field-banner-image-small',
        'field-ingress-bilde',
        'field-related-emneartikkel'
      );
      if(in_array($ref, $strip_if_field)) {
        $form['ndla_type']['#access'] = FALSE;
      }
    }
  }
  if(!empty($form['#node']) && $form['#node']->type . '_node_form' == $form_id) {
    //echo "<pre>" . print_r($form['#node'], true) . "</pre>"; die("OK");
    if(!empty($_REQUEST['fag'])) {
      if(!ndla_emnesider_is_valid_fag($form['#node']->type, check_plain($_REQUEST['fag']))) {
        if(arg(1) == 'add') {
          drupal_set_message(t('The subject you choose is not valid for this content type.'), 'warning', FALSE);
        }
        
        drupal_goto(implode("/", arg()));
      }
    }

    drupal_add_js(drupal_get_path('module', 'ndla_emnesider') . "/js/ndla_emnesider.js");
    
    /*if(isset($form['field_from_node']) && empty($_REQUEST['fagstoff'])) {
      $form['field_from_node']['#access'] = FALSE;
    }*/
    if($form['#node']->type == 'emneside') {
      ndla_emnesider_strip_emneside_form($form);
    }
    else if($form['#node']->type == 'fagressurs') {
      ndla_emnesider_strip_fagressurs_form($form);
    }
    else if($form['#node']->type == 'emneartikkel') {
      if (isset($form['#node']->form_content_language)) {
        $form['language']['#default_value'] = $form['#node']->form_content_language;
      }

      if(empty($_GET['fagstoff'])) {
        $form['ndla_select_fagstoff'] = array(
          '#type' => 'fieldset',
          '#title' => t('Select Fagstoff node to convert'), 
          '#weight' => -5000,
          '#collapsible' => TRUE, 
          '#collapsed' =>FALSE,
        );
        
        $form['ndla_select_fagstoff']['fagstoff_nid'] = array(
          '#type' => 'textfield',
          '#title' => t('Fagstoff'),
          '#size' => 60,
          '#maxlength' => 128,
          '#required' => FALSE,
          '#autocomplete_path' => 'ndla_emnesider/fagstoff/autocomplete',
          '#description' => t('<strong>NOTE</strong>: Any unsaved data will be lost!'),
        );

        $form['ndla_select_fagstoff']['fagstoff_select_button'] = array(
          '#name' => 'fagstoff_select_button',
          '#type' => 'button', 
          '#value' => t('Select'),
        );
      }
      

      ndla_emnesider_strip_emneartikkel_form($form);
    }
    else if($form['#node']->type == 'nodemenu') {
      ndla_emnesider_strip_nodemenu($form);
    }
    else {
      ndla_emnesider_strip_old_content($form);
    }
  }
}

function ndla_emnesider_form_select_fag($form, &$form_state) {
  echo 'YOLO';exit;
}

/**
 * Removes fields from the emneside node form.
 */
function ndla_emnesider_strip_emneside_form(&$form) {
  //The fields which is not wanted
  $unwanted_fields = array(
    'options',
    'book',
    'path',
    'og_private',
    'og_language',
    'og_description',
    'og_selective',
    'og_directory',
    'og_register',
    'themes',
    'utdanning_rdf',
    'additional_license_informaiton',
    'nodewords',
    'mail_to_red',
    'show_edit_dates',
    'lighbox2_conf',
    'ndla_notify',
    'ndla_solr',
    'ndla_paragraphs',
  );
  
  foreach($unwanted_fields as $field) {
    if(isset($form[$field])) {
      $form[$field]['#access'] = FALSE;
    }
  }
  
  if(!empty($form['ndla_content_translation'])) {
    $form['ndla_content_translation']['#title'] = t('Text');
  }
  
  ndla_menu_form_fag_node_form_alter($form);
  $form['ndla_menu']['topic']['#access'] = FALSE;
  $form['ndla_menu']['left']['#autocomplete_path'] = 'ndla-menu/autocomplete/menu/boombasticfagforsider';
}

/**
 * Removes fields from the emneside node form.
 */
function ndla_emnesider_strip_fagressurs_form(&$form) {
  //The fields which is not wanted

  $unwanted_fields = array(
    'options',
    'book',
    'path',
    'utdanning_rdf',
    'additional_license_informaiton',
    'nodewords',
    'mail_to_red',
    'lighbox2_conf',
    'ndla_notify',
    'ndla_solr',
    'og_nodeapi',
  );
  
  foreach($unwanted_fields as $field) {
    if(isset($form[$field])) {
      $form[$field]['#access'] = FALSE;
    }
  }
  
  if(!empty($form['ndla_workflow'])) {
    $form['ndla_workflow']['#collapsed'] = FALSE;
  }
  if(!empty($form['group_visuellt_element'])) {
    $form['group_visuellt_element']['#collapsed'] = FALSE;
  }
  if(!empty($form['field_ingress_bilde'][0])) {
    $form['field_ingress_bilde'][0]['#required'] = TRUE;
  }

  ndla_emnesider_set_body_format($form);
}

function ndla_emnesider_set_body_format(&$form) {
  foreach(element_children($form['ndla_paragraphs']['paragraphs']) as $index) {
    $form['ndla_paragraphs']['paragraphs'][$index]['left']['format']['#prefix'] = "<div style='display: none;'>";
    $form['ndla_paragraphs']['paragraphs'][$index]['left']['format']['#suffix'] = "</div>";
    foreach(element_children($form['ndla_paragraphs']['paragraphs'][$index]['left']['format']) as $format) {
      $form['ndla_paragraphs']['paragraphs'][$index]['left']['format'][$format]['#default_value'] = variable_get('ndla_emnesider_body_format', 2);
      if($format != variable_get('ndla_emnesider_body_format', 2)) {
        $form['ndla_paragraphs']['paragraphs'][$index]['left']['format'][$format]['#access'] = FALSE;
      }
    }
  }
}

/**
 * Removes fields from the emneside node form.
 */
function ndla_emnesider_strip_emneartikkel_form(&$form) {
  //The fields which is not wanted
  $unwanted_fields = array(
    'options',
    'book',
    'path',
    'utdanning_rdf',
    //'additional_license_informaiton',
    'nodewords',
    'mail_to_red',
    'lighbox2_conf',
    'ndla_notify',
    //'ndla_solr',
    'og_nodeapi',
  );
  
  foreach($unwanted_fields as $field) {
    if(isset($form[$field])) {
      $form[$field]['#access'] = FALSE;
    }
  }

  if(!empty($form['ndla_workflow'])) {
    $form['ndla_workflow']['#collapsed'] = FALSE;
  }
  if(!empty($form['group_visuellt_element'])) {
    $form['group_visuellt_element']['#collapsed'] = FALSE;
  }
  if(!empty($form['field_ingress_bilde'][0])) {
    $form['field_ingress_bilde'][0]['#required'] = TRUE;
  }

  $form['title']['#description'] = 'Legg in begrep som skal være overskrift i artikkelen';
  $form['field_ingress_bilde'][0]['#description'] = 'Bilde skal benyttes i Googles søkeliste og i sosiale media.';
  $form['field_ingress_bilde'][0]['#required'] = FALSE;

  //Change titles om ndla_subject_connection fields.
  $form['ndla_subject_connection']['#title'] = 'Koble emneartikkelen til fag <font color="red">*</font>';
  foreach(element_children($form['ndla_subject_connection']['ndla_subject_connections']) as $index) {
    if(!empty($form['ndla_subject_connection']['ndla_subject_connections'][$index]['tid'])) {
      $form['ndla_subject_connection']['ndla_subject_connections'][$index]['tid']['#title'] = 'Innholdskategori';
    }
    if(!empty($form['ndla_subject_connection']['ndla_subject_connections'][$index]['node_nid'])) {
      $form['ndla_subject_connection']['ndla_subject_connections'][$index]['node_nid']['#title'] = 'Velg fag';
    }
  }

  ndla_emnesider_set_body_format($form);
  
  if(isset($_GET['fagstoff'])) {
    //If we are picking up data from a fagstoff make sure
    //default_value is preserverd. For some reason taxonomy play well
    //with us without this.

    if(isset($form['taxonomy']['tags'])) {
      foreach($form['taxonomy']['tags'] as $vid => $data) {
        if(!isset($data['#default_value'])) {
          continue;
        }

        $terms = array();
        foreach($form['#node']->taxonomy as $tid => $taxdata) {
          if($taxdata->vid == $vid) {
            $term = taxonomy_get_term($tid);
            $terms[] = $term->name;
          }
        }

        if(empty($data['#default_value'])) {
          $form['taxonomy']['tags'][$vid]['#default_value'] = implode(", ", $terms);
        }
      }
    }

    foreach($form['taxonomy'] as $vid => $data) {
      if(!isset($data['#default_value'])) {
        continue;
      }

      $terms = array();
      foreach($form['#node']->taxonomy as $tid => $taxdata) {
        if($taxdata->vid == $vid) {
          $terms[] = $tid;
        }
      }

      if(empty($data['#default_value'])) {
        $form['taxonomy'][$vid]['#default_value'] = $terms;
      }
    }
  }
}

function ndla_emnesider_strip_nodemenu(&$form) {
  if(!empty($form['#node']->ndla_menu_type) || arg(3) == 'new') {
    $list = language_list();
    $list['und'] = new stdClass();
    $hide_fields = array('title', 'image_upload', 'expanded', 'description', 'include_text', 'has_content');
    foreach($list as $prefix => $dummy) {
      if(!empty($form['ndla_menu']['item'][$prefix])) {
        foreach($hide_fields as $field) {
          if(!empty($form['ndla_menu']['item'][$prefix][$field]['#prefix'])) {
            $form['ndla_menu']['item'][$prefix][$field]['#prefix'] .= "<div style='display:none'>";
          }
          else {
            $form['ndla_menu']['item'][$prefix][$field]['#prefix'] = "<div style='display:none'>";
          }
          
          if(!empty($form['ndla_menu']['item'][$prefix][$field]['#suffix'])) {
            $form['ndla_menu']['item'][$prefix][$field]['#suffix'] .= "</div>";
          }
          else {
            $form['ndla_menu']['item'][$prefix][$field]['#suffix'] = "</div>";
          }
        }
      }
    }
  }
}
