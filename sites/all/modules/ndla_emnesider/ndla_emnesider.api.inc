<?php

/**
 * @param $node
 *  The node from which we will fetch the related emnartikkels
 * @param $sort
 *  If 'draggable': return the items in order as the editor added them
 *  If 'alphabetical': return the items in alphabetical order
 * @return
 *  Array with items
 */
function ndla_emnesider_get_related_emneartikkel($node, $sort = 'draggable') {
  $items = array();
  $order = array();
  $items_out = array();
  $index = 0;

  if(!empty($node->field_related_emneartikkel)) {
    foreach($node->field_related_emneartikkel as $item) {
      $items[] = $item['nid'];
      //Store the order of the articles.
      $order[$item['nid']] = $index++;
    }
  }
  
  if(!empty($items)) {
    $ids = implode(",", $items);
    //Dont do node_load - this is faster.
    $result = db_query("SELECT nid, title FROM {node} WHERE nid IN(%s) ORDER BY title ASC", $ids);
    $new_order = 0;
    while($row = db_fetch_object($result)) {
      $placement = ($sort == 'alphabetical') ? $new_order++ : $order[$row->nid];
      $items_out[$placement] = array(
        'nid' => $row->nid,
        'title' => $row->title,
      );
    }
  
    ksort($items_out);
    $items_out_tmp = $items_out;
    $items_out = array();
    
    //Make sure nid is used as the key
    foreach($items_out_tmp as $data) {
      $items_out[$data['nid']] = $data;
    }
  }
  
  return $items_out;
}

/**
 * Returns the referenced nodes from the ndla_relations
 */
function ndla_emnesider_get_resources($node) {
  if(!empty($node->relations)) {
    $items_out = array();
    foreach($node->relations as $data) {
      $items_out[$data['node_nid']] = array();
      $row = db_fetch_object(db_query("SELECT nid,title,language,type FROM {node} WHERE nid = %d", $data['node_nid']));
      $items_out[$data['node_nid']]['title'] = $row->title;
      $items_out[$data['node_nid']]['nid'] = $row->nid;
      $items_out[$data['node_nid']]['language'] = $row->language;
      $items_out[$data['node_nid']]['type'] = $row->type;
      $items_out[$data['node_nid']]['terms'] = array();
      $data['tid'] = !is_array($data['tid']) ? array($data['tid']) : $data['tid'];
      foreach($data['tid'] as $tid) {
        $term = taxonomy_get_term($tid);
        $name = tt('taxonomy:term:' . $term->tid . ':name', $term->name);
        if(empty($name)) {
          $term->name;
        }
        $items_out[$data['node_nid']]['terms'][$term->tid] = $name;
      }
    }
  }

  return $items_out;
}

/**
 * Returns the subject connections
 */
function ndla_emnesider_get_subject_connections($node) {
  $node_tmp = $node;
  $node_tmp->relations = $node->ndla_subject_connections;
  return ndla_emnesider_get_resources($node_tmp);
}

/**
 * Returns the filepaths for the banner images.
 */
function ndla_emnesider_get_banners($node) {
  $data = array('banner' => NULL, 'banner_small' => NULL);
  if(!empty($node->field_banner_image[0]['nid'])) {
    $nid = $node->field_banner_image[0]['nid'];
    $data['banner'] = ndla_emnesider_get_filepath($nid);
  }
  if(!empty($node->field_banner_image_small[0]['nid'])) {
    $nid = $node->field_banner_image_small[0]['nid'];
    $data['banner_small'] = ndla_emnesider_get_filepath($nid);
  }

  return $data;
}

/**
 * Returns data related to the visual element.
 */
function ndla_emnesider_get_visual_element($node) {
  $data = array();
  if(!empty($node->field_visual_element[0]['nid'])) {
    $nid = $node->field_visual_element[0]['nid'];
    $small = ndla_utils_load_node($nid);
    $data['node'] = array(
      'title' => trim($small->title),
      'nid' => $small->nid,
      'type' => $small->type,
      'language' => $small->language,
    );
    
    if($small->type == 'image') {
      $data['node']['image']['path'] = ndla_emnesider_get_filepath($small->nid);
      
      //Pick up the alt-text (if any)
      if(!empty($node->field_alt_text[0]['value'])) {
        $data['node']['image']['alt'] = check_plain($node->field_alt_text[0]['value']);
      }
      //Pick up the caption (if any)
      if(!empty($node->field_image_caption[0]['value'])) {
        $data['node']['caption'] = check_plain($node->field_image_caption[0]['value']);
      }
    }
  }

  return $data;
}

/**
 * Returns the metadata for the node (ie description, image etc) which should be sent to Google etc.
 * This data is not supposed to be printed for the NDLA visitor.
 */
function ndla_emnesider_get_meta($node) {
  $data = array();
  if(!empty($node->field_meta_description[0]['value'])) {
    $data['description'] = check_plain($node->field_meta_description[0]['value']);
  }
  if(!empty($node->field_ingress_bilde[0]['nid'])) {
    $data['image'] = ndla_emnesider_get_filepath($node->field_ingress_bilde[0]['nid']);
  }

  return $data;
}

function ndla_emnesider_get_filepath($image_nid) {
  $path = '';
  $row = db_fetch_object(db_query("SELECT f.filepath FROM {image} i
  INNER JOIN {files} f ON f.fid = i.fid
  WHERE i.nid = %d AND status = 1 AND i.image_size = '_original'", $image_nid));
  if(!empty($row->filepath)) {
    $path = $row->filepath;
  }
  
  return $path;
}