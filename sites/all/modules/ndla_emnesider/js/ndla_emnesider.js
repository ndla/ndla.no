function ndla_emnesider_fetch_alt_text() {
  var value = $('#edit-field-visual-element-0-nid-nid').val();
  var nid = null;
  try {
    nid = value.match(/nid:[0-9]+/)[0].replace("nid:", "");
  }
  catch(e) {
  }
  
  if(nid) {
    $.ajax({
      url: Drupal.settings.basePath + "get_alt/" + nid,
      type: "GET",
      dataType: 'text',
      cache: 0,
      async: 0,
      success: function(data){
        $('#edit-field-alt-text-0-value').val(data);
        ndla_emnesider_remove_class();
      }
    });
  } 
}

function ndla_emnesider_add_class() {
  $('#edit-field-alt-text-0-value').addClass('form-autocomplete');
  $('#edit-field-alt-text-0-value').addClass('throbbing');  
}

function ndla_emnesider_remove_class() {
  $('#edit-field-alt-text-0-value').removeClass('form-autocomplete');
  $('#edit-field-alt-text-0-value').removeClass('throbbing');
}

Drupal.behaviors.ndla_emnesider = function() {
  $('#edit-field-visual-element-0-nid-nid').change(function() {
    ndla_emnesider_add_class();
    setTimeout(ndla_emnesider_fetch_alt_text, 500);
  });
};