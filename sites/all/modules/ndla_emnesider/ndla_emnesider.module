<?php
/**
 * @brief
 *  This module handles logic for theme switching, removal of fields etc which all are related to the new content-types Emneside/Emneartikkel/Fagressurs.
 * @author
 *  jonas.seffel@cerpus.com
 */

/**
 * Implementation of hook_init()
 */
function ndla_emnesider_init() {
  module_load_include('inc', 'ndla_emnesider', 'ndla_emnesider.forms');
  module_load_include('inc', 'ndla_emnesider', 'ndla_emnesider.api');

  return;
}

/**
 * Implementation of hook_menu
 */
function ndla_emnesider_menu() {
  $items = array();

  $items['admin/settings/ndla_emnesider'] = array(
    'title' => 'NDLA Emnesider',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_emnesider_admin_form'),
    'access arguments' => array('administer site configuration'),
  );

  $items['get_alt/%'] = array(
    'page callback' => 'ndla_emnesider_get_image_alt',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
  );

  $items['ndla_emnesider/fagstoff/autocomplete'] = array(
    'title' => 'Fagstoff autocomplete callback',
    'type' => MENU_CALLBACK,
    'page callback' => 'ndla_emnesider_fagstoff_autocomplete',
    'access arguments' => array('access content'),
  );

  return $items;
}


function ndla_emnesider_fagstoff_autocomplete($search) {
  global $language, $dbg;
  if(strlen($search) <= 2) return drupal_json(array());
  
  $dbg = TRUE;
  if(is_numeric($search)) {
    $search = (int)$search;
    $result = db_query("SELECT language, nid, title FROM {node} WHERE status = 1 AND type  = 'fagstoff' AND nid = '%s' LIMIT 30", $search);
  } else {
    $result = db_query("SELECT language, nid, title FROM {node} WHERE status = 1 AND type  = 'fagstoff' AND title LIKE '%s%%' LIMIT 30", $search);
  }
  $dbg = FALSE;
  
  $nodes = array();
  while ($node = db_fetch_object($result)) {
    $lang = $node->language;
    if(empty($lang)) {
      $lang = t('neutral');
    }
    $nodes[$node->title . ' (Fagstoff, ' . $lang . ') [nid:' . $node->nid . ']'] = $node->title . ' (Fagstoff, ' . $lang . ') [nid:' . $node->nid . ']';
  }
  drupal_json($nodes);
}

/**
 * Ajax callback. Returns the alt-text for image node with nid $nid
 */
function ndla_emnesider_get_image_alt($nid) {
  $nid = check_plain($nid);
  $out = "";
  if($node = node_load($nid)) {
    if($node->type == 'image' && !empty($node->field_alt_text[0]['value'])) {
      $out = $node->field_alt_text[0]['value'];
    }
  }
  print $out;
  exit();
}

function ndla_emnesider_admin_form() {
  $formats = filter_formats();
  $format_options = array();
  foreach($formats as $format) {
    $format_options[$format->format] = $format->name;
  }
  
  $form['ndla_emnesider_body_format'] = array(
    '#title' => t('Default format for body on Emneartikkel and Fagressurs'),
    '#type' => 'select',
    '#options' => $format_options,
    '#default_value' => variable_get('ndla_emnesider_body_format', 2),
  );
  
  return system_settings_form($form);
}

function ndla_emnesider_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if(($node->type == 'fagressurs' || $node->type == 'emneartikkel') && $op != 'view') {
    if(module_exists('ndla_subject_connection')) {
      switch($op) {
        case 'insert':
        case 'update':
          break;
        case 'delete':
          db_query("DELETE FROM {og_ancestry} WHERE nid = %d", $node->nid);
          break;
        case 'presave':
          ndla_emnesider_relation_to_og($node);
          break;
        case 'prepare translation':
          ndla_emnesider_prepare_translation($node);
          break;
      }
    }

    /* Run only when these conditions apply
      > We are working on a new unsaved node
      > We are on the node/add/emneartikkel form
      > We have a fagstoff nid in the url
    */

    $is_on_edit = FALSE;
    if($node->type == 'emneartikkel' && arg(0) == 'node' && (arg(2) == 'edit' || arg(1) == 'add')) {
      $is_on_edit = TRUE;
    }
    if($op === 'prepare' && $is_on_edit && !empty($_GET['fagstoff']) ) {
      $node = ndla_emnesider_convert_from_fagstoff($node, $_GET['fagstoff']);
    }
    if($op == 'validate'
        && $is_on_edit
        && empty($_GET['fagstoff'])
        && !empty($_GET['fag'])
        && !empty($a3['#post']['fagstoff_nid'])) {
          $nid = ndla_utils_get_autocomplete_nid($a3['#post']['fagstoff_nid']);
          $base_url = implode("/", arg());
          $query = array(
            'fag' => $_GET['fag'],
            'fagstoff' => $nid,
          );
          drupal_get_messages();
          drupal_goto($base_url, $query);
      }
  } 
}

function ndla_emnesider_convert_from_fagstoff($emneartikkel_node, $fagstoff_nid) {
  $fagstoff_node = node_load($fagstoff_nid);
  if(empty($fagstoff_node->nid)) {
    return;
  }
  $fields_for_copy = array(
      'title' => 'title',
      'teaser' => 'teaser',
      'uid' => 'uid',
      'field_visual_element' => 'field_visual_element',
      'field_alt_text' => 'field_alt_text',
      'field_image_caption' => 'field_image_caption',
      'authors' => 'authors',
      'keywords' => 'keywords',
      'original_keywords' => 'original_keywords',
      'keyword_formdata' => 'keyword_formdata',
      'taxonomy' => 'taxonomy',
      'field_ingress' => 'field_meta_description',
      'field_ingress_bilde' => 'field_ingress_bilde',
      'promote' => 'promote',
      'comment' => 'comment',
      'sticky' => 'sticky',
      'moderate' => 'moderate',
      'cc_lite_license' => 'cc_lite_license',
      'ndla_workflow_status' => 'ndla_workflow_status',
      'ndla_workflow_message' => 'ndla_workflow_message',
      'ndla_workflow_responsible' => 'ndla_workflow_responsible',
      'page_title' => 'page_title',
      'additional_license_informaiton' => 'additional_license_informaiton',
      'version_reference' => 'version_reference',
      'editors_comment',
      'ndla_solr_index_node',
  );

  //Normal fields
  foreach($fields_for_copy as $from => $to) {
    if(!empty($fagstoff_node->$from) && empty($emneartikkel_node->$to)) {
      $emneartikkel_node->$to = $fagstoff_node->$from;
    }
    else if(is_array($emneartikkel_node->{$to}) && isset($emneartikkel_node->{$to}[0])) {
      if(empty($emneartikkel_node->{$to}[0]['nid']) && empty($emneartikkel_node->{$to}[0]['value'])) {
        $emneartikkel_node->$to = $fagstoff_node->$from;
      }
    }
    else if($from == 'ndla_solr_index_node') {
      $emneartikkel_node->ndla_solr_index_node = $fagstoff_node->ndla_solr_index_node;
    }

    if($to == 'field_meta_description' && !empty($emneartikkel_node->{$to}[0]['value'])) {
      $emneartikkel_node->{$to}[0]['value'] = strip_tags($emneartikkel_node->{$to}[0]['value']);
    }
  }

  //Utdanning RDF data - Needs a tiny bit more of massage than regular fields.
  if(!empty($fagstoff_node->utdanning_rdf) && empty($emneartikkel->relations)) {
    $taxonomy_relations = array();
    if(module_exists('ndla_relations')) {
      $taxonomy_relations = _ndla_relations_get_types();
    }
    module_load_include('inc', 'utdanning_rdf', 'utdanning_rdf.pages');
    $reverse_relations = _utdanning_rdf_get_reverse_for_form($fagstoff_node);
    if(!empty($reverse_relations)) {
      $fagstoff_node->utdanning_rdf = array_merge($fagstoff_node->utdanning_rdf, $reverse_relations);
    }

    $added_rdf_nids = array();
    foreach($fagstoff_node->utdanning_rdf as $relation) {
      $NID_INDEX = 2;
      if(strpos($relation[0], 'nid:') !== FALSE) {
        $NID_INDEX = 0;
      }
      $to_nid = str_replace("nid:", "", $relation[$NID_INDEX]);
      if(!in_array($to_nid, $added_rdf_nids)) {
        $relation_name = utdanning_retrive_label($relation[1]);
        $tid = array_search($relation_name, $taxonomy_relations);      
        $emneartikkel_node->relations[] = array(
          'node_nid' => $to_nid,
          'tid' => array($tid),
        );
        
        $added_rdf_nids[] = $to_nid;
      }
    }
  }
  
  $emneartikkel_node->form_content_language = $fagstoff_node->language;
  if(empty($emneartikkel_node->paragraphs)) {
    $emneartikkel_node->paragraphs = _ndla_emnesider_convert_field_paragraphs($fagstoff_node->paragraphs);
  }

  $emneartikkel_node->field_from_node[] = array('nid' => $fagstoff_node->nid);

  unset($fagstoff_node);
  return $emneartikkel_node;
}

function _ndla_emnesider_convert_field_paragraphs($paragraphs) {
  $weight = 0;
  $converted_paragraphs = array();
  foreach($paragraphs as $paragraph) {
    $converted_paragraphs[] = array(
      'left' => array(
          'field' => $paragraph['left']['field'],
          'format' => 9,
      ),
      'right' => array(
          'field' => NULL,
          'format' => 0,
      ),
      'weight' => $weight++,
    );
    if(!empty($paragraph['right']['field'])) {
      $converted_paragraphs[] = array(
        'left' => array(
            'field' => $paragraph['right']['field'],
            'format' => 9,
        ),
        'right' => array(
            'field' => NULL,
            'format' => 0,
        ),
        'weight' => $weight++,
      );
    }
  }

  return $converted_paragraphs;
}

function ndla_emnesider_relation_to_og(&$node) {
  foreach ($node->ndla_subject_connections as $related_node) {
    $group_nid = ndla_subject_connection_get_nid_from_autocomplete($related_node['node_nid']);
    if(!empty($group_nid)) {
      db_query("DELETE FROM {og_anecstry} WHERE nid = &d", $node->nid);
      db_query("INSERT INTO {og_ancestry} (nid, group_nid, is_public) VALUES(%d, %d, %d)", $node->nid, $group_nid, 1);
      $result = db_query("SELECT tid FROM {ndla_fag_taxonomy_map} WHERE nid = %d", $group_nid);
      while($row = db_fetch_object($result)) {
        $node->taxonomy[$row->tid] = taxonomy_get_term($row->tid);
      }
    }
    break;
  }
}
function ndla_emnesider_views_query_alter(&$view, &$query) {
  if(!empty($query->where[0])) {
    //Pickup clauses and args
    $clauses = !empty($query->where[0]) ? $query->where[0]['clauses'] : array();
    $args = !empty($query->where[0]) ? $query->where[0]['args'] : array();
    
    //Try to get the node-type arguments
    $index_to_remove = FALSE;
    $multi_content = FALSE;
    foreach($args as $index => $arg) {
      $multi_content = array_map('trim', explode("|", $arg));
      if(count($multi_content) > 1) {
        $index_to_remove = $index;
        break;
      }
      else {
        $multi_content = FALSE;
      }
    }
    
    //If we had such arguments, locate the clauses.
    if($index_to_remove) {
      unset($query->where[0]['args'][$index_to_remove]);
      $find = "node.type = '%s'";
      foreach($clauses as $index => $clause) {
        if($clause == $find) {
          $query->where[0]['clauses'][$index] = "node.type IN('" . implode("','", $multi_content) . "')";
          break;
        }
      }
    }
  }
}

function ndla_emnesider_is_valid_fag($type, $gid) {
  $result = db_query("SELECT type FROM {node} WHERE nid = %d", $gid);
  while($row = db_fetch_object($result)) {
    if($row->type == 'emneside' && in_array($type, array('emneartikkel', 'fagressurs'))) {
      return TRUE;
    }
    else if($row->type == 'fag' && !in_array($type, array('emneartikkel', 'fagressurs'))) {
      return TRUE;
    }
  }
  
  return FALSE;
}

/**
 * Switch theme if needed. This function is called from ndla_handheld, because the weight of this module
 * is too heavy for theme-switching.
 */
function ndla_emnesider_switch() {
  return;
  /** @noinspection PhpUnreachableStatementInspection */
  if(arg(0) == 'admin' || (arg(0) == 'node' && (arg(1) == 'add' || arg(2) == 'edit'))) {
    return;
  }
  
  $switch = FALSE;
  $arg = arg();
  if(arg(0) == 'node' && !empty($arg[1]) && is_numeric($arg[1])) {
    $result = db_query("
      SELECT n.nid FROM {og_ancestry} oga
      INNER JOIN {node} n ON oga.group_nid = n.nid
      WHERE n.type = 'emneside' AND oga.nid = %d", check_plain($arg[1]));
    $row = db_fetch_object($result);
    //Is the node owned by a emneside-fag?
    if(!empty($row->nid)) {
      $switch = TRUE;
    }
    else {
      //Is the node an actual emneside?
      module_load_include('inc', 'ndla_utils', 'ndla_utils.helpers');
      $small = ndla_utils_load_node($nid);
      if($small->type == 'emneside') {
        $switch = TRUE;
      }
    }
  }
  
  //Switch is due...
  if($switch) {
    global $conf;
    $conf['theme_default'] = variable_get('ndla_emnesider_theme', 'ndla2010');
  }
}

function ndla_emnesider_strip_old_content(&$form) {
}

function ndla_emnesider_prepare_translation(&$node) {
  $fields = array('field_related_emneartikkel');
  foreach($fields as $f) {
    $old_data = $node->{$f};
    $node->{$f} = array();
    foreach($old_data as $index => $values) {
      $node_language = db_fetch_object(db_query("SELECT language FROM {node} WHERE nid = %d", $values['nid']))->language;
      if (!empty($node_language) && $node_language != $node->language) {
        $translation = db_fetch_object(db_query("SELECT n2.nid FROM {node} n1
                                                 INNER JOIN {node} n2 ON n1.tnid = n2.tnid
                                                 WHERE n1.nid = %d AND n2.language IN('%s') AND n1.tnid > 0 LIMIT 1", $values['nid'], $node->language));

        if ($translation->nid) {
          $node->{$f}[]['nid'] = $translation->nid;
        }
      }
      else {
        $node->{$f}[]['nid'] = $values['nid'];
      }
    }
  }
}