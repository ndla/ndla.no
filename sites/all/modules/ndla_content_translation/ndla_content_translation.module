<?php
/** \addtogroup ndla_content_translation */

function ndla_content_translation_init() {

}

/**
 * @file
 * @ingroup ndla_content_translation
 * @brief
 *  Allows translation of media and organic groups nodes.
 */
/*
 * Implementation of hook_form_alter()
 */
function ndla_content_translation_form_alter(&$form, $form_state, $form_id) {
  global $user;
  if (isset($form['#node']) && ndla_content_translation_enabled($form['#node']->type) && $form_id == $form['#node']->type . "_node_form") {
    $languages = language_list();
    $node = $form['#node'];
    $form['ndla_content_translation'] = array(
      '#type' => 'fieldset',
      '#title' => t('Translate') . (!empty($node->ndla_content_translation) ? ' *' : ''),
      '#collapsed' => FALSE,
      '#collapsible' => TRUE,
      '#tree' => TRUE,
    );

    /**
     * Add your field to $form['ndla_content_translation'][$lang] and it will be saved. You have to implement the viewing part.
     */
    foreach ($languages as $lang => $data) {
      $def = isset($node->ndla_content_translation[$lang]) ? $node->ndla_content_translation[$lang] : array();
      $form['ndla_content_translation'][$lang] = array(
        '#type' => 'fieldset',
        '#title' => t('Title and body in ' . $data->native),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#weight' => -49,
        '#attributes' => array('class' => 'click_and_close'),
      );

      $form['ndla_content_translation'][$lang]['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#size' => 60,
        '#maxlength' => 128,
        '#default_value' => !empty($def['title']) ? $def['title'] : NULL,
      );

      if(module_exists('page_title')) {
        $form['ndla_content_translation'][$lang]['pagetitle'] = array(
          '#type' => 'textfield',
          '#title' => t('Tab title'),
          '#size' => 60,
          '#maxlength' => 128,
          '#default_value' => !empty($def['pagetitle']) ? $def['pagetitle'] : NULL,
        );
      }
      
      $form['ndla_content_translation'][$lang]['text']['text_' . $lang] = array(
        '#type' => 'textarea',
        '#title' => t('Text'),
        '#rows' => 20,
        '#default_value' => !empty($def['text']['text_' . $lang]) ? $def['text']['text_' . $lang] : NULL,
      );

      if ($form_id == 'lenke_node_form') {
        //Naming convetions isnt NDLAs strongpoint
        $form['ndla_content_translation'][$lang]['ingressvispaasiden'] = array(
          '#type' => 'checkbox',
          '#title' => t('Vis på siden'),
          '#default_value' => !empty($def['ingressvispaasiden'])  ? $def['ingressvispaasiden'] : '',
        );

        $form['ndla_content_translation'][$lang]['teaser']['teaser_' . $lang] = array(
          '#type' => 'textarea',
          '#title' => t('Ingress'),
          '#rows' => 20,
          '#default_value' => !empty($def['teaser']['teaser_' . $lang]) ? $def['teaser']['teaser_' . $lang] : NULL,
        );
      }

      if (isset($form['ndla_content_translation'][$lang]['teaser'])) {
        $form['ndla_content_translation'][$lang]['teaser']['format'] = filter_form(FILTER_FORMAT_DEFAULT, null, array('teaser_' . $data->native, 'format'));
        $form['ndla_content_translation'][$lang]['teaser']['format']['#attributes']['class'] = 'hidden';
      }

      $form['ndla_content_translation'][$lang]['text']['format'] = filter_form(FILTER_FORMAT_DEFAULT, NULL, array('text_' . $lang, 'format'));
      $form['ndla_content_translation'][$lang]['text']['format']['#attributes']['class'] = 'hidden';
    }
  }
}

function ndla_content_translation_enabled($type) {
  return in_array($type, array('emneside', 'kilde', 'image', 'flashnode', 'lenke', 'audio', 'video', 'begrep', 'fag', 'fil'));
}

function ndla_content_translation_save_node($node) {
  db_query("DELETE FROM {ndla_ct_serial} WHERE nid = %d AND vid = %d", array($node->nid, $node->vid));
  if(!empty($node->ndla_content_translation)) {
    $data = serialize($node->ndla_content_translation);
    db_query("INSERT INTO {ndla_ct_serial} (nid, vid, data) VALUES(%d, %d, '%s')", array($node->nid, $node->vid, $data));
  }
}

/*
 * Implementation of hook_nodeapi()
 */

function ndla_content_translation_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case "view":
      if (ndla_content_translation_enabled($node->type)) {
        ndla_content_translation_view($node, $a3, $a4);
      }

      break;
    case 'insert':
    case 'update':
      ndla_content_translation_save_node($node);
      break;
      
    case 'delete':
      db_query('DELETE FROM {ndla_ct_serial} WHERE nid = %d', $node->nid);
      break;
      
    case 'delete revsion':
    db_query('DELETE FROM {ndla_ct_serial} WHERE nid = %d AND vid = %d', $node->nid, $node->vid);
      break;
      
    case 'load':
      // Loads the translation fields for media nodes inserted in hook_form_alter
      if (ndla_content_translation_enabled($node->type)) {
        ndla_content_translation_ct_load($node);
      }
      break;
  }
}
function ndla_content_translation_ct_load(&$node) {
  $row = db_fetch_object(
              db_query("SELECT data FROM {ndla_ct_serial} WHERE nid = %d AND vid = %d", array($node->nid, $node->vid))
            );
  if(!empty($row->data)) {
    $node->ndla_content_translation = unserialize($row->data);
  }
}

function ndla_content_translation_view(&$node, $teaser, $page) {
  global $language;
  $translation = !empty($node->ndla_content_translation[$language->language]) ? $node->ndla_content_translation[$language->language] : array();
  if (!empty($translation['title'])) {
    $t_title = check_plain($translation['title']);
  }
    
  if ($node->type == 'fag') {
    if (!empty($translation['text']['text_' . $language->language])) {
      $node->field_ingress[0]['safe'] = check_markup($translation['text']['text_' . $language->language], FILTER_FORMAT_DEFAULT, TRUE);
    }
  }
  else if ($node->type == 'lenke') {
    if (!empty($translation['text']['text_' . $language->language])) {
      $node->content['body'] = array(
        '#value' => check_markup($translation['text']['text_' . $language->language], FILTER_FORMAT_DEFAULT, TRUE),
        '#weight' => 1);
    }
    
    if ($translation['ingressvispaasiden'] == 1 && !empty($translation['teaser']['teaser_' . $language->language])) {
      if (isset($node->field_ingress_valgfri)) {
        $node->field_ingress_valgfri[0]['safe'] = $translation['teaser']['teaser_' . $language->language];
      }
      if (isset($node->field_ingress)) {
        $node->field_ingress[0]['safe'] = $translation['teaser']['teaser_' . $language->language];
      }
    }
  } // end type lenke 
  else {
    if (!empty($translation['text']['text_' . $language->language])) {
      $node->content['body'] = array(
        '#value' => check_markup($translation['text']['text_' . $language->language], FILTER_FORMAT_DEFAULT, TRUE),
        '#weight' => 1);
    }
  }//end every other content type 

  if ($page) {
    drupal_set_title($t_title);
  }
}

/**
 * Fetches the translated title
 *
 * @param $nid
 *  The node id
 * @param $vid
 *  The node vid. If omitted it will be fetched from the node table.
 * @param $lang
 *  The language prefix. If omitted the current language will be used.
 * @return
 *  The translated title. May be blank.
 */
function ndla_content_translation_get_translated_title($nid, $vid = NULL, $lang = NULL) {
  if (!$lang) {
    global $language;
    $lang = $language->language;
  }

  if (!$vid) {
    $vid = db_fetch_object(db_query("SELECT vid FROM {node} WHERE nid = %d", $nid))->vid;
  }
  
  $data = db_fetch_object(db_query("SELECT data FROM {ndla_ct_serial} WHERE nid = %d AND vid = %d", $nid, $vid));
  if(!empty($data)) {
    $data = unserialize($data->data);
    if(!empty($data[$lang]['title'])) {
      return $data[$lang]['title'];
    }
  }
}

function ndla_content_translation_page_title_pattern_alter(&$pattern, &$data) {
  global $language;
  if(!empty($data['node']->ndla_content_translation[$language->language]['pagetitle'])) {
    $data['node']->page_title = $data['node']->ndla_content_translation[$language->language]['pagetitle'];
  }
  else if(!empty($data['node']->ndla_content_translation[$language->language]['title'])) {
    $data['node']->page_title = $data['node']->ndla_content_translation[$language->language]['title'];
  }
}

/**
 * Implementation of hook_translation_link_alter()
 */
function ndla_content_translation_translation_link_alter(&$links, $path) {
  if(empty($links)) {
    $parts = explode("/", $path);
    if(!empty($parts[0]) && $parts[0] == 'node' && !empty($parts[1]) && is_numeric($parts[1])) {
      foreach(language_list() as $prefix => $data) {
        $trans_title = ndla_content_translation_get_translated_title($parts[1], NULL, $prefix);
        if(!empty($trans_title)) {
          $links[$prefix] = array(
            'title' => $data->native,
            'href' => $path,
            'options' => array(
              'language' => $data,
              'attributes' => 'language-link',
            )
          );
        }
      }
    }
  }
}