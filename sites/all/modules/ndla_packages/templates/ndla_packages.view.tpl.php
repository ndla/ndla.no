<div class='packages-container<?php if($full) print ' full'; ?>'>
  <div class='duration'>
    <i class="fa fa-clock-o"></i> <?php print t('Duration'); ?>: <?php print $duration; ?>
  </div>
  <div class='content'>
    <?php if(!empty($image)) print $image; ?>
    <h1><?php print $title; ?></h1>
    <div class='owner'><?php print $authors; ?></div>
    <?php print html_entity_decode($text); ?>
  </div>
  <div id='packages-canvas'></div>
  <div id='packages-text'>(<?php print t('Choose page to see title'); ?>)</div>
  <!-- Open in new tab/window because that is the only way the package can
       "return" to NDLA correctly.
       TODO: Fix this on the package server then remove target from button -->
  <a target = "_blank" href='<?php print $url; ?>' class='button'>
    <i class="fa fa-play-circle"></i> <?php print t('Start course'); ?>
  </a>
</div>
