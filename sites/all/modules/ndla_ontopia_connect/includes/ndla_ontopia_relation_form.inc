<?php
// $Id$  ndla_ontopia_relation_form.inc, v 1.0 2010/04/14 13:00 minimalismore Exp $
/**
 * @brief
 *  Provides form functions for ndla_ontopia_connect
 * @ingroup ndla_ontopia_connect
 * @file
 */



function ndla_ontopia_relation_form(&$form_state) {
  drupal_add_js(drupal_get_path('module', 'ndla_ontopia_connect') . '/js/treeview.js');
  drupal_add_css(drupal_get_path('module', 'ndla_ontopia_connect') . '/css/treeview.css');
  $is_deling = variable_get('ndla_ontopia_connect_activate_deling', '');
  if($is_deling == 'yes') {
    drupal_add_js(drupal_get_path('module', 'ndla_ontopia_connect') . '/js/curriculaDelingTree.js');
    drupal_add_js(drupal_get_path('module', 'ndla_ontopia_connect') . '/js/filterTree.js');
    drupal_add_js(drupal_get_path('module', 'ndla_ontopia_connect') . '/js/aim_search_treeview.js');
  }
  else {
    drupal_add_js(drupal_get_path('module', 'ndla_ontopia_connect') . '/js/curriculatree.js');
    $fagmap = ndla_ontopia_connect_getFagMap('json');
    drupal_add_js(array(
    'ndla_ontopia_connect' => array(
      'fagmap' => $fagmap,
      )),
      'setting'
    );
  }
  
  $form['ndla_ontopia_connect_grep']  = array(
    '#type' => 'fieldset',
    '#title' => t('GREP'),
    '#description' => t('Adds GREP to content. Click the link below to select GREP.')."$req",
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
    
  $nodeurl = $_GET['q'];
  $grepdata = ndla_ontopia_connect_getGrepData(arg(1),$nodeurl,'red');
  $list = ndla_ontopia_connect_showRedResources($grepdata,$nodeurl,$type);
  $prefix = '';
  
  if ($is_deling == 'yes') {
    $prefix = "<div id='my-subjects-block' class='my-subjects'><br /><strong>&gt;&gt;</strong> ".l(t("Browse competence aims"), 'ndla_ontopia_connect_grep_GUI', array('attributes' => array('rel' => 'lightmodal[|width:1024px; height:600px; scrolling: auto;]')));
    $prefix .= "<br / ><strong>&gt;&gt;</strong> ".l(t("Search for competence aims"), 'ndla_ontopia_connect_search_GREP_GUI', array('attributes' => array('rel' => 'lightmodal[|width:1024px; height:600px; scrolling: auto;]')));
    $prefix .= "<div id='grep_content' class='my-shares' style='clear:both'>";
  }
  else {
    $prefix = l(t("Browse competence aims"), 'ndla_ontopia_connect_grep_GUI', array('attributes' => array('rel' => 'lightmodal[|width:1024px; height:600px; scrolling: auto;]')))."<div id='grep_content'>";
  }
  
  $form['ndla_ontopia_connect_grep']['grep_area_udir_codes'] = array(
      '#type' => 'hidden',
      '#value=' => ''
  );
  
  $form['ndla_ontopia_connect_grep']['grep_area'] = array(
    '#type' => 'hidden',
    '#prefix' => $prefix,
    '#suffix' => $list."</div></div>",
  );
  
   $form['ndla_ontopia_connect_grep']['grep_area_delete'] = array(
    '#type' => 'hidden',
    '#value' => '',
   );
  
   $q = "SELECT title from {node} where nid = %d";
   $res = db_query($q,arg(1));
   $tarr = db_fetch_array($res);
   
   $form['ndla_ontopia_connect_grep']['title'] = array(
    '#type' => 'hidden',
    '#value' => $tarr['title'],
   );
   
  $nodeobj = new StdClass();
  $nodeobj->nid = arg(1);
  $published = 'false';
  
  if(module_exists('ndla_publish_workflow') && ndla_publish_workflow_is_published($nodeobj)){
    $published = 'true';
  }
  
  $form['ndla_ontopia_connect_grep']['grep-published'] = array(
    '#type' => 'hidden',
    '#value' => $published,
   );
   
   $form['ndla_ontopia_connect_grep']['grep_area_update'] = array(
    '#type' => 'hidden',
    '#value' => '',
   );
   
   $form['ndla_ontopia_connect_grep']['message'] = array(
    '#type' => 'markup',
    '#value' => '<div id="grep_save_message"></div>',
   );
   
   $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => array('ndla_ontopia_connect_save_ltm'),
   );
   
   return $form; 
}

function ndla_ontopia_connect_save_ltm($form, &$form_state) {
  $q = "select type from {node} where nid = %d";
  $res = db_query($q,arg(1));
  $atmp = db_fetch_array($res);
  $fragment = $form['#post']['grep_area'];
  $ucodes = $form['#post']['grep_area_udir_codes'];
  
  $ucodes = array_filter(explode("|",$ucodes));
  $ucodesjson = json_encode($ucodes);
  
  $ndla_ontopia_connect_base_url = variable_get('ndla_ontopia_connect_base_url',  '');
  if (strpos($fragment,'ndla-node') !== false) {
    $psiurl .= ' @"'.$ndla_ontopia_connect_base_url.'/node/'.arg(1).'"';
    $occurl = '"'.$ndla_ontopia_connect_base_url.'/node/'.arg(1).'"';
    $fragment = str_replace('@"psi_ndla-node"',$psiurl,$fragment);
    $fragment = str_replace('"occ_ndla-node"',$occurl,$fragment);
  }
  else if (strpos($fragment,'deling-node') !== false){
    $psiurl .= ' @"http://deling.ndla.no/node/'.arg(1).'"';
    $occurl = '"http://deling.ndla.no/node/'.arg(1).'"';
    $fragment = str_replace('@"psi_deling-node"',$psiurl,$fragment);
    $fragment = str_replace('"occ_deling-node"',$occurl,$fragment);
  }
  
  //print $fragment;
  if(strpos($fragment,"||") != false) {
    $ltmarr = explode('||',$fragment);
    $title = str_replace('"', '\u0022',$ltmarr[1]);
    $title = str_replace('\'', '\u2019',$title);
    $title = str_replace('(', '\u0029',$title);
    $title = str_replace(')', '\u0028',$title);
    $title = str_replace('[', '\u005D',$title);
    $title = str_replace(']', '\u005B',$title);
    $title = str_replace(':', '\u003A',$title);
    $title = str_replace('/', '\u2215',$title);
    $title = str_replace('@', '\u0040',$title);
    $title = str_replace('~', '\u223C',$title);
    $title = str_replace('ü', '\u00FC',$title);
    $title = str_replace('Ü', '\u00DC',$title);
    $title = str_replace('ö', '\u00F6',$title);
    $title = str_replace('Ö', '\u00D6',$title);
    $fragment = $ltmarr[0].$title.$ltmarr[2];
    $fragment = preg_replace("/\s+/", " ", $fragment);
  }
  
  _ndla_ontopia_connect_add_red_ressurs($fragment,$ucodesjson);
  drupal_set_message(t('The relations were saved'));
}


function ndla_ontopia_copy_grep_to_translation($grepinfo,$nid,$title,$lisens,$ingress,$language) {
  global $user;

  $is_deling = variable_get('ndla_ontopia_connect_activate_deling', '');
  $dato = time();

  $fagname = '';
  $fagvar = variable_get('ndla_ontopia_connect_fagmap','');
  if ($fagvar != '') {
    $fag_arr = unserialize($fagvar);
    foreach($fag_arr as $uuid => $mapdata) {
      if ($mapdata['nodeid'] == $grepinfo['meta']['group_nid']) {
        $fagname = $mapdata['topicid'];
      }
    }
  }//end if
  
   
  $topicid = '';
  $site_url = '';
  
  
  if($is_deling == 'yes') {
    $topicid = 'delingnode_'.$nid;
    $site_url = 'http://deling.ndla.no/node/';
    $user_url = 'http://deling.ndla.no/';
    $nodetype = 'deling-node';
  }
  else{
    $topicid = 'ndlanode_'.$nid;
    $site_url = 'http://ndla.no/node/';
    $user_url = 'http://ndla.no/';
    $nodetype = 'ndla-node';
  }
  
  $nodeurl = $site_url.$nid;
  
  $resource = '@"utf-8" ['.$topicid.' : '.$nodetype.' = "'.$title.'" /'.$language.'  @"'.$nodeurl.'"]';
  if($user_url != "" && $user->uid != ""){
    $resource .= ' {'.$topicid.', node-author-url, "'.$user_url.'user/'.$user->uid.'"}';
  }
  if($user->name != ""){
    $resource .= ' {'.$topicid.', node-author, [['.$user->name.']]}';
  }
  $resource .= ' {'.$topicid.', published, [[false]]}';
  if($ingress != ""){
    $resource .= ' {'.$topicid.', node-ingress, [['.$ingress.']]}';
  }
  
  if($lisens != ""){
    $resource .= ' {'.$topicid.', node-usufruct, "http://creativecommons.org/licenses/'.$lisens.'"} /en';
    $resource .= ' {'.$topicid.', node-usufruct, "http://creativecommons.org/licenses/'.$lisens.'/no"} /nb';
  }

  $addltm = $resource;
  
  $teller = 0;
  foreach ($grepinfo['instanser'] as $assoctype => $data) {
    foreach ($data['psis'] as $elm) {
      $maalid = $elm['topicref'];
      //$assoctype
      $assoc = $assoctype.'('.$topicid.' : laeringsressurs, '.$maalid.' : kompetansemaal) ~ a_'.$dato.'_'.$teller;
      
      $reifiertopic = '[a_'.$dato.'_'.$teller.' = "reifier for a_'.$dato.'_'.$teller.'" /language-neutral] ';
      if($elm['apprentice'] != ""){
        $reifiertopic .= '{a_'.$dato.'_'.$teller.', apprentice, [['.$elm['apprentice'].']] } ';
      }
      if($grepinfo['meta']['uuid'] != ""){
        $reifiertopic .= '{a_'.$dato.'_'.$teller.', fag, [['.$grepinfo['meta']['uuid'].']] } ';
      }
      $reifiertopic .= '{a_'.$dato.'_'.$teller.', timestamp, [['.$dato.']] } ';
      if($user->name != ""){
        $reifiertopic .= '{a_'.$dato.'_'.$teller.', bruker-navn, [['.$user->name.']] } ';
      }
      if($user->uid != ""){
        $reifiertopic .= '{a_'.$dato.'_'.($teller++).', bruker-url, "http://ndla.no/user/'.$user->uid.'" } ';
      }
      
      $addltm .= ' '.$assoc. ' '.$reifiertopic;
    }//end foreach
  }//end foreach
  _ndla_ontopia_connect_add_ressurs($addltm);
}

function ndla_ontopia_delete_grep_for_node($grepinfo,$nid) {
  $node_topicid = '';
  foreach ($grepinfo['instanser'] as $assoctype => $data) {
    foreach ($data['psis'] as $elm) {
      $node_topicid = $elm['ressursid'];
      $delete_tolog = 'delete $assoc from reifies('.$elm['reifier'].',$assoc)';
      $res = _update_topic($delete_tolog);
      $reifier_tolog = 'delete '.$elm['reifier'];
      $reifres =  _update_topic($reifier_tolog);
    }//end foreach
  }//end foreach
  
  $node_tolog = 'delete '.$node_topicid;
  $rres =  _update_topic($node_tolog);
  
  if ($rres != false) {
    drupal_set_message(t("The node was deleted on the relate server"));
    drupal_set_message(t("Competence aims connected to the node were deleted"));
  }
}