<?php
/**
 * @file
 * @ingroup ndla_ontopia_connect
 * @brief
 *  Template file for search / render topics to be deleted.
 */
?>
<fieldset class="collapsible">
  <legend><?php echo t('Search')?></legend>
  <label><?php echo t('Title')?>: </label>
  <input type="text" name="topic_search_title" id ="topic_search_title" value="" />&nbsp;
  <input type="button" name="topic_search_submit" id="topic_search_submit" onclick="searchTopics()" value='<?php echo t('Search')?>' />
</fieldset>
<div id="nav_buttons"></div>
<table width="635" id="topic_table">
  <thead>
    <tr>
    <th class="th_sort_header" id="th_sort_header_topictitle"><?php print t('Title'); ?></th>
    <th class="th_sort_header" id="th_sort_header_topicnode" colspan="2"><?php print t('Node'); ?></th>
    </tr>
  </thead>
  <tbody id="janitor_table_body">
  </tbody>
</table>