aim_search_treeview = function(tData,container)
{
  this.build = function(nodeInfo)
  {
    var stem = $('<div id="laereplan" class="GREPTreeView"></div>');
    
   
    
    
      for (elm in nodeInfo){
       // alert("ELM: "+elm+" => "+nodeInfo[elm]);
        node = $('<div id="'+elm+'-LaereplanNode">'+nodeInfo[elm].curricula_title+'</div>')
        .css({'margin-top': 5});
        $('<div class="expandNode expand"></div>')
        .prependTo(node);
        
        
        node.children('.expandNode').click(function() {
          var contents = $(this).parent().children(".NodeContents");
          if (contents.length > 0) {
            contents.toggle();
            if(contents.css('display') != "none")
            {
              if (typeof($(this)) !== 'undefined') {
                $(this).attr("class", "expandNode collapse");
              }
              
            }
            else
            {
              if (typeof($(this)) !== 'undefined') {
                $(this).attr("class", "expandNode expand");
              }
            }
          }
        });
         
        var elmdata = nodeInfo[elm].curricula_data;
        for (main_area in elmdata){
          hovedomraade_contents = $('<div class="NodeContents"></div>');
          hovedomraade_node = $('<div id="'+elm+'-HovedomraadeNode">'+main_area+'</div>')
          .css({'margin-top': 5});
          $('<div class="expandNode expand"></div>')
          .prependTo(hovedomraade_node);
          
          
          kompetansemaal_contents = $('<div class="NodeContents"></div>');
          //alert(JSON.stringify(main_area));
          aim_data = elmdata[main_area];
          for(tmp in aim_data) {
            tmpdata = aim_data[tmp];
            //alert(JSON.stringify(tmpdata));
            for (aim in tmpdata) {
              /*
              kompetansemaal_node = $('<div id="'+elm+'-KompetansemaalNode"><span class="aim_title">'+tmpdata.aim_title+'</span></div>')
              .css({'margin-top': 5});
              
              $('<div class="expandNode expand"><span id="'+tmpdata.aim_uuid.replace(':','_')+'" class="uuid" /></div>')
              .prependTo(kompetansemaal_node);
              */
              var kompetansemaal_node = $('<div id="'+tmpdata.aim_uuid.replace(':','_')+'-KompetansemaalNode"><input type="checkbox" name="maal_'+tmpdata.aim_uuid.replace(':','_')+'" id="maal_'+tmpdata.aim_uuid.replace(':','_')+'"  class="newport-checkbox ochidden" />&nbsp;<span class="curriculatree-aimtitle">'+tmpdata.aim_title+' </span></div>')
              .css({'margin-top': 5});
              $('<div class="expandNode expand"><span id="'+tmpdata.aim_uuid.replace(':','_')+'" class="uuid" /></div>')
              .prependTo(kompetansemaal_node);
              
              
              
              
              var relasjonsnode_contents = $('<div class="NodeContents"></div>');
              var relasjon_node = $('&nbsp; <div id="assoctypes_'+tmpdata.aim_uuid.replace(':','_')+'" class="port_assoctypes">'+getAssoctypes(tmpdata.aim_uuid.replace(':','_'))+'</div>').appendTo(relasjonsnode_contents);

              kompetansemaal_node.append(relasjonsnode_contents);
              
            }
            kompetansemaal_contents.append(kompetansemaal_node);
            

            kompetansemaal_node.children('.expandNode').click(function() {
              var contents = $(this).parent().children(".NodeContents");
              var uuid = $(this).parent().attr('id').substr(0,$(this).parent().attr('id').lastIndexOf('-KompetansemaalNode'));
              
              contents.toggle();
              if(contents.css('display') != "none")
              {
                $(this).attr("class", "expandNode collapse");
                $('#maal_'+uuid).attr('checked','checked');
                $('#maal_'+uuid).attr('class','newport-checkbox');
              }
              else
              {
                $('#maal_'+uuid).removeAttr('checked');
                $('#maal_'+uuid).attr('class','newport-checkbox ochidden');
                $(this).attr("class", "expandNode expand");
              }
             
            });
            
          }//end for
          hovedomraade_node.append(kompetansemaal_contents);
          /*
         
          for(aim in aim_data) {
            kompetansemaal_node = $('<div id="'+elm+'-KompetansemaalNode"><span class="aim_title">'+aim.aim_title+'</span></div>')
            .css({'margin-top': 5});
            $('<span id="'+aim.aim_uuid.replace(':','_')+'" class="uuid" /></div>')
            .prependTo(kompetansemaal_node);
          }//end for
          
          */
          hovedomraade_node.children('.expandNode').click(function() {
            var contents = $(this).parent().children(".NodeContents");
            contents.toggle();
            if(contents.css('display') != "none")
            {
              $(this).attr("class", "expandNode collapse");
            }
            else
            {
              $(this).attr("class", "expandNode expand");
            }
           
          });
          
          
          
          hovedomraade_contents.append(hovedomraade_node);
          node.append(hovedomraade_contents);
              
        }//end for

        stem.append(node);
      }//end for
    
    return stem;
  }
  this.tree = this.build(tData);
  var treeCon = container;
  $('.search_aim_window_loader_nn').remove();
  treeCon.append(this.tree);
}