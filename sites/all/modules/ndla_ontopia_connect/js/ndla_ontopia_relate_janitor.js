/**
 * @file
 * @ingroup ndla_ontopia_connect
 * @brief
 *  Handles the Janitor navigation.
 */

Drupal.behaviors.ontopia_relate_janitor = function (context) {
  $("#th_sort_header_node").click(function(){
    var offset = $(':input[name=offset]')[0].value;
    var search_offset = $(':input[name=search_offset]')[0].value;
    var html = '';

    var jdata = $.evalJSON(unescape($(':input[name=jdata]')[0].value));
    jdata.sort(compareTitle);
    var jlen = jdata.length;
    for (var i = 0; i < jlen; i++) {
      if(i%2 == 1) {
        var stripes = 'odd';
      }
      else {
        var stripes = 'even';
      }
      html += '<tr class="'+stripes+'">';
      html += '<td><a href="/node/'+jdata[i].nid+'">'+jdata[i].title+'</a></td>';
      html += '<td>'+jdata[i].language+'</td>';
      html += '<td>'+jdata[i].type+'</td>';
      html += '<td>'+jdata[i].published+'</td>';
      html += '<td id="topic-name_'+jdata[i].nid+'">'+jdata[i].topicname+'</td>';
      html += '<td><span id="'+jdata[i].topicid+'" class="edit-link">'+Drupal.t('Edit')+'</span></td>';
      html += '<td><span id="'+jdata[i].nid+'" class="delete-link">'+Drupal.t("Delete")+'</span></td>';
      html += '</tr>';
    }
    html += '<tr><td><div id="edit-window"><div id="edit-body"></div></div></td></tr>';
    html += '<tr><td><input type="hidden" name="offset" id="offset" value="'+offset+'" />';
    html += '<input type="hidden" name="search_offset" id="search_offset" value="'+search_offset+'" />'
    html += '<input type="hidden" name="jdata" id="jdata" value="'+escape(JSON.stringify(jdata))+'" /></td></tr>';


    //$("#janitor_table_body").html('');
    $("#janitor_table_body").html(html);
    Drupal.attachBehaviors($('#janitor_table_body'));
    
  });
  
  $("#th_sort_header_type").click(function(){
    var offset = $(':input[name=offset]')[0].value;
    var search_offset = $(':input[name=search_offset]')[0].value;
    var html = '';
    var jdata = $.evalJSON(unescape($(':input[name=jdata]')[0].value));
    jdata.sort(compareType);
    var jlen = jdata.length;
    for (var i = 0; i < jlen; i++) {
      if(i%2 == 1) {
        var stripes = 'odd';
      }
      else {
        var stripes = 'even';
      }
      html += '<tr class="'+stripes+'">';
      html += '<td><a href="/node/'+jdata[i].nid+'">'+jdata[i].title+'</a></td>';
      html += '<td>'+jdata[i].language+'</td>';
      html += '<td>'+jdata[i].type+'</td>';
      html += '<td>'+jdata[i].published+'</td>';
      html += '<td id="topic-name_'+jdata[i].nid+'">'+jdata[i].topicname+'</td>';
      html += '<td><span id="'+jdata[i].topicid+'" class="edit-link">'+Drupal.t('Edit')+'</span></td>';
      html += '<td><span id="'+jdata[i].nid+'" class="delete-link">'+Drupal.t("Delete")+'</span></td>';
      html += '</tr>';
    }
    html += '<tr><td><div id="edit-window"><div id="edit-body"></div></div></td></tr>';
    html += '<tr><td><input type="hidden" name="offset" id="offset" value="'+offset+'" />';
    html += '<input type="hidden" name="search_offset" id="search_offset" value="'+search_offset+'" />'
    html += '<input type="hidden" name="jdata" id="jdata" value="'+escape(JSON.stringify(jdata))+'" /></td></tr>';
    $("#janitor_table_body").html('');
    $("#janitor_table_body").html(html);
    Drupal.attachBehaviors($('#janitor_table_body'));
    
  });
  
  $("#th_sort_header_published").click(function(){
    var offset = $(':input[name=offset]')[0].value;
    var search_offset = $(':input[name=search_offset]')[0].value;
    var html = '';
    var jdata = $.evalJSON(unescape($(':input[name=jdata]')[0].value));
    jdata.sort(comparePublished);
    var jlen = jdata.length;
    for (var i = 0; i < jlen; i++) {
      if(i%2 == 1) {
        var stripes = 'odd';
      }
      else {
        var stripes = 'even';
      }
      html += '<tr class="'+stripes+'">';
      html += '<td><a href="/node/'+jdata[i].nid+'">'+jdata[i].title+'</a></td>';
      html += '<td>'+jdata[i].language+'</td>';
      html += '<td>'+jdata[i].type+'</td>';
      html += '<td>'+jdata[i].published+'</td>';
      html += '<td id="topic-name_'+jdata[i].nid+'">'+jdata[i].topicname+'</td>';
      html += '<td><span id="'+jdata[i].topicid+'" class="edit-link">'+Drupal.t('Edit')+'</span></td>';
      html += '<td><span id="'+jdata[i].nid+'" class="delete-link">'+Drupal.t("Delete")+'</span></td>';
      html += '</tr>';
    }
    html += '<tr><td><div id="edit-window"><div id="edit-body"></div></div></td></tr>';
    html += '<tr><td><input type="hidden" name="offset" id="offset" value="'+offset+'" />';
    html += '<input type="hidden" name="search_offset" id="search_offset" value="'+search_offset+'" />'
    html += '<input type="hidden" name="jdata" id="jdata" value="'+escape(JSON.stringify(jdata))+'" /></td></tr>';
    $("#janitor_table_body").html('');
    $("#janitor_table_body").html(html);
    Drupal.attachBehaviors($('#janitor_table_body'));
    
  });
  
  $("#th_sort_header_resource").click(function(){
    var jdata = $(':input[name=jdata]')[0].value;
  });

  
  $('.edit-link').click(function(){
    topicarr = $(this).attr('id').split('§');
    topicname = $("#topic-name_"+topicarr[0]).html();
    server = topicarr[2];
    openEditWindow(topicarr,topicname,server);
  });
  
  /*
  $('.delete-link').click(function(){
    topicid = $(this).attr('id');
    updateTopic(topicid,'delete');
  });
  
  */
  $('#edit-window-close').click(function(){
    $("#edit-window").fadeOut(750);
  });
  
  $('#flush_search').click(function () {
     location.href = location.href;
  });
}


function openEditWindow(topicarr,topicname,server) {
  html = '<div id="topic_form">';
  html += '<fieldset>';
  html += '<legend>'+Drupal.t('Edit topic')+'</legend>';
  html += '<div id="topic_form_messages"></div>';
  html += '<label id="label_title">'+Drupal.t('Title')+': </label><input type="text" name="topic_name" id="topic_name" value="'+topicname+'" size="60" />';
  html += '<input type="hidden" name="topic_id" id="topic_id" value="'+topicarr[1]+'" />';
  html += '<br /><br /><input type="button" name="topic_form_save_button" id="topic_form_save_button" onclick="updateTopic(\''+topicarr[0]+'\',\'update\',\''+server+'\')" value="'+Drupal.t('Save')+'" />';
  html += '</fieldset>';
  html += '<div id="edit-window-close"></div>';
  html += '</div>';
  
  $("#edit-body").html(html);
  Drupal.attachBehaviors($('#edit-body'));
  $("#edit-window").fadeIn(750);
  
}


function getPage(direction) {
  
  var offset = $(':input[name=offset]')[0].value;

  
  if (direction == 'next') {
    var jasonUrl = Drupal.settings.ndla_ontopia_relate_janitor.browse_json_url+"/"+offset+"/no/";
  }
  else if (direction == 'previous') {
    var jasonUrl = Drupal.settings.ndla_ontopia_relate_janitor.browse_json_url+"/"+(offset-200)+"/no/";
  }
  
  //alert (jasonUrl);
  $("#janitor_table_body").html('');
  $("#janitor_table_body").html('<tr><td colspan="7" class="aim_window_loader_nb"></td></tr>');
  $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'json',
    success: function(data) {
    showPage(data);
  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
    $("#ontopia_connect_errors").html(Drupal.t('<p>Unable to fetch data.</p>'));
  }
  
  });
  
} 

function searchTopics() {
  var search = $(':input[name=topic_search_title]')[0].value;
  
  $("#janitor_table_body").html('');
  $("#topic_table").css('display','none');
  
  var jasonUrl = Drupal.settings.ndla_ontopia_relate_janitor.search_topic_json_url+"/"+search;
  $.ajaxSetup({ 
    scriptCharset: "utf-8" , 
    contentType: "application/x-www-form-urlencoded; charset=utf-8"
  });
  
  $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'json',
    success: function(data) {
    //console.log(JSON.stringify(data));
    
    if(typeof($(':input[name=jdata]')[0]) != 'undefined') {
      $(':input[name=jdata]').remove();
    }
    
    if(typeof($(':input[name=count]')[0]) != 'undefined') {
      $(':input[name=count]').remove();
    }
   
    
    
    if(typeof($(':input[name=search]')[0]) != 'undefined') {
      $(':input[name=search]').remove();
    }
    
    
    if(typeof($(':input[name=offset]')[0]) != 'undefined') {
      $(':input[name=offset]').remove();
    }
    
      showTopics(data,'next');
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $("#ontopia_connect_errors").html(Drupal.t('<p>Unable to fetch data.</p>'));
    }
    
    });
  
}


function showPage(data) {
  if(data.count == 0) {
    $(".messages").remove();
    $("#headerSeparator").prepend('<div class="messages error">' + Drupal.t('Something went wrong, and the nodes could not be fetched. Contact administrator') + '</div>');
  }
  else {
    $("#janitor_table_body").html(data.html);
    Drupal.attachBehaviors($('#janitor_table_body'));
    
    
    if($(':input[name=offset]')[0].value > 100) {
      $("#janitor_get_next").before('<div id="janitor_get_previous" onclick="getPage(\'previous\')">&lt;&lt; '+Drupal.t('Previous')+'</div>');
      $("#janitor_get_previous").css('display','inline');
    } else {
      $("#janitor_get_previous").remove();
    } 
  }
}

function showTopics(data,direction) {
  
  var json = '';
  var navig = false;
  if (data == '') {
    data = $(':input[name=jdata]')[0].value;
    json = $.evalJSON(data);
    navig = true;
  }
  else {
    json = $.evalJSON(data.jdata);  
  }
  
  //$(':input[name=jdata]').remove();
  
  
  
  if(data.count == 0) {
    
    if (navig){
      var search = $(':input[name=search]')[0].value;
      var params = {"@search" : search};
    }
    else {
      var params = {"@search" : data.search};  
    }
    
    $(".messages").remove();
    $("#headerSeparator").prepend('<div class="messages error">' + Drupal.t('The search "@search" did not return any results',params) + '</div>');
  }
  else {
    
    var current = 0;
    var show_length = 0;
    
    if (json.length < 20) {
      show_length = json.length;
    }
    else {
      
      if (typeof($(':input[name=offset]')[0]) != 'undefined') {
        var offset = $(':input[name=offset]')[0].value;
        current = (offset*1);
        
        if (direction == 'next'){
          show_length = ((offset*1) + 20);
        }
        else {
          show_length = ((offset*1) - 20);
          current = (show_length -20);
        }
        
      }
      else {
        show_length = 20;
      }
      
    }
    
    if (navig){
      var search = $(':input[name=search]')[0].value;
      var count = $(':input[name=count]')[0].value;
      if (show_length > count) {
        var params = {"@search" : search, "@data_count" : count, "@current" : current, "@offset" : count};
      }
      else {
        var params = {"@search" : search, "@data_count" : count, "@current" : current, "@offset" : show_length};
      }
      
    }
    else {
      if (show_length > data.count) {
        var params = {"@search" : data.search, "@data_count" : data.count, "@current" : current, "@offset" : data.count}; 
      }
      else {
        var params = {"@search" : data.search, "@data_count" : data.count, "@current" : current, "@offset" : show_length};
      }
      
    }
    
    $(".messages").remove();
    $("#headerSeparator").prepend('<div class="messages status">' + Drupal.t('The search "@search" returned @data_count results',params) + '<br /> '+Drupal.t('Showing from @current to @offset out of @data_count',params)+'</div>');
    
    var html = '';
    var nav = '';
    
    
    if (current > 0) {
      nav += '<div id="delete_get_previous" onclick="showTopics(\'\',\'previous\')">&lt;&lt; '+Drupal.t('Previous')+'</div>';
    }
    
    if (navig) {
      if (show_length >= 20 && show_length < count) {      
        nav += '<div id="delete_get_next" onclick="showTopics(\'\',\'next\')">'+Drupal.t('Next')+' &gt;&gt;</div>';
      }
    }
    else {
      if (show_length >= 20 && show_length < data.count) {      
        nav += '<div id="delete_get_next" onclick="showTopics(\'\',\'next\')">'+Drupal.t('Next')+' &gt;&gt;</div>';
      }
    }
    
    $('#nav_buttons').html(nav);
    
    
    if (current > 0) {
      $('#delete_get_previous').css('display','block');
    }
    
    for(var i = current; i < show_length; i++ ) {
      var topic = json[i];
      if(i%2 == 1) {
        var stripes = 'odd';
      }
      else {
        var stripes = 'even';
      }
      var nodeid = topic.psi.substring(topic.psi.lastIndexOf('/')+1);
      html += '<tr class="'+stripes+'">';
      html += '<td id="topic-name_'+nodeid+'">'+topic.title+'</td>';
      html += '<td><a href="'+topic.psi+'" rel="lightframe[|width:900px;height:675px;]">'+topic.psi+'</a></td>';
      server = topic.psi.substring(7,topic.psi.indexOf('ndla')-1);
      html += '<td><span id="'+nodeid+'" class="delete-link" onclick=" updateTopic('+nodeid+',\'delete\',\''+server+'\');">'+Drupal.t("Delete")+'</span></td>';
      html += '</tr>';
    }//end for
    
    $("#janitor_table_body").html('');
    $("#janitor_table_body").html(html);
    $("#topic_table").css('display','block');
    Drupal.attachBehaviors($('#janitor_table_body'));
    
    if(typeof($(':input[name=jdata]')[0]) == 'undefined') {
      $("#main_1").append('<input type="hidden" name="jdata" value=\''+data.jdata+'\' />');
    }
    else {
      $(':input[name=jdata]')[0].value = data;
    }
    
    
    if(typeof($(':input[name=count]')[0]) == 'undefined') {
      $("#main_1").append('<input type="hidden" name="count" value=\''+data.count+'\' />');
    }
    else {
      $(':input[name=count]')[0].value = count;
    }
    
    
    if(typeof($(':input[name=search]')[0]) == 'undefined') {
      $("#main_1").append('<input type="hidden" name="search" value=\''+data.search+'\' />');
    }
    
    
    if(typeof($(':input[name=offset]')[0]) == 'undefined') {
      $("#main_1").append('<input type="hidden" name="offset" value="'+show_length.toString()+'" />');
    }
    else {
      $(':input[name=offset]')[0].value = show_length.toString();
    }
      
    
    
  }
}

function searchNodes(search,direction) {
  
  if(search == '') {
    search = $(':input[name=search_title]')[0].value;
  }
  
  
  if(typeof $(':input[name=search_offset]')[0] != 'undefined') {
    var offset = $(':input[name=search_offset]')[0].value;
  }
  else {
    var offset = 0;
  }
  

  
  if (direction == 'next') {
    var jasonUrl = Drupal.settings.ndla_ontopia_relate_janitor.search_json_url+"/"+search+"/"+offset;
  }
  else if (direction == 'previous') {
    var jasonUrl = Drupal.settings.ndla_ontopia_relate_janitor.search_json_url+"/"+search+"/"+(offset-200);
  }
  
  $("#janitor_get_next").css('display','none');
  
  //alert("JURL: "+jasonUrl);
  $("#janitor_table_body").html('');
  $("#janitor_table_body").html('<tr><td colspan="7" class="aim_window_loader_nb"></td></tr>');
  $.ajaxSetup({ 
    scriptCharset: "utf-8" , 
    contentType: "application/x-www-form-urlencoded; charset=utf-8"
  });
  
  $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'json',
    success: function(data) {
      showSearch(data);
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $("#ontopia_connect_errors").html(Drupal.t('<p>Unable to fetch data.</p>'));
    }
    
    });
}

function showSearch(data) {
  if(data.count == 0) {
    var params = {"@search" : data.search};
    $(".messages").remove();
    //$("#headerSeparator").prepend('<div class="messages error">' + Drupal.t('The search "@search" did not return any results',params) + '</div>');
    $("#janitor_table_body").html('');
    $("#janitor_table_body").html(Drupal.t('The search "@search" did not return any results',params));
  }
  else {
    var params = {"@search" : data.search, "@data_count" : data.count};
    $(".messages").remove();
    $("#headerSeparator").prepend('<div class="messages status">' + Drupal.t('The search "@search" returned @data_count results',params) + '</div>');
    $("#janitor_table_body").html('');
    $("#janitor_table_body").html(data.html);
    
    if ($(':input[name=search_offset]')[0].value <= 100 && typeof $("#search_get_next").attr('id') == 'undefined') {
      $("#janitor_get_next").remove();
      $("#navigators").append('<div id="search_get_next" onclick="searchNodes(\''+data.search+'\',\'next\')">'+Drupal.t('Next')+' &gt;&gt;</div>');
    }
    
    $('#flush_search').css('display','block');
    if($(':input[name=search_offset]')[0].value > 100) {
      $("#search_get_next").before('<div id="search_get_previous" onclick="searchNodes(\''+data.search+'\',\'previous\')">&lt;&lt; '+Drupal.t('Previous')+'</div>');
      $("#search_get_previous").css('display','inline');
    } else {
      $("#search_get_previous").remove();
    }
    Drupal.attachBehaviors($('#janitor_table_body'));
  }
}

function updateTopic(topicid,modus,server) {
  var tolog = '';
  if (modus == 'update') {
    var nodeid = topicid;
    var topic_name = sanitizeTopicName($(':input[name=topic_name]')[0].value);
    topicid = $(':input[name=topic_id]')[0].value;
    topicname = topic_name;
    alert(topicname);
    tolog = 'UPDATE|'+topic_name+'|'+topicid
  }
  else if (modus == 'delete'){
    var topicname = $("#topic-name_"+topicid).html();
    var params = {"@topicname" : topicname};
    var answer = confirm(Drupal.t('Are you sure you want to delete the topic named @topicname ?',params));
    if (answer == true) {
      tolog = topicid;
    }
    else {
      return false;
    }
    
  }
  
  var jasonUrl = Drupal.settings.ndla_ontopia_relate_janitor.updatetopic_json_url+"/"+encodeURIComponent(tolog)+"/"+server;
  
  $.ajaxSetup({ 
    scriptCharset: "utf-8" , 
    contentType: "application/x-www-form-urlencoded; charset=utf-8"
  });
  
  $.ajax({
    type: 'GET',
    url: jasonUrl,
    dataType: 'text',
    success: function(data) {
      if(data == '-1') {
        $("#topic_form_messages").html('<p>'+Drupal.t('Something went wrong, and the topic was not updated. Contact administrator')+'</p>')
        $("#topic_form_messages").removeClass('topic_form_success_message');
        $("#topic_form_messages").addClass('topic_form_error_message');
        $("#topic_form_messages").css('display','block');
      }
      else {
        $("#topic_form_messages").html('<p>'+Drupal.t('The topic was updated')+'</p>')
        $("#topic_form_messages").removeClass('topic_form_error_message');
        $("#topic_form_messages").addClass('topic_form_success_message');
        $("#topic_form_messages").css('display','block');
        $(':input[name=topic_form_save_button]').css('display','none');
        $(':input[name=topic_name]').css('display','none');
        $('#label_title').css('display','none');
        
        if (modus == 'update') {
          $("#topic-name_"+nodeid).html();
          $("#topic-name_"+nodeid).html(topic_name);
        }
        else {
          $("#topic-name_"+topicid).parent().remove();
          $(".messages").remove();
          $("#headerSeparator").prepend('<div class="messages status">' + Drupal.t('The topic named @topicname was deleted',params) + '</div>');
        }
      }
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      $("#ontopia_connect_errors").html(Drupal.t('<p>Unable to fetch data.</p>'));
    }
    
    });
    
}


function sanitizeTopicName(topicname) {
  topicname = trim(topicname);
  
  var regstr1 = new RegExp('"',"g");
  topicname = topicname.replace(regstr1,' \u0022 ');

  var regstr2 = new RegExp('“',"g");
  topicname = topicname.replace(regstr2,' \u201C ');
  
  var regstr3 = new RegExp('”',"g");
  topicname = topicname.replace(regstr3,' \u201D ');
  
  var regstr4 = new RegExp('’',"g");
  topicname = topicname.replace(regstr4,' \u2019 ');

  var regstr5 = new RegExp('‘',"g");
  topicname = topicname.replace(regstr5,' \u2018 ');
  
  var regstr6 = new RegExp('[\(]',"g");
  topicname = topicname.replace(regstr6,' \u0029 ');

  var regstr7 = new RegExp('[\)]',"g");
  topicname = topicname.replace(regstr7,' \u0028 ');
  
  var regstr8 = new RegExp('[\[]',"g");
  topicname = topicname.replace(regstr8,' \u005D ');

  var regstr9 = new RegExp('[\]]',"g");
  topicname = topicname.replace(regstr9,' \u005B ');
  
  var regstr10 = new RegExp(':',"g");
  topicname = topicname.replace(regstr10,' \u003A ');
  
  var regstr11 = new RegExp('/',"g");
  topicname = topicname.replace(regstr11,' \u2215 ');
  
  var regstr12 = new RegExp('@',"g");
  topicname = topicname.replace(regstr12,' \u0040 ');
  
  var regstr13 = new RegExp('~',"g");
  topicname = topicname.replace(regstr13,' \u223C ');
  
  
  topicname = topicname.replace(/\^/,' \u2038 ');
  
  var regstr15 = new RegExp(' – ',"g");
  topicname = topicname.replace(regstr15,'\u2014 ');
  
  var regstr16 = new RegExp('[\?]',"g");
  topicname = topicname.replace(regstr16,'\u003F ');
  
  var regstr17 = new RegExp('Æ',"g");
  topicname = topicname.replace(regstr17,'\u00C6');
  var regstr18 = new RegExp('æ',"g");
  topicname = topicname.replace(regstr18,'\u00E6');
  var regstr19 = new RegExp('Ø',"g");
  topicname = topicname.replace(regstr19,'\u00D8');
  var regstr20 = new RegExp('ø',"g");
  topicname = topicname.replace(regstr20,'\u00F8');
  var regstr21 = new RegExp('Å',"g");
  topicname = topicname.replace(regstr21,'\u00C5');
  var regstr22 = new RegExp('å',"g");
  topicname = topicname.replace(regstr22,'\u00E5');
  
  return topicname;
}

function compareTitle(a,b) {
  var titleA = a.title;
  var titleB = b.title;
  if (titleA < titleB) {return -1}
  if (titleA > titleB) {return 1}
  return 0;
}

function compareType(a,b) {
  var typeA = a.type;
  var typeB = b.type;
  if (typeA < typeB) {return -1}
  if (typeA > typeB) {return 1}
  return 0;
}

function comparePublished(a,b) {
  var pubA = a.published;
  var pubB = b.published;
  if (pubA < pubB) {return -1}
  if (pubA > pubB) {return 1}
  return 0;
}