batch_treeview = function(tData,div_id, container,modus)
{
  this.build = function(nodeInfo,div_id)
  {
    var stem = $('<div id="laereplan_'+div_id+'" class="GREPTreeView"></div>');
    var length = nodeInfo.values.length;
    if (length > 0) {
      for(var i=0;i< length;i++) {
        var node = $('<div id="'+div_id+'-LaereplanNode">'+nodeInfo.values[i].fag_tittel+'</div>')
        .css({'margin-top': 5});
        $('<div class="expandNode expand"></div>')
        .prependTo(node);
        
        var hovedomraade_contents = $('<div class="NodeContents"></div>');
        var fag_data = nodeInfo.values[i].fag_data
        var hlen = fag_data.length;
        
        for(var j = 0; j < hlen; j++) {
          
          var hovedomraade_node = $('<div id="'+div_id+'-HovedomraadeNode">'+fag_data[j].hovedomraade_tittel+'</div>')
          .css({'margin-top': 5});
          $('<div class="expandNode expand"></div>')
          .prependTo(hovedomraade_node);
          
          
          var kompetansemaal_contents = $('<div class="NodeContents"></div>');
          var maaldata = fag_data[j].maaldata;
          var mlen = maaldata.length;
          
          for(var k = 0; k < mlen; k++) {
            var kompetansemaal_node = $('<div id="'+div_id+'-KompetansemaalNode"><span class="aim_title">'+maaldata[k].aim_title+'</span></div>')
            .css({'margin-top': 5});
            $('<div class="expandNode expand"><span id="'+maaldata[k].uuid.replace(':','_')+'" class="uuid" /></div>')
            .prependTo(kompetansemaal_node);
            
            
            //hovedomraade_node.append(kompetansemaal_contents);
            kompetansemaal_contents.append(kompetansemaal_node);
            
            kompetansemaal_node.children('.expandNode').click(function() {
              var contents = $(this).parent().children(".NodeContents");
              if (modus == 'plan') {
                $(this).parent().append($('<div class="aim_window_loader_white_nb"></div>'));
                var uuid = $(this).children().filter('.uuid').attr('id').replace('_',':');
                getCurriculaResourcesBySubject(uuid,div_id,'ressurs','');
              }
              
              contents.toggle();
              if(contents.css('display') != "none")
              {
                $(this).attr("class", "expandNode collapse");
              }
              else
              {
                $(this).attr("class", "expandNode expand");
              }
             
            });
            //$('<div class="NodeItem"></div>').html('<div class="ItemTxt"><input type="checkbox" name="'+div_id+'_'+maaldata[k].uuid.replace(':','_')+'" value="'+nodeInfo.values[i].fag_psi+"§"+fag_data[j].hovedomraade_uid+"§"+maaldata[k].uuid+"§"+maaldata[k].aim_title+'" class="aimbox" /> '+maaldata[k].aim_title+' ('+maaldata[k].sort_key+') </div>').appendTo(kompetansemaal_contents);
          }//end for
          
          hovedomraade_node.append(kompetansemaal_contents);
          hovedomraade_contents.append(hovedomraade_node);
          
          
          hovedomraade_node.children('.expandNode').click(function() {
            var contents = $(this).parent().children(".NodeContents");
            contents.toggle();
            if(contents.css('display') != "none")
            {
              $(this).attr("class", "expandNode collapse");
            }
            else
            {
              $(this).attr("class", "expandNode expand");
            }
           
          });
        }//end for

        node.append(hovedomraade_contents);
        
        
        node.children('.expandNode').click(function() {
          var contents = $(this).parent().children(".NodeContents");
          if (contents.length > 0) {
            contents.toggle();
            if(contents.css('display') != "none")
            {
              if (typeof($(this)) !== 'undefined') {
                $(this).attr("class", "expandNode collapse");
              }
              
            }
            else
            {
              if (typeof($(this)) !== 'undefined') {
                $(this).attr("class", "expandNode expand");
              }
            }
          }
          
         
        });

        stem.append(node);
      }//end for
    }//end if
   
   
    
   
    return stem;
  }
 
  this.tree = this.build(tData,div_id);
  var treeCon = container;

  treeCon.append(this.tree);
}