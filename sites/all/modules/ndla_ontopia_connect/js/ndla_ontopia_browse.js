Drupal.behaviors.ontopia_browse = function(context) {
  $("a.grep-leaf").bind('click', function() {
    if(!$(this).hasClass('grep-has-data')) {
      $(this).addClass('grep-has-data');
      $(this).addClass('grep-show');
      the_link = $(this);
      $.ajax({
        async: false,
        data: {'searched_uuid[]': Drupal.settings.ndla_ontopia_browse_searched},
        type: 'GET',
        url: Drupal.settings.basePath + "ndla_ontopia/get_leaf/" + $(this).attr('id').replace("grep-leaf-", ""),
        success: function(data) {
          $($(the_link).parent()).append(data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
          alert("fel fel");
        }
      });
    }
    else if($(this).hasClass('grep-show')) {
      $(this).removeClass('grep-show');
      $(this).siblings('div.grep-leafs-container').slideUp();
    }
    else {
      $(this).addClass('grep-show');
      $(this).siblings('div.grep-leafs-container').slideDown();
    }
  });
  
  $('a.grep-auto-click').each(function() {
    $(this).click();
  });
  
  $('#ndla_ontopia_browse_search_form').bind('submit', ndla_ontopia_browse_pickup_parents);
}

function ndla_ontopia_browse_pickup_parents() {
  var openLeafs = new Array();
  $('form#ndla_ontopia_browse_search_form input:checked').each(function() {
    if($(this).val() != undefined) {
      the_parent = $(this).parent("div.leaf");
      element_id = $(the_parent).attr('id').replace("parent-leaf-", "");
      $('form#ndla_ontopia_browse_search_form').append("<input type='hidden' name='open_parents[]' value='" + element_id + "' />");
    }
  })
}