<?php
/**
 * @file
 * @ingroup ndla_publish_workflow
 */
 
/**
 * Implementation of hook_install
 */
function ndla_publish_workflow_install() {
  drupal_install_schema('ndla_publish_workflow');
  error_log("Installed schema");

  // Populate the ndla_publish_workflow_status table based on the current
  // published status in the node table.
  // Note! The SQL code is very specific for the ndla.no site, as there are
  // some book nodes that should not be published
  db_query("INSERT INTO {ndla_publish_workflow_status} 
            SELECT n.nid, n.vid, 2, r.timestamp FROM {node} n 
            INNER JOIN {node_revisions} r ON n.vid = r.vid 
            WHERE n.status=1 
              AND (
                (type IN ('amendor_electure','amendor_ios','amendor_ios_task',
                          'audio','begrep','biblio','fag','fagstoff','fil',
                          'flashnode','image','lenke','mmenu',
                          'oppgave','person','quiz','test','veiledning',
                          'video')) 
                OR 
                (type='book' AND n.nid<20000)
              )
            ORDER BY nid");

  // Set all nodes as published for Drupal
  db_query("UPDATE {node} SET status=1");

  // Set vid to the latest revision for all nodes, as we no longer have a
  // concept of published or non-published nodes in a Drupal context.
  db_query("UPDATE {node} n SET vid=(SELECT MAX(r.vid) FROM {node_revisions} r WHERE n.nid=r.nid)");
  error_log("Populated the publish workflow table and set all nodes as published locally.");
}


/**
 * Implementation of hook_uninstall
 */
function ndla_publish_workflow_uninstall() {
    drupal_uninstall_schema('ndla_publish_workflow');
}


/**
 * Implementation of hook_schema
 */
function ndla_publish_workflow_schema() {
  $schema = array();
  
  $schema['ndla_publish_workflow_status'] = array(
    'description' => t('Keep track of workflow status for nid/vid pairs'),
    'fields' => array(
      'nid' => array(
        'description' => 'The {node}.nid this status belongs to.',
        'type'     => 'int',
        'unsigned' => TRUE,
	      'not null' => TRUE,
        'default'  => 0,
      ),
      'vid' => array(
        'description' => 'The {node_revisions}.vid this status belongs to.',
        'type'     => 'int',
        'unsigned' => TRUE,
	      'not null' => TRUE,
        'default'  => 0,
      ),
      'status' => array(
        'description' => 'The status for the given nid/vid pair',
        'type'     => 'int',
        'unsigned' => TRUE,
	      'not null' => TRUE,
        'default'  => 0,
      ),
      'timestamp' => array(
        'description' => 'A Unix timestamp indicating when this status was set.',
        'type'     => 'int',
        'unsigned' => TRUE,
	      'not null' => TRUE,
        'default'  => 0,
      ),
      'uid' => array(
        'description' => 'The user performing the action.',
        'type'     => 'int',
        'unsigned' => TRUE,
	      'not null' => TRUE,
        'default'  => 0,
      ),
      'message' => array(
        'description' => 'Potential message about the status update.',
        'type'     => 'text',
        'size'     => 'normal',
	      'not null' => FALSE,
	      'default'  => '',
      ),
      'responsible_uid', array(
        'description' => 'The user responisble for the nest step in the workflow.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default'  => 0,
      ),
      
    ),
    'primary key' => array('nid', 'vid', 'status', 'timestamp'),
  );
  
  return $schema;
}

function ndla_publish_workflow_update_6001() {
  $results = array();
  db_add_field($results, 'ndla_publish_workflow_status', 'uid', array(
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => TRUE,
    'default' => 0,
    'description' => 'The user performing the action.',
  ));
  return $results;
}

function ndla_publish_workflow_update_6002() {
  $results = array();
  db_add_field($results, 'ndla_publish_workflow_status', 'message', array(
    'type' => 'varchar',
    'length' => 255,
    'not null' => FALSE,
    'default' => '',
    'description' => 'Potential message about the status update.',
  ));
  return $results;
}

function ndla_publish_workflow_update_6003() {
  $results = array();

  db_add_field($results, 'ndla_publish_workflow_status', 'responsible_uid', array(
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => TRUE,
    'default'  => 0,
    'description' => 'The user responisble for the nest step in the workflow.',
  ));

  // Change message field from varchar to text type
  db_change_field($results, 'ndla_publish_workflow_status', 'message', 'message', array(
    'type'     => 'text',
    'size'     => 'normal',
    'not null' => FALSE,
    'default' => '',
    'description' => 'Potential message about the status update.',
  ));
  
  // Shift all status codes one step up
  $results[] = update_sql('UPDATE {ndla_publish_workflow_status} SET status=5 WHERE status=4');
  $results[] = update_sql('UPDATE {ndla_publish_workflow_status} SET status=4 WHERE status=3');
  $results[] = update_sql('UPDATE {ndla_publish_workflow_status} SET status=3 WHERE status=2');
  $results[] = update_sql('UPDATE {ndla_publish_workflow_status} SET status=2 WHERE status=1');
  $results[] = update_sql('UPDATE {ndla_publish_workflow_status} SET status=1 WHERE status=0');
  
  return $results;
}

/**
 * Re-save all slideshows so they sync again.
 */
function ndla_publish_workflow_update_6004() {
  $time = time();
  if(module_exists('rdf')) {
    module_load_include('inc', 'rdf', 'rdf.db');
  }

  $query = "SELECT s1.nid, s1.vid, s1.status, s1.uid, s1.message, s1.responsible_uid FROM {ndla_publish_workflow_status} s1
            INNER JOIN node n ON s1.vid = n.vid 
            WHERE s1.status = 3 AND n.type = 'slideshow' AND s1.vid =
              (SELECT MAX(s2.`vid`) 
              FROM {ndla_publish_workflow_status} s2 
              WHERE s2.nid=s1.nid
              AND s2.status = 3)
            GROUP BY s1.nid, s1.vid";

  $result = db_query($query);
  while($row = db_fetch_object($result)) {
    _ndla_publish_workflow_set_status($row->nid, $row->vid, $row->status, 'ndla_publish_workflow_update_6004', $row->responsible_uid);
  }
  $time_two = time();

  echo "Slideshow update took " . ($time_two-$time) . " seconds\n";
  $ret[] = array('success' => TRUE, 'query' => '');
  return $ret;
}

/**
 * Re-save all H5Ps so they sync again.
 */
function ndla_publish_workflow_update_6005() {
  $time = time();
  if(module_exists('rdf')) {
    module_load_include('inc', 'rdf', 'rdf.db');
  }

  $query = "SELECT s1.nid, s1.vid, s1.status, s1.uid, s1.message, s1.responsible_uid FROM {ndla_publish_workflow_status} s1
            INNER JOIN node n ON s1.vid = n.vid 
            WHERE s1.status = 3 AND n.type = 'h5p_content' AND s1.vid =
              (SELECT MAX(s2.`vid`) 
              FROM {ndla_publish_workflow_status} s2 
              WHERE s2.nid=s1.nid
              AND s2.status = 3)
            GROUP BY s1.nid, s1.vid";

  $result = db_query($query);
  while($row = db_fetch_object($result)) {
    _ndla_publish_workflow_set_status($row->nid, $row->vid, $row->status, 'ndla_publish_workflow_update_6004', $row->responsible_uid);
  }
  $time_two = time();

  echo "H5P update took " . ($time_two-$time) . " seconds\n";
  $ret[] = array('success' => TRUE, 'query' => '');
  return $ret;
}

/**
 * Re-save all Packages so they sync again.
 */
function ndla_publish_workflow_update_6006() {
  $time = time();
  if(module_exists('rdf')) {
    module_load_include('inc', 'rdf', 'rdf.db');
  }

  $query = "SELECT s1.nid, s1.vid, s1.status, s1.uid, s1.message, s1.responsible_uid FROM {ndla_publish_workflow_status} s1
            INNER JOIN node n ON s1.vid = n.vid 
            WHERE s1.status = 3 AND n.type = 'package' AND s1.vid =
              (SELECT MAX(s2.`vid`) 
              FROM {ndla_publish_workflow_status} s2 
              WHERE s2.nid=s1.nid
              AND s2.status = 3)
            GROUP BY s1.nid, s1.vid";

  $result = db_query($query);
  while($row = db_fetch_object($result)) {
    _ndla_publish_workflow_set_status($row->nid, $row->vid, $row->status, 'ndla_publish_workflow_update_6004', $row->responsible_uid);
  }
  $time_two = time();

  echo "Package update took " . ($time_two-$time) . " seconds\n";
  $ret[] = array('success' => TRUE, 'query' => '');
  return $ret;
}