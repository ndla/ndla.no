<?php
/**
 * @file
 * @ingroup ndla_publish_workflow
 */

/**
 * Generates the page for the publishing queue itself.
 * 
 * Page callback for content/node/publishing_queue.
 */
function ndla_publish_workflow_page() {
  drupal_set_title(t('Publishing queue'));
  $output .= drupal_get_form('ndla_publish_workflow_publish_queue_form', TRUE);
  return $output;
}

function _ndla_publish_workflow_get_group_options(&$options = array(), $parent = NULL, &$level = 0) {
  if(!$parent) {
    $parent = ndla_fag_taxonomy_get_vocabulary_tree();
    $parent = $parent->terms;
    $options[''] = t('All');
    _ndla_publish_workflow_get_group_options($options, $parent, $level);
  }
  
  foreach($parent as $term) {
    $suffix = ($level > 0) ? implode("", array_fill(0, $level , "-")) : "";
    $level_before = $level;
    $options[$term->tid] = trim($suffix . " " . $term->name);
    
    if(is_array($term->terms)) {
      $level += 1;
      _ndla_publish_workflow_get_group_options($options, $term->terms, $level);
    }
    $level = $level_before;
  }
  
  return $options;
}

/**
 * Implementation of hook_form().
 *
 * Display form showing a list of the nodes in the publishing queue.
 *
 * @return array
 *   An array containing the form elements to be displayed in the form. 
 * 
 */
function ndla_publish_workflow_publish_queue_form($form_state = array()) {
  global $_SESSION;
  $enabled_filters = $form = array();
  $enabled_filters['title'] = isset($_REQUEST['title']) ? check_plain($_REQUEST['title']) : '';
  $enabled_filters['language'] = isset($_REQUEST['language']) ? check_plain($_REQUEST['language']) : '';
  $enabled_filters['type'] = isset($_REQUEST['type']) ? check_plain($_REQUEST['type']) : '';
  $enabled_filters['og_tax'] = isset($_REQUEST['og_tax']) ? check_plain($_REQUEST['og_tax']) : '';
  $enabled_filters['user'] = isset($_REQUEST['og_tax']) ? check_plain($_REQUEST['user']) : '';
  $enabled_filters['sent_in'] = isset($_REQUEST['sent_in']) ? check_plain($_REQUEST['sent_in']) : '';

  //While using #method = get the hook_FORM_ID_submit() no longer gets triggered. Call it manually.
  if(isset($_POST) && in_array($_POST['op'], array(t('Decline'), t('Publish'), t('Publish, also publish inserted nodes')))) {
    $fake_state['values'] = $_POST;
    ndla_publish_workflow_publish_queue_form_submit($form, $fake_state);
  }
  
  if(!empty($_POST)) {
      drupal_goto('content/node/publishing_queue', $enabled_filters);  
  }

  $tax_groups = _ndla_publish_workflow_get_group_options();
  $lang_options = array('' => t('All'), 'neutral' => t('Neutral'), );
  $type_options = array('' => t('All'));
  
  foreach (language_list() as $lang) {
    $lang_options[$lang->language] = $lang->name;
  }
  
  foreach (node_get_types() as $type) {
    $type_options[$type->type] = $type->name;
  }
  
  $form['#method'] = 'post';
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('Filter'),
    'filter' => array(
      'title' => array(
        '#title' => t('Title'),
        '#type' => 'textfield',
        '#size' => 20,
        '#default_value' => $enabled_filters['title'],
      ),
      'user' => array(
        '#type' => 'textfield',
        '#title' => t('Author'),
        '#size' => 20,
        '#default_value' => $enabled_filters['user'],
        '#autocomplete_path' => 'content/user/autocomplete',
      ),
      'sent_in' => array(
        '#type' => 'textfield',
        '#title' => t('Submitted by'),
        '#size' => 20,
        '#default_value' => $enabled_filters['sent_in'],
        '#autocomplete_path' => 'content/user/autocomplete',
      ),
      'language' => array(
        '#title' => t('Language'),
        '#type' => 'select',
        '#multiple' => FALSE,
        '#options' => $lang_options,
        '#default_value' => $enabled_filters['language'],
      ),
      'type' => array(
        '#title' => t('Type'),
        '#type' => 'select',
        '#multiple' => FALSE,
        '#options' => $type_options,
        '#default_value' => $enabled_filters['type'],
      ),
      'og_tax' => array(
        '#title' => t('Group'),
        '#type' => 'select',
        '#options' => $tax_groups,
        '#multiple' => FALSE,
        '#default_value' => $enabled_filters['og_tax'], 
      ),
      'search' => array(
        '#type' => 'submit',
        '#value' => t('Filter'),
      ),
    ),
  );
  
  $sort = arg(3);

  if (empty($sort)) {
    $sort = isset($_SESSION['publishing_queue_SORT']) && $_SESSION['publishing_queue_SORT'] != 'undefined' ? $_SESSION['publishing_queue_SORT'] : "";
    if ($sort == '') {
      $sort = 'changed_rev';
    }
  }

  $_SESSION['publishing_queue_SORT'] = $sort;
  $items = _ndla_publish_workflow_get_items($sort, $enabled_filters);
  $sort = explode('_', $sort);
  
  if (in_array($sort[0], array('author', 'group', 'statusuid'))) {
    if($sort[0] == 'statusuid') {
      $sort[0] = 'status_user';
    }
    $items = _ndla_publish_workflow_sort($items, $sort[0], $sort[1]);
  }

  if (user_access('administer nodes')) {
    $form['options'] = array('#type' => 'fieldset',
      '#title' => t('Update options'),
    );
    $form['options']['publish_message'] = array(
      '#type' => 'textarea',
      '#title' => t('Publish message'),
      '#description' => t('Enter publish message. Please note that all checked nodes will get the same message.'),
    );
    $form['options']['publish'] = array(
      '#type' => 'submit',
      '#value' => t('Publish')
    );
    $form['options']['message'] = array(
      '#type' => 'textarea',
      '#title' => t('Decline message'),
      '#description' => t('Enter reason for the declination. Please note that all checked nodes will get the same message.'),
    );
    $form['options']['decline'] = array(
      '#type' => 'submit',
      '#value' => t('Decline')
    );
    
  }

  $langs = language_list();
  
  foreach($items as $node) {
    $id = $node['nid'] . '_' . $node['vid'];
    $nodes[$id] = '';
    if(!$node['group']) {
      $node['group'] = "";
    }
    $form['group'][$node['nid'] . '_' . $node['vid']] = array(
      '#value' => $node['group'],
    );
    $form['type'][$id] = array(
      '#value' => $node['type'],
    );
    $form['published'][$id] = array(
      '#value' =>  date('d.m.Y - H:i', $node['changed']),
    );
    $form['submitted'][$id] = array(
      '#value' =>  date('d.m.Y - H:i', $node['submitted']),
    );
    $form['author'][$id] = array(
      '#value' => l($node['author'], 'user/' . $node['uid']),
    );
    $form['status_user'][$id] = array(
      '#value' => l($node['status_user'], 'user/' . $node['status_uid']),
    );
    $form['language'][$id] = array(
      '#value' => $node['language'],
    );
    
    $message = _ndla_publish_workflow_get_message($node);
    if(count($message) && $message['message']) {
      $message = _ndla_publish_workflow_get_message($node);
      $msg_text = l(t('View'), '', array('attributes' => array('class' => 'pub_message_link', 'onclick' => "alert('{$message['message']}'); return false;")));
      $form['message'][$id] = array(
        '#value' => $msg_text,
      );
    }

    $mark = '';
    $saved_states = array();
    $current_status = ndla_publish_workflow_get_current_state($node, $saved_states);
    if (isset($saved_states[PUBLISH_WORKFLOW_PUBLISHED])) {
      // If node has been published earlier, mark it as updated
      $mark .= theme('mark', MARK_UPDATED);
    }
    
    if($node['type_machine'] == 'nodemenu') {
      $form['title'][$id]['#value'] = l($node['title'], 'node/' . $node['nid'], array('query' => array('all' => 1)));
    }
    else {
      $form['title'][$id] = array(
        '#value' => l($node['title'], 'node/' . $node['nid'], array('language' => $langs[$node['language']])) . $mark,
      );
    }
    $form['operations'][$id] = array(
      '#value' => l(t('Edit'), 'node/' . $node['nid'] . '/edit', array('language' => $langs[$node['language']])),
    );
  }
  
  $form['nodes'] = array(
    '#type' => 'checkboxes',
    '#options' => $nodes,
  );
  $form['pager'] = array(
    '#value' => theme('pager', NULL, 5, 0),
  );

  return $form;
}


/**
 * Helper function for sorting a list of nodes with a specified field as sorting key.
 *
 * @param array $items
 *   A list of items to be sorted.
 * @param string $key
 *   The name of the field used for sorting the items.
 * @return array
 */
function _ndla_publish_workflow_sort($items, $key, $reverse = '') {
  for ($i = 0; $i < sizeof($items); ++$i) {
    $prefix = "0000";
    if($items[$i][$key]) {
      $prefix = $items[$i][$key];
    }
    $new_items[strtolower($prefix . '_' . $items[$i]['title'] . '_' . $items[$i]['nid'] . '_' . $items[$i]['vid'])] = $items[$i];
  }
  
  
  if($reverse) {
    krsort($new_items);
  }
  else {
    ksort($new_items); 
  }

  return $new_items;
}


/**
 * @brief
 *   Submit function for the publishing queue node list form.
 *
 * @param array $form
 *   Nested array of form elements that comprise the form.
 * @param array $form_state
 *   A keyed array containing the current state of the form. 
 * @return 
 *   drupal_access_denied() if the user has no access to the publishing queue.
 *   
 */

function ndla_publish_workflow_publish_queue_form_submit($form, &$form_state) {
  global $user;
  if (user_access('workflow publish')) {
    $form_values = $form_state['values'];
    if(is_array($form_values['nodes'])) {
      foreach ($form_values['nodes'] as $key) {
        if ($key != 0) {
          $node_info = explode('_', $key);
          $nid = $node_info[0];
          $vid = $node_info[1];
          if($form_values['op'] == t('Publish, also publish inserted nodes') || $form_values['op'] == t('Publish')) {
            // Notify the person who put the node into the publishing queue
            $msg = isset($form_values['publish_message']) ? trim(check_plain($form_values['publish_message'])) : '';
            _ndla_publish_workflow_set_status($nid, $vid, PUBLISH_WORKFLOW_PUBLISHED, $msg, $user->name);
            
          }
          else if($form_values['op'] == t('Decline')) {
            $msg = isset($form_values['message']) ? trim(check_plain($form_values['message'])) : '';
            _ndla_publish_workflow_set_status($nid, $vid, PUBLISH_WORKFLOW_PUBLISH_REJECTED, $msg, $user->name);
            
            // Notify the person who put the node into the publishing queue
            _ndla_publish_workflow_notify_owner($nid, PUBLISH_WORKFLOW_PUBLISH_REJECTED);
          }
        }
      }
    }
  }
  else {
    return drupal_access_denied(); 
  }
}


/**
 * Helper function for loading list of nodes for the publishing queue list.
 *
 * @param string $sort
 *   The name of the field used for sorting the items.
 * @return array
 *   The items node list.
 */
function _ndla_publish_workflow_get_items($sort, $filters = array()) {
  global $_SESSION;
  $items = array();

  $split_sort = array();
  $split_sort = explode('_', $sort);
  $sort_key = $split_sort[0];
  switch ($sort_key) {
    case 'title':
      $sort_field = 'r.title';
      break;
    case 'type':
      $sort_field = 'n.type';
      break;
    case 'changed':
      $sort_field = 'r.timestamp';
      break;
    default:
      $sort_field = 'n.vid';
  }

  if ($split_sort[1] == 'rev') {
    $sort_field .= ' DESC';
  }
  else {
    $sort_field .= ' ASC';
  }

  $where = array();
  //Node title
  if(isset($filters['title']) && !empty($filters['title'])) {
    $where[] = "r.title LIKE '" . $filters['title'] . "%'";
  }
  
  //Node type
  if(isset($filters['type']) && !empty($filters['type'])) {
    $where[] = "n.type = '" . $filters['type'] . "'";
  }
  
  //OG Taxonomy
  if(isset($filters['og_tax']) && !empty($filters['og_tax'])) {
    $where[] = "td.tid = " . $filters['og_tax'];
  }
  
  //Author
  if(isset($filters['user']) && !empty($filters['user'])) {
    $where[] = "u.name = '" . $filters['user'] ."'";
  }
  
  //Node language
  if(isset($filters['language']) && !empty($filters['language'])) {
    if($filters['language'] == 'neutral') {
      $where[] = "(n.language IS NULL OR n.language = '')";
    }
    else {
      $where[] = "n.language = '" . $filters['language'] . "'";
    }
  }

  if(isset($filters['sent_in']) && !empty($filters['sent_in'])) {
    $where[] = "u2.name = '" . $filters['sent_in'] ."'";
  }

  if(count($where)) {
    $where = " AND " . implode(" AND ", $where);
  }
  else {
    $where = '';
  }

  $vocabulary = ndla_fag_taxonomy_get_default_vocabulary();
  $vid = $vocabulary->vid;
  $sql = "SELECT n.nid, n.vid AS n_vid, r.vid, r.uid, n.type, r.title, r.timestamp, n.language, s1.uid as status_uid, s1.timestamp as submitted
          FROM {node} n
          INNER JOIN {node_revisions} r ON n.vid=r.vid
          INNER JOIN {ndla_publish_workflow_status} s1 ON s1.vid=n.vid
          INNER JOIN {users} u ON r.uid = u.uid
          LEFT JOIN {users} u2 ON u2.uid = s1.uid
          LEFT JOIN {term_node} tn ON r.vid = tn.vid AND r.nid = tn.nid
          LEFT JOIN {term_data} td ON td.tid = tn.tid AND td.vid = $vid
          LEFT JOIN {ndla_fag_taxonomy_map} nftm ON nftm.tid = td.tid
          WHERE s1.timestamp=(
            SELECT MAX(s2.timestamp) 
            FROM ndla_publish_workflow_status s2 
            WHERE s2.nid=s1.nid 
          )
          AND s1.status=%d $where GROUP BY n.nid, n.vid";

  $sql .= " ORDER BY $sort_field";

  $result = db_query($sql, PUBLISH_WORKFLOW_QUEUE);
  $types = node_get_types('names');

  while ($node = db_fetch_array($result)) {
    if ($node['vid'] == $node['n_vid']) {
      $found = FALSE;
      for ($i = 0; $i < sizeof($items); $i++) {
        if ($items[$i]['nid'] == $node['nid']) {
          $found = TRUE;
          break;
        }
      }
      if ($found) {
        continue;
      }
    }
    $found = FALSE;
    for ($i = 0; $i < sizeof($items); $i++) {
      if ($items[$i]['nid'] == $node['nid'] && $items[$i]['uid'] == $node['uid']) {
        $found = TRUE;
        break;
      }
    }
    if ($found) {
      continue;
    }

    $node['author'] = ndla_utils_get_username($node['uid']);
    $node['status_user'] = ndla_utils_get_username($node['status_uid']);
    $node['type_machine'] = $node['type'];
    $node['type'] = $types[$node['type']];
    $node['changed'] = $node['timestamp'];
    $fags = ndla_fag_taxonomy_get_node_terms($node['nid'], $node['vid']);
    $node['group'] = array();
    foreach($fags as $fag) {
      if($fag->gid) {
        $node['group'][] = $fag->name;
      }
    }

    if (count($node['group'])) {
      $node['group'] = implode(", ", $node['group']);
    }
    $items[] = $node;
  }

  return $items;
}

/**
 * Theme the publishing queue table
 *
 * @param array $form
 *   Nested array of form elements that comprise the form.
 * @return string
 *   The generated HTML code for the form.
 */
function theme_ndla_publish_workflow_publish_queue_form($form) {
  global $_SESSION;
  $base = base_path() . 'content/node/publishing_queue/';

  // Overview table:
  $split_sort = explode('_', $_SESSION['publishing_queue_SORT']);
  $sort_key = $split_sort[0];
  $sort_order = $split_sort[1];
  
  if ($sort_order == 'rev') {
    $sort_icon = 'misc/arrow-asc.png';
  }
  else {
    $sort_icon = 'misc/arrow-desc.png';
  }

  $header = array(
    theme('table_select_header_cell'), 
    '<a href="' . $base . 'title' . ($sort_order == '' && $sort_key == 'title' ? '_rev' : '') . '">' . t('Title') . '</a>' . ($sort_key == 'title' ? ' <img src="'.base_path().$sort_icon.'" alt="*">' : ''),
    '<a href="' . $base . 'type' . ($sort_order == '' && $sort_key == 'type' ? '_rev' : '') . '">' . t('Type') . '</a>' . ($sort_key == 'type' ? ' <img src="'.base_path().$sort_icon.'" alt="*">' : ''),
    '<a href="' . $base . 'group' . ($sort_order == '' && $sort_key == 'group' ? '_rev' : '') . '">' . t('Group') . '</a>' . ($sort_key == 'group' ? ' <img src="'.base_path().$sort_icon.'" alt="*">' : ''),
    t('Language'),
    '<a href="' . $base . 'author' . ($sort_order == '' && $sort_key == 'author' ? '_rev' : '') . '">' . t('Author') . '</a>' . ($sort_key == 'author' ? ' <img src="'.base_path().$sort_icon.'" alt="*">' : ''),
    '<a href="' . $base . 'statusuid' . ($sort_order == '' && $sort_key == 'statusuid' ? '_rev' : '') . '">' . t('Submitted by') . '</a>' . ($sort_key == 'statusuid' ? ' <img src="'.base_path().$sort_icon.'" alt="*">' : ''),
    '<a href="' . $base . 'changed' . ($sort_order == '' && $sort_key == 'changed' ? '_rev' : '') . '">' . t('Last updated') . '</a>' . ($sort_key == 'changed' ? ' <img src="'.base_path().$sort_icon.'" alt="*">' : ''),
    '<a href="' . $base . 'submitted' . ($sort_order == '' && $sort_key == 'submitted' ? '_rev' : '') . '">' . t('Date submitted') . '</a>' . ($sort_key == 'submitted' ? ' <img src="'.base_path().$sort_icon.'" alt="*">' : ''),
    t('Operations'),
    t('Message'),
    t('Inserted nodes'),
  );
  
  $output .= drupal_render($form['filters']);
  $output .= drupal_render($form['options']);
  if (isset($form['title']) && is_array($form['title'])) {
    foreach (element_children($form['title']) as $key) {
      $nid_vid = explode("_", $key);
      $row = array();
      $row[] = drupal_render($form['nodes'][$key]);
      $row[] = drupal_render($form['title'][$key]);
      $row[] = drupal_render($form['type'][$key]);
      $row[] = drupal_render($form['group'][$key]);
      $row[] = drupal_render($form['language'][$key]);
      $row[] = drupal_render($form['author'][$key]);
      $row[] = drupal_render($form['status_user'][$key]);
      $row[] = drupal_render($form['published'][$key]);
      $row[] = drupal_render($form['submitted'][$key]);
      $row[] = drupal_render($form['operations'][$key]);
      $row[] = drupal_render($form['message'][$key]);
      $row[]= l(t('View'), 'node/' . $nid_vid[0] . "/inserted_status", array('attributes' => array('rel' => 'lightframe')));
      $rows[] = $row; 
    }

  }
  else  {
    $rows[] = array(array('data' => t('No posts in the publishing queue.'), 'colspan' => '6'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}
