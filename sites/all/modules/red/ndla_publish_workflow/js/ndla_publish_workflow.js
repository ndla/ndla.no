Drupal.behaviors.ndla_publish_workflow = function () {
  $(".revision-message").click(function(e) {
    var idTag = $(this).attr('id');
    var revision = idTag.split('_')[1];
    var message_id = '#workflow_message_' + revision;
    $(message_id).toggleClass("hidden");
    if ($(this).html() == Drupal.t('View')) {
      $(this).html(Drupal.t('Hide'));
    }
    else {
      $(this).html(Drupal.t('View'));
    }
    return false;
  });
};
