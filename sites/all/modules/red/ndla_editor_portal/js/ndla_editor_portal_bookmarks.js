Drupal.behaviors.ndla_editor_portal_bookmarks = function(context) {
  $('#ndla_editor_portal_bookmarks').change(function(){
    url = $(this).val();
    if(!url.match(/^http[s]*:\/\//)) {
      url = 'http://' + url; 
    }
    window.location = url;
  });
}