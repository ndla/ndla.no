<?php

/**
 * Admin form for special operations
 */
function ndla_convert_ios_to_h5p_admin_form() {
  $form = array();

  $form['radio_fix'] = array(
    '#type' => 'submit',
    '#value' => t('Re-convert radio button tasks'),
    '#suffix' => '<p>' . t('Warning! This will overwrite converted H5Ps that should have had radio buttons. Any changes made to this content will be overwritten!') . '</p>',
    '#disabled' => variable_get('ndla_convert_ios_to_h5p_radio_fix_ran', FALSE)
  );

  $form['translate'] = array(
    '#type' => 'submit',
    '#value' => t('Update translations'),
    '#suffix' => '<p>' . t('Go through tasks with different language and replace default values with translated values.') . '</p>',
  );

  $form['replace'] = array(
    '#type' => 'submit',
    '#value' => t('Replace special chars'),
    '#suffix' => '<p>' . t('Replace all special characters in converted tasks..') . '</p>',
  );

  $form['one_fix'] = array(
    '#type' => 'submit',
    '#value' => t('Only one'),
    '#suffix' => '<p>' . t('Convert all converted Question Sets with only one question to Multi Choice.') . '</p>',
  );

  $form['disable_image_zoom'] = array(
    '#type' => 'submit',
    '#value' => t('Disable image zoom'),
    '#suffix' => '<p>' . t('Will disable image zoom for all Multiple Choice tasks.') . '</p>',
  );

  $form['update_drag_strings'] = array(
    '#type' => 'submit',
    '#value' => t('Update Drag and Drop strings'),
    '#suffix' => '<p>' . t('Will convert the feedback string of all Drag and Drop tasks in NB and NN nodes to "2 av 9 poeng".') . '</p>',
  );

  return $form;
}

/**
 * Handles admin form submission
 */
function ndla_convert_ios_to_h5p_admin_form_submit($form, &$form_state) {
  $batch['file'] = drupal_get_path('module', 'ndla_convert') . '/ndla_convert.admin.inc';
  $batch['progress_message'] = t('Completed @current out of @total jobs.');

  if ($form_state['values']['op'] === t('Re-convert radio button tasks')) {
    variable_set('ndla_convert_ios_to_h5p_radio_fix_ran', TRUE);

    // Create a batch job
    $batch['title'] = t('Re-converting radio button tasks');
    $batch['finished'] = 'ndla_convert_ios_to_h5p_fix_radios_finished';
    $batch['operations'][] = array('ndla_convert_ios_to_h5p_fix_radios', array());
  }
  elseif ($form_state['values']['op'] === t('Update translations')) {

    // Create a batch job
    $batch['title'] = t('Updating translations');
    $batch['finished'] = 'ndla_convert_ios_to_h5p_translation_finished';
    $batch['operations'][] = array('ndla_convert_ios_to_h5p_translation', array());
  }
  elseif ($form_state['values']['op'] === t('Replace special chars')) {
    // Create a batch job
    $batch['title'] = t('Replacing special chars');
    $batch['finished'] = 'ndla_convert_ios_to_h5p_replacechars_finished';
    $batch['operations'][] = array('ndla_convert_ios_to_h5p_replacechars', array());
  }
  elseif ($form_state['values']['op'] === t('Only one')) {
    // Create a batch job
    $batch['title'] = t('Converting Question Sets');
    $batch['finished'] = 'ndla_convert_ios_to_h5p_onlyone_finished';
    $batch['operations'][] = array('ndla_convert_ios_to_h5p_onlyone', array());
  }
  elseif ($form_state['values']['op'] === t('Disable image zoom')) {
    // Create a batch job
    $batch['title'] = t('Disabling image zoom');
    $batch['finished'] = 'ndla_convert_ios_to_h5p_updatetasks_finished';
    $batch['operations'][] = array('ndla_convert_ios_to_h5p_updatetasks', array(
      (object) array(
        'name' => 'H5P.MultiChoice',
        'major' => 1,
        'minor' => 5
      ),
      'disableimagezoom'
    ));
  }
  elseif ($form_state['values']['op'] === t('Update Drag and Drop strings')) {
    // Create a batch job
    $batch['title'] = t('Updating Drag and Drop strings');
    $batch['finished'] = 'ndla_convert_ios_to_h5p_updatetasks_finished';
    $batch['operations'][] = array('ndla_convert_ios_to_h5p_updatetasks', array(
      (object) array(
        'name' => 'H5P.DragQuestion'
      ),
      'updatedragstrings'
    ));
  }
  else {
    return;
  }

  batch_set($batch);
  batch_process();
}

/**
 * Process batch job
 */
function ndla_convert_ios_to_h5p_fix_radios(&$context) {
  if (!isset($context['sandbox']['current'])) {
    // Initial setup
    $context['results']['done'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['tasks'] = array();

    // Find tasks
    $result = db_query("SELECT n.ios_nid, n.h5p_nid FROM {amendor_ios_task} a JOIN {ndla_convert_ios_to_h5p} n ON n.ios_nid = a.nid WHERE xmlContent LIKE '<MCSTask%' AND xmlContent LIKE '%radiobuttons=%'");
    while ($task = db_fetch_object($result)) {
      $context['sandbox']['tasks'][] = $task;
    }
  }

  $number = count($context['sandbox']['tasks']);
  if (!$number) {
    return; // Nothing to fix
  }

  // Process
  $task = $context['sandbox']['tasks'][$context['sandbox']['current']];

  $xmlContent = db_result(db_query("SELECT xmlContent FROM {amendor_ios_task} WHERE nid = %d", $task->ios_nid));
  $h5p_node = new stdClass();
  ndla_convert_xmlContent_to_h5p($xmlContent, $h5p_node);
  db_query("UPDATE {h5p_nodes} SET filtered = '', json_content = '%s' WHERE nid = %d", $h5p_node->json_content, $task->h5p_nid);

  // Track progress
  $context['results']['done']++;
  $context['sandbox']['current']++;
  $context['message'] = t('Re-converted %current out of %number tasks.', array('%current' => $context['sandbox']['current'], '%number' => $number));

  if ($context['sandbox']['current'] !== $number) {
    $context['finished'] = $context['sandbox']['current'] / $number;
  }
}

/**
 * Batch job completed
 */
function ndla_convert_ios_to_h5p_fix_radios_finished($success, $results) {
  drupal_set_message(t('Successfully re-converted %num tasks.', array('%num' => $results['done'])));
}

/**
 * Process batch job
 */
function ndla_convert_ios_to_h5p_translation(&$context) {
  if (!isset($context['sandbox']['current'])) {
    // Initial setup
    $context['results']['updated'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['tasks'] = array();

    // Find tasks
    $result = db_query("SELECT n.nid, n.language FROM {ndla_convert_ios_to_h5p} nc JOIN node n ON n.nid = nc.h5p_nid WHERE language IN ('nb', 'nn')");
    while ($task = db_fetch_object($result)) {
      $context['sandbox']['tasks'][] = $task;
    }
  }

  $number = count($context['sandbox']['tasks']);
  if (!$number) {
    return; // Nothing to fix
  }

  // Process
  $task = $context['sandbox']['tasks'][$context['sandbox']['current']];

  $h5p = db_fetch_object(db_query("SELECT hn.json_content, hl.machine_name FROM {h5p_nodes} hn JOIN {h5p_libraries} hl ON hl.library_id = hn.main_library_id WHERE hn.nid = %d", $task->nid));
  $params = json_decode($h5p->json_content);
  switch ($h5p->machine_name) {
    case 'H5P.QuestionSet':
      ndla_convert_ios_to_h5p_translate_questionset($params, $task->language, $context['results']['updated']);
      break;

    case 'H5P.MultiChoice':
      ndla_convert_ios_to_h5p_translate_multichoice($params, $task->language, $context['results']['updated']);
      break;

    case 'H5P.DragQuestion':
      ndla_convert_ios_to_h5p_translate_dragquestion($params, $task->language, $context['results']['updated']);
      break;
  }
  db_query("UPDATE {h5p_nodes} SET filtered = '', json_content = '%s' WHERE nid = %d", json_encode($params), $task->nid);

  // Track progress
  $context['sandbox']['current']++;
  $context['message'] = t('Translated %current out of %number tasks.', array('%current' => $context['sandbox']['current'], '%number' => $number));

  if ($context['sandbox']['current'] !== $number) {
    $context['finished'] = $context['sandbox']['current'] / $number;
  }
}

/**
 * Update question set translations
 */
function ndla_convert_ios_to_h5p_translate_questionset(&$params, $language, &$counter) {
  if ($params->introPage->startButtonText === 'Start Quiz') {
    $params->introPage->startButtonText = 'Start';
    $counter++;
  }
  if ($params->texts->prevButton === 'Previous') {
    $params->texts->prevButton = 'Forrige';
    $counter++;
  }
  if ($params->texts->nextButton === 'Next') {
    $params->texts->nextButton = 'Neste';
    $counter++;
  }
  if ($params->texts->finishButton === 'Finish') {
    $params->texts->finishButton = 'Avslutt';
    $counter++;
  }
  if ($params->texts->textualProgress === 'Question: @current of @total questions') {
    $params->texts->textualProgress = ($language === 'nb' ? 'Deloppgave @current av @total' : 'Deloppgåve @current av @total');
    $counter++;
  }
  if (!isset($params->endGame->scoreString)) {
    $params->endGame->scoreString = 'Du fikk @score poeng av @total mulige.';
    $counter++;
  }
  if ($params->endGame->successGreeting === 'Congratulations!') {
    $params->endGame->successGreeting = 'Gratulerer!';
    $counter++;
  }
  if ($params->endGame->successComment === 'You did very well!') {
    $params->endGame->successComment = 'Du presterte meget godt!';
    $counter++;
  }
  if ($params->endGame->failGreeting === 'Oh, no!') {
    $params->endGame->failGreeting = ($language === 'nb' ? 'Ikke bestått' : 'Ikkje bestått');
    $counter++;
  }
  if ($params->endGame->failComment === "This didn't go so well.") {
    $params->endGame->failComment = 'Det er litt mange feil her.';
    $counter++;
  }
  if ($params->endGame->solutionButtonText === 'Show solution') {
    $params->endGame->solutionButtonText = 'Vis fasit';
    $counter++;
  }
  if ($params->endGame->finishButtonText === 'Finish') {
    $params->endGame->finishButtonText = 'Avslutt';
    $counter++;
  }
  if ($params->endGame->skipButtonText === 'Skip video') {
    $params->endGame->skipButtonText = 'Hopp over';
    $counter++;
  }

  // Update questions
  foreach ($params->questions as &$question) {
    if ($question->library === 'H5P.MultiChoice 1.0') {
      ndla_convert_ios_to_h5p_translate_multichoice($question->params, $language, $counter);
    }
    elseif ($question->library === 'H5P.DragQuestion 1.0') {
      ndla_convert_ios_to_h5p_translate_dragquestion($question->params, $language, $counter);
    }
  }
}

/**
 * Update multi choice translations
 */
function ndla_convert_ios_to_h5p_translate_multichoice(&$params, $language, &$counter) {
  if ($params->UI->showSolutionButton === 'Show solution') {
    $params->UI->showSolutionButton = 'Vis svar';
    $counter++;
  }
  if (!isset($params->UI->tryAgainButton) || $params->UI->tryAgainButton === 'Retry') {
    $params->UI->tryAgainButton = 'Prøv igjen';
    $counter++;
  }
  if ($params->UI->correctText === 'Correct!') {
    $params->UI->correctText = 'Rett!';
    $counter++;
  }
  if ($params->UI->almostText === 'Almost!') {
    $params->UI->almostText = 'Nesten!';
    $counter++;
  }
  if ($params->UI->wrongText === 'Wrong!') {
    $params->UI->wrongText = 'Feil';
    $counter++;
  }
}

/**
 * Update drag question translations
 */
function ndla_convert_ios_to_h5p_translate_dragquestion(&$params, $language, &$counter) {
  if ($params->scoreShow === 'Show score') {
    $params->scoreShow = 'Vis svar';
    $counter++;
  }
  if (!isset($params->tryAgain) || $params->tryAgain === 'Retry') {
    $params->tryAgain = 'Prøv igjen';
    $counter++;
  }
  if ($params->correct === 'Solution') {
    $params->correct = 'Korrekt';
    $counter++;
  }

  foreach ($params->question->task->elements as &$element) {
    if ($element->library === 'H5P.Image 1.0') {
      $element->params->contentName = ($language === 'nb' ? 'Bilde' : 'Bilete');
      $counter++;
    }
  }
}

/**
 * Batch job completed
 */
function ndla_convert_ios_to_h5p_translation_finished($success, $results) {
  drupal_set_message(t('Successfully updated %num translations.', array('%num' => $results['updated'])));
}

/**
 * Process batch job
 */
function ndla_convert_ios_to_h5p_replacechars(&$context) {
  if (!isset($context['sandbox']['current'])) {
    // Initial setup
    $context['results']['updated'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['tasks'] = array();

    // Find tasks
    $result = db_query("SELECT hn.nid FROM {ndla_convert_ios_to_h5p} nc JOIN {h5p_nodes} hn ON hn.nid = nc.h5p_nid WHERE hn.json_content LIKE '%qzx%' OR hn.json_content LIKE '%qzy%'");
    while ($task = db_fetch_object($result)) {
      $context['sandbox']['tasks'][] = $task->nid;
    }
  }

  $number = count($context['sandbox']['tasks']);
  if (!$number) {
    return; // Nothing to fix
  }

  // Process
  $task = $context['sandbox']['tasks'][$context['sandbox']['current']];

  $parameters = db_result(db_query("SELECT json_content FROM {h5p_nodes} WHERE nid = %d", $task));
  $counter = 0;
  $parameters = str_replace(array('qzx', 'qzy'), array('%', '+'), $parameters, $counter);
  db_query("UPDATE {h5p_nodes} SET filtered = '', json_content = '%s' WHERE nid = %d", $parameters, $task);
  $context['results']['updated'] += $counter;

  // Track progress
  $context['sandbox']['current']++;
  $context['message'] = t('Replaced chars in %current out of %number tasks.', array('%current' => $context['sandbox']['current'], '%number' => $number));

  if ($context['sandbox']['current'] !== $number) {
    $context['finished'] = $context['sandbox']['current'] / $number;
  }
}

/**
 * Batch job completed
 */
function ndla_convert_ios_to_h5p_replacechars_finished($success, $results) {
  drupal_set_message(t('Successfully replaced %num special chars.', array('%num' => $results['updated'])));
}

/**
 * Process batch job
 */
function ndla_convert_ios_to_h5p_onlyone(&$context) {
  if (!isset($context['sandbox']['current'])) {
    // Initial setup
    $context['results']['done'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['tasks'] = array();
    $context['results']['kalle'] = array();

    // Find all questions sets that has been converted
    $library_id = ndla_convert_get_library_id('H5P.Questionset', 1, 0);
    $result = db_query("SELECT hn.nid FROM {h5p_nodes} hn JOIN {ndla_convert_ios_to_h5p} n ON n.h5p_nid = hn.nid WHERE hn.main_library_id = %d", $library_id);
    while ($nid = db_result($result)) {
      $context['sandbox']['tasks'][] = $nid;
    }
  }

  $number = count($context['sandbox']['tasks']);
  if (!$number) {
    return; // Nothing to fix
  }

  // Process
  $nid = $context['sandbox']['tasks'][$context['sandbox']['current']];
  $set = json_decode(db_result(db_query("SELECT json_content FROM {h5p_nodes} WHERE nid = %d", $nid)));
  if (count($set->questions) === 1) {
    // Let's converte!
    $library_id = ndla_convert_get_library_id('H5P.MultiChoice', 1, 0);
    db_query("UPDATE {h5p_nodes} SET main_library_id = %d, json_content = '%s' WHERE nid = %d", $library_id, json_encode($set->questions[0]->params), $nid);
    $context['results']['done']++;
  }

  // Track progress
  $context['sandbox']['current']++;
  $context['message'] = t('Checking %current out of %number question sets.', array('%current' => $context['sandbox']['current'], '%number' => $number));

  if ($context['sandbox']['current'] !== $number) {
    $context['finished'] = $context['sandbox']['current'] / $number;
  }
}

/**
 * Batch job completed
 */
function ndla_convert_ios_to_h5p_onlyone_finished($success, $results) {
  drupal_set_message(t('Successfully converted %num question sets.', array('%num' => $results['done'])));
}

function ndla_convert_ios_fix_amendor_form() {
  $form = array();
  /* Tables to wipe:
   *  amendor_ios_task
   *  amendor_ios_user
   *  amendor_ios_includes
   */
  $amendor_ios_task = _ndla_convert_get_amendor_ios_task();
  $amendor_ios_user = _ndla_convert_get_amendor_ios_user();
  $amendor_ios_includes = _ndla_convert_get_amendor_ios_includes();
  $amendor_ios_includes_reverse = _ndla_convert_get_amendor_ios_includes_reverse();

  $message = "Table amendor_ios_task contains <strong>" . count($amendor_ios_task) . "</strong> faulty rows.<br />";
  $message .= "Table amendor_ios_user contains <strong>" . count($amendor_ios_user) . "</strong> faulty rows.<br />";
  $message .= "Table amendor_ios_includes contains <strong>" . count($amendor_ios_includes) . "</strong> faulty rows.<br />";
  $message .= "Table amendor_ios_includes (reverse) contains <strong>" . count($amendor_ios_includes_reverse) . "</strong> faulty rows (media_nid is H5P).<br /><br />";

  $message .= "<p>" . t('There are <strong>NO turning back</strong> after you click the button. The data is lost forever.') . "</p>";

  $form['message'] = array(
    '#type' => 'markup',
    '#value' => $message,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Clean tables'),
  );

  return $form;
}

function ndla_convert_ios_fix_amendor_form_submit() {
  //Start cleaning the tables.
  $amendor_ios_task = _ndla_convert_get_amendor_ios_task();
  $amendor_ios_user = _ndla_convert_get_amendor_ios_user();
  $amendor_ios_includes = _ndla_convert_get_amendor_ios_includes();
  $amendor_ios_includes_reverse = _ndla_convert_get_amendor_ios_includes_reverse();

  db_query("DELETE FROM {amendor_ios_task} WHERE nid IN (" . implode(", ", $amendor_ios_task) . ")");
  db_query("DELETE FROM {amendor_ios_user} WHERE nid IN (" . implode(", ", $amendor_ios_user) . ")");
  db_query("DELETE FROM {amendor_ios_includes} WHERE nid IN (" . implode(", ", $amendor_ios_includes) . ")");
  db_query("DELETE FROM {amendor_ios_includes} WHERE media_nid IN (" . implode(", ", $amendor_ios_includes_reverse) . ")");

  drupal_set_message(t('All tables wiped and cleaned'));
}

function _ndla_convert_get_amendor_ios_task() {
  $rows = array();
  $result = db_query("
    SELECT n.nid, n.type FROM {amendor_ios_task} ait
    INNER JOIN {node} n ON n.nid = ait.nid
    WHERE n.type NOT LIKE 'amendor%'
  ");

  while($row = db_fetch_object($result)) {
    $rows[] = $row->nid;
  }

  return $rows;
}
function _ndla_convert_get_amendor_ios_user() {
  $rows = array();
  $result = db_query("
    SELECT n.nid, n.type FROM {amendor_ios_user} aiu
    INNER JOIN {node} n ON n.nid = aiu.nid
    WHERE n.type NOT LIKE 'amendor%'
  ");

  while($row = db_fetch_object($result)) {
    $rows[] = $row->nid;
  }

  return $rows;
}

function _ndla_convert_get_amendor_ios_includes() {
  $rows = array();
  $result = db_query("
    SELECT n.nid, n.type FROM {node} n
    INNER JOIN {amendor_ios_includes} ai ON ai.nid = n.nid
    WHERE n.type NOT IN('amendor_ios', 'amendor_ios_task')
  ");

  while($row = db_fetch_object($result)) {
    $rows[] = $row->nid;
  }

  return $rows;
}

function _ndla_convert_get_amendor_ios_includes_reverse() {
  $rows = array();
  $result = db_query("
    SELECT n.nid, n.type FROM {node} n
    INNER JOIN {amendor_ios_includes} ai ON ai.media_nid = n.nid
    WHERE n.type IN('h5p_content')
  ");

  while($row = db_fetch_object($result)) {
    $rows[] = $row->nid;
  }

  return $rows;
}

/**
 * Process batch job
 */
function ndla_convert_ios_to_h5p_updatetasks($lib, $job, &$context) {
  if (!isset($context['sandbox']['current'])) {
    // Initial setup
    $context['results']['done'] = 0;
    $context['sandbox']['current'] = 0;
    $context['sandbox']['tasks'] = array(
      'standalone' => array(),
      'embedded' => array()
    );

    // Custom "where" for selecting tasks for each job
    switch ($job) {
      case 'disableimagezoom':
        $standalone_where =
            "(l.major_version > {$lib->major} OR
               (l.major_version = {$lib->major} AND l.minor_version >= {$lib->minor})
             )";
        // Standalone where must select the correct library
        $embedded_where = "n.json_content LIKE '%\"library\":\"{$lib->name} %'";
        // Embedded where doesn't have to be very accurate, library will be checked before updating.
        break;

      case 'updatedragstrings':
        $standalone_where = "n2.language IN ('nb', 'nn')";
        $embedded_where = $standalone_where . " AND n.json_content LIKE '%\"library\":\"{$lib->name} %'";
        break;
    }

    // Find all standalone tasks
    $result = db_query(
        "SELECT n.nid
           FROM {h5p_nodes} n
           JOIN {h5p_libraries} l ON l.library_id = n.main_library_id
      LEFT JOIN {node} n2 ON n2.nid = n.nid
          WHERE l.machine_name = '{$lib->name}'
            AND {$standalone_where}");
    while ($nid = db_result($result)) {
      $context['sandbox']['tasks']['standalone'][] = $nid;
    }

    // Find all content with embedded tasks
    $result = db_query(
        "SELECT n.nid
           FROM {h5p_nodes} n
           JOIN {h5p_libraries} l ON l.library_id = n.main_library_id
      LEFT JOIN {node} n2 ON n2.nid = n.nid
          WHERE l.machine_name != '{$lib->name}'
            AND {$embedded_where}");
    while ($nid = db_result($result)) {
      $context['sandbox']['tasks']['embedded'][] = $nid;
    }
  }

  $num_standalone = count($context['sandbox']['tasks']['standalone']);
  $num_embedded = count($context['sandbox']['tasks']['embedded']);
  $num_total = $num_standalone + $num_embedded;
  if (!$num_total) {
    return; // Nothing to update
  }

  if ($context['sandbox']['current'] < $num_standalone) {
    // Convert standalone

    // Load parameters
    $nid = $context['sandbox']['tasks']['standalone'][$context['sandbox']['current']];
    $params = ndla_convert_ios_to_h5p_updatetasks_loadparams($nid);

    // Do the job
    ndla_convert_ios_to_h5p_updatetasks_dojob($params, $job);
  }
  else {
    // Convert embedded

    // Load parameters
    $nid = $context['sandbox']['tasks']['embedded'][$context['sandbox']['current'] - $num_standalone];
    $params = ndla_convert_ios_to_h5p_updatetasks_loadparams($nid);

    // Find lib inside content
    ndla_convert_ios_to_h5p_updatetasks_traverse($params, $lib, $job);
  }

  // Save
  db_query("UPDATE {h5p_nodes} SET json_content = '%s', filtered = '' WHERE nid = %d", json_encode($params), $nid);

  // Make sure new revision is created, so that NDLA's sync will trigger on this
  // node
  $node = node_load($nid);
  node_save($node);

  $context['results']['done']++;

  // Track progress
  $context['sandbox']['current']++;
  $context['message'] = t('Updating %current out of %number tasks.', array('%current' => $context['sandbox']['current'], '%number' => $num_total));
  if ($context['sandbox']['current'] !== $num_total) {
    $context['finished'] = $context['sandbox']['current'] / $num_total;
  }
}

/**
 * Load params for given nid.
 *
 * @param int $nid
 * @return array
 */
function ndla_convert_ios_to_h5p_updatetasks_loadparams($nid) {
  return json_decode(db_result(db_query("SELECT json_content FROM {h5p_nodes} WHERE nid = %d", $nid)), TRUE);
}

/**
 * Execute the specified job for the given params.
 *
 * @param array $params
 * @param string $job
 */
function ndla_convert_ios_to_h5p_updatetasks_dojob(&$params, $job) {
  // Choose operation
  switch ($job) {
    case 'disableimagezoom':
      $params['behaviour']['disableImageZooming'] = TRUE;
      break;

    case 'updatedragstrings':
      $params['feedback'] = '@score av @total poeng (feil gir minuspoeng)';
      break;
  }
}

/**
 * Traverse H5P parameters looking for multiple choice tasks to update.
 *
 * @param array $params
 * @param string $lib
 * @param string $job
 */
function ndla_convert_ios_to_h5p_updatetasks_traverse(&$params, $lib, $job) {
  foreach ($params as &$param) {
    if (isset($param['library'])) {
      // Found lib, check type and version
      $param_library = array();
      preg_match("/^([^ ]+) ([0-9]+)\.([0-9]+)$/", $param['library'], $param_library);

      if ($param_library[1] !== $lib->name) {
        continue; // Not the correct lib
      }
      if (isset($lib->major)) {
        if ($param_library[2] < $lib->major) {
          continue; // Too small major version
        }
        elseif ($param_library[2] == $lib->major && // Same major version
            isset($lib->minor) && // Must check minor version
            $param_library[3] < $lib->minor) {
          continue; // Too small minor version
        }
      }

      // Do the job
      ndla_convert_ios_to_h5p_updatetasks_dojob($param['params'], $job);
    }
    elseif (is_array($param)) {
      // Traverse sub-object
      ndla_convert_ios_to_h5p_updatetasks_traverse($param, $lib, $job);
    }
  }
}

/**
 * Batch job completed, print num updated content.
 */
function ndla_convert_ios_to_h5p_updatetasks_finished($success, $results) {
  drupal_set_message(t('Successfully updated %num tasks.', array('%num' => $results['done'])));
}
