<?php
/**
 * Quiz to H5P Conversion Form
 */
function ndla_convert_quiz_to_h5p_form(&$form_state, $node) {
  $form = array(
    '#theme' => 'ndla_convert_quiz_to_h5p_form',
    'quiz' => array(
      '#tree' => TRUE,
    ),
  );

  $result = db_query("SELECT n.title, n.nid, n.vid, n.language, qh.h5p_nid
                      FROM {quiz_node_properties} q
                      LEFT JOIN {node} n on q.nid = n.nid
                      LEFT JOIN {ndla_convert_quiz_to_h5p} qh ON qh.quiz_nid = n.nid
                      WHERE n.nid IS NOT NULL");

  $workflow_states = ndla_publish_workflow_get_states();

  while ($quiz = db_fetch_object($result)) {

    $node = node_load($quiz->nid);

    $form['quiz'][$quiz->nid] = array(
      'convert' => array(
        '#type' => 'checkbox',
        '#disabled' => ($quiz->h5p_nid !== NULL),
      ),
      'name' => array(
        '#type' => 'markup',
        '#value' => l($quiz->title, 'node/' . $quiz->nid . '/take', array('attributes' => array('target' => '_blank'))),
      ),
      'h5p' => array(
        '#type' => 'markup',
        '#value' => ($quiz->h5p_nid === NULL ? '' : l(t('H5P'), 'node/' . $quiz->h5p_nid, array('attributes' => array('target' => '_blank')))),
      ),
      'status' => array(
        '#type' => 'markup',
        '#value' => isset($node->ndla_workflow_status) ? $workflow_states[$node->ndla_workflow_status] : t('Unknown'),
      ),
      'language' => array(
        '#type' => 'markup',
        '#value' => $node->language,
      ),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => ('Convert'),
  );

  return $form;
}

/**
 * Submit handler for QUIZ to H5P
 */
function ndla_convert_quiz_to_h5p_form_submit($form, &$form_state) {
  // Required libraries
  $libraries = array(
    (object) array(
      'name' => 'H5P.MultiChoice',
      'major' => 1,
      'minor' => 5
    ),
    (object) array(
      'name' => 'H5P.Questionset',
      'major' => 1,
      'minor' => 6
    )
  );

  // Check that libraries exists
  foreach ($libraries as $library) {
    $library->id = ndla_convert_get_library_id($library->name, $library->major, $library->minor);
    if ($library->id === FALSE) {
      drupal_set_message(t('Cannot convert! Missing library %lib.', array('%lib' => $library->name . ' ' . $library->major . '.' . $library->minor)), 'error');
      return;
    }
  }

  // Loop through nids and convert. Collect result messages.
  $messages = array();
  foreach ($form_state['values']['quiz'] as $nid => $values) {
    if ($values['convert'] === 1) {
      $messages[] = ndla_convert_quiz_to_h5p_qs($nid);
    }
  }

  // Hide alarming messages about stuff being successfully deleted and etc.
  drupal_get_messages('status');
  foreach ($messages as $message) {
    drupal_set_message($message);
  }

  // Hacky way to show list again (but the only way I got it working):
  $_REQUEST['destination'] = 'admin/content/quiz-to-h5p';
}


function ndla_convert_quiz_to_h5p_qs($quiz_nid) {
  $quiz_node = node_load($quiz_nid);

  if ($quiz_node->type !== 'quiz') {
    return t('This is not a quiz');
  }

  $old_quiz_vid = $quiz_node->vid;

  /**
   * Create The H5Ps JSON content
   */
  $h5p_json_content;
  if(($status = ndla_convert_quiz_to_h5p_qs_json($quiz_node, $h5p_json_content)) !== TRUE) {
    // Not able to convert it
    return $status;
  }

  /**
   * Clone Quiz node.
   */
  unset($quiz_node->nid);
  // Update NDLA Workflow.
  $quiz_node->ndla_workflow_message = 'Clone of Quiz converted to H5P.';
  $quiz_node->ndla_workflow_status = PUBLISH_WORKFLOW_UNPUBLISHED;
  // Save clone
  node_save($quiz_node);

  // Update all dependencies between quiz-node and tables
  // quiz_node_properties is handled by Quiz module
  db_query("UPDATE {quiz_node_result_options} SET nid = %d, vid = %d WHERE nid = %d AND vid=%d", $quiz_node->nid, $quiz_node->vid, $quiz_nid, $old_quiz_vid);
  db_query("UPDATE {quiz_node_results} SET nid = %d, vid = %d WHERE nid = %d AND vid=%d", $quiz_node->nid, $quiz_node->vid, $quiz_nid, $old_quiz_vid);
  db_query("UPDATE {quiz_question_latest_quizzes} SET quiz_nid = %d WHERE quiz_nid = %d", $quiz_node->nid, $quiz_nid);
  db_query("UPDATE {quiz_node_relationship} SET parent_nid = %d, parent_vid = %d WHERE parent_nid = %d AND parent_vid=%d", $quiz_node->nid, $quiz_node->vid, $quiz_nid, $old_quiz_vid);

  /**
   * Change type of old QUIZ node
   */
  $h5p_node = node_load($quiz_nid, NULL, TRUE);
  node_delete($h5p_node->nid);
  db_query("INSERT INTO {node} (nid, vid, type) VALUES (%d, %d, %d)", $h5p_node->nid, $h5p_node->vid, $h5p_node->type);
  db_query("INSERT INTO {node_revisions} (nid, vid) VALUES(%d, %d)", $h5p_node->nid, $h5p_node->vid);

  /**
   * Update NDLA Workflow for H5P.
   */
  $h5p_node->ndla_workflow_status = PUBLISH_WORKFLOW_DRAFT;
  $h5p_node->ndla_workflow_message = 'Converted from Quiz.';

  /**
   * Set mandatory ingress field if empty and hide it in the view
   */
  if (!isset($h5p_node->field_ingress[0]['value'])) {
    $h5p_node->field_ingress[0]['value'] = 'Auto converted from Quiz.';
  }
  $h5p_node->field_ingressvispaasiden[0]['value'] = '0';

  /**
   * Set H5P specific data, and save the H5P node.
   */
  $h5p_node->type = 'h5p_content';
  $h5p_node->h5p_library = $h5p_node->main_library_id = ndla_convert_get_library_id('H5P.QuestionSet', 1, 6);
  $h5p_node->h5p_type = 'create';
  $h5p_node->embed_type = 'iframe';
  $h5p_node->json_content = json_encode($h5p_json_content);
  node_save($h5p_node);

  /**
   * Set taxonomy terms:
   * -- "Vanskelighetsgrad" to "middels" (tid=48644 + vid=14)
   * -- "Læringsressurstype" to "Tester og oppgaver" (tid=48614 + vid=19)
   * -- "Innholdstype" to "Oppgave -> Quiz" (tid=70939/70936 + vid=100004)
   */
  taxonomy_node_save($h5p_node, array(48644, 48614, 70939, 70936));

  /**
   * Make sure the new node isn't deleted in the workflow.
   */
  db_query("DELETE FROM {ndla_publish_workflow_status} WHERE nid = %d AND vid = %d AND status = %d", $h5p_node->nid, $h5p_node->vid, PUBLISH_WORKFLOW_DELETED);

  /**
   * Update mapping table
   */
  db_query("INSERT INTO {ndla_convert_quiz_to_h5p} (quiz_nid, h5p_nid) VALUES (%d, %d)", $quiz_node->nid, $h5p_node->nid);

  return t('Converted <a href="!quiz" target="_blank">Quiz</a> to <a href="!h5p" target="_blank">H5P</a>: %title. Num questions: %num_questions', array('!quiz' => url('node/' . $quiz_node->nid), '!h5p' => url('node/' . $h5p_node->nid), '%title' => $h5p_node->title, '%num_questions' => sizeof($h5p_json_content->questions)));
}

function ndla_convert_quiz_to_h5p_qs_json($node, &$h5p_qs) {
  $quiz_nid = $node->nid;
  $quiz_vid = $node->vid;

  // Create the QS skeleton
  $h5p_qs = ndla_convert_create_h5p_questionset($node->language);

  // Set pass percentage. Use default value if set to 0
  if ($node->pass_rate !== 0) {
    $h5p_qs->passPercentage = $node->pass_rate;
  }

  // Set backwards navigation allowed
  $h5p_qs->disableBackwardsNavigation = ($node->backwards_navigation === 0);

  // Set feedback on end screen
  if ($node->summary_pass) {
    $h5p_qs->endGame['successComment'] = $node->summary_pass;
  }
  if ($node->summary_default) {
    $h5p_qs->endGame['failComment'] = $node->summary_default;
  }

  // Set randomization parameters
  $h5p_qs->poolSize = $node->number_of_random_questions;
  $h5p_qs->randomQuestions = ($node->randomization !== 0);

  // Load questions
  $questions = db_query('SELECT n.title, n.type, n.vid, nr.body
    FROM {quiz_node_relationship} qnr
    INNER JOIN {node_revisions} nr ON nr.vid = qnr.child_vid
    LEFT JOIN {node} n on nr.nid = n.nid
    WHERE qnr.parent_vid = %d', $quiz_vid);

  while($question = db_fetch_object($questions)) {
    if ($question->type !== 'multichoice') {
      return t('This Quiz contains not only multichoice questions, and can therefore not be converted.');
    }

    // Get answers
    $result = db_query('SELECT * FROM {quiz_multichoice_answers} WHERE question_vid = %d', $question->vid);
    $answers = array();
    while($answer = db_fetch_object($result)) {
      $answers[] = array(
        'text' => $answer->answer,
        'correct' => $answer->score_if_chosen > 0,
        'tipsAndFeedback' => array(
          'tip' => '',
          'chosenFeedback' => $answer->feedback_if_chosen,
          'notChosenFeedback' => $answer->feedback_if_not_chosen,
        ),
      );
    }

    // If question body contains an image, move it to the image part of MC
    // Handle images inserted using contentbrowser:
    $matches = array();
    preg_match_all("/\[contentbrowser.*nid=(\d*).*contentbrowser\]/", $question->body, $matches, PREG_SET_ORDER);

    // Is any images inserted using <img>-tag? If so, don't convert!

    // If more than one image, do not allow convert
    $media = NULL;
    if (sizeof($matches) > 1) {
      return t('One of the questions contains more than one image. That is not supported by H5P.QuestionSet. Please fix this manually before converting this node');
    }
    elseif (sizeof($matches) === 1) {
      $question->body = preg_replace("/\[contentbrowser.*contentbrowser\]/", '', $question->body);
      $node_id = $matches[0][1];
      $media = ndla_h5p_get_node_data($node_id);
    }

    $question_config = array(
      'library' => 'H5P.MultiChoice 1.5',
      'subContentId' => ndla_convert_create_uuid(),
      'params' => array(
        'question' => $question->body,
        'answers' => $answers,
        'UI' => ndla_convert_get_mc_translation($node->language),
        'behaviour' => array(
          'enableRetry' => TRUE,
          'enableSolutionsButton' => TRUE,
          'singlePoint' => FALSE,
          'randomAnswers' => TRUE,
          'showSolutionsRequiresInput' => TRUE,
          'disableImageZooming' => TRUE,
          'type' => 'auto'
        ),
      ),
    );

    if (isset($media)) {
      $question_config['params']['media'] = array(
        'library' => 'H5P.Image 1.0',
        'subContentId' => ndla_convert_create_uuid(),
        'params' => array(
          'contentName' => 'Image',
          'file' => array(
            'path' => $media->data->path,
            'mime' => $media->data->mime,
            'nodeId' => $media->data->nodeId,
            'copyright' => array(
              'title' => $media->copyright->title,
              'author' => $media->copyright->author,
              'license' => $media->copyright->license,
            ),
          ),
          "alt" => $media->copyright->title,
        ),
      );
    }

    $h5p_qs->questions[] = $question_config;
  }

  if (empty($h5p_qs->questions)) {
    return t("No questions in Quiz with nid !nid - won't convert it", array('!nid' => $quiz_nid));
  }

  return TRUE;
}

function ndla_convert_get_mc_translation($language) {
  switch ($language) {
    case 'en':
      return array(
        'checkAnswerButton' => 'Check',
        'showSolutionButton' => 'Show solution',
        'tryAgainButton' => 'Retry',
        'correctText' => 'Correct!',
        'almostText' => 'Almost!',
        'wrongText' => 'Wrong!'
      );

    default:
      return array(
        'checkAnswerButton' => 'Sjekk',
        'showSolutionButton' => 'Vis svar',
        'tryAgainButton' => 'Prøv igjen',
        'correctText' => 'Rett!',
        'almostText' => 'Nesten!',
        'wrongText' => 'Feil!',
      );
  }
}

/**
 * Create new questionset
 *
 * @return object for json.
 */
function ndla_convert_create_h5p_questionset($language) {
  return (object) array_replace_recursive(array(
    'progressType' => 'dots',
    'passPercentage' => 50,
    'questions' => array(),
    'introPage' => array(
      'showIntroPage' => FALSE,
    ),
    'endGame' => array(
      'showResultPage' => TRUE,
      'showAnimations' => FALSE,
      'skippable' => FALSE,
    ),
    'override' => array(
      'overrideButtons' => FALSE,
      'overrideShowSolutionButton' => FALSE,
      'overrideRetry' => FALSE,
    ),
  ), ndla_convert_get_qs_translation($language));
}

function ndla_convert_get_qs_translation($language) {
  switch ($language) {
    case 'en':
      return array(
        'introPage' => array(
          'startButtonText' => 'Start Quiz',
        ),
        'texts' => array(
          'prevButton' => 'Previous',
          'nextButton' => 'Next',
          'finishButton' => 'Finish',
          'textualProgress' => 'Question: @current of @total questions',
        ),
        'endGame' => array(
          'scoreString' => 'You got @score points of @total possible.',
          'successGreeting' => 'Congratulations!',
          'successComment' => 'You did very well!',
          'failGreeting' => 'Oh, no!',
          'failComment' => "This didn't go so well.",
          'solutionButtonText' => 'Show solution',
          'retryButtonText' => 'Retry',
          'finishButtonText' => 'Finish',
          'skipButtonText' => 'Skip video',
        ),
        'questionLabel' => 'Question',
      );
    case 'nn':
      return array(
        'introPage' => array(
          'startButtonText' => 'Start',
        ),
        'texts' => array(
          'prevButton' => 'Forrige',
          'nextButton' => 'Neste',
          'finishButton' => 'Avslutt',
          'textualProgress' => 'Deloppgave @current av @total',
        ),
        'endGame' => array(
          'scoreString' => 'Du fikk @score poeng av @total mulige.',
          'successGreeting' => 'Gratulerer!',
          'successComment' => 'Du presterte godt!',
          'failGreeting' => 'Ikkje bestått',
          'failComment' => 'Dette er litt mange feil her. Prøv igjen!',
          'solutionButtonText' => 'Vis fasit',
          'retryButtonText' => 'Prøv igjen',
          'finishButtonText' => 'Avslutt',
          'skipButtonText' => 'Hopp over',
        ),
        'questionLabel' => 'Spørsmål',
      );
    default:
      return array(
        'introPage' => array(
          'startButtonText' => 'Start',
        ),
        'texts' => array(
          'prevButton' => 'Forrige',
          'nextButton' => 'Neste',
          'finishButton' => 'Avslutt',
          'textualProgress' => 'Deloppgave @current av @total',
        ),
        'endGame' => array(
          'scoreString' => 'Du fikk @score poeng av @total mulige.',
          'successGreeting' => 'Gratulerer!',
          'successComment' => 'Du presterte godt!',
          'failGreeting' => 'Ikke bestått',
          'failComment' => "Ikke bestått. Prøv igjen!",
          'solutionButtonText' => 'Vis fasit',
          'retryButtonText' => 'Prøv igjen',
          'finishButtonText' => 'Avslutt',
          'skipButtonText' => 'Hopp over',
        ),
        'questionLabel' => 'Spørsmål',
      );
  }
}

/**
 * Generates a UUID v4. Taken from: http://php.net/manual/en/function.uniqid.php
 * @return string uuid
 */
function ndla_convert_create_uuid() {
  return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
    // 32 bits for "time_low"
    mt_rand(0, 0xffff), mt_rand(0, 0xffff),

    // 16 bits for "time_mid"
    mt_rand(0, 0xffff),

    // 16 bits for "time_hi_and_version",
    // four most significant bits holds version number 4
    mt_rand(0, 0x0fff) | 0x4000,

    // 16 bits, 8 bits for "clk_seq_hi_res",
    // 8 bits for "clk_seq_low",
    // two most significant bits holds zero and one for variant DCE1.1
    mt_rand(0, 0x3fff) | 0x8000,

    // 48 bits for "node"
    mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
  );
}
