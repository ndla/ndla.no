<?php


/**
 * Return a persistent variable, similar to variable_get() only data are not stored in memory.
 *
 * @param string $name Variable name.
 * @param mixed $default Value to return if nothing found in table.
 * @return mixed
 */
function utdanning_create_course_variable_get($name, $default) {
    $db = db_query("SELECT value FROM {utdanning_create_course_variable} WHERE name = '%s' LIMIT 1", $name);
    $data = db_fetch_object($db);
    if ($data) {
        return unserialize($data->value);
    }
    return $default;
}


/**
 * Deletes a persistent variable, similar to variable_del().
 *
 * @param string $name String of value.
 */
function utdanning_create_course_variable_del($name) {
    db_query("DELETE FROM {utdanning_create_course_variable} WHERE name = '%s'", $name);
}


/**
 * Stores a persistent variable, similar to variable_set(), except data are not fetched into memory on page load.
 *
 * @see serialize($value)
 * @param string $name Variable name.
 * @param mixed $value Value to serialize and store for later.
 */
function utdanning_create_course_variable_set($name, $value) {
    $serialized_value = serialize($value);
    db_query("UPDATE {utdanning_create_course_variable} SET value = '%s' WHERE name = '%s'", $serialized_value, $name);
    if (!db_affected_rows()) {
        @db_query("INSERT INTO {utdanning_create_course_variable} (name, value) VALUES ('%s', '%s')", $name, $serialized_value);
    }
}
