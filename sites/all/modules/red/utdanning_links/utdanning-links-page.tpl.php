<div class="utdanning_links_filter">
  <span>Filter on subject:</span>
  <select id="__utdanning_links--select">
    <option value="all" selected>All subjects</option>
  </select>
</div>

<table class="utdanning_links sticky-enabled">
  <thead class="utdanning_links--head">
    <tr>
      <td width="10%">Node</td>
      <td width="80%">Response</td>
      <td width="10%">Link</td>
    </tr>
  </thead>
  <tbody id="__utdanning_links--body">
  </tbody>
</table>
