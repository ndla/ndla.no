(function () {
  $(document).ready(function () {

    /**
    * Object containing arrays and functions related to
    * populating a table with rows.
    **/
    var table = {
      data: [],
      element: $('#__utdanning_links--body'),
      filterFragments: [],

      clearTable: function () {
        this.element[0].innerHTML = '';
      },

      /**
      * Parse and filter the table data to create table-rows
      *
      * @param {String} filter
      * @returns {String} HTML
      **/
      parseData: function (filter) {
        var _tableData = this.data;
        var _tableLength = _tableData.length;
        var _fragment = '';

        for (var i = 0; i < _tableLength; i++) {
          var _data = _tableData[i];
          var _nid = _data.nid;

          // Skip nodes that don't match 'filter'
          if (filter !== _data.group_nid && filter !== 'all') {
            continue;
          }

          _fragment += '<tr>' +
            '<td><a href="/node/' + _nid + '/">' + _nid + '</a></td>' +
            '<td>' + _data.response + '</td>' +
            '<td><a href="' + _data.url + '">Vis url</a></td>' +
            '</tr>';
        }

        return _fragment;
      },

      /**
      * Load filtered table-rows into 'table.element'
      *
      * @param {String} filter
      **/
      render: function (filter) {
        var _filterFragments = this.filterFragments[filter];
        var _element = this.element;

        this.clearTable();

        // Use cached filter fragment if it exists. Otherwise
        // parse data and cache fragment until next time.
        if (_filterFragments) {
          return _element.append(_filterFragments);
        }
        else {
          var _fragment = this.parseData(filter);

          this.filterFragments[filter] = _fragment;
          return _element.append(_fragment);
        }
      }
    };

    /**
    * Object containing arrays and function related to
    * polulating a selection with options, and attaching
    * an event listener to the selection.
    **/
    var options = {
      data: [],
      element: $('#__utdanning_links--select'),

      /**
      * Append options data to 'options.element' and attach
      * eventListener on selection change.
      **/
      init: function () {
        var _element = this.element;

        _element.append(this.parseData());
        _element.change(function () { table.render(this.value); });
      },

      /**
      * Parse the options data to create selectable options
      *
      * @returns {String} HTML
      **/
      parseData: function () {
        var _optionsData = this.data;
        var _optionsLength = _optionsData.length;
        var _fragment = '';

        for (var i = 0; i < _optionsLength; i++) {
          var _data = _optionsData[i];

          _fragment += '<option value="' + _data.nid + '">' + _data.subject + '</option>';
        }

        return _fragment;
      }
    };

    $.ajax({
      type: 'GET',
      url: '/admin/reports/utdanning_dead_links/data.json',
      dataType: 'json',
      success: function (data) {
        var _table = table;
        var _options = options;

        _table.data = data.table;
        _options.data = data.options;
        _options.init();
        _table.render('all');
      }
    });
  });
})();
