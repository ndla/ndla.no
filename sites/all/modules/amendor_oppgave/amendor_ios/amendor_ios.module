<?php

/**
 * @file amendor_ios.module php file
 *   Insert file description here.
 */

/**
 * Implementation of hook_node_info().
 */
function amendor_ios_node_info() {
  return array(
    'amendor_ios_task' => array(
      'name' => t('Interactive task'),
      'module' => 'amendor_ios',
      'description' => t('With this node type you can create interactive tasks for your users to solve.')
    ),
    'amendor_ios' => array(
      'name' => t('Interactive task set'),
      'module' => 'amendor_ios',
      'description' => t('With this node type you can put together multiple interactive task in a task set.')
    )
  );
}

/**
 * Implementation of hook_perm().
 */
function amendor_ios_perm() {
  return array('create amendor_ios', 'access amendor_ios', 'edit own amendor_ios', 'edit any amendor_ios', 'access amendor_ios_task', 'create amendor_ios_task', 'edit own amendor_ios_task', 'edit any amendor_ios_task');
}

/**
 * Implementation of hook_access().
 */
function amendor_ios_access($op, $node) {
  global $user;
  $type = is_object($node) ? $node->type : $node;

  if ($op == 'create' && (user_access('create amendor_ios') && $type == 'amendor_ios' || user_access('create amendor_ios_task') && $type == 'amendor_ios_task')) {
    return TRUE;
  }

  if ($op == 'update' || $op == 'delete') {
    if (user_access('edit any amendor_ios') && $type == 'amendor_ios' || user_access('edit any amendor_ios_task') && $type == 'amendor_ios_task') {
      return TRUE;
    }
    if ((user_access('edit own amendor_ios') && $type == 'amendor_ios' || user_access('edit own amendor_ios_task') && $type == 'amendor_ios_task') && $user->uid == $node->uid) {
      return TRUE;
    }
  }
}

/**
 * Implementation of hook_form().
 */
function amendor_ios_form(&$node) {
  if ($node->nid == NULL && $node->amendor_code == 'skippern' && $_POST['amendor_code'] != 'skippern' && !isset($node->xmlContent)) {
    amendor_ios_get_last_node_values($node);
    drupal_set_message('Data from your last task has been automatically inserted.');
  }
  //to know that the node has been submitted
  $form['amendor_code'] = array(
    '#type' => 'hidden',
    '#default_value' => 'skippern'
  );

  if ($node->type == 'amendor_ios_task') {
    // List related tasks
    $result = db_query("SELECT nid, xmlContent FROM {amendor_ios_task} WHERE xmlContent LIKE '%%%s%' AND xmlContent LIKE '%%%s%'", 'IOS', 'nid="' . $node->nid . '"');
    $form['task_relation'] = array(
      '#type' => 'fieldset',
      '#title' => t('A part of the following task sets'),
    );
    $row_counter = 0;
    while ($rowO = db_fetch_object($result)) {
      $row_counter++;
      $form['task_relation'][$row_counter] = array(
        '#value' => l(node_load(array('nid' => $rowO->nid))->title . '<br>', "node/" . $rowO->nid, array('html' => TRUE)),
      );
    }
  }

  $type = node_get_types('type', $node);
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#default_value' => $node->title
  );
  if ($type->has_body) {
    $form['body'] = array(
      '#type' => 'textarea',
      '#title' => check_plain($type->body_label),
      '#rows' => 20,
      '#required' => false,
      '#default_value' => $node->body
    );
  }
  if (isset($_POST['c'])) {
    $form['xmlContent'] = array(
      '#type' => 'hidden',
      '#required' => TRUE,
      '#default_value' => isset($_POST['c']) ? $_POST['c'] : ''
    );
  }
  else {
    $form['xmlContent'] = array(
      '#type' => 'value',
      '#value' => $node->xmlContent
    );
  }

  return $form;
}

function amendor_ios_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['#node']->amendor_ios['alter'])) {
    if ($form['#node']->type == 'image') {
      $form['buttons']['submit']['#submit'][] = 'amendor_ios_image_form_submit';

      // Value must be stored for the waiting system.
      if (isset($form['#node']->amendor_ios['value']) && is_numeric($form['#node']->amendor_ios['value'])) {
        $form['amendorVerdi'] = array(
          '#type' => 'hidden',
          '#value' => $form['#node']->amendor_ios['value']
        );
      }
    }
    elseif ($form['#node']->type == 'audio') {
      $form['buttons']['submit']['#submit'][] = 'amendor_ios_audio_form_submit';
    }
    unset($form['buttons']['preview']);
  }
}

/**
 * Handles submit for the modified image node form.
 */
function amendor_ios_image_form_submit($form, &$form_state) {
  $node = node_load($form_state['nid']);
  $_SESSION["waiting" . $form_state['values']['amendorVerdi']] = base_path() . $node->images[variable_get('amendor_ios_image_size', '_original')] . "@" . $node->nid;
  $form_state['redirect'] = 'amendor-ios/form/complete/' . $node->nid . '/image';
}

/**
 * Handles submit for the modified audio node form.
 */
function amendor_ios_audio_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'amendor-ios/form/complete/' . $form_state['nid'] . '/audio';
}

/**
 * Implementation of hook_validate().
 */
function amendor_ios_validate($node, &$form) {
  if ($node->type == 'amendor_ios_task') {
    if (strlen(strip_tags($node->body)) < 15) {
      form_set_error('body', t('The description must be atleast @num characters long.', array('@num' => 15)));
    }
    if (strlen(strip_tags($node->title)) < 4) {
      form_set_error('title', t('The title must be atleast @num characters long.', array('@num' => 4)));
    }
  }
}

/**
 * Implementation of hook_insert().
 */
function amendor_ios_insert($node) {
  switch ($node->type) {
    case 'amendor_ios':
      //updates the last node submitted by this user
      amendor_ios_update_user($node);
      $additions->names = array($node->author);
      amendor_ios_update_user($node);
      db_query("INSERT INTO {amendor_ios_task} (nid, xmlContent) VALUES (%d, '%s')", $node->nid, $node->xmlContent);
      break;

    case 'amendor_ios_task':
      $additions->names = array($node->author);
      amendor_ios_update_user($node);
      db_query("INSERT INTO {amendor_ios_task} (nid, xmlContent) VALUES (%d, '%s')", $node->nid, $node->xmlContent);
      break;
  }
  amendor_ios_update_includes($node->xmlContent, $node->nid);
}

/**
 * Implementation of hook_update().
 */
function amendor_ios_update($node) {
  switch ($node->type) {
    case 'amendor_ios':
      amendor_ios_update_user($node);
      if (isset($node->xmlContent)) {
        db_query("UPDATE {amendor_ios_task} SET xmlContent = '%s' WHERE nid = %d", $node->xmlContent, $node->nid);
      }
      break;

    case 'amendor_ios_task':
      amendor_ios_update_user($node);
      if (isset($node->xmlContent)) {
        db_query("UPDATE {amendor_ios_task} SET xmlContent = '%s' WHERE nid = %d", $node->xmlContent, $node->nid);
      }
      break;
    amendor_ios_update_includes($node->xmlContent, $node->nid);
  }
}

/**
 * Implementation of hook_delete().
 */
function amendor_ios_delete($node) {
  switch ($node->type) {
    case 'amendor_ios':
      db_query("DELETE FROM {amendor_ios_task} WHERE nid = %d", $node->nid);
      break;

    case 'amendor_ios_task':
      db_query("DELETE FROM {amendor_ios_task} WHERE nid = %d", $node->nid);
      break;
  }
  amendor_ios_update_includes('', $node->nid); //Remove the node from the includes table as well...
}

/**
 * Implementation of hook_view().
 */
function amendor_ios_view($node) {
  switch ($node->type) {
    case 'amendor_ios':
      if (user_access('access amendor_ios')) {
        $node->content['flash'] = array(
          '#value' => amendor_ios_pupil($node)
        );
      }
      break;

    case 'amendor_ios_task':
      if (user_access('access amendor_ios_task')) {
        $node->content['flash'] = array(
          '#value' => amendor_ios_pupil($node)
        );
      }
      break;
  }
  return $node;
}

/**
 * Implementation of hook_load().
 */
function amendor_ios_load($node) {
  $additions->xmlContent = amendor_ios_load_xml($node->nid);
  if ($node->type == 'amendor_ios_task' && isset($node->author)) {
    $additions->names = array($node->author);
  }
  return $additions;
}

/**
 * Load XML for a given node id.
 * 
 * @param mixed $nid Node id or array of node ids.
 * @return mixed XML content or array of nodes with XML content.
 */
function amendor_ios_load_xml($nid) {
  if (is_array($nid)) {
    for ($i = 0, $s = count($nid); $i < $s; $i++) {
      $nid[$i] = (object) array(
          'nid' => $nid[$i],
          'xml_content' => amendor_ios_load_xml($nid[$i])
      );
    }
    return $nid;
  }
  else {
    return db_result(db_query("SELECT ait.xmlContent FROM {amendor_ios_task} ait WHERE ait.nid = %d", $nid));
  }
}

/**
 * Update what nodes an ios node uses
 *
 * @param string $xml
 *  XML representing the ios node
 * @param int $nid
 *  nid for the ios node
 */
function amendor_ios_update_includes($xml, $ios_nid) {
  db_query('DELETE FROM {amendor_ios_includes} WHERE nid = %d', $ios_nid);
  $matches = array();
  preg_match_all("/nid=\"([0-9]+)\"/", $xml, $matches);
  if (isset($matches[1])) {
    $nids = $matches[1];
    foreach ($nids as $nid) {
      if ($nid != $ios_nid) {
        $results[] = update_sql("INSERT IGNORE INTO {amendor_ios_includes} (nid, media_nid) VALUES($ios_nid, $nid)");
      }
    }
  }
}

/**
 * Implementation of hook_help().
 */
function amendor_ios_help($path, $arg) {
  switch ($path) {
    case 'admin/help#amendor_ios':
      return t('This module has been produced by Amendor AS. Contact Amendor for support.');
      break;
  }
}

/**
 * Implementation of hook_menu().
 */
function amendor_ios_menu() {
  $items = array(
    'node/%node/edit/node' => array(
      'title' => 'Node',
      'access callback' => 'node_access',
      'access arguments' => array('update', 1),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => 1
    ),
    'node/%node/edit/amendor-ios' => array(
      'title' => 'Task set',
      'page callback' => 'amendor_ios_editor',
      'page arguments' => array(1),
      'access callback' => 'amendor_ios_check_type',
      'access arguments' => array('update', 1),
      'type' => MENU_LOCAL_TASK,
      'weight' => 2
    ),
    'node/%node/edit/amendor-ios-task' => array(
      'title' => 'Task',
      'page callback' => 'amendor_ios_editor',
      'page arguments' => array(1),
      'access callback' => 'amendor_ios_task_check_type',
      'access arguments' => array('update', 1),
      'type' => MENU_LOCAL_TASK,
      'weight' => 2
    ),
    'amendor-ios/flash-api' => array(
      'title' => 'Amendor IOS Flash API',
      'page callback' => 'amendor_ios_flash_api',
      'access arguments' => array('access amendor_ios'),
      'file' => 'amendor_ios.pages.inc',
      'type' => MENU_CALLBACK
    ),
    'amendor-ios/form/image/%' => array(
      'title' => 'Create Image',
      'page callback' => 'amendor_ios_show_form',
      'page arguments' => array('image', 3),
      'access arguments' => array('create images'),
      'file' => 'amendor_ios.pages.inc',
      'type' => MENU_CALLBACK
    ),
    'amendor-ios/form/audio' => array(
      'title' => 'Create Audio',
      'page callback' => 'amendor_ios_show_form',
      'page arguments' => array('audio'),
      'access arguments' => array('create audio'),
      'file' => 'amendor_ios.pages.inc',
      'type' => MENU_CALLBACK
    ),
    'amendor-ios/form/amendor-ios-task' => array(
      'title' => 'Task',
      'page callback' => 'amendor_ios_show_amendor_ios_task_form',
      'access arguments' => array('create amendor_ios'),
      'file' => 'amendor_ios.pages.inc',
      'type' => MENU_CALLBACK
    ),
    'amendor-ios/form/amendor-ios' => array(
      'title' => 'Task Set',
      'page callback' => 'amendor_ios_show_amendor_ios_form',
      'access arguments' => array('create amendor_ios'),
      'file' => 'amendor_ios.pages.inc',
      'type' => MENU_CALLBACK
    ),
    'amendor-ios/form/complete' => array(
      'title' => 'Please wait',
      'page callback' => 'amendor_ios_show_form_complete',
      'access arguments' => array('create amendor_ios'),
      'file' => 'amendor_ios.pages.inc',
      'type' => MENU_CALLBACK
    ),
    'amendor-ios/download' => array(
      'title' => 'Download Amendor IOS',
      'page callback' => 'amendor_ios_download',
      'access arguments' => array('access amendor_ios'),
      'file' => 'amendor_ios.pages.inc',
      'type' => MENU_CALLBACK
    ),
    'admin/settings/amendor_ios' => array(
      'title' => 'Amendor IOS Settings',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('amendor_ios_admin'),
      'access arguments' => array('administer site configuration'),
      'file' => 'amendor_ios.pages.inc',
      'type' => MENU_NORMAL_ITEM,
    )
  );
  foreach (amendor_ios_node_info() as $name => $info) {
    $items['node/add/' . str_replace('_', '-', $name)] = array(
      'title' => $info['name'],
      'page callback' => 'amendor_ios_add',
      'page arguments' => array(2),
      'access callback' => 'node_access',
      'access arguments' => array('create', $name),
      'description' => $info['description'],
    );
  }
  return $items;
}

/**
 * Implementation of hook_theme_registry_alter().
 */
function amendor_ios_theme_registry_alter(&$registry) {
  $registry['page']['theme paths'][] = drupal_get_path('module', 'amendor_ios') . '/templates';
}

/**
 * Checks if we got the amendor_ios node type and the right access permissions
 * 
 * @param string $action Action to perform on the node.
 * @param int $node The node object.
 * @return bool To display or not to display.
 */
function amendor_ios_check_type($action, $node) {
  if ($node->type == 'amendor_ios') {
    return node_access($action, $node);
  }
  return FALSE;
}

/**
 * Checks if we got the amendor_ios_task node type and the right access permissions
 * 
 * @param string $action Action to perform on the node.
 * @param int $node The node object.
 * @return bool To display or not to display.
 */
function amendor_ios_task_check_type($action, $node) {
  if ($node->type == 'amendor_ios_task') {
    return node_access($action, $node);
  }
  return FALSE;
}

/**
 * Determine if we should show editor or node form.
 * 
 * @param type $type
 * @return type 
 */
function amendor_ios_add($type) {
  if ($_POST['amendor_code']) {
    return node_add($type);
  }
  return amendor_ios_editor('new');
}

/**
 * Implementation of hook_theme().
 */
function amendor_ios_theme() {
  return array(
    'amendor_ios_flash' => array(
      'arguments' => array('editor' => FALSE, 'flashvars' => array(), 'width' => 720, 'height' => 1170, 'scale_ratio' => 1, 'params' => NULL)
    )
  );
}

/**
 * Render the editor for creating and altering IOS tasks and task sets.
 *
 * @param mixed $nid Node id or node object.
 * @return string HTML.
 */
function amendor_ios_editor($nid) {
  drupal_add_js(drupal_get_path('module', 'amendor_ios') . '/javascripts/amendor-ios.js');
  if (is_object($nid)) {
    $nid = $nid->nid;
  }
  $module_path = base_path() . drupal_get_path('module', 'amendor_ios');
  $flashvars = array(
    'flashCom' => url('amendor-ios/flash-api'),
    'taskSave' => url('amendor-ios/form/amendor-ios-task'),
    'setSave' => url('amendor-ios/form/amendor-ios'),
    'imageSave' => url('amendor-ios/form/image'),
    'audioSave' => url('amendor-ios/form/audio'),
    'soundSave' => url('amendor-ios/form/audio'),
    'testFlash' => $module_path . '/flash/pupil.swf',
    'helpFile' => $module_path . '/amendor-ios-help.pdf',
    'site' => variable_get('amendor_ios_site_settings', 'ndla'),
  );
  if ($nid != 'new') {
    $flashvars['nid'] = $nid;
  }

  return theme('amendor_ios_flash', TRUE, $flashvars);
}

/**
 * Theme function for displaying a amendor_ios flash.
 * 
 * @param bool $editor True if we should display the editor.
 * @param array $flashvars Variables for the flash.
 * @param int $width Width of the flash.
 * @param int $height Height of the flash.
 * @param array $params Parameters for the html object.
 * @return string HTML.
 */
function theme_amendor_ios_flash($editor = FALSE, $flashvars = array(), $width = 720, $height = 1376, $scale_ratio = 1, $params = NULL) {
  $module_path = drupal_get_path('module', 'amendor_ios');
  if (variable_get('amendor_ios_include_swfobject', '1')) {
    drupal_add_js($module_path . '/javascripts/swfobject.js');
  }
  if ($params == NULL) {
    $params = array(
      'wmode' => 'opaque',
      'allowFullScreen' => 'true'
    );
  }
  $id = 'amendor-ios-flash';
  if (isset($flashvars['nid'])) {
    $id .= '-' . $flashvars['nid'];
  }
  drupal_add_js('$(document).ready(function(){swfobject.embedSWF("' . base_path() . $module_path . '/flash/' . ($editor ? 'editor.swf' : 'pupil.swf') . '", "' . $id . '", "' . ceil(720 * $scale_ratio) . '", "' . ceil(($editor ? 1376 : 1170) * $scale_ratio) . '", "8", "expressinstall.swf", ' . drupal_to_js($flashvars) . ', ' . drupal_to_js($params) . ');});', 'inline');
  return '<div style="position:relative;max-width:' . ceil($width * $scale_ratio) . 'px;max-height:' . ceil($height * $scale_ratio) . 'px;overflow:hidden" class="amendor-ios-wrapper"><div id="' . $id . '">' . t("If you see this text you don't have the newest flashplayer.") . ' ' . l(t('Download flash'), 'http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash') . '</div></div>';
}

/**
 * Render the pupil page for taking IOS tasks and task sets.
 *
 * @param mixed $nid Node or node id.
 * @return string HTML.
 */
function amendor_ios_pupil($node, $width = 720) {
  $module_path = base_path() . drupal_get_path('module', 'amendor_ios');
  if (!is_object($node)) {
    $node = node_load($node);
  }
  $flashvars = array(
    'flashCom' => url('amendor-ios/flash-api'),
    'nid' => $node->nid,
    'lang' => $node->language,
    'site' => variable_get('amendor_ios_site_settings', 'ndla'),
  );

  $tasks = amendor_ios_get_tasks($node->xmlContent);
  if (count($tasks) != 0) {
    $height = 0;
    $tasks = amendor_ios_load_xml($tasks);
    for ($i = 0, $s = count($tasks); $i < $s; $i++) {
      $tasks[$i]->size = amendor_ios_get_size($tasks[$i]->xml_content);
      if ($tasks[$i]->size->height > $height) {
        $height = $tasks[$i]->size->height;
      }
    }
  }
  else {
    $size = amendor_ios_get_size($node->xmlContent);
    $height = $size->height;
  }
  $height = ($height == 0 || $height > 1100 ? 1100 : $height) + 136;
  if ($height < 710) {
    // Make sure we have enough room for image copyright window.
    $height = 710;
  }

  return theme('amendor_ios_flash', FALSE, $flashvars, $width, $height, ($width / 720));
}

/**
 * Find all tasks for a given XML string.
 * 
 * @param string $xml
 * @return array
 */
function amendor_ios_get_tasks($xml) {
  try {
    $xml = new SimpleXMLElement($xml);
  }
  catch (Exception $e) {
    return array();
  }

  for ($i = 0, $s = count($xml->Task); $i < $s; $i++) {
    $tasks[] = (int) $xml->Task[$i]['nid'];
  }

  return isset($tasks) ? $tasks : array();
}

/**
 * Find max size for a task from a given XML string.
 * 
 * @param string $xml
 * @return array
 */
function amendor_ios_get_size($xml) {
  $size = new stdClass();

  try {
    $xml = new SimpleXMLElement($xml);
    $size->width = ceil((int) $xml->design->bg['taskWidth']);
    $size->height = ceil((string) $xml->design->bg['taskHeight']);
  }
  catch (Exception $e) {
    $size->width = 720;
    $size->height = 1100;
  }

  return $size;
}

/**
 * stores the last amendor_ios or amendor_ios_task node the user has saved.
 */
function amendor_ios_update_user($node) {
  $num_rows = db_result(db_query("SELECT COUNT(*) FROM {amendor_ios_user} WHERE uid = %d", $node->uid));
  if ($num_rows < 1)
    db_query("INSERT INTO {amendor_ios_user} (uid,nid) VALUES (%d,%d)", $node->uid, $node->nid);
  else
    db_query("UPDATE {amendor_ios_user} SET nid = %d WHERE uid = %d", $node->nid, $node->uid);
}

/**
 * returns the nid for the last node the user has saved.
 */
function amendor_ios_get_last_nid() {
  global $user;
  $res = db_query("SELECT nid FROM amendor_ios_user WHERE uid = %d", $user->uid);
  $num_rows = db_result(db_query("SELECT COUNT(*) FROM amendor_ios_user WHERE uid = %d", $user->uid));
  if ($num_rows < 1)
    return false;
  else
    return db_fetch_object($res)->nid;
}

/**
 * autotagging the node with the last tags used by the user...
 *
 */
function amendor_ios_get_last_node_values(&$node) {
  if ($lastNid = amendor_ios_get_last_nid()) {
    if ($lastNode = node_load($lastNid)) {
      $tempNode = new StdClass();
      $tempNode->nid = $lastNid;
      $tempNode->vid = $lastNode->vid;
      if ($extra = node_invoke_nodeapi($tempNode, 'load')) {
        foreach ($extra as $key => $value) {
          $node->$key = $value;
        }
      }
    }
  }
}

/**
 * Makes a form for saving tasks
 *
 * @param $node
 * @param $xmlContent - the xml representing the task
 * @param $value - int used by the waiting system.
 * @return a form
 */
function amendor_ios_amendor_ios_task_form(&$form_state, &$node, $xmlContent, $value) {
  module_load_include("inc", "node", "node.pages");
  $toReturn = node_form($form_state, $node);
  foreach (module_implements('form_alter') as $module) {
    $function = $module . '_form_alter';
    $function($toReturn, $form_state, 'amendor_ios_task_node_form');
  }
  if (isset($toReturn['buttons']['submit']))
    $toReturn['buttons']['submit']['#submit'] = array('amendor_ios_amendor_ios_task_form_submit');
  if (isset($toReturn['buttons']['save']))
    $toReturn['buttons']['save']['#submit'] = array('amendor_ios_amendor_ios_task_form_submit');
  unset($toReturn['buttons']['submit']['#validate']);
  $toReturn['#validate'][] = 'amendor_ios_node_form_validate';
  unset($toReturn['buttons']['preview']);
  unset($toReturn['buttons']['delete']);
  if (isset($xmlContent)) {
    $toReturn['xmlContent'] = array(
      '#type' => 'hidden',
      '#value' => $xmlContent
    );
    //Condition: Form is beeing submitted
  }
  else {
    $toReturn['xmlContent'] = array(
      '#type' => 'hidden'
    );
  }
  //used by the waiting system
  if (isset($value) && is_numeric($value)) {
    $toReturn['amendorVerdi'] = array(
      '#type' => 'hidden',
      '#value' => $value
    );
  }
  return $toReturn;
}

/**
 * returns a form for saving sets of tasks.
 */
function amendor_ios_amendor_ios_form(&$form_state, &$node, $xmlContent, $value) {
  module_load_include("inc", "node", "node.pages");
  $toReturn = node_form($form_state, $node);
  foreach (module_implements('form_alter') as $module) {
    $function = $module . '_form_alter';
    $function($toReturn, $form_state, 'amendor_ios_node_form');
  }
  if (isset($toReturn['buttons']['submit']))
    $toReturn['buttons']['submit']['#submit'] = array('amendor_ios_amendor_ios_form_submit');
  if (isset($toReturn['buttons']['save']))
    $toReturn['buttons']['save']['#submit'] = array('amendor_ios_amendor_ios_form_submit');
  unset($toReturn['buttons']['preview']);
  unset($toReturn['buttons']['delete']);
  if (isset($xmlContent)) {
    $toReturn['xmlContent'] = array(
      '#type' => 'hidden',
      '#value' => $xmlContent
    );
    //Condition: form is beeing submitted
  }
  else {
    $toReturn['xmlContent'] = array(
      '#type' => 'hidden'
    );
  }
  if (isset($value) && is_numeric($value)) {
    $toReturn['amendorVerdi'] = array(
      '#type' => 'hidden',
      '#value' => $value
    );
  }

  return $toReturn;
}

/**
 * handles the amendor_ios_taskform
 *
 * @return a page
 */
function amendor_ios_amendor_ios_task_form_submit($form, &$form_state) {
  module_load_include("inc", "node", "node.pages");
  node_form_submit($form, $form_state);
  //Finds the nodes nid.
  if (is_numeric($form_state['values']['amendorVerdi']) && is_numeric($form_state['nid'])) {
    $_SESSION["waiting" . $form_state['values']['amendorVerdi']] = $form_state['nid'];
  }
  $form_state['redirect'] = 'amendor-ios/form/complete/' . $temp[1] . '/task';
  return 'amendor-ios/form/complete/' . $temp[1] . '/task';
}