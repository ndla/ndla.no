<?php

/**
 * @file
 *  amendor_electure_framework.module php file
 *  Drupal module amendor_electure_framework.
 */

/**
 * Implementation of hook_info().
 */
function amendor_electure_framework_node_info() {
  return array(
    'amendor_electure_framework' => array(
      'name' => t('eLecture Framework'),
      'module' => 'amendor_electure_framework',
      'description' => t('Makes it possible to add electures to frameworks.'),
    )
  );
}

/**
 * Implementation of hook_perm().
 */
function amendor_electure_framework_perm() {
  return array('create amendor_electure_framework', 'access amendor_electure_framework');
}

/**
 * Implementation of hook_access().
 */
function amendor_electure_framework_access($op, $node, $account) {
  if (!user_access('access amendor_electure_framework', $account)) {
    return FALSE;
  }
  if ($op == 'create' || $op == 'update' || $op == 'delete') {
    return user_access('create amendor_electure_framework', $account);
  }
}

/**
 * Implementation of hook_view().
 */
function amendor_electure_framework_view($node) {
  $node->content['flash'] = array(
    '#value' => t('Click <a href="!url">here</a> to open the electure framework.', array('!url' => url('amendor-electure/fullscreen/' . $node->nid)))
  );
  return $node;
}

/**
 * Implementation of hook_menu().
 */
function amendor_electure_framework_menu() {
  $items = array();
  //This "page" handles the communication between flash and drupal.
  $items['amendor_electure_flash'] = array(
    'title' => 'eForelesninger',
    'page callback' => 'amendor_electure_framework_show_flash_com',
    'access arguments' => array('access amendor_electure_framework'),
    'type' => MENU_CALLBACK
  );
  $items['admin/settings/amendor_electure_framework'] = array(
    'title' => 'Amendor eLecture - settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('amendor_electure_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Admin settings form.
 */
function amendor_electure_admin_settings() {
  $form = array();
  // We add a setting for this to fit use-cases where the authors and the normal users use different
  // urls.
  $form['amendor_electure_url_start'] = array(
    '#type' => 'textfield',
    '#title' => t('Host url without trailing slash'),
    '#description' => t('For instance http://ndla.no. Do not use a trailing slash.'),
    '#default_value' => variable_get('amendor_electure_url_start', ''),
  );
  return system_settings_form($form);
}

/**
 * Implementation of hook_form().
 */
function amendor_electure_framework_form(&$node) {
  //beenCopied is true if this node is created by translating another node
  $beenCopied = false;
  //If the node is a translation:
  if (($node->tnid != 0) && ($node->nid != $node->tnid)) {
    $result = db_query("SELECT xmlContent FROM {amendor_electure_topic} WHERE nid = %d", $node->tnid);
    $originalFrameworkXML = db_result($result, 0);
    //If the node has copied the xml of the original node we need to reset the xml:
    if ($node->xmlContent == $originalFrameworkXML) {
      $result = db_query("UPDATE {amendor_electure_topic} SET xmlContent = '<menu><topic name=\"Tema navn\" /></menu>' WHERE nid = %d", "", $node->nid);
      $beenCopied = true;
    }
  }
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  //We use javascript to handle comunication between flash elements and other elements of the form.
  $form['helper']['#theme'] = 'amendor_electure_form_js';

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Tittel'),
    '#required' => TRUE,
    '#description' => t('Skriv inn tittelen til rammeverket.'),
    '#default_value' => $node->title,
    '#id' => 'titleTxt',
    '#attributes' => array('onkeyup' => 'updateTitle(document.getElementById("titleTxt").value)',
      'onmouseout' => 'updateTitle(document.getElementById("titleTxt").value)'),
  );

  //path_dir is needed because of the use of clean urls.
  $start_path = base_path();
  $path_dir = $start_path . drupal_get_path('module', 'amendor_electure_framework');

  /*
   * Making javascripts used to embed the menu and feedback controller in the form:
   */
  $menu_flashvars = $feedback_flashvars = 'flashComPage=' . base_path() . 'index.php?q=amendor_electure_flash';

  if (is_numeric($node->nid) && !$beenCopied) {
    $menu_flashvars = $feedback_flashvars = $menu_flashvars . '&amp;nid=' . $node->nid;
  }

  $menu_flashvars .= '&amp;mode=0';

  $embed_menu = '<object height="400" width="300" id="flashcontent" type="application/x-shockwave-flash" data="' . $path_dir . '/flash/index.swf">
                   <param name="movie" value="' . $path_dir . '/flash/index.swf"/>
                   <param name="wmode" value="transparent"/>
                   <param name="menu" value="false"/>
                   <param name="quality" value="high"/>
                   <param name="allowFullScreen" value="true"/>
                   <param name="FlashVars" value="' . $menu_flashvars . '"/>
                   <embed height="400" width="300" flashvars="' . $menu_flashvars . '" src="' . $path_dir . '/flash/index.swf"></embed>
                 </object>';

  $embed_feedback = '<object height="400" width="300" id="flashcontent2" type="application/x-shockwave-flash" data="' . $path_dir . '/flash/feedback2.swf">
                       <param name="movie" value="' . $path_dir . '/flash/feedback2.swf" />
                       <param name="wmode" value="transparent" />
                       <param name="menu" value="false" />
                       <param name="quality" value="high" />
                       <param name="allowFullScreen" value="true" />
                       <param name="FlashVars" value="' . $feedback_flashvars . '" />
                       <embed height="400" width="300" flashvars="' . $feedback_flashvars . '" src="' . $path_dir . '/flash/feedback2.swf"></embed>
                     </object>';
  /*
   * Adding menu and feedbackController to the form:
   */
  $form['topic_menu'] = array(
    '#value' => $embed_menu,
  );
  $form['feedback'] = array(
    '#value' => $embed_feedback,
  );

  /*
   * Adding hidden fields for the menu and feedback controller to update(using javascript):
   */
  $form['feedback_hidden'] = array(
    '#type' => 'hidden',
    '#title' => 'feedback_makeHIDDEN',
    '#id' => 'fb',
  );

  $form['feedback_hidden_rep'] = array(
    '#type' => 'hidden',
    '#title' => 'feedback_makeHIDDEN_rep',
    '#id' => 'fb_rep',
  );
  $form['xmlContent'] = array(
    '#type' => 'hidden',
    '#title' => t('XML'),
    '#id' => 'xmlText',
  );
  //Making flash elements update all hidden fields:
  $form['js'] = array(
    '#value' => "<script type=\"text/javascript\">
 	updateFromFlash();
 	</script>",
  );
  return $form;
}

/**
 * Implementation of hook_insert().
 */
function amendor_electure_framework_insert($node) {
  db_query("INSERT INTO {amendor_electure_topic} (nid, title, xmlContent, mails, rep) VALUES (%d, '%s', '%s', '%s', %d)", $node->nid, $node->title, $node->xmlContent, $node->feedback_hidden, $node->feedback_hidden_rep);
}

/**
 * Implementation of hook_update().
 */
function amendor_electure_framework_update($node) {
  if (isset($node->xmlContent)) {
    $theXML = amendor_electure_makeDOM($node, true);
    db_query("UPDATE {amendor_electure_topic} SET title = '%s', xmlContent = '%s' WHERE nid = %d", $node->title, $theXML, $node->nid);
  }
  db_query("UPDATE {amendor_electure_topic} SET mails = '%s', rep = %d WHERE nid = %d", $node->feedback_hidden, $node->feedback_hidden_rep, $node->nid);
}

/**
 * Implementation of hook_load().
 */
function amendor_electure_framework_load($node) {
  $additions = db_fetch_object(db_query("SELECT xmlContent FROM {amendor_electure_topic} WHERE nid = %d", $node->nid));
  return $additions;
}

/**
 * Implementation of hook_delete().
 */
function amendor_electure_framework_delete($node) {
  db_query("UPDATE {amendor_electure_sequence} SET sid = %d WHERE sid = %d", 0, $node->nid);
  db_query("DELETE FROM {amendor_electure_topic} WHERE nid=%d", $node->nid);
}

/**
 * Implementation of hook_validate().
 */
function amendor_electure_framework_validate(&$node) {
  $doc = new DOMDocument();
  set_error_handler('_amendor_electure_xml_error_handler');
  //Validating the menu(xml)
  try {
    $doc->loadXML($node->xmlContent);
    $topics = $doc->getElementsByTagName('topic');
    if ($topics->length < 1) {
      form_set_error('', t('The eLecture menu you are trying to save is empty. This is probably due to a missing flash file. Contact your site administrator if this problem continues.'));
    }
  }
  catch (Exception $e) {
    form_set_error('', t('Det oppstod en feil. Vennligst prøv igjen. Kontakt Amendor om problemet fortsetter. Feilkode er 2820.'));
  }
  restore_error_handler();

  $mails = $node->feedback_hidden;
  $mailArray = split(",", $mails);

  //check if the mails are ok
  for ($i = 0; $i < sizeOf($mailArray); $i++) {
    $regex = "^[_+a-z0-9-]+(\.[_+a-z0-9-]+)*"
      . "@[a-z0-9-]+(\.[a-z0-9-]{1,})*"
      . "\.([a-z]{2,}){1}$";
    if (!eregi($regex, $mailArray[$i]))
      form_set_error('notValideMail', t("Denne ePost adressen er ugyldig: %mailArray[@i]", array('%mailArray[@i]' => $mailArray[$i])));
  }

  //if the feedback interval hasn't been entered:
  if (!is_numeric($node->feedback_hidden_rep))
    form_set_error('noMailIntervall', t('Det må skrives inn hvor ofte man ønsker å motta påminnelse om ubesvarte spørsmål i rammeverket.'));
}

/**
 * Implementation of hook_help().
 */
function amendor_electure_framework_help($path, $arg) {
  switch ($path) {
    case 'admin/help#amendor_electure_framework':
      return t('Denne modulen er laget av Amendor AS. Kontakt Amendor for support.');
      break;
  }
}

/**
 * Implementation of hook_cron().
 */
function amendor_electure_framework_cron() {
  // Check if there are any unanswered questions in the framework, and resends out a reminder to the answering people.
  global $account;
  $rows = db_result(db_query("SELECT COUNT(*) FROM {amendor_electure_topic}"));
  //If we have any frameworks:
  if ($rows > 0) {
    $res = db_query("SELECT nid, mails, rep, timestamp, title FROM {amendor_electure_topic}");
    for ($i = 0; $i < $rows; $i++) {
      $resO = db_fetch_object($res);
      $framework = $resO->title;
      $nodeId = $resO->nid;
      $mails = $resO->mails;
      //rep is how often reminders shall be sent:
      $rep = $resO->rep;
      $timestamp = $resO->timestamp;
      $sequence = time(); // - ($rep * 24 * 60 * 60);
      //If it is time for a new reminder for this framework:
      if (strtotime($timestamp) < $sequence) {
        $questRows = db_result(db_query("SELECT COUNT(*) FROM {amendor_electure_questions} WHERE nid=%d and published=%d", $nodeId, 0));
        //If we have unansweared questions:
        if ($questRows > 0) {
          $keyCode = "electureFbQuestion";
          $to = $mails;
          $subject = t('Påminnelse: Ubesvarte spørsmål');
          //TODO: Add admin interface to change this...
          $from = t('NDLA<do-not-reply@ndla.no>');
          $theContent = "Hei, \n\n";
          $theContent .= "Det er nå @quest ubesvarte spørsmål i rammeverket @framework. Lenke: \n";
          $theContent .= "http://!link!page/@nodeId. \n";
          $theContent .= "Du må være pålogget for å besvare spørsmålene. Spørsmålene finner og besvarer du ved å gjøre følgende:\n";
          $theContent .= "1. Logg deg inn på ndla.no(Hvis du ikke allerede er pålogget)\n";
          $theContent .= "2. Klikk på lenken over\n";
          $theContent .= "3. Når eForelesningene kommer opp holder du musen over spørsmålsadministrasjon i admin linjen øverst.\n";
          $theContent .= "4. Klikk på ubesvarte spørsmål.\n";
          $theContent .= "5. Velg et spørsmål å besvare fra listen over ubesvarte spørsmål."; //TODO: REMOVE HARDCODE UNDER:
          $content = t($theContent, array('@quest' => $questRows, '@framework' => $framework, '!link' => 'ndla.no/index.php?q=', '!page' => 'eForelesningFullskjerm', '@nodeId' => $nodeId));

          $language = user_preferred_language($account);
          $object = array(); // Replace this as needed
          $params['subject'] = $subject;
          $params['body'] = $content;
          $params['headers'] = array($from);

          if (drupal_mail('amendor_electure_framework', $keyCode, $to, $language, $params, $from)) {
            db_query("UPDATE {amendor_electure_topic} SET timestamp = CURRENT_TIMESTAMP WHERE nid = %d", $nodeId);
          }
        }
      }
    }
  }
}

function amendor_electure_framework_mail($key, &$message, $params) {
  $message['subject'] = $params['subject'];
  $message['body'][] = $params['body'];
  $message['headers'] = array_merge($message['headers'], $params['headers']);
}

/**
 * Implementation of hook_alter().
 */
function amendor_electure_framework_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == "amendor_electure_framework_node_form") {
    //Removing the preview button:
    unset($form['buttons']['preview']);
  }
}

/**
 *  checkStatus is a function that check the status for each node in the menu(xml)
 *
 * @param xml $xmlContent
 * @return the
 */
function amendor_electure_checkStatus($xmlContent) {
  $doc = new DOMDocument('1.0', 'utf-8');
  //validating xml:
  try {
    $doc->loadXML(urldecode($xmlContent));
  }
  catch (Exception $e) {
    watchdog("eLecture", "eForelesning systemet mottok en ugyldig XML. Funksjon: amendor_electure_checkStatus");
    form_set_error('', t('Ugyldig xml'));
  }
  //updating status for all eLectures in the menu(xml)
  foreach ($doc->getElementsByTagName('eLecture') as $electure) {
    $eNid = $electure->getAttribute('nid');
    $eStatus = $electure->getAttribute('status');
    $result = db_query("SELECT status from {node} WHERE nid = %d", $eNid);
    $savedStatus = db_fetch_object($result)->status;
    $electure->setAttribute("status", $savedStatus);
  }
  $toReturn = $doc->saveXML();
  //Removing xml header:
  $xmlTemp = spliti(">\n", $toReturn, 2);

  $toReturn = urlencode($xmlTemp[1]);
  return $toReturn;
}

/**
 * Implementation of the method amendor_electure_framework_remove_el
 * Removes an eLecture from a framework(i.e. if the eLecture has been added to another framework.)
 *
 * @param $fNid is a framework nid
 * @param $eLNid is a electure nid(the eLecture to be removed)
 */
function amendor_electure_framework_remove_el($fNid, $eLNid) {
  set_error_handler('_amendor_electure_xml_error_handler');
  $doc = new DOMDocument('1.0', 'utf-8');
  $node = node_load(array('nid' => $fNid));
  try {
    $doc->loadXML($node->xmlContent);
  }
  catch (Exception $e) {
    form_set_error('', t('Ugyldig xml'));
    watchdog("eLecture", "eForelesning systemet mottok en ugyldig XML. Funksjon: amendor_electure_framework_remove_el");
  }
  foreach ($doc->getElementsByTagName('topic') as $topic) {
    foreach ($topic->getElementsByTagName('eLecture') as $eLecture) {
      $foundNid = $eLecture->getAttribute('nid');
      if ($foundNid == $eLNid) {
        $topic->removeChild($eLecture);
      }
    }
  }
  $node->xmlContent = $doc->saveXML();
  node_save($node);
  restore_error_handler();
}

/**
 * Implementation of the method amendor_electure_framework_parseQuestion
 * Makes xml out of a query result...
 *
 * @param $result is the result of an sql query
 * @param $title is the title of the question
 */
function amendor_electure_framework_parseQuestion($result, $title) {
  $doc = new DOMDocument('1.0', 'utf-8');
  //$number = db_num_rows($result);
  $assessment = $doc->createElement($title);
  while ($element = db_fetch_object($result)) {
    $assessment = $doc->createElement('Question');

    $assessment->setAttributeNode(new DOMAttr('id', $element->id));
    $assessment->setAttributeNode(new DOMAttr('nid', $element->nid));
    $assessment->setAttributeNode(new DOMAttr('sid', $element->sid));
    $assessment->setAttributeNode(new DOMAttr('qTitle', $element->qTitle));
    $assessment->setAttributeNode(new DOMAttr('question', $element->question));
    $assessment->setAttributeNode(new DOMAttr('frame', $element->frame));
    $assessment->setAttributeNode(new DOMAttr('startFrame', $element->startFrame));
    $assessment->setAttributeNode(new DOMAttr('endFrame', $element->endFrame));
    $assessment->setAttributeNode(new DOMAttr('answer', $element->answer));
    $assessment->setAttributeNode(new DOMAttr('mail', $element->email));
    $assessment->setAttributeNode(new DOMAttr('published', $element->published));
    if ($element->published == 0) {
      $assessment->setAttributeNode(new DOMAttr('sentMail', true));
    }
    else {
      $assessment->setAttributeNode(new DOMAttr('sentMail', false));
    }
    $assessment->setAttributeNode(new DOMAttr('important', $element->important));
    $doc->appendChild($assessment);
  }
  return urlencode($doc->saveXML());
}

/**
 * This method handle all communication with index.swf
 */
function amendor_electure_framework_show_flash_com() {
  global $account;
  switch ($_POST['q']) {
    //case: loads the topic menu for an existing framework
    case "loadMenu":
      if (user_access('access amendor_electure_framework')) {
        if (is_numeric($_POST['nid'])) {
          $res = db_query("SELECT xmlContent, title FROM {amendor_electure_topic} WHERE nid = %d", $_POST['nid']);
          $resO = db_fetch_object($res);

          header('Content-type: application/x-www-urlform-encoded');
          if ($resO != NULL) {
            $xmlContent = $resO->xmlContent;
            $title = $resO->title;
            //check publish status of all eLectures in the menu:
            $xmlContent = amendor_electure_checkStatus($xmlContent);
          }
          else {
            $xmlContent = "unknown";
          }
          echo "&q=loadMenu&xmlContent=" . $xmlContent . "&frameworkName=" . $title;
          //if the user is an electure administrator:
          if (user_access('create amendor_electure_framework')) {
            //This will make flash show the admin menu:
            //It is easy to hack this, but the user won't be able to do anything with the adminmenu
            //without having admin rights in drupal.
            echo "&adm=1";
          }
        }
      }
      break;
    //loadSubtitles loads the subtitles for the given sequence
    case "loadSubtitles":
      if (user_access('access amendor_electure_framework')) {
        if (is_numeric($_POST['nid'])) {
          $res = db_query("SELECT subtitles FROM {amendor_electure_sequence} WHERE nid = %d", $_POST['nid']);
          $res0 = db_fetch_object($res);
          header('Content-type: application/x-www-urlform-encoded');
          if ($res0 != NULL) {
            $subtitles = $res0->subtitles;
          }
          else {
            $subtitles = "null";
          }
          echo "&q=loadSubtitles&subtitles=" . $subtitles . "&nid=" . $_POST['nid'];
        }
      }
      break;
    /* adds a new question to the sequence.
      Check if the user are sending a sequence of questions during a limited time. */
    case "newQuestion":
      if (user_access('access amendor_electure_framework')) {
        $res = db_query("SELECT sid FROM {amendor_electure_sequence} WHERE nid = %d", $_POST['nid']);
        $resO = db_fetch_object($res);
        header('Content-type: application/x-www-urlform-encoded');
        $runScript = false;
        //How often a user is allowed to send questions:
        $minutes = 2;
        if ($resO != NULL) {
          if (isset($_SESSION['numberQ'])) {
            $time = time();
            $diff = 0;
            for ($i = 0; $i < $_SESSION['numberQ']; $i++) {
              $diff += $time - $_SESSION['newQ'][$i];
            }
            if ($diff / ($_SESSION['numberQ']) < 60 * $minutes) {
              echo "&q=newQuestion&code=" . $minutes;
            }
            else {
              $_SESSION['newQ'][$_SESSION['numberQ']] = time();
              $_SESSION['numberQ'] = $_SESSION['numberQ'] + 1;
              echo "&q=newQuestion&code=OK";
              $runScript = true;
            }
          }
          else {
            $_SESSION['numberQ'] = 0;
            $_SESSION['newQ'][$_SESSION['numberQ']] = time();
            $_SESSION['numberQ'] = $_SESSION['numberQ'] + 1;
            echo "&q=newQuestion&code=OK";
            $runScript = true;
          }
        }
        //the question has been allowed:
        if ($runScript) {
          db_query("INSERT INTO {amendor_electure_questions} (nid, sid, question, email, frame, startFrame, endFrame, published) VALUES (%d, %d, '%s', '%s', %d,%d,%d, %d)", $resO->sid, $_POST['nid'], urldecode($_POST['question']), $_POST['eMail'], $_POST['frame'], $_POST['bF'], $_POST['eF'], 0);
        }
        amendor_electure_framework_cron();
      }
      break;
    //load the relevant questions for an electure
    case "loadELQ":
      if (user_access('access amendor_electure_framework')) {
        $res = db_query("SELECT sid FROM {amendor_electure_sequence} WHERE nid = %d", $_POST['sequence']);
        $resO = db_fetch_object($res);
        if ($resO != NULL) {
          $result = db_query("SELECT nid, startFrame, endFrame, qTitle, question, answer, important FROM {amendor_electure_questions} WHERE nid = %d and sid = %d and published= %d", $resO->sid, $_POST['sequence'], 1);
          header('Content-type: application/x-www-urlform-encoded');
          echo "&q=loadELQ&nid=" . $_POST['sequence'] . "&questions=";
          $doc = new DOMDocument('1.0', 'utf-8');
          $assessment = $doc->createElement('questions');
          while ($element = db_fetch_object($result)) {
            $assessment = $doc->createElement('qHolder');
            $assessment->setAttributeNode(new DOMAttr('nid', $element->nid));
            $assessment->setAttributeNode(new DOMAttr('h', $element->qTitle));
            $assessment->setAttributeNode(new DOMAttr('bf', $element->startFrame));
            $assessment->setAttributeNode(new DOMAttr('ef', $element->endFrame));
            $assessment->setAttributeNode(new DOMAttr('important', $element->important));
            $assessment->setAttributeNode(new DOMAttr('quest', $element->question));
            $assessment->setAttributeNode(new DOMAttr('answ', $element->answer));
            $doc->appendChild($assessment);
          }
          echo urlencode($doc->saveXML());
        }
      }
      break;
    //gets all the unanswered questions for a given framework
    case "getUnansweredQuestions":
      if (user_access('create amendor_electure_framework')) {
        $result = db_query("SELECT id, nid, sid, qTitle, frame, startFrame, endFrame, question, answer, email, published, important FROM {amendor_electure_questions} WHERE nid = %d and published =%d", $_POST['nid'], 0);
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=getUnansweredQuestions&xmlContent=";
        echo amendor_electure_framework_parseQuestion($result, "eLectureQuestions");
      }
      break;
    //case: gets alle the published questions in the given framework
    case "getPublishedQuestions":
      if (user_access('create amendor_electure_framework')) {
        $result = db_query("SELECT id, nid, sid, qTitle, frame, startFrame, endFrame, question, answer, email, published, important FROM {amendor_electure_questions} WHERE nid = %d and published= %d", $_POST['nid'], 1);
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=getPublishedQuestions&xmlContent=";
        echo amendor_electure_framework_parseQuestion($result, "eLectureQuestions");
      }
      break;
    //gets all the relevant questions for the framework, both answered and unanswered
    case "getQuestions":
      if (user_access('create amendor_electure_framework')) {
        $result = db_query("SELECT id, nid, sid, qTitle, frame, startFrame, endFrame, question, answer, email, published, important FROM {amendor_electure_questions} WHERE nid = %d", $_POST['nid']);
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=getQuestions&xmlContent=";
        echo amendor_electure_framework_parseQuestion($result, "eLectureQuestions");
      }
      break;
    //gets all the saved questions in the framework
    case "getSavedQuestions":
      if (user_access('create amendor_electure_framework')) {
        $result = db_query("SELECT id, nid, sid, qTitle, frame, startFrame, endFrame, question, answer, email, published, important FROM {amendor_electure_questions} WHERE nid = %d and published=%d", $_POST['nid'], 2);
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=getSavedQuestions&xmlContent=";
        echo amendor_electure_framework_parseQuestion($result, "eLectureQuestions");
      }
      break;
    //case: updates a question and answer for a given framework
    case "updateQuestion":
      if (user_access('create amendor_electure_framework')) {
        if ($_POST['published'] == 3) {
          db_query("DELETE FROM {amendor_electure_questions} WHERE id = %d", $_POST['id']);
        }
        else {
          db_query("UPDATE {amendor_electure_questions} SET nid = %d, sid = %d, qTitle = '%s', question = '%s', answer = '%s', frame = %d, startFrame = %d, endFrame = %d, email = '%s', published = %d, important = %d WHERE id = %d", $_POST['nid'], $_POST['sid'], urldecode($_POST['qTitle']), urldecode($_POST['question']), urldecode($_POST['answer']), $_POST['frame'], $_POST['startFrame'], $_POST['endFrame'], $_POST['mail'], $_POST['published'], $_POST['important'], $_POST['id']);
        }
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=success";
      }
      break;
    //case: send a reply to the person that asked the question
    case "sendReplyToQuestionHolder":
      if (user_access('create amendor_electure_framework')) {
        header('Content-type: application/x-www-urlform-encoded');
        $keyCode = "electureQuestion";
        $to = $_POST['mail'];
        $subject = urldecode($_POST['qTitle']);
        // TODO: Settings page...
        $from = t('NDLA<do-not-reply@ndla.no>');
        $content = t("Hei, \n\nVi har mottatt følgende spørsmål fra deg:\n*\n@quest \n*\nDu var her når du stilte spørsmålet: \n*\nhttp://!link!page/@id/@sid/@sf/@ef  \n*\nSVAR PÅ SPØ˜RSMÅLET: \n*\n@answer \n*\nHåper svaret kan være til hjelp. \n*\nHa en trivelig dag! \n*\nMed vennlig hilsen \nNDLA", array('@quest' => strip_tags(urldecode($_POST['question'])), '!link' => $_SERVER['HTTP_HOST'] . base_path() . "index.php?q=", '!page' => 'eForelesningFullskjerm', '@id' => $_POST['nid'], '@sid' => $_POST['sid'], '@sf' => $_POST['startFrame'], '@ef' => $_POST['endFrame'], '@answer' => strip_tags(urldecode($_POST['answer']))));
        $mailOk = false;
        $regex = "^[_+a-z0-9-]+(\.[_+a-z0-9-]+)*"
          . "@[a-z0-9-]+(\.[a-z0-9-]{1,})*"
          . "\.([a-z]{2,}){1}$";
        if (eregi($regex, $to)) {
          //old: if(drupal_mail($keyCode, $to, $subject, $content, $from, $headers = array($from)))
          $language = user_preferred_language($account);
          $params['subject'] = $subject;
          $params['body'] = $content;
          $params['headers'] = array($from);
          $params = array('subject' => $subject, 'body' => $content, 'headers' => array($from));
          if (drupal_mail('amendor_electure_framework', $keyCode, $to, $language, $params, $from)) {
            echo "&q=success";
            $mailOk = true;
          }
        }
        if (!$mailOk) {
          db_query("UPDATE {amendor_electure_questions} SET published = %d WHERE id = %d", 0, $_POST['id']);
        }
      }
      break;
    //case: gets the mails and the intervall for how often a reminder are sent with information about unanswered questions
    case "loadFeedbackSettings":
      if (user_access('create amendor_electure_framework')) {
        if (is_numeric($_POST['nid'])) {
          $res = db_query("SELECT mails,rep FROM {amendor_electure_topic} WHERE nid = %d", $_POST['nid']);
          $row0 = db_fetch_object($res);
          header('Content-type: application/x-www-urlform-encoded');
          if ($row0 != NULL) {
            $mails = $row0->mails;
            $days = $row0->rep;
          }
          else {
            $mails = "unknown";
          }
          echo "&q=loadSettings&mails=" . $mails . "&rep=" . $days;
        }
      }
      break;
    //search for a given sequence of characters in the framework
    //searches in title, subtitles and tags(ctegories)
    case "searchELecture":
      if (user_access('access amendor_electure_framework')) {
        $searchWords = spliti(" ", urldecode($_POST['searchWords']), 4);
        $likes = "";
        for ($i = 0; $i < count($searchWords); $i++) {
          $likes .= " AND CONCAT_WS( ' ', n.title, elt.subtitles, t.name, t.description ) LIKE '%%%s%' ";
        }

        //Condition: there are only entered one letter to search for
        if (count($searchWords) == 1) {
          if (strlen($searchWords[0]) < 2) {
            $likes = "";
          }
        }
        $result = db_query("SELECT DISTINCT n.nid AS nodeid
		                    FROM {node} n
		                    LEFT JOIN {amendor_electure_sequence} elt ON n.nid = elt.nid
		                    LEFT JOIN {term_node} tn ON n.nid = tn.nid
		                    LEFT JOIN {term_data} t ON tn.tid = t.tid
		                    WHERE n.type = 'amendor_electure'" . $likes . " 
		                    ORDER BY n.nid DESC ", $searchWords[0], $searchWords[1], $searchWords[2], $searchWords[3], $searchWords[4]);
        $output = array();
        header('Content-type: application/x-www-urlform-encoded');
        echo "&q=searchELecture&electures=";
        $doc = new DOMDocument('1.0', 'utf-8');
        while ($rowO = db_fetch_object($result)) {
          $temp = node_load(array('nid' => $rowO->nodeid));
          if ($temp->framework == $_POST['framework']) {
            $electure = $doc->createElement('el');
            $electure->setAttributeNode(new DOMAttr('nid', $temp->nid));
            $doc->appendChild($electure);
          }
        }
      }
      echo $doc->saveHTML();
      break;
    //Only used for the medisin project. Not in use for NDLA...
    case "logActivity":
      $increment = 0;
      if (is_numeric($_POST['increment'])) {
        if ($_POST['increment'] > 0 && $_POST['increment'] < 1000) {
          $increment = $_POST['increment'];
        }
      }
      $rows = db_query("SELECT COUNT(*) AS count FROM student_activity WHERE short='%s'", $_POST['short']);
      $q = "";
      if (db_fetch_object($rows)->count > 0) {
        $q = "UPDATE student_activity SET logcount=logcount+%d WHERE short='%s'";
      }
      else {
        $q = "INSERT INTO student_activity (logcount,short) VALUES (%d,'%s')";
      }
      db_query($q, $increment, $_POST['short']);
      header('Content-type: application/x-www-urlform-encoded');
      echo "&Jepp=japp";
      break;
  }
  exit();
}