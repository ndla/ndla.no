<div id="amendor-electure-<?php print $nid ?>"><?php print t('You must enable javascript in your browser to view this electure.') ?></div>
<script>
  $(document).ready(function (){
    var flashVars = {
      playerSwf: '<?php print $player_swf_url ?>',
      flashCom: '<?php print $flash_com_url ?>',
      l: '<?php print $fullscreen_url ?>',
      nid: '<?php print $framework_nid ?>',
      e: '<?php print $nid ?>',
      bF: '<?php print $start_frame ?>',
      sf: '<?php print $stop_frame ?>',
      ev: '<?php print $fullscreen ?>',
      lang: '<?php print $language ?>',
      uName: '<?php print $user_id ?>'
    },
    params = {
      allowFullScreen: 'true',
      wmode: 'transparent',
      scale: '<?php print $fullscreen == '' ? 'default' : 'exactfit' ?>'
    };
    <?php if (isset($swf_url)): print 'flashVars.eLF = \'' . $swf_url . '\';'; endif ?>
    function swf<?php print $nid ?>Loaded(e) {
      var $flash = $('#amendor-electure-<?php print $nid ?>');
      var $video = $flash.parent().find('.istribute_video');
      if ($video.length) {
        if (e.success) {
          $video.hide();
        }
        else {
          $flash.hide();
        }
      } else if (!e.success) {
        $flash.html('<p><a href="http://get.adobe.com/flashplayer/">Get Adobe Flash Player.</a></a>');
      }
    }
    swfobject.embedSWF('<?php print $start_swf_url ?>', 'amendor-electure-<?php print $nid ?>', '<?php print $width ?>', '<?php print $height ?>', '8.0.0', '<?php print $express_install_swf_url ?>', flashVars, params, null, swf<?php print $nid ?>Loaded);
  });
</script>