ndla_grep_tray_set_active_curriculum = function(element) {
  if($(element).hasClass('active')) {
    return;
  }
  $('.ndla-grep-tray-curriculas a').removeClass('active');
  $(element).addClass('active');
  ndla_grep_tray_update_levels();
  $('.ndla-grep-tray-levels a').addClass('active');
  ndla_grep_tray_update_groups();
  $('.ndla-grep-tray-main-groups a').removeClass('active');
  $('.ndla-grep-tray-main-groups a.visible:first').addClass('active');
  ndla_grep_tray_update_aims();
}

ndla_grep_tray_update_levels = function() {
  $('.ndla-grep-tray-levels a.visible').removeClass('visible');
  $('.ndla-grep-tray-curriculas a.active').each(function() {
    $('.ndla-grep-tray-levels a.' + $(this).attr('id')).addClass('visible');
  });
}

ndla_grep_tray_set_active_level = function(element) {
    $(element).toggleClass('active');
    ndla_grep_tray_update_groups();
    ndla_grep_tray_update_aims();
}

ndla_grep_tray_update_groups = function() {
  $('.ndla-grep-tray-main-groups a.visible').removeClass('visible');
  $('.ndla-grep-tray-curriculas a.active').each(function(_, curriculum) {
    $('.ndla-grep-tray-levels a.active.visible').each(function() {
      $('.ndla-grep-tray-main-groups a.' + $(this).attr('id') + '.' + $(curriculum).attr('id')).addClass('visible');
    });
  });
}

ndla_grep_tray_set_active_group = function(element) {
  $(element).toggleClass('active');
  ndla_grep_tray_update_aims();
}

ndla_grep_tray_update_aims = function() {
  $('.ndla-grep-tray-aims a.visible').removeClass('visible');
  $('.ndla-grep-tray-curriculas a.active').each(function(_, curriculum) {
    $('.ndla-grep-tray-levels a.active').each(function(_, level) {
        var setId = $(this).attr('data-setid');
      $('.ndla-grep-tray-main-groups a.active.visible').each(function() {
        //$('.ndla-grep-tray-aims a.' + '.setId' + setId   + $(this).attr('id') + '.' + $(curriculum).attr('id') +'.' + $(level).attr('id')).addClass('visible');
          $('a.' + 'setId-' + setId + '.'+ $(this).attr('id') + '.' + $(curriculum).attr('id')).addClass('visible');
      });
    });
  });
}

ndla_grep_tray_init = function() {
  if($('.ndla-grep-tray-curriculas a').length < 2) {
    $('.ndla-grep-tray-curriculas').hide();
  }
  if($('.ndla-grep-tray-levels a').length < 2) {
    $('.ndla-grep-tray-levels').hide();
  }
  element = $('.ndla-grep-tray-curriculas .active')[0];
  $('.ndla-grep-tray-curriculas a').removeClass('active');
  $(element).addClass('active');
  ndla_grep_tray_update_levels();
  $('.ndla-grep-tray-levels a').addClass('active');
  ndla_grep_tray_update_groups();
  $('.ndla-grep-tray-main-groups a').removeClass('active');
  $('.ndla-grep-tray-main-groups a.visible:first').addClass('active');
  ndla_grep_tray_update_aims();
}