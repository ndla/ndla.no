<?php

function ndla_grep_ndla_form() {
  global $user;
  $nid = arg(1);
  $node = node_load($nid);
  $aims = NdlaGrepNdlaClient::load_aims($node);
  $ids = ndla_grep_ndla_get_fag_ids();
  $items = NdlaGrepNdlaClient::get_courses($ids);

  $marked = array();
  foreach($user->og_groups as $group) {
    $marked[] = 'subject_' . $group['nid'];
  }

  foreach($items as &$item) {
    if(in_array($item['extra_id'], $marked)) {
      $item['marked'] = TRUE;
    }
  }

  $menu = theme('ndla_grep_ndla_aim_select_list', $items, url('ndla_grep/ajax/curriculum'), '', '');
  $form['nid'] = array(
    '#type' => 'hidden',
    '#default_value' => $nid,
  );
  $form['list'] = array(
    '#type' => 'item',
    '#value' => theme('ndla_grep_ndla_aim_select_container', $menu, array()),
  );
  $form['aims'] = array(
    '#type' => 'hidden',
    '#value' => json_encode($aims),
  );
  $form['selected'] = array(
    '#type' => 'item',
    '#prefix' => '<div id="ndla_grep_selected">',
    '#suffix' => '</div>',
    '#value' => theme('ndla_grep_ndla_aim_selected_list', $aims, ndla_grep_get_relation_types()),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#default_value' => t('Save'),
  );
  drupal_add_js(drupal_get_path('module', 'ndla_grep_ndla') . '/js/ndla_grep_ndla.form.js');
  drupal_add_css(drupal_get_path('module', 'ndla_grep_ndla') . "/css/ndla_grep_ndla.form.css");
  return $form;
}

function ndla_grep_ndla_form_submit($form, $form_state) {
  $nid = $form_state['clicked_button']['#post']['nid'];
  $node = node_load($nid);
  NdlaGrepNdlaClient::delete_aims($node);
  $curricula = json_decode($form_state['clicked_button']['#post']['aims'], TRUE);
  $add = array();
  foreach($curricula as $curriculum) {
    foreach(array_intersect_key($curriculum, array_flip(ndla_grep_element_children($curriculum))) as $main_group) {
      foreach(array_intersect_key($main_group, array_flip(ndla_grep_element_children($main_group))) as $aim) {
        $add[$aim['_id']] = array(
          'id' => $aim['_id'],
          'value' => $aim['_value'],
          'apprentice' => $aim['_apprentice'],
          'curriculum_id' => $curriculum['_id'],
          'competence_aim_set_id' => $main_group['_id'],
        );
      }
    }
  }

  NdlaGrepNdlaClient::save_aims($node, $add);
  drupal_goto('node/' . $node->nid);
}

function ndla_grep_ndla_get_fag_ids() {
  $ids = array();
  $result = db_query("SELECT nid FROM {node} WHERE type = 'fag' AND status = 1");
  while ($row = db_fetch_object($result)) {
    $ids[] = 'subject_' . $row->nid;
  }
  return $ids;
}

function ndla_grep_ndla_ajax_curriculum() {
  $tree = $_GET['tree'];
  $stored = json_decode($_GET['stored'], TRUE);
  $selected = array();
  foreach($stored as $curriculum) {
    foreach(array_intersect_key($curriculum, array_flip(ndla_grep_element_children($curriculum))) as $main_group) {
      foreach(array_intersect_key($main_group, array_flip(ndla_grep_element_children($main_group))) as $aim) {
        $selected[$aim['_id']] = array(
          'value' => $aim['_value'],
          'apprentice' => $aim['_apprentice'],
        );
      }
    }
  }

  $course_id = str_replace('subject_', '', $tree[0]['_id']);
  $set_ids = array(variable_get('ndla_grep_competence_aim_sets', array()));
  if(!empty($set_ids[0][$course_id])) {
    $set_ids = explode("\r\n", $set_ids[0][$course_id]);
  } else {
    $set_ids = array();
  }
  $items = NdlaGrepNdlaClient::get_curriculum($_GET['endpoint'], $set_ids);
  foreach($items as $key => $item) {
    $main_groups_tree = $tree;
    $main_groups_tree[] = array(
      '_name' => $item['name'],
      '_id' => $item['id'],
    );
    foreach($item['aims'] as $akey => $aim) {
      $aim_tree = $main_groups_tree;
      $aim_tree[] = array(
        '_id' => $aim['id'],
        '_name' => $aim['name'],
      );
      $value = !empty($selected[$aim['id']]) ? $selected[$aim['id']]['value'] : 'none';
      $apprentice = !empty($selected[$aim['id']]) ? $selected[$aim['id']]['apprentice'] : FALSE;
      $item['aims'][$akey]['children'] = theme('ndla_grep_ndla_aim_select_radios', $aim_tree, $aim['id'], $value, $apprentice, ndla_grep_get_relation_types());;
    }
    $main_groups = theme('ndla_grep_ndla_aim_select_list', $item['aims'], '', TRUE, $main_groups_tree);
    $items[$key]['children'] = $main_groups;
  }
  print theme('ndla_grep_ndla_aim_select_list', $items, '', FALSE, $tree);
  exit;
}

function ndla_grep_ndla_ajax_store() {
  $stored = json_decode($_GET['stored'], TRUE);
  $apprentice = $_GET['apprentice'];
  $tree = $_GET['tree'];
  unset($tree[0]);
  $tree = array_values($tree);
  $value = $_GET['value'];
  $tree[2]['_value'] = $value;
  $tree[2]['_apprentice'] = $apprentice;
  if($value == 'none') {
    $stored = ndla_grep_ajax_remove($stored, $tree);
  } else {
    $stored = ndla_grep_ajax_set($stored, $tree);
  }
  $html = theme('ndla_grep_ndla_aim_selected_list', $stored, ndla_grep_get_relation_types());
  print json_encode(array('json' => json_encode($stored), 'html' => $html));
  exit;
}

function ndla_grep_ajax_set($stored, $tree) {
  if(empty($stored[$tree[0]['_id']])) {
    $stored[$tree[0]['_id']] = $tree[0];
  }
  if(empty($stored[$tree[0]['_id']][$tree[1]['_id']])) {
    $stored[$tree[0]['_id']][$tree[1]['_id']] = $tree[1];
  }
  $stored[$tree[0]['_id']][$tree[1]['_id']][$tree[2]['_id']] = $tree[2];
  return $stored;
}

function ndla_grep_ajax_remove($stored, $tree) {
  if(!empty($stored[$tree[0]['_id']][$tree[1]['_id']][$tree[2]['_id']])) {
    unset($stored[$tree[0]['_id']][$tree[1]['_id']][$tree[2]['_id']]);
  }
  if(sizeof($stored[$tree[0]['_id']][$tree[1]['_id']]) < 3) {
    unset($stored[$tree[0]['_id']][$tree[1]['_id']]);
  }
  if(sizeof($stored[$tree[0]['_id']]) < 3) {
    unset($stored[$tree[0]['_id']]);
  }
  return $stored;
}

function ndla_grep_get_relation_types() {
  return array(
    'none' => t('None'),
    'related' => t('Related'),
  );
}

function ndla_grep_ndla_ajax_remove() {
  $id_list = $_GET['id_list'];
  $stored = json_decode($_GET['stored'], TRUE);
  $stored = ndla_grep_ndla_recursive_remove($id_list, $stored);
  $html = '';
  $html = theme('ndla_grep_ndla_aim_selected_list', $stored, ndla_grep_get_relation_types());
  echo json_encode(array('json' => json_encode($stored), 'html' => $html));
}

function ndla_grep_ndla_recursive_remove($list, $array) {
  $item = array_shift($list);
  $id = $item;
  if(empty($list)) {
    unset($array[$id]);
    return $array;
  } else {
    $array[$id] = ndla_grep_ndla_recursive_remove($list, $array[$id]);
    if(sizeof(ndla_grep_element_children($array[$id])) == 0) {
      unset($array[$id]);
    }
  }
  return $array;
}