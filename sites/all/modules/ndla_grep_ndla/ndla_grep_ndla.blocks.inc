<?php

/**
 * Implementation of hook_block()
 */
function ndla_grep_ndla_block($op = 'list', $delta = 0, $edit = array()) {
  global $base_path, $language;
  switch ($op) {
    case 'list':
      return array(
        'ndla_grep_comptetence_aims' => array(
          'info' => t('NDLA Grep: Competence aims'),
          'weight' => 3,
          'status' => 1,
          'region' => 'right'
        ),
      );
    case 'view':
      if($delta == 'ndla_grep_comptetence_aims') {
        $path = $_GET['q'];
        if(preg_match('/^node\/([\d]+)$/', $path, $matches)) {
          $node = node_load($matches[1]);
          $aims = NdlaGrepNdlaClient::load_aims($node);
          if(empty($aims)) {
            return;
          }
          $context = ndla_utils_disp_get_context();
          $fag_link = '';
          if(!empty($context['course'])) {
            $gid = ndla_fag_taxonomy_get_term_from_og($context['course']);
            $fag_link = '&im_vid_100016[]=' . $gid;
          }
          drupal_add_css(drupal_get_path('module', 'ndla_grep_ndla') . "/css/ndla_grep_ndla.blocks.css");
          $list = array();
          foreach($aims as $curriculum) {
            $item = array('data' => $curriculum['_name'], 'children' => array());
            foreach(array_intersect_key($curriculum, array_flip(ndla_grep_element_children($curriculum))) as $set) {
              foreach(array_intersect_key($set, array_flip(ndla_grep_element_children($set))) as $aim) {
                $apprentice = empty($aim['_apprentice']) ? '' : ' ('. t('For apprentices') . ')';
                $item['children'][] = '<a href="' . $base_path . $language->language . '/search/apachesolr_search/?language[]=und&language[]=' . $language->language . '&sm_ndla_grep_competence_aims_id[]=' . urlencode($aim['_id']) . $fag_link . '">' . $aim['_name'] . "</a>" . $apprentice;
              }
            }
            $list[] = $item;
          }

          return array('subject' => t('Competence aims'), 'content' => theme('item_list', $list));
        }
      }
      break;
  }
}