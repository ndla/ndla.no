<ul>
<?php foreach($data as $curriculum): ?>
  <li>
    <span><?php print $curriculum['_name']; ?></span>
    <ul>
    <?php foreach(array_intersect_key($curriculum, array_flip(ndla_grep_element_children($curriculum))) as $main_group): ?>
      <li>
        <span><?php print $main_group['_name']; ?></span>
        <ul>
          <?php foreach(array_intersect_key($main_group, array_flip(ndla_grep_element_children($main_group))) as $aim): ?>
          <li>
            <span><?php print $aim['_name']; ?> (<?php print $types[$aim['_value']]; ?>)<?php if($aim['_apprentice'] == 'true') print " (" . t('For apprentices') . ")";
            ?></span>
            <?php
            $id_list = array(
              $curriculum['_id'],
              $main_group['_id'],
              $aim['_id'],
            );
            ?>
            <i class="fa fa-times aim-list-remove" data-tree='<?php echo(json_encode($id_list)); ?>'></i>
          </li>
          <?php endforeach; ?>
        </ul>
      </li>
    <?php endforeach; ?>
    </ul>
  </li>
<?php endforeach; ?>
</ul>