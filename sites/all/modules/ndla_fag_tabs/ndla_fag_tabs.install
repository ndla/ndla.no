<?php
/**
 * @file
 * @ingroup ndla_fag_tabs
 * @brief
 *  Install file
 */
 
function ndla_fag_tabs_schema() {
  $schema['ndla_fag_tabs'] = array(
    'description' => t('Table for storing "fag" tab configurations'),
    'fields' => array(
      'nid' => array(
        'description' => t('The node ID the row applies to'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
	   'enabled_tabs' => array(
        'description' => t('Column for storing enabled tabs'),
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
        'not null' => TRUE,
      ),
      'default_tab' => array(
        'description' => t('Column for storing default expanded tab'),
        'type' => 'varchar',
        'length' => 30,
        'default' => '',
        'not null' => TRUE,
      ),
      'topics_tab_name_en' => array(
        'description' => t('Name of the topics tab in english'),
        'type' => 'varchar',
        'length' => 32,
        'default' => 'Topics',
        'not null' => TRUE,
      ),
      'topics_tab_name_nn' => array(
        'description' => t('Name of the topics tab in norwegian nynorsk'),
        'type' => 'varchar',
        'length' => 32,
        'default' => 'Tema',
        'not null' => TRUE,
      ),
      'topics_tab_name_nb' => array(
        'description' => t('Name of the topics tab in norwegian bokmål'),
        'type' => 'varchar',
        'length' => 32,
        'default' => 'Tema',
        'not null' => TRUE,
      ),
      'topic_menu_tab_name_en' => array(
        'description' => t('Name of the topic menu tab in english'),
        'type' => 'varchar',
        'length' => 32,
        'default' => 'Sequence',
        'not null' => TRUE,
      ),
      'topic_menu_tab_name_nn' => array(
        'description' => t('Name of the topic menu tab in norwegian nynorsk'),
        'type' => 'varchar',
        'length' => 32,
        'default' => 'Sekvens',
        'not null' => TRUE,
      ),
      'topic_menu_tab_name_nb' => array(
        'description' => t('Name of the topic menu tab in norwegian bokmål'),
        'type' => 'varchar',
        'length' => 32,
        'default' => 'Sekvens',
        'not null' => TRUE,
      ),
      'menu_tab_name_en' => array(
        'description' => t('Name of the menu tab in english'),
        'type' => 'varchar',
        'length' => 32,
        'default' => 'Material',
        'not null' => TRUE,
      ),
      'menu_tab_name_nn' => array(
        'description' => t('Name of the menu tab in norwegian nynorsk'),
        'type' => 'varchar',
        'length' => 32,
        'default' => 'Fagstoff',
        'not null' => TRUE,
      ),
      'menu_tab_name_nb' => array(
        'description' => t('Name of the menu tab in norwegian bokmål'),
        'type' => 'varchar',
        'length' => 32,
        'default' => 'Fagstoff',
        'not null' => TRUE,
      ),
      'menu_in_tray' => array(
        'description' => 'Boolean indicating whether the topic menu should be loaded into the topic tray as well as in the left menu.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny'
      ),
    ),
    'primary key' => array('nid'),
  );

  return $schema;
}

/**
 * Implementation of hook_update_N
 *
 * Add topics_tab_name to the table
 *
 * @return array
 */
function ndla_fag_tabs_update_6001() {
  $results = array();
  db_add_field($results, 'ndla_fag_tabs', 'topics_tab_name', array(
    'type' => 'varchar',
    'length' => 32,
    'default' => 'Topics',
    'not null' => TRUE,
  ));
  return $results;
}

/**
 * Implementation of hook_update_N
 *
 * Add topics_tab_name for bokmål and nynorsk
 *
 * @return array
 */
function ndla_fag_tabs_update_6002() {
  $results = array();
  db_change_field($results, 'ndla_fag_tabs', 'topics_tab_name', 'topics_tab_name_en', array(
    'type' => 'varchar',
    'length' => 32,
    'default' => 'Topics',
    'not null' => TRUE,
  ));
  db_add_field($results, 'ndla_fag_tabs', 'topics_tab_name_nn', array(
    'type' => 'varchar',
    'length' => 32,
    'default' => 'Tema',
    'not null' => TRUE,
  ));
  db_add_field($results, 'ndla_fag_tabs', 'topics_tab_name_nb', array(
    'type' => 'varchar',
    'length' => 32,
    'default' => 'Tema',
    'not null' => TRUE,
  ));
  return $results;
}

/**
 * Implementation of hook_update_N
 *
 * Add topic_menu_tab_name
 *
 * @return array
 */
function ndla_fag_tabs_update_6003() {
  $results = array();
  db_add_field($results, 'ndla_fag_tabs', 'topic_menu_tab_name_en', array(
    'type' => 'varchar',
    'length' => 32,
    'default' => 'Sequence',
    'not null' => TRUE,
  ));
  db_add_field($results, 'ndla_fag_tabs', 'topic_menu_tab_name_nn', array(
    'type' => 'varchar',
    'length' => 32,
    'default' => 'Sekvens',
    'not null' => TRUE,
  ));
  db_add_field($results, 'ndla_fag_tabs', 'topic_menu_tab_name_nb', array(
    'type' => 'varchar',
    'length' => 32,
    'default' => 'Sekvens',
    'not null' => TRUE,
  ));
  db_add_field($results, 'ndla_fag_tabs', 'topic_menu_in_tray', array(
    'description' => 'Boolean indicating whether the topic menu should be loaded into the topic tray as well as in the left menu.',
    'type' => 'int',
    'not null' => TRUE,
    'default' => 0,
    'size' => 'tiny'
  ));
  return $results;
}

/**
 * Implementation of hook_update_N
 *
 * Adding fields for storing custom name of menu tab...
 */
function ndla_fag_tabs_update_6004() {
  $results = array();
  db_add_field($results, 'ndla_fag_tabs', 'menu_tab_name_en', array(
    'type' => 'varchar',
    'length' => 32,
    'default' => 'Material',
    'not null' => TRUE,
  ));
  db_add_field($results, 'ndla_fag_tabs', 'menu_tab_name_nn', array(
    'type' => 'varchar',
    'length' => 32,
    'default' => 'Fagstoff',
    'not null' => TRUE,
  ));
  db_add_field($results, 'ndla_fag_tabs', 'menu_tab_name_nb', array(
    'type' => 'varchar',
    'length' => 32,
    'default' => 'Fagstoff',
    'not null' => TRUE,
  ));
  return $results;
}

function ndla_fag_tabs_enable() {
  // Make sure all courses have have a row in the ndla_fag_tabs table.
  // nid2 will be NULL for all rows missing.
  $query = "SELECT n.nid nid1, nft.nid nid2 FROM {node} n LEFT OUTER JOIN {ndla_fag_tabs} nft ON n.nid=nft.nid WHERE n.type='fag'";
  $result = db_query($query);
  while ($row = db_fetch_object($result)) {
	if (!$row->nid2) {
      $default_enabled = serialize(array('menu' => 'menu', 'topics' => 'topics', 'grep' => 'grep'));
      $default_tab = 'none';
      $insert_query = "INSERT INTO {ndla_fag_tabs} (nid, enabled_tabs, default_tab) VALUES (%d, '%s', '%s')";

      db_query($insert_query, $row->nid1, $default_enabled, $default_tab);
    }
  }
}

function ndla_fag_tabs_install() {
  $result = drupal_install_schema('ndla_fag_tabs');

  if (count($result) > 0) {
    drupal_set_message(t('Module ndla_fag_tabs installed successfully'));
  } else {
    drupal_set_message(t('Installation failed: ndla_fag_tabs'));
  }
}

function ndla_fag_tabs_uninstall() {
	drupal_uninstall_schema('ndla_fag_tabs');
}
