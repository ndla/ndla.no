<?php
/**
/**
 * @file
 * @ingroup ndla_authors
 * @brief
 *  Theme file for theming a list of links to authors.
 *
 * @param $node
 *  The node
 * @param $field
 */
 
 $authors = ndla_authors_get_authors($node, TRUE);
 $shown = array();
 $utdanning_rdf = (module_exists('utdanning_rdf')) ? TRUE : FALSE;
 $name = '';
 foreach($authors as $author) {
   if($field == $author['tid']) {
     $name = $author['term_name'];
     foreach($author['authors'] as $author_node) {
       if($utdanning_rdf) {
         $shown[] = l($author_node['title'], "node/".$author_node['nid']."/lightbox", array('attributes' => array('rel' => 'lightmodal')));
       }
       else {
         $shown[] = l($author_node['title'], "node/".$author_node['nid'], array('attributes' => array('rel' => 'lightmodal')));
       }
     }
   }
 }

 if(count($shown)) {
   print $name . ": " . implode(", ", $shown);
 }
 