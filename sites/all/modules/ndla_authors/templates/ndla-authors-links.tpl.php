<?php
/**
 * @file
 * @ingroup ndla_authors
 * @brief
 *  Theme file for theming a list of links to authors.
 *
 * @param $node
 *  The node
 */
 
 $authors = ndla_authors_get_authors($node, TRUE);
 $shown = array();
 $printed = array();
 foreach($authors as $author) {
   foreach($author['authors'] as $author_node) {
     if(!in_array($author_node['nid'], $printed)) {
       $shown[] = l($author_node['title'], "node/".$author_node['nid']);
       $printed[] = $author_node['nid'];
     }
   }
 }
 
 print implode(", ", $shown);
 