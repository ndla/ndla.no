Drupal.behaviors.ndla_feeds = function() {
  $('.feed-content').each(function() {
    var classes = $(this).attr('class').split(" ");
    var vid = ndla_feeds_get_vid(classes);
    var feed = ndla_feeds_get_feed(classes);
    if(vid && feed) {
      var that = this;
      $.get(Drupal.settings.ndla_feeds.callback + vid + "/" + feed, function(data) {
        $(that).html(data);
      });
      
    }
    else {
      $(this).html(Drupal.t('Something went wrong, can not find node id and/or feed id.'));
    }
  });
}

function ndla_feeds_get_vid(classes) {
  for(i in classes) {
    if(classes[i].indexOf('node-') != -1) {
      return classes[i].replace("node-", "");
    }
  }
}

function ndla_feeds_get_feed(classes) {
  for(i in classes) {
    if(classes[i].indexOf('feed-') != -1 && classes[i].indexOf('feed-content') == -1) {
      return classes[i].replace("feed-", "");
    }
  }
}