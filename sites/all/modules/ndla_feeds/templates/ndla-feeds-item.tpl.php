<div class='ndla-feeds-item'>
  <h3><a target='_blank' href='<?php print $item['perma_link']; ?>'><?php print $item['title']; ?></a></h3>
  <div class='feed-description'>
    <?php 
    print  node_teaser($item['description'], NULL, 140);
    ?>
    <div class='clearfix'></div>
    <div class='feed-date'><?php print $item['date']; ?></div>
  </div>
</div>