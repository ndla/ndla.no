/**
 * @file
 * @ingroup utdanning_expander
 */

function insertAction() {
	tinyMCEPopup.execCommand("mceBeginUndoLevel");
	var synlig=document.getElementById("synlig").value;
	var usynlig=document.getElementById("usynlig").value;
	var expand=document.getElementById("expand").value;
	var hide=document.getElementById("hide").value;
	var html='<div class="hide">'+ synlig +' <a href="#" class="read-more"> '+ expand +'</a> <div name="details" class="details"> <p>'+usynlig+'</p> <a class="re-collapse" href="#">'+hide+'</a></div></div> <p>&nbsp;</p>';
	
	tinyMCE.execCommand('mceInsertContent', false, html);
	tinyMCEPopup.execCommand("mceEndUndoLevel");
	tinyMCEPopup.close();
}

function getSelectValue(form_obj, field_name) {
	var elm = form_obj.elements[field_name];

	if (elm == null || elm.options == null)
		return "";

	return elm.options[elm.selectedIndex].value;
}
