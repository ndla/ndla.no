<?php 
// $Id$

/** \addtogroup ndla_fag_taxonomy */

/**
 * @file
 * @ingroup ndla_fag_taxonomy
 */

include_once(drupal_get_path('module', 'ndla_fag_taxonomy') .'/ndla_fag_taxonomy.synchronize.inc');

function ndla_fag_taxonomy_str_starts_with($Haystack, $Needle){
    return strpos($Haystack, $Needle) === 0;
}

function ndla_fag_taxonomy_str_ends_with($Haystack, $Needle){
    return strrpos($Haystack, $Needle) === strlen($Haystack)-strlen($Needle);
}

/**
 * Get default vocabulary
 */
function ndla_fag_taxonomy_get_default_vocabulary() {
  static $voc;

  if(!$voc) {
    $vid = variable_get('ndla_fag_taxonomy_vocabulary' , -1);
    $voc = db_fetch_object(db_query("SELECT vid, name FROM {vocabulary} WHERE vid = %d", $vid));
  }
  return $voc;
}

/**
 * Check if node nid is allowed for vocabulary
 */
function ndla_fag_taxonomy_allow_default_vocabulary($node_type, $vid = NULL) {
  if ($vid == NULL) {
    static $vocabuary;
    if ($vocabuary == NULL) {
      $vocabuary = ndla_fag_taxonomy_get_default_vocabulary();
    }
    $vid = $vocabuary->vid;
  }
  $ret = db_result(db_query("SELECT type FROM {vocabulary_node_types} WHERE type = '%s' AND vid = %d", $node_type, $vid));
  if ($ret == FALSE) {
    return FALSE;
  } else {
    return TRUE;
  }
}

/**
 * Get terms of vocabulary as a flat list
 */
function ndla_fag_taxonomy_get_vocabulary_list($vid = NULL, $associative = FALSE) {
  if ($vid == NULL) {
    $vocabuary = ndla_fag_taxonomy_get_default_vocabulary();
    $vid = $vocabuary->vid;
  }
  $sql = "SELECT nftm.nid AS gid, td.tid, th.parent AS ptid, td.name FROM {term_data} td INNER JOIN {vocabulary} v ON v.vid = td.vid INNER JOIN {term_hierarchy} th ON th.tid = td.tid LEFT JOIN {ndla_fag_taxonomy_map} nftm ON td.tid = nftm.tid WHERE v.vid = %d ORDER BY th.parent, td.weight";
  $dbh = db_query($sql, $vid);
  $terms = array();
  while ($term = db_fetch_object($dbh)) {
    if ($associative) {
      $terms[$term->tid] = $term;
    } else {
      $terms[] = $term;
    }
  }
  return $terms;
}

/**
 * Get terms of vocabulary as a hierarchical tree
 */
function ndla_fag_taxonomy_get_vocabulary_tree($vid = NULL) {
  static $tree;
  if ($vid == NULL) {
    $vocabuary = ndla_fag_taxonomy_get_default_vocabulary();
    $vid = $vocabuary->vid;
  }
  
  if(isset($tree[$vid])) {
    return $tree[$vid];
  }
  
  $root = new stdClass();
  $root->vid = $vid;
  $root->terms = array();
  $terms = ndla_fag_taxonomy_get_vocabulary_list($vid, TRUE);
  foreach ($terms as $tid => $term) {
    if ($term->ptid == 0) {
      $root->terms[] = $term;
      unset($term->ptid);
    } else {
      if (!empty($terms[$term->ptid]->terms) && !is_array($terms[$term->ptid]->terms)) {
        $terms[$term->ptid]->terms = array();
      }
      $terms[$term->ptid]->terms[] = $term;
      unset($term->ptid);
    }
  }
  $tree[$vid] = $root;
  return $root;
}

/**
 * Get leaf terms of vocabulary
 */
function ndla_fag_taxonomy_get_vocabulary_leafs(&$term = NULL, &$leafs = array()) {
  if ($term == NULL) {
    $term = ndla_fag_taxonomy_get_vocabulary_tree();
  }
  if (!empty($term->terms) && !is_array($term->terms)) {
    $leafs[] = $term;
  } else {
    if(!empty($term->gid)) {
      $leafs[] = $term;
    }
    
    if(!empty($term->terms)) {
      foreach ($term->terms as $subterm) {
        ndla_fag_taxonomy_get_vocabulary_leafs($subterm, $leafs);
      }
    }
  }
  return $leafs;
}

/**
 * Get terms of vocabulary for node
 */
function ndla_fag_taxonomy_get_node_terms($node_nid, $node_vid) {
  static $term_collections = array();
  if (isset($term_collections[$node_vid])) {
    return $term_collections[$node_vid];
  }
  $vocabuary = ndla_fag_taxonomy_get_default_vocabulary();
  $vid = $vocabuary->vid;
  $sql = "SELECT nftm.nid AS gid, td.tid, td.name FROM {term_node} tn INNER JOIN {term_data} td ON tn.tid = td.tid LEFT JOIN {ndla_fag_taxonomy_map} nftm ON nftm.tid = td.tid WHERE tn.nid = %d AND tn.vid = %d AND td.vid = %d";
  $dbh = db_query($sql, $node_nid, $node_vid, $vid);
  $terms = array();
  while ($term = db_fetch_object($dbh)) {
    $terms[] = $term;
  }
  $term_collections[$node_vid] = $terms;
  return $terms;
}

/**
 * Get terms of groups for node
 */
function ndla_fag_taxonomy_get_node_group_terms($node_nid, $node_vid) {
  static $node_group_terms;

  if(!isset($node_group_terms[$node_nid.$node_vid])) {
    $sql = "SELECT oga.group_nid AS gid, nftm.tid, td.name  FROM {og_ancestry} oga LEFT JOIN {ndla_fag_taxonomy_map} nftm ON nftm.nid = oga.group_nid LEFT JOIN {term_data} td ON td.tid = nftm.tid WHERE oga.nid = %d";
    $dbh = db_query($sql, $node_nid);
    $terms = array();
    while ($term = db_fetch_object($dbh)) {
      $terms[] = $term;
    }
    $node_group_terms[$node_nid.$node_vid] = $terms;
  }

  return $node_group_terms[$node_nid.$node_vid];
}


/**
 * Get terms of user's groups
 */
function ndla_fag_taxonomy_get_user_terms($uid = NULL) {
  if ($uid == NULL) {
    global $user;
    $uid = $user->uid;
  }
  $dbh = db_query("SELECT ogu.nid AS gid, nftm.tid, td.name  FROM {og_uid} ogu LEFT JOIN {ndla_fag_taxonomy_map} nftm ON nftm.nid = ogu.nid LEFT JOIN {term_data} td ON td.tid = nftm.tid WHERE ogu.is_active = 1 AND ogu.uid = %d", $uid);
  $terms = array();
  while ($term = db_fetch_object($dbh)) {
    $terms[] = $term;
  }
  return $terms;
}

/**
 * Set terms of vocabulary for node
 */
function ndla_fag_taxonomy_set_node_terms($node_nid, $node_vid, $terms_tid = array()) {
  // add og groups terms
  $dbh = db_query("SELECT group_nid FROM {og_ancestry} WHERE nid = %d", $node_nid);
  while ($gid = db_result($dbh)) {
    $tid = db_result(db_query("SELECT tid FROM {ndla_fag_taxonomy_map} WHERE nid = %d", $gid));
    if ($tid != FALSE && !in_array($tid, $terms_tid)) {
      $terms_tid[] = $tid;
    }
  }
  // add parent lineage terms
  $terms_final = array();
  foreach ($terms_tid as $tid) {
    if (is_numeric($tid)) {
      foreach (taxonomy_get_parents_all($tid) as $term) {
        $terms_final[$term->tid] = $term->tid;
      }
    }
  }
  $vocabuary = ndla_fag_taxonomy_get_default_vocabulary();
  db_query("DELETE FROM {term_node} WHERE nid = %d AND vid = %d AND tid IN (SELECT tid FROM {term_data} WHERE vid = %d)", $node_nid, $node_vid, $vocabuary->vid);
  foreach ($terms_final as $tid) {
    db_query("INSERT INTO {term_node} (nid, vid, tid) values (%d, %d, %d)", $node_nid, $node_vid, $tid);
  }
}

/**
 * Get nid of co-responding OG Group for given term
 */
function ndla_fag_taxonomy_get_og_from_term($tid) {
  static $group_gids;
  if(!isset($group_gids[$tid])) {
    $group_gids[$tid] = db_fetch_object(db_query("SELECT nid FROM {ndla_fag_taxonomy_map} WHERE tid = %d", $tid))->nid;
  }
  
  return $group_gids[$tid];
}

/**
 * Get tid of co-responding term for given OG Group
 */
function ndla_fag_taxonomy_get_term_from_og($gid) {
  static $group_tids;
  if(!isset($group_tids[$gid])) {
    $object = db_fetch_object(db_query("SELECT tid FROM {ndla_fag_taxonomy_map} WHERE nid = %d", $gid));
    if(!empty($object->tid)) {
      $group_tids[$gid] = $object->tid;
    }
  }

  return !empty($group_tids[$gid]) ? $group_tids[$gid] : NULL;
}

function ndla_fag_taxonomy_get_term_name_from_og($gid) {
  static $group_names;
  
  if(!isset($group_names[$gid])) {
    $tid = ndla_fag_taxonomy_get_term_from_og($gid);
    if($tid) {
      $name = i18ntaxonomy_translate_term_name($tid);
      if(empty($name)) {
        $group_names[$gid] = db_fetch_object(db_query("SELECT name FROM {term_data} WHERE tid = %d", $tid))->name;
      }
      else {
        $group_names[$gid] = $name;
      }
    }
  }

  return $group_names[$gid];
}

/**
 * Implementation of hook_menu().
 */
function ndla_fag_taxonomy_menu() {
  $items = array();
  
  if(!module_exists('og')) {
    return $items;
  }
  
  $items['ndla_fag_taxonomy/update/%/%'] = array(
    'page callback' => 'ndla_fag_taxonomy_node_update',
    'page arguments' => array(2,3),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/ndla/ndla_fag_taxonomy'] = array(
    'title' => t('NDLA Fag Taxonomy'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_fag_taxonomy_synchronize_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['admin/settings/ndla/ndla_fag_taxonomy/synchronize/status'] = array(
    'page callback' => 'ndla_fag_taxonomy_synchronize_status',
    'page arguments' => array(),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/ndla/ndla_fag_taxonomy/synchronize/groups'] = array(
    'page callback' => 'ndla_fag_taxonomy_synchronize_groups',
    'page arguments' => array(),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
  );

  $items['admin/settings/ndla/ndla_fag_taxonomy/synchronize/nodes'] = array(
    'page callback' => 'ndla_fag_taxonomy_synchronize_nodes',
    'page arguments' => array(),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implementation of hook_block().
 */
function ndla_fag_taxonomy_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    $blocks[0] = array(
      'info' => t('Group and Taxonomy Block'),
      'weight' => 0,
      'status' => 1,
      'region' => 'right',
      'cache' => BLOCK_NO_CACHE
    );
    $blocks[1] = array(
      'info' => t('Course Affiliation'),
      'weight' => 0,
      'status' => 1,
      'region' => 'right',
      'cache' => BLOCK_CACHE_PER_PAGE
    );
    return $blocks;
  }
  elseif ($op == 'view') {
    switch($delta) {
      case 0:
        $node_type = db_result(db_query("SELECT type FROM {node} WHERE nid = %d", arg(1)));
        if (ndla_fag_taxonomy_allow_default_vocabulary($node_type)) {
          $htm = '';
          $htm .= '<div id="ogtax_block_div"></div>';
          $htm .= '<script type="text/javascript">$(document).ready(function() { ndla_fag_taxonomy_block_render(); });</script>';
          $block = array(
            'subject' => t('Group &amp; Taxonomy'),
            'content' => $htm,
          );
        }
        break;
      case 1:
        $node_type = db_result(db_query("SELECT type FROM {node} WHERE nid = %d", arg(1)));
        if (ndla_fag_taxonomy_allow_default_vocabulary($node_type)) {
          $htm = '';
          $htm .= '<div id="coure-affiliation"></div>';
          $htm .= '<script type="text/javascript">$(document).ready(function() { ndla_fag_taxonomyRenderCourseBlock(); });</script>';
          $block = array(
            'subject' => t('Course Affiliation'),
            'content' => $htm,
          );
        }
        break;
    }
    return !empty($block) ? $block : NULL;
  }
}

/**
 * Ajax callback function for updating terms on a node.
 */
function ndla_fag_taxonomy_node_update($nid, $vid) {
  $terms = explode(',', $_GET['terms']);
  if (is_numeric($nid) && is_numeric($vid) && $terms != FALSE) {
    ndla_fag_taxonomy_set_node_terms($nid, $vid, $terms);
  }
  exit();
}

/**
 * Implementation of hook_form_alter().
 */
function ndla_fag_taxonomy_form_alter(&$form, $form_state, $form_id) {
  if(!empty($form['#node']) && $form_id == $form['#node']->type . '_node_form') {
    $abort = array('emneside', 'fagressurs', 'emneartikkel');
    if(in_array($form['#node']->type, $abort)) {      
      $vocabuary = ndla_fag_taxonomy_get_default_vocabulary();
      unset($form['taxonomy'][$vocabuary->vid]);
      return;
    }
  }
  drupal_add_js(drupal_get_path('module', 'ndla_fag_taxonomy') . '/ndla_fag_taxonomy.js');
  if (isset($form['type']) && isset($form['#node'])) {
    if ($form['type']['#value'] .'_node_form' == $form_id)  {
      $node = $form['#node'];
      if (ndla_fag_taxonomy_allow_default_vocabulary($node->type)) {
        $form['ndla_fag_taxonomy'] = array(
          '#type'          => 'fieldset',
          '#title'         => t('Group & Taxonomy'),
          '#collapsible'   => TRUE,
          '#collapsed'     => FALSE,
          '#prefix'        => '<div class="ndla_fag_taxonomy">',
          '#suffix'        => '</div>',
          '#weight'        => 20,
        );
        // remove existing og and taxonomy form
        $vocabuary = ndla_fag_taxonomy_get_default_vocabulary();
        unset($form['taxonomy'][$vocabuary->vid]);
        unset($form['og_nodeapi']);
        // inject group & taxonomy form
        ndla_fag_taxonomy_node_form($form, $form_state, $node, TRUE);
        // add validation hook
        $form['#validate'][] = 'ndla_fag_taxonomy_node_validate';
      }
    }
  }
  //Remove OG from Biblios first form during node/add/biblio
  else if(!empty($form['#node']) && $form['#node']->type ."_node_form" == $form_id && empty($form['type'])) {
    unset($form['og_nodeapi']);
  }
}

/**
 * Implementation of hook_form_FORM_ID_alter
 *
 * Add extra fields to the terms overview form for the fag_taxonomy vocabulary
 */
function ndla_fag_taxonomy_form_taxonomy_overview_terms_alter(&$form, &$form_state) {
  if ($form['#vocabulary']['vid'] == variable_get('ndla_fag_taxonomy_vocabulary', 0)) {
    $form['#submit'][] = 'ndla_fag_taxonomy_overview_terms_submit';
    $form['#theme'] = 'ndla_fag_taxonomy_overview_terms';
    $res = db_query('SELECT tid, collapsed FROM {ndla_fag_taxonomy_term}');
    $extras = array();
    while ($res_o = db_fetch_object($res)) {
      $extras[$res_o->tid] = $res_o->collapsed;
    }
    foreach($form as $key => $value) {
      if (strncmp($key, 'tid:', 4) == 0) {
        $form[$key]['collapsed'] = array(
          '#type' => 'checkbox',
          '#default_value' => $extras[$value['tid']['#value']],
        );
      }
    }
  }
}

/**
 * Implementation of hook_theme
 */
function ndla_fag_taxonomy_theme() {
  return array(
    'ndla_fag_taxonomy_overview_terms' => array(
      'arguments' => array('form' => NULL),
    ),
  );
}

/**
 * Submit function for the overview terms form.
 *
 * We store extra data for each term(currently the collapsed status)
 */
function ndla_fag_taxonomy_overview_terms_submit($form, &$form_state) {
  foreach($form_state['values'] as $key => $value) {
    if (strncmp($key, 'tid:', 4) == 0) {
      $sql = 'UPDATE {ndla_fag_taxonomy_term}
              SET collapsed = %d
              WHERE tid = %d';
      db_query($sql, $value['collapsed'], $value['tid']);
    }
  }
  drupal_set_message(t('If you have changed what menu items are collapsed you need to empty cache for the changes to take effect.'));
}

/**
 * Add custom form for group & taxonomy selection
 */
function ndla_fag_taxonomy_node_form(&$form, $form_state, $node, $is_embedded = FALSE) {
  // find original selected groups for node
  $original_groups = array();
  foreach ($node->ndla_fag_taxonomy_node_group_terms as $term) {
    $original_groups[] = $term->gid;
  }

  $original_groups = implode(',', $original_groups);
  $form['ndla_fag_taxonomy']['ogtax_original_groups'] = array(
    '#type' => 'hidden',
    '#default_value' => $original_groups,
  );

  // find original selected terms for node
  $original_terms = array();
  foreach ($node->ndla_fag_taxonomy_node_terms as $term) {
    $original_terms[] = $term->tid;
  }
  $original_terms = implode(',', $original_terms);
  $form['ndla_fag_taxonomy']['ogtax_original_terms'] = array(
    '#type' => 'hidden',
    '#default_value' => $original_terms,
  );

  // find selected groups for node
  $selected_groups = $original_groups;
  if (isset($form_state['values']['ogtax_selected_groups'])) {
    $selected_groups = $form_state['values']['ogtax_selected_groups'];
  }
  $form['ndla_fag_taxonomy']['ogtax_selected_groups'] = array(
    '#type' => 'hidden',
    '#default_value' => $selected_groups,
  );

  // find selected terms for node
  $selected_terms = $original_terms;
  if (isset($form_state['values']['ogtax_selected_terms'])) {
    $selected_terms = $form_state['values']['ogtax_selected_terms'];
  }
  $form['ndla_fag_taxonomy']['ogtax_selected_terms'] = array(
    '#type' => 'hidden',
    '#default_value' => $selected_terms,
  );

  $htm = '';
  $htm .= '<div id="ogtax_edit_div"></div>';
  $htm .= '<script type="text/javascript">Drupal.behaviors.fagTaxonomyForm = function() { ndla_fag_taxonomy_edit_render(); };</script>';

  $form['ndla_fag_taxonomy']['ogtax_js'] = array(
    '#value' => $htm,
  );
}

/**
 * Implementation of hook_form_validate().
 */
function ndla_fag_taxonomy_node_validate($form, &$form_state) {
  if (isset($form['#node']->type) && og_is_group_post_type($form['#node']->type)) {
    if ($form['#post']['ogtax_selected_groups'] == '') {
      form_set_error('text', t('Node must be owned by atleast one group!'));
    }
  }
}

/**
 * Implementation of hook_nodeapi().
 */
function ndla_fag_taxonomy_nodeapi(&$node, $op, $arg = 0) {  
  //Biblio sends an empty node without content type. No need to carry on.
  if(empty($node->type)) {
    return;
  }
  
  if(in_array($op,array('prepare', 'validate', 'prepare translation')) || ($op == 'view' && module_exists('og') && !$arg && ndla_fag_taxonomy_allow_default_vocabulary($node->type))) {
    static $ndla_fag_taxonomy_js = FALSE;
    $current_node = ($op == 'prepare translation') ? $node->translation_source : $node;
    if(!empty($current_node->clone_from_original_nid)) {
      $current_node = node_load($current_node->clone_from_original_nid);
    }
    if (!$ndla_fag_taxonomy_js || $op == 'prepare translation') {
      $node->ndla_fag_taxonomy_node_terms = ndla_fag_taxonomy_get_node_terms($current_node->nid, $current_node->vid);
      $node->ndla_fag_taxonomy_node_group_terms = ndla_fag_taxonomy_get_node_group_terms($current_node->nid, $current_node->vid);
      $ndla_fag_taxonomy_js = TRUE;
      $user_terms = $user_terms_temp = ndla_fag_taxonomy_get_user_terms();
      $node_terms = $node->ndla_fag_taxonomy_node_terms;
      $node_groups = $node->ndla_fag_taxonomy_node_group_terms;
      
      $all_terms = $all_terms_tmp = ndla_fag_taxonomy_get_vocabulary_leafs();
      //Remove unwanted groups
      $is_new_context = ($node->type == 'emneartikkel' || $node->type == 'fagressurs');
      foreach($all_terms_tmp as $index => $data) {
        if(!empty($data->gid)) {
          $result = db_fetch_object(db_query("SELECT type FROM {node} n WHERE nid = %d", $data->gid));
          if(!empty($result->type) && $result->type == 'emneside' && !$is_new_context) {
            unset($all_terms[$index]);
          }
          else if(!empty($result->type) && $result->type == 'fag' && $is_new_context) {
            unset($all_terms[$index]);
          }
        }
      }
      //Preserve the keys else mr Javascript thinks this is an object -> fails miserably.
      $all_terms = array_values($all_terms);
      //Remove groups which are not allowed to create this kind of content.
      if(module_exists('ndla_group_access')) {
        foreach($user_terms_temp as $index => $uterm) {
          //Is this content type allowed for this group?
          if(!ndla_group_access_is_type_allowed_for_group($node->type, $uterm->gid)) {
            $is_chosen = FALSE;
            //Is the group already chosen?
            foreach($node_terms as $term) {
              if($term->gid == $uterm->gid) {
                $is_chosen = TRUE;
              }
            }
            
            //This node wasnt owned by the group. Remove it.
            if(!$is_chosen) {
              unset($user_terms[$index]);
            }
          }
        }  
        //Repair the indexes.
        $user_terms = array_values($user_terms);
      }
      
      $js = "var ogtax_node = " . drupal_to_js(
        array(
          'nid' => $node->nid,
          'vid' => $node->vid,
          'og_is_group_type' => og_is_group_type($node->type),
          'og_is_group_post_type' => og_is_group_post_type($node->type),
          )
        ) . ";\n";
      $js .= "var ogtax_texts = " . drupal_to_js(
        array(
          'header0' => t('Group & Taxonomy'),
          'header1' => t('Name:'),
          'header2' => t('Owned by:'),
          'header3' => t('Used by:'),
          ) 
        ) . ";\n";
      $js .= "var ogtax_all_terms = " . drupal_to_js($all_terms) . ";\n";
      $js .= "var ogtax_user_terms = " . drupal_to_js($user_terms) . ";\n";
      $js .= "var ogtax_node_terms = " . drupal_to_js($node_terms) . ";\n";
      $js .= "var ogtax_node_groups = " . drupal_to_js($node_groups) . ";\n";

      drupal_add_js($js, 'inline');
    }
  }

  if ($op == 'presave' && ndla_fag_taxonomy_allow_default_vocabulary($node->type)) {
    global $user;
    $node->og_public = 1;
    // rebuild organic group on node
    if (isset($node->ogtax_selected_groups)) {
      $groups = explode (',', $node->ogtax_selected_groups);
      if (is_array($groups)) {
        if (!is_array($node->og_groups)) {
          $node->og_groups = array();
        }
        foreach ($groups as $gid) {
          if (is_numeric($gid)) {
            $node->og_groups[$gid] = $gid;
          }
        }
      }
    }
    // rebuild fag taxonomy on node
    if (isset($node->ogtax_selected_terms)) {
      $terms = explode (',', $node->ogtax_selected_terms);
      if (is_array($terms)) {
        foreach ($terms as $tid) {
          if (is_numeric($tid)) {
            foreach (taxonomy_get_parents_all($tid) as $term) {
              $node->taxonomy[$term->tid] = $term;
            }
          }
        }
      }
    }
    unset($node->ogtax_original_groups);
    unset($node->ogtax_original_terms);
    unset($node->ogtax_selected_groups);
    unset($node->ogtax_selected_terms);
  }

  if ($op == 'insert' && ($node->type == 'fag' || $node->type == 'emneside')) {
    $tid = ndla_fag_taxonomy_get_term_from_og($node->nid);
    if ($tid == FALSE) {
      $vocabuary = ndla_fag_taxonomy_get_default_vocabulary();
      $vid = $vocabuary->vid;
      $sql  = "INSERT INTO {term_data}
               (vid, name, description, weight, trinn, UUID, URI, description_source, name_source, language, trid)
               VALUES (%d, '%s', '', 0, NULL, NULL, NULL, NULL, NULL, '', 0)";
      db_query($sql, $vid, $node->title);
      $tid = db_last_insert_id('term_data', 'tid');

      $sql  = "INSERT INTO {term_hierarchy} (tid, parent) VALUES (%d, 0)";
      db_query($sql, $tid);

      $sql  = "INSERT INTO {ndla_fag_taxonomy_map} (nid, tid) VALUES (%d, %d)";
      db_query($sql, $node->nid, $tid);

      db_query('INSERT INTO {ndla_fag_taxonomy_term} (tid) VALUES (%d)', $tid);
    }
  }
  
  if ($op == 'delete' && $node->type == 'fag') {
    db_query("DELETE FROM {ndla_fag_taxonomy_map} WHERE nid = %d", $node->nid);
  }
}

/**
 * Implementation of hook_taxonomy().
 */
function ndla_fag_taxonomy_taxonomy($op, $type, $array = NULL) {
  $vocabuary = ndla_fag_taxonomy_get_default_vocabulary();
  if ($type == 'term' && isset($array['vid'])) {
    if ($array['vid'] == $vocabuary->vid) {
      if (module_exists('og')) {
        $msg = t('Changes to this vocabulary might require <a href="@url">NDLA Fag Taxonomy Re-Synchronization</a>!!!',     array('@url'=> url('admin/settings/ndla/ndla_fag_taxonomy')));
        drupal_set_message($msg, 'warning', FALSE);
      }
      switch ($op) {
        case 'delete':
          db_query('DELETE FROM {ndla_fag_taxonomy_term}
                    WHERE tid = %d', $array['tid']);
          break;
        case 'insert':
          db_query('INSERT INTO {ndla_fag_taxonomy_term} (tid)
                    VALUES (%d)', $array['tid']);
          break;
      }
    }
  }
}

/**
 * Theme the overview of the fag vocabulary
 */
function theme_ndla_fag_taxonomy_overview_terms($form) {
  $page_increment  = $form['#page_increment'];
  $page_entries    = $form['#page_entries'];
  $back_peddle     = $form['#back_peddle'];
  $forward_peddle  = $form['#forward_peddle'];

  // Add drag and drop if parent fields are present in the form.
  if ($form['#parent_fields']) {
    drupal_add_tabledrag('taxonomy', 'match', 'parent', 'term-parent', 'term-parent', 'term-id', FALSE);
    drupal_add_tabledrag('taxonomy', 'depth', 'group', 'term-depth', NULL, NULL, FALSE);
    drupal_add_js(drupal_get_path('module', 'taxonomy') .'/taxonomy.js');
    drupal_add_js(array('taxonomy' => array('backPeddle' => $back_peddle, 'forwardPeddle' => $forward_peddle)), 'setting');
    drupal_add_css(drupal_get_path('module', 'taxonomy') .'/taxonomy.css');
  }

  $errors = form_get_errors() != FALSE ? form_get_errors() : array();
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['#term'])) {
      $term = &$form[$key];

      $row = array();
      $row[] = (isset($term['#term']['depth']) && $term['#term']['depth'] > 0 ? theme('indentation', $term['#term']['depth']) : '') . drupal_render($term['view']);
      if ($form['#parent_fields']) {
        $term['tid']['#attributes']['class'] = 'term-id';
        $term['parent']['#attributes']['class'] = 'term-parent';
        $term['depth']['#attributes']['class'] = 'term-depth';
        $row[0] .= drupal_render($term['parent']) . drupal_render($term['tid']) . drupal_render($term['depth']);
      }
      $row[] = drupal_render($term['edit']);
      $row[] = drupal_render($term['collapsed']);

      $row = array('data' => $row);
      $rows[$key] = $row;
    }
  }

  // Add necessary classes to rows.
  $row_position = 0;
  foreach ($rows as $key => $row) {
    $classes = array();
    if (isset($form['#parent_fields'])) {
      $classes[] = 'draggable';
    }

    // Add classes that mark which terms belong to previous and next pages.
    if ($row_position < $back_peddle || $row_position >= $page_entries - $forward_peddle) {
      $classes[] = 'taxonomy-term-preview';
    }

    if ($row_position !== 0 && $row_position !== count($rows) - 1) {
      if ($row_position == $back_peddle - 1 || $row_position == $page_entries - $forward_peddle - 1) {
        $classes[] = 'taxonomy-term-divider-top';
      }
      elseif ($row_position == $back_peddle || $row_position == $page_entries - $forward_peddle) {
        $classes[] = 'taxonomy-term-divider-bottom';
      }
    }

    // Add an error class if this row contains a form error.
    foreach ($errors as $error_key => $error) {
      if (strpos($error_key, $key) === 0) {
        $classes[] = 'error';
      }
    }
    $rows[$key]['class'] = implode(' ', $classes);
    $row_position++;
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => $form['#empty_text'], 'colspan' => '2'));
  }

  $header = array(t('Name'), t('Operations'), t('Collapsed'));
  $output = theme('table', $header, $rows, array('id' => 'taxonomy'));
  $output .= drupal_render($form);
  $output .= theme('pager', NULL, $page_increment);

  return $output;
}

function ndla_fag_taxonomy_node_in_course($nid, $gid) {
  return  db_result(db_query("SELECT tm.nid
                      FROM {ndla_fag_taxonomy_map} tm
                      INNER JOIN {term_node} tn ON tm.tid=tn.tid 
                      WHERE tm.nid = %d
                        AND tn.nid = %d", $gid, $nid)) !== FALSE;
}

/**
 * Update course relations when two nodes get related to each other
 *
 * @param object $node
 *  The node that is getting a new relation
 * @param $nid
 *  The nid of the node we're creating a relationship with
 */
function ndla_fag_taxonomy_update_node_courses($node, $nid) {
  // TODO: See TGP-2781
  /*$group_nid = db_result(db_query('SELECT group_nid FROM {og_ancestry} WHERE nid = %d', $node->nid));
  $related_group_nid = db_result(db_query('SELECT group_nid FROM {og_ancestry} WHERE nid = %d', $nid));
  if ($group_nid != $related_group_nid) {
    $tid = ndla_fag_taxonomy_get_term_from_og($group_nid);
    $related_tid = ndla_fag_taxonomy_get_term_from_og($related_group_nid);
    db_query("INSERT IGNORE INTO {term_node} (nid, vid, tid) VALUES (%d, %d, %d)", $node->nid, $node->vid, $related_tid);
    // for each revision
    $res = db_query("SELECT vid FROM {node_revisions} WHERE nid = %d", $nid);
    while ($vid = db_result($res)) {
      db_query("INSERT IGNORE INTO {term_node} (nid, vid, tid) VALUES (%d, %d, %d)", $nid, $vid, $tid);
    }
  }*/
}

function ndla_fag_taxonomy_page_title_pattern_alter(&$pattern, &$data) {
  global $language;
  $args = arg();
  if(arg(0) == 'node' && !empty($args[1]) && is_numeric($args[1])) {
    $obj = db_fetch_object(db_query("SELECT nid, type FROM {node} WHERE (type = 'fag' OR type = 'emneside') AND nid = " . check_plain($args[1])));
    if(!empty($obj->nid)) {
      $data['node']->page_title = ndla_fag_taxonomy_get_term_name_from_og($obj->nid);
    }
    else {
      $fag = ndla_utils_disp_get_context_value('course');
      if(!empty($fag)) {
        $pattern = str_replace("[ogname]", ndla_fag_taxonomy_get_term_name_from_og($fag), $pattern);
      }
    }
  }
}