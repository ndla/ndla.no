Drupal.behaviors.cb_events = function() {
  window.oncontextmenu = function () {
    //Right-clicking in chrome reports mouse button 1 when CTRL-key is pressed. 
    var pattern = 'contentbrowser/node';
    if($(document.activeElement).attr('href').match(/\/contentbrowser\/node/)) {
      $(document.activeElement).attr('data-lburl', $(document.activeElement).attr('href'));
      $(document.activeElement).attr('href', $(document.activeElement).attr('href').replace("/contentbrowser", ""));
    }
  }
  
  $('.contentbrowser a').mousedown(function(event) {
    if($(this).attr('data-lburl')) {
      $(this).attr('href', $(this).attr('data-lburl'));
      $(this).attr('data-lburl', '');
    }
    
    //Trigger on all buttons except the left one
    if(event.which != 1) {
      var pattern = 'contentbrowser/node';
      if($(this).attr('href').match(/\/contentbrowser\/node/)) {
        $(this).attr('data-lburl', $(this).attr('href'));
        $(this).attr('href', $(this).attr('href').replace("/contentbrowser", ""));
      }
    }
  });
}