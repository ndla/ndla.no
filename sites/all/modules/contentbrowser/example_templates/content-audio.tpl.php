<?php
// $Id$
$out = ($from_editor) ? _contentbrowser_render_editor_image($tag) : theme('audio_display', $node);

$caption_data = array_filter(array_merge(array($caption_title), $caption_data));
if(count($caption_data)) {
  $out .= "<span class='contentbrowser_caption'>" . implode("<br />", array_filter($caption_data)) . "</span>";
}

print $out;
?>