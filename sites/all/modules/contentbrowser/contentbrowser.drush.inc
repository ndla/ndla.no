<?php

/**
 * @file
 * @ingroup contentbrowser
 *   drush integration for the contentbrowser.
 */

/**
 * Implementation of hook_drush_command().
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing the commands.
 */
function contentbrowser_drush_command() {
  $items = array();

  $items['cb-populate-insertion'] = array(
    'description' => dt('Populate the insertion column.'),
  );

  return $items;
}

function drush_contentbrowser_cb_populate_insertion() {
  contentbrowser_populate_insertion();
}
