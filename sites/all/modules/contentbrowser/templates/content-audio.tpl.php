<?php
/**
 * @file
 * @ingroup ndla2010
 * @brief
 *  Template file for audio nodes, used by the content browser
 *
 * Params available:
 *  - $node: The node object
 *  - $tag: The decoded tag.
 *  - $from_editor: TRUE if we are called from an editor.
 *  - $caption_data: The text under the image (not CCK fields)
 */

if(in_array(arg(0), array('print', 'ndla_node_export')) || !empty($GLOBALS['fag_export'])) {
    $url = url('node/' . $node->nid, array('absolute' => true));
    $out = "<img src='http://chart.apis.google.com/chart?chs=150x150&cht=qr&chl=$url'/><br/>$url";
    print $out;
    return;
}

if($from_editor) {
  $out = _contentbrowser_render_editor_image($tag);
} else {
  $out = "";
  //Itslearning + NDLA is nowhere near good enough - make it a tiny whiny little bit better.
  if(arg(1) == 'itslearning_embed_details') {
    $variables = ndla_utils_get_embed_vars($node);
    global $base_url;
    $out = '<object height="24" width="290" data="' . $base_url . '/sites/all/modules/audio/players/1pixelout.swf" type="application/x-shockwave-flash">
      <param value="' . $base_url . '/sites/all/modules/audio/players/1pixelout.swf" name="movie" />    
      <param value="transparent" name="wmode" />
      <param value="false" name="menu" />
      <param value="high" name="quality" />
      <param value="soundFile=' . $base_url . '/' . $variables['filepath'] . '" name="FlashVars" />
      <embed height="24" width="290" flashvars="soundFile=' . $base_url . '/' . $variables['filepath'] . '" src="' . $base_url . '/sites/all/modules/audio/players/1pixelout.swf"></embed>
    </object>';
  }
  else {
    $out = theme('audio_display', $node);
  }
  
  if(empty($tag['imagecache']) || $tag['imagecache'] != 'Fullbredde') {
    $out = "<div class='ndla_audio_small'>$out</div>";
  }
}

$caption_data = array_filter(array_merge(array($caption_title), $caption_data));
if(count($caption_data)) {
  $out .= "<span class='contentbrowser_caption'>" . implode("<br />", array_filter($caption_data)) . "</span>";
}

print $out;
?>