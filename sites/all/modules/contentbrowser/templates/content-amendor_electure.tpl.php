<?php
/**
 * @file
 * @ingroup ndla2010
 * @brief
 *  Template used by content browser while rendering Amendors eLecture
 *
 * Generic template file.
 * Params available:
 *  - $node: The node object
 *  - $tag: The decoded tag.
 *  - $from_editor: TRUE if we are called from an editor.
 *  - $caption_data: The text under the image (not CCK fields)
 */
global $base_url;
$default_height = 568;
$default_width = 720;
$out = '';
$link_title = '';
$link_node_title = $node->title;
if(isset($tag['link_title_text']) && !empty($tag['link_title_text'])) {
  $link_title = "title=\"" . $tag['link_title_text'] . "\"";
}

if(!empty($tag['link_text'])) {
  $link_node_title = $tag['link_text'];
}

$caption_data = array_filter(array_merge(array($caption_title), $caption_data));
$alt = t('Image showing the thumbnail for content named \"@name\"', array('@name' => $node->title));
if($from_editor) {
  if($tag['insertion'] != 'inline') {
    if($tag['insertion'] == 'thumbnail') {
      $thumbnail = _contentbrowser_get_image_thumbnail($node->nid);
    }
    if(empty($thumbnail)) {
      $out .= l($link_node_title, 'node/' . $node->nid);
      $caption_data = array(); //No caption while rendering links
    } else {
      if($tag['imagecache'] && !empty($tag['imagecache'])) {
        $out .= '<a ' . $link_node_title . ' href="' . url('node/' . $node->nid) . '">' . theme('image', imagecache_create_path($tag['imagecache'], $thumbnail), $alt, '', NULL, FALSE) . '</a>';
      } else {
        $out .= '<a ' . $link_node_title . ' href="' . url('node/' . $node->nid) . '">' . theme('image', imagecache_create_path('Liten', $thumbnail), $alt, '', NULL, FALSE) . '</a>';
      }
    }
  }
  else {
    $out = _contentbrowser_render_editor_image($tag);
  }
}
//We only support inline and link right now, make everything which isnt 'line' go this way.
else if(!isset($tag['insertion']) || $tag['insertion'] != 'inline') {
  if($tag['insertion'] == 'thumbnail') {
    $thumbnail = _contentbrowser_get_image_thumbnail($node->nid);
  }
  if(empty($thumbnail)) {
    $out .= l($link_node_title, 'node/' . $node->nid, array('attributes' => array('title' => $tag['link_title_text'])));
    $caption_data = array(); //No caption while rendering links
  } else {
    if($tag['imagecache'] && !empty($tag['imagecache'])) {
      $out .= '<a ' . $link_node_title . ' href="' . url('node/' . $node->nid) . '">' . theme('image', imagecache_create_path($tag['imagecache'], $thumbnail), $alt, '', NULL, FALSE) . '</a>';
    } else {
      $out .= '<a ' . $link_node_title . ' href="' . url('node/' . $node->nid) . '">' . theme('image', imagecache_create_path('Liten', $thumbnail), $alt, '', NULL, FALSE) . '</a>';
    }
  }
}
//Show the node in the body field.
else if($tag['insertion'] == 'inline') {
  $width = (isset($tag['width']) && $tag['width'] > 0) ? $tag['width'] : NULL;
  if($width) {
    $size = _contentbrowser_resize($default_width, $default_height, $tag['width']);
    $out = theme('amendor_electure_external_embed', $node, $size['width'], $size['height']);
  }
  else {
    $out = theme('amendor_electure_external_embed', $node);
  }
}
if(count($caption_data)) {
  $out .= "<span class='contentbrowser_caption'>" . implode("<br />", array_filter($caption_data)) . "</span>";
}

print $out;
?>
