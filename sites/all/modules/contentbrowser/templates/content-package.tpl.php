<?php

/**
 * @file
 * @ingroup contentbrowser
 * @brief
 *  Template file for rendering the type if insertion.
 *
 * Parameters available:
 *  - $node
 *  - $tag
 *  - $from_editor
 */

if($from_editor) {
  print t('Node with nid #%nid (%type) will be inserted here', array('%nid' => $node->nid, '%type' => $node->type));
  return;
}

print theme('ndla_package_view', $node->nid);