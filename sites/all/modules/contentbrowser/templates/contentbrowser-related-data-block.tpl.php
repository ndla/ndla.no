<?php
// $Id$
/**
 * @file
 * @ingroup contentbrowser
 */
 
print "<a id='contentbrowser_show_link' href='javascript:void(0)' onclick='contentbrowser_fetch($nid)'>" . t('Show information'). "</a>";
print "<a style='display: none;' id='contentbrowser_hide_link' href='javascript:void(0)' onclick='contentbrowser_hide_and_show()'>" . t('Hide information'). "</a>"; 
print "<br /><br />";
?>

<div style='display: none;' id='contentbrowser_inserted_nodes'>
</div>

<script type='text/javascript'>
function contentbrowser_fetch(node_id) {
  $.ajax({
    url: Drupal.settings.basePath + "contentbrowser/get_used_nodes/" + node_id,
    cache: 1,
    success: function(rendered_html){
      //Place the result in the 'title_description' div.
      $('#contentbrowser_inserted_nodes').html(rendered_html);
      $('#contentbrowser_inserted_nodes').slideDown('slow');
      $('#contentbrowser_show_link').fadeOut('slow');
      $('#contentbrowser_hide_link').fadeIn('slow');
    }
  });
}

function contentbrowser_hide_and_show() {
  $('#contentbrowser_show_link').fadeIn('slow');
  $('#contentbrowser_hide_link').fadeOut('slow');
  $('#contentbrowser_inserted_nodes').slideUp('slow');
}
</script>
