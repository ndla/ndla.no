<?php
/**
 * @file
 * @ingroup ndla2010
 * @brief
 *  Template for rendering begrep nodes, used by the content browser.
 * Params available:
 *  - $node: The node object
 *  - $tag: The decoded tag.
 *  - $attachment: FALSE or Array with file information.
 *  - $from_editor: TRUE if we are called from an editor. It is not safe to render embed-code, javascript etc...
 *  - $caption_data: The text under the image (not CCK fields)
 */

if(module_exists('ndla_content_translation')) {
  if(ndla_content_translation_enabled($node->type)) {
    $old_title = $node->title;
    ndla_content_translation_view($node, FALSE, FALSE, TRUE);
    $node->body = $node->content['body']['#value'];
    $node->teaser = $node->field_ingress[0]['safe'];
    //If the title isnt translated, use the title which has been entered by the user.
    if($old_title != $node->title && $old_title == $tag['link_text']) {
      $tag['link_text'] = $node->title;
    }
  }
}

print theme('contentbrowser_insertion', $node, $tag, $attachment, $from_editor);
if(!$from_editor && (strpos($tag['insertion'], 'lightbox') !== FALSE || $tag['link_type'] == 'lightbox' || $tag['insertion'] == 'link')) {
  $body = _ndla_content_purifier_purify_html($node->body, PURIFY_FOR_EMBED);
  $body = preg_replace('/(\r?\n)/', '', $body);
  $body = strip_tags($body);
  if(!empty($body)) {
    print "<span class='contentbrowser-tooltip'>" . strip_tags($body) . "</span>";
    drupal_add_js(drupal_get_path('module', 'contentbrowser') . "/js/contentbrowser-tooltip.js");
  }
}