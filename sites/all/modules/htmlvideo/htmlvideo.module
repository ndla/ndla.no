<?php
/**
 * Implementation of hook_theme().
 */
function htmlvideo_theme($existing, $type, $theme, $path) {
  return array(
    'htmlvideo' => array(
      'arguments' => array('light_node' => NULL, 'attributes' => NULL),
      'path' => $path .'/theme',
      'template' => 'htmlvideo',
    ),
  );
}

/**
 * Implementation of hook_node_info
 */
function htmlvideo_node_info() {
  return array(
    'htmlvideo' => array(
      'name' => t('Video (HTML)'),
      'module' => 'htmlvideo',
      'description' => t('An HTML Video (with poster). This is ideal for publishing videos that should be made available for most browsers.'),
    )
  );
}

/**
 * Implementation of hook_perm
 */
function htmlvideo_perm() {
  return array('create HTML videos', 'edit own HTML videos', 'edit any HTML videos', 'delete own HTML videos', 'delete any HTML videos');
}

/**
 * Implementation of hook_access().
 */
function htmlvideo_access($op, $node, $account) {
  switch ($op) {
    case 'create':
      if (user_access('create HTML videos', $account)) {
        return TRUE;
      }
      break;

    case 'update':
      if (user_access('edit any HTML videos', $account) || ($account->uid == $node->uid && user_access('edit own HTML videos', $account))) {
        return TRUE;
      }
      break;

    case 'delete':
      if (user_access('delete any HTML videos', $account) || ($account->uid == $node->uid && user_access('delete own HTML videos', $account))) {
        return TRUE;
      }
      break;
  }
}

/**
 * Implementation of hook_menu
 */
function htmlvideo_menu() {
  $items = array();
  $items['admin/settings/htmlvideo'] = array(
    'title' => 'HTML5 Videos',
    'description' => 'Configure the location of video files.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('htmlvideo_admin_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'htmlvideo.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_cron
 *
 * Delete old temp files.
 */
function htmlvideo_cron() {
  $path = file_directory_path() . '/' . variable_get('htmlvideo_default_path', 'htmlvideos') . '/temp';
  $files = file_scan_directory(file_create_path($path), '.*');
  foreach ($files as $file => $info) {
    if (time() - filemtime($file) > 60 * 60 * 6) {
      file_delete($file);
    }
  }
}

/**
 * Implementation of hook_file_download().
 */
function htmlvideo_file_download($filename) {
  $filepath = file_create_path($filename);
  $result = db_query("SELECT v.nid, f.filemime, f.filesize FROM {htmlvideo} v INNER JOIN {files} f ON v.fid = f.fid WHERE f.filepath = '%s'", $filepath);
  if ($file = db_fetch_object($result)) {
    $node = node_load($file->nid);
    if (node_access('view', $node)) {
      return array(
        'Content-Type: ' . mime_header_encode($file->filemime),
        'Content-Length: ' . (int) $file->filesize,
      );
    }
    return -1;
  }
}

/**
 * Implementation of hook_link.
 */
/* TODO: Add this in a later version
function htmlvideo_link($type, $node, $main = 0) {
  $links = array();

  if ($type == 'node' && $node->type == 'htmlvideo' && !$main) {
    // TODO: Add links to the video files here.
  }

  return $links;
}
 *
 */

/**
 * Implementation of hook_form().
 */
function htmlvideo_form(&$node, $form_state) {
  _htmlvideo_check_settings();

  _htmlvideo_maintain_session_variables();

  $type = node_get_types('type', $node);

  $form['#validate'][] = 'htmlvideo_form_validate';
  $form['#submit'][] = 'htmlvideo_form_submit';

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#default_value' => $node->title,
  );

  $form['#attributes'] = array('enctype' => 'multipart/form-data');

  $form['htmlvideos'] = array(
    '#type' => 'fieldset',
    '#title' => 'Video',
    //'#tree' = TRUE, CAN'T USE TREE BECAUSE OF A BUG IN DRUPAL CORE
  );
  $form['htmlvideos']['htmlvideo_width'] = array( // Prefixes with htmlvideo to avoid namespace issues...
    '#type' => 'textfield',
    '#title' => t('Width in pixels'),
    '#size' => 4,
    '#required' => TRUE,
    '#description' => t('Specify the width in pizels to be used when this video is embedded.'),
    '#default_value' => isset($node->htmlvideo_width) ? $node->htmlvideo_width : '',
  );
  $form['htmlvideos']['htmlvideo_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height in pixels'),
    '#size' => 4,
    '#required' => TRUE,
    '#description' => t('Specify the height in pizels to be used when this video is embedded.'),
    '#default_value' => isset($node->htmlvideo_height) ? $node->htmlvideo_height : '',
  );

  $video_types = htmlvideo_get_types(TRUE);
  foreach ($video_types as $video_type => $data) {
    $form['htmlvideos']['new_' . $video_type] = array(
      '#type' => 'value',
      '#default_value' => isset($node->{'new_' . $video_type}) ? $node->{'new_' . $video_type} : FALSE,
    );
    $form['htmlvideos'][$video_type] = array(
      '#type' => 'fieldset',
      '#title' => $data['title'],
      '#description' => t('Upload a file or specify an external url'),
      '#collapsible' => TRUE,
      '#collapsed' => $video_type != 'htmlvideo_mp4',
    );
    $form['htmlvideos'][$video_type][$video_type] = array(
      '#type' => 'file',
      '#title' => $data['title'],
      '#size' => 40,
      '#description' => t('Select a file to upload. Extension(s): %extensions', array('%extensions' => $data['extensions'])),
    );
    $form['htmlvideos'][$video_type][$video_type . '_external_url'] = array(
      '#type' => 'textfield',
      '#title' => t('External url for %title', array('%title' => $data['title'])),
      '#size' => 40,
      '#description' => t('External url to the video file. Extension(s): %extensions', array('%extensions' => $data['extensions'])),
      '#default_value' => $node->{$video_type . '_external_url'},
    );
  }

  if ($type->has_body) {
    $form['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);
  }

  return $form;
}

/**
 * We don't want to keep any session variables around if this
 * is a new form...
 */
function _htmlvideo_maintain_session_variables() {
  if (!$_POST) {
    $types = htmlvideo_get_types();
    foreach ($types as $type => $data) {
      if (!empty($_SESSION[$type . '_upload'])) {
        unset($_SESSION[$type . '_upload']);
      }
    }
  }
}

/**
 * Get all file types that may be associated with these nodes...
 *
 * @param $include_titles
 *  Whether or not the titles should be included in the array
 * @return array $types
 *  Array with type as index
 */
function htmlvideo_get_types($include_titles = FALSE) {
  $types = array(
    'htmlvideo_mp4' => array(
      'type' => 'htmlvideo_mp4',
      'required' => TRUE,
      'extensions' => 'mp4',
    ),
    'htmlvideo_ogv' => array(
      'type' => 'htmlvideo_ogv',
      'required' => FALSE,
      'extensions' => 'ogv',
    ),
    'htmlvideo_webm' => array(
      'type' => 'htmlvideo_webm',
      'required' => FALSE,
      'extensions' => 'webm'
    ),
    'htmlvideo_poster' => array(
      'type' => 'htmlvideo_poster',
      'required' => FALSE,
      'extensions' => 'png gif jpg',
    ),
  );
  if ($include_titles) {
    $types['htmlvideo_mp4']['title'] = t('Mp4(H.264) Video');
    $types['htmlvideo_ogv']['title'] = t('OGV Video');
    $types['htmlvideo_webm']['title'] = t('WebM Video');
    $types['htmlvideo_poster']['title'] = t('Poster Image');
  }
  return $types;
}

/**
 * Validate the video files
 */
function htmlvideo_form_validate($form, &$form_state) {
  // Avoid blocking node deletion with missing files.
  if ($form_state['values']['op'] == t('Delete')) {
    return;
  }
  $types = htmlvideo_get_types(TRUE);
  foreach ($types as $type) {
    htmlvideo_validate_file($type, $form, $form_state);
  }
}

/**
 * Implementation of hook_validate
 */
function htmlvideo_validate($node) {
  if (!_htmlvideo_is_int($node->htmlvideo_width)) {
    form_set_error('htmlvideo_width', t('Width must be an integer greater than 0'));
  }
  if (!_htmlvideo_is_int($node->htmlvideo_height)) {
    form_set_error('htmlvideo_height', t('Height must be an integer greater than 0'));
  }
}

/**
 * Helper function to validate video files.
 *
 * @param array $type
 *  video type as defined in htmlvideo_get_types
 * @param array $form
 * @param array $form_state
 */
function htmlvideo_validate_file($type, &$form, &$form_state) {
  // Validate external url if any. We cannot validate extensions here
  // since the url might not lead directly to the file itself...
  $external = FALSE;
  if (!empty($form_state['values'][$type['type'] . '_external_url'])) {
    if (!valid_url($form_state['values'][$type['type'] . '_external_url'])) {
      form_set_error($type['type'] . '_external_url', t('Invalid url'));
    }
    else {
      $external = TRUE;
    }
  }

  $validators = array(
    "file_validate_extensions" => array($type['extensions']),
  );
  // New uploads need to be saved in temp in order to be viewable
  // during node preview.
  $temporary_file_path = file_create_path(file_directory_path() . '/' . variable_get('htmlvideo_default_path', 'htmlvideos') . '/temp');
  if ($file = file_save_upload($type['type'], $validators, $temporary_file_path)) {
    // We're good to go.
    $form_state['values']["new_{$type['type']}"] = TRUE;
    $_SESSION[$type['type'] . '_upload'] = $form_state['values'][$type['type']] = $file->filepath;
  }
  elseif (!isset($form['#node']->nid)
    && empty($form_state['values'][$type['type']])
    && $type['required']
    && !$external
    && empty($_SESSION[$type['type'] . '_upload'])) {

    form_set_error($type['type'], t('You must upload a @type file.', array('@type' => $type['title'])));
  }
}

/**
 * Helper function to prepare the form for beeing submitted.
 *
 * We use session to keep track of videofiles when form errors
 * occur or similar events. We put the videos into the
 * form_state here...
 */
function htmlvideo_form_submit($form, &$form_state) {
  $types = htmlvideo_get_types();
  foreach ($types as $type) {
    if (!empty($_SESSION[$type['type'] . '_upload'])) {
      $form_state['values'][$type['type']] = $_SESSION[$type['type'] . '_upload'];
      $form_state['values']['new_' . $type['type']] = TRUE;
      unset($_SESSION[$type['type'] . '_upload']);
    }
  }
}

/**
 * Implementation of hook_view
 */
function htmlvideo_view($node, $teaser = 0, $page = 0) {
  $node = node_prepare($node, $teaser);
  $node->content['htmlvideo'] = array(
    '#value' => htmlvideo_display($node),
    '#weight' => 0,
  );

  return $node;
}

/**
 * Implementation of hook_load().
 */
function htmlvideo_load(&$node) {
  $result = db_query("SELECT v.htmlvideo_width, v.htmlvideo_height, v.htmlvideo_type, v.external_url, f.filepath FROM {htmlvideo} v LEFT JOIN {files} f ON v.fid = f.fid WHERE v.nid = %d", $node->nid);
  while ($file = db_fetch_object($result)) {
    if ($file->filepath) {
      $node->{$file->htmlvideo_type} = file_create_path($file->filepath);
    }
    if (!empty($file->external_url)) {
      $node->{$file->htmlvideo_type . '_external_url'} = $file->external_url;
    }
    $node->htmlvideo_width = $file->htmlvideo_width;
    $node->htmlvideo_height = $file->htmlvideo_height;
  }
}

/**
 * Helperfunction used to copy content from sourcenode
 * to new node when a node is translated.
 *
 * @param stdClass $node
 */
function htmlvideo_translate($node) {
  $types = htmlvideo_get_types();
  foreach ($types as $type) {
    if (empty($node->{$type['type']})) {
      db_query("INSERT INTO {htmlvideo} (nid, fid, external_url, htmlvideo_type, htmlvideo_width, htmlvideo_height) SELECT %d, fid, external_url, htmlvideo_type, htmlvideo_width, htmlvideo_height FROM {htmlvideo} WHERE nid = %d AND htmlvideo_type = '%s'", $node->nid, $node->translation_source->nid, $type['type']);
      return;
    }
  }
}

/**
 * Implementation of hook_insert().
 */
function htmlvideo_insert($node) {
  if (!empty($node->translation_source)) {
    htmlvideo_translate($node);
  }
  $types = htmlvideo_get_types();
  foreach ($types as $type => $data) {
    $fid = 0;
    if (!empty($node->{$type})) {
      $fid = _htmlvideo_insert_file($node, $type, $node->{$type});
    }
    if ($fid > 0 || !empty($node->{$type . '_external_url'})) {
      _htmlvideo_write_type($node, $type, $fid);
    }
  }
}

function _htmlvideo_write_type($node, $type, $fid) {
  $htmlvideo = array(
    'fid' => $fid,
    'nid' => $node->nid,
    'htmlvideo_type' => $type,
    'htmlvideo_width' => $node->htmlvideo_width,
    'htmlvideo_height' => $node->htmlvideo_height,
    'external_url' => $node->{$type . '_external_url'},
  );
  drupal_write_record('htmlvideo', $htmlvideo);
}

/**
 * Moves temporary (working) files to the final directory and stores
 * relevant information in the files table
 */
function _htmlvideo_insert_file(&$node, $type, $path) {
  if (file_move($path, _htmlvideo_filename($path))) {
    // Update the node to reflect the actual filename, it may have been changed
    // if a file of the same name already existed.
    $node->{$type} = $path;

    $file = array(
      'uid' => $node->uid,
      'filename' => $size,
      'filepath' => $path,
      //'filemime' => $image_info['mime_type'],
      //'filesize' => $image_info['file_size'],
      'status' => FILE_STATUS_PERMANENT,
      'timestamp' => time(),
    );
    drupal_write_record('files', $file);
    return $file['fid'];
  }
  return FALSE;
}

/**
 * Implementation of hook_update().
 */
function htmlvideo_update(&$node) {
  $types = htmlvideo_get_types();
  foreach ($types as $type => $data) {
    $fid = 0;
    // If a file is uploaded it will be used and external urls are ignored.
    if ($node->{'new_' . $type}) {
      _htmlvideo_remove_type($node, $type);
      $fid = _htmlvideo_insert_file($node, $type, $node->{$type});
      _htmlvideo_write_type($node, $type, $fid);
    }
    else {
      $existing = db_fetch_object(db_query("SELECT fid, external_url FROM {htmlvideo} WHERE nid = %d AND htmlvideo_type = '%s'", $node->nid, $type));
      if ($existing) {
        if (!empty($node->{$type . '_external_url'}) || $existing->fid) {
          db_query(
            "UPDATE {htmlvideo}
            SET external_url = '%s'
            WHERE nid = %d AND htmlvideo_type = '%s'",
            $node->{$type . '_external_url'}, $node->nid, $type);
        }
        // We have no file and no external url for this type...
        else {
          _htmlvideo_remove_type($node, $type);
        }
      }
      // Do we have an external url?
      elseif (!empty($node->{$type . '_external_url'})) {
        _htmlvideo_write_type($node, $type, $fid);
      }
    }
  }
  db_query(
    "UPDATE {htmlvideo}
     SET htmlvideo_width = %d, htmlvideo_height = %d
     WHERE nid = %d",
    $node->htmlvideo_width, $node->htmlvideo_height, $node->nid
  );
}

/**
 * Helper function replacing old files with new ones on update
 *
 * @param stdClass $node
 * @param string $type
 *  video type
 */
function _htmlvideo_remove_type(&$node, $type) {
  // Remove existing file.
  $result = db_query("SELECT f.fid, f.filepath FROM {htmlvideo} v INNER JOIN {files} f ON v.fid = f.fid WHERE v.nid = %d AND v.htmlvideo_type='%s'", $node->nid, $type);
  while ($file = db_fetch_object($result)) {
    db_query("DELETE FROM {htmlvideo} WHERE nid = %d AND fid = %d", $node->nid, $file->fid);
    _htmlvideo_file_remove($file);
  }
}

/**
 * Implementation of hook_delete().
 */
function htmlvideo_delete($node) {
  $result = db_query('SELECT i.fid, f.filepath FROM {htmlvideo} i INNER JOIN {files} f ON i.fid = f.fid WHERE i.nid = %d', $node->nid);
  while ($file = db_fetch_object($result)) {
    db_query("DELETE FROM {htmlvideo} WHERE nid = %d AND fid = %d", $node->nid, $file->fid);
    _htmlvideo_file_remove($file);
  }
}

/**
 * Get light_node object for use with htmlvideo_display.
 *
 * @param int $nid
 * @return object
 *  light_node object
 */
function htmlvideo_get_light_node($nid) {
  $light_node = new stdClass();
  $light_node->nid = $nid;
  htmlvideo_load($light_node);
  return $light_node;
}

/**
 * Display a video node
 *
 * @param object $light_node
 *  Full or partial node object
 * @param array $attributes
 *  Attributes used when embedding the video. The following attributes are supported:
 *   - class (string with classes separated by whitespace)
 *   - controls (true by default, may be set to false)
 *   - autoload (true by default, may be set to false)
 *   - width (in pixels)
 *   - height (in pixels)
 * @return <type>
 */
function htmlvideo_display($light_node, $attributes = array()) {
  // Add videojs
  drupal_add_css(drupal_get_path('module', 'htmlvideo') . '/video-js/video-js.css');
  drupal_add_js(drupal_get_path('module', 'htmlvideo') . '/video-js/video.js');

  // Setup videojs
  drupal_add_js(drupal_get_path('module', 'htmlvideo') . '/theme/htmlvideo.js');

  return theme('htmlvideo', $light_node, $attributes);
}

/**
 * Preprocess variables for the htmlvideo template
 */
function template_preprocess_htmlvideo(&$vars) {
  $vars['attributes']['class'] = "htmlvideo" . (isset($vars['attributes']['class']) ? ' ' . $vars['attributes']['class'] : '');
  $vars['attributes']['extra_video_attributes'] = '';

  // controls and preload are on by default, but may be unset in $attributes...
  $vars['attributes']['extra_video_attributes'] .= isset($vars['attributes']['controls']) && !$vars['attributes']['controls']  ? '' : ' controls="controls"';
  $vars['attributes']['extra_video_attributes'] .= isset($vars['attributes']['preload']) && !$vars['attributes']['preload']  ? '' : ' preload="preload"';

  // autoplay is off by default, but may be set to on in $attributes...
  $vars['attributes']['extra_video_attributes'] .= isset($vars['attributes']['autoplay']) && $vars['attributes']['autoplay']  ? ' autoplay="autoplay"' : '';


  if (!empty($vars['light_node']->poster)) {
    $vars['attributes']['extra_video_attributes'] .= ' poster="' . $light_node->poster . '"';
  }
  $types = htmlvideo_get_types();
  foreach ($types as $type => $data) {
    if (!empty($vars['light_node']->{$type}) || !empty($vars['light_node']->{$type . '_external_url'})) {
      $url = !empty($vars['light_node']->{$type . '_external_url'})
        ? $vars['light_node']->{$type . '_external_url'}
        : file_create_url($vars['light_node']->{$type});
      $vars['light_node']->{$type . '_url'} = $url;
    }
  }
  $vars['attributes']['width'] = isset($vars['attributes']['width']) ? $vars['attributes']['width'] : $vars['light_node']->htmlvideo_width;
  $vars['attributes']['height'] = isset($vars['attributes']['height']) ? $vars['attributes']['height'] : $vars['light_node']->htmlvideo_height;
}

/**
 * Verify the video module.
 */
function _htmlvideo_check_settings() {
  // File paths
  $path = file_create_path(file_directory_path() . '/' . variable_get('htmlvideo_default_path', 'htmlvideos'));
  $temp_path = $path . '/temp';

  if (!file_check_directory($path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
    return FALSE;
  }
  if (!file_check_directory($temp_path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Creates a video filename.
 *
 * @param $filepath
 *   The full path and filename of the original file,relative to Drupal
 *   root, eg 'sites/default/files/htmlvideos/myvideo.ogv'.
 *
 * @return
 *   A full path and filename.
 */
function _htmlvideo_filename($filepath, $temp = FALSE) {
  // Get default path for a new file.
  $path = file_directory_path() . '/' . variable_get('htmlvideo_default_path', 'htmlvideos');
  if ($temp) {
    $path .= '/temp';
  }
  /*
   * We remove problematic charecters to avoid Drupal core bugs.
   * If transliteration is used it should override the changes made below.
   */
  $filename = preg_replace('![^0-9A-Za-z_.-]!', '', basename($filepath));

  return file_create_path($path . '/' . $filename);
}



/**
 * Remove file if no other node references it.
 *
 * @param $file
 *   An object representing a table row from {files}.
 */
function _htmlvideo_file_remove($file) {
  if (!db_result(db_query("SELECT COUNT(*) FROM {htmlvideo} WHERE fid = %d", $file->fid))) {
    file_delete(file_create_path($file->filepath));
    db_query('DELETE FROM {files} WHERE fid = %d', $file->fid);
  }
}

/**
 * Helper function used when validating integers.
 *
 * @param $value
 *   The value to be validated.
 * @param $min
 *   The minimum value $value is allowed to be.
 * @param $max
 *   The maximum value $value is allowed to be.
 *
 * @return
 *   TRUE if integer in the allowed range. FALSE otherwise.
 */
function _htmlvideo_is_int($value, $min = 1, $max = NULL) {
  $to_return = ((string)$value === (string)(int)$value);
  // $value is not an integer.
  if (!$to_return)
    return FALSE;
  // $value is too small.
  if ($value < $min)
    return FALSE;
  // $value is too big.
  if (isset($max)) {
    if ($value > $max)
      return FALSE;
  }
  // $value is an integer in the allowed range.
  return TRUE;
}