/**
 * @file
 *  exercise.js javascript file
 *  The main javascript file for the exercise planner.
 */
 Drupal.behaviors.exercise_auth = function(context) {
   if(typeof ndla_auth == 'undefined') {
     return;
   }
   ndla_auth(function(auth) {     
     if(auth.authenticated()) {
       
       // Set user name
       var logout = "<a class='logout-url' href='" + auth.logout_url(window.location.href) + "'>(" + Drupal.t('Logout') + ")</a>";
       $('#sub-header .name').html(auth.display_name() + " " + logout);
     }
   });
};

var Exercise = Exercise || {};
Exercise.calender = Exercise.calender || {};
Exercise.dialog = Exercise.dialog || {};
Exercise.workout = Exercise.workout || {};
Exercise.library = Exercise.library || {};
Exercise.lightbox = Exercise.lightbox || {};
Exercise.datePicker = Exercise.datePicker || {};
Exercise.storage = Exercise.storage || {};

/**
 * Initialize the exercise planner.
 */
Exercise.initialize = function (skipAccessControl) {
  // Hide "enable javascript text" and show loader.
  var $start = $('#start');
  $('span', $start).hide();
  $('img', $start).show();

  Exercise.$subHeader = $('#sub-header');
  Exercise.$header = $('h2', Exercise.$subHeader);
  
  $('.help', Exercise.$subHeader).click(function () {
    Exercise.lightbox.showHelp();
  });
  Exercise.$loadingBars = $('.loading .bar');
  Exercise.$widthTester = $('<div></div>').appendTo('body').hide().css('fontSize', '10pt');

  new JStorage('exercise', function (storage) {
    Exercise.storage.general = storage;
    
    // Access control
    if (skipAccessControl === undefined) {
      if (Drupal.settings['exercise']['jstorageAccessControl'] && !storage.isLoggedIn()) {
        $('img', $start).hide();
        Exercise.$header.html(Drupal.t('Not logged in'));
        var $loginFrame = $('#login');
        $('.continue', $loginFrame).click(function () {
          Exercise.initialize(true);
        });
        $loginFrame.show();
        return;
      }
    }
    else {
      $('#login').hide();
    }

    // Initialize the different functionalities
    Exercise.calender.initialize();
    Exercise.dialog.initialize();
    Exercise.workout.initialize();
    Exercise.library.initialize();
    Exercise.lightbox.initialize();
    Exercise.datePicker.initialize();

    $('.messages').each(function () { 
      Exercise.dialog.addCloseButton(); 
    });
    $('.tip').mouseover(Exercise.dialog.showTip).mouseout(Exercise.dialog.hideTip);
    $('.help').show();

    if ($.browser.msie && $.browser.version < 9) {
      $('.table-top').addClass('ie');
      $('#workout .table-wrap').addClass('ie');
    }
  
    $(window).bind('beforeunload', function () {
      if (Exercise.leaveWarning !== undefined && Exercise.leaveWarning === true) {
        return Drupal.t("All of your work haven't been saved. Do you still wish to leave?");
      }
    }).resize(function () {
      Exercise.dialog.position();
      Exercise.library.scrollBar();
    });

    // Hide loader
    $('img', $start).hide();
    Exercise.calender.show();
  });
};

/**
 * Fetches the storage object for the specified namespace, queues callbacks until the fetching is complete.
 */
Exercise.storage.getStorage = function (namespace, callback) {
  if (Exercise.storage[namespace] == undefined) {
    var callbacks = namespace + '_callbacks',
        fetching = namespace + '_fetching';
    
    // Add callback to queue.
    if (Exercise.storage[callbacks] == undefined) {
      Exercise.storage[callbacks] = [];
    }
    Exercise.storage[callbacks].push(callback);
    
    if (Exercise.storage[fetching] == undefined) {
      // Fetch namespace
      Exercise.storage[fetching] = true;
      new JStorage(namespace, function (storage) {
        Exercise.storage[namespace] = storage;
        
        // Run callback queue.
        for (var i = 0; i < Exercise.storage[callbacks].length; i++) {
          Exercise.storage.getStorage(namespace, Exercise.storage[callbacks][i]);
        }
      }, {
        profile: true,
        browser: false
      }); 
    }
  }
  else {
    callback(Exercise.storage[namespace]);
  }
}

/**
 * Return a workout or a set of workouts.
 */
Exercise.storage.getWorkout = function (date, callback, id) {
  var year = date.getFullYear(),
  month = date.getMonth(),
  day = date.getDate() - 1,
  namespace = 'exercise_workouts_' + year;

  Exercise.storage.getStorage(namespace, function (storage) {
    var data = storage.get(month);
    if (data != null && data[day] != undefined) {
      if (id == undefined) {
        return callback(data[day]);
      }
      if (data[day][id] != undefined) {
        return callback(data[day][id]);
      }
    }
    return callback(null);
  });
}

/**
 * Save or delete a workout.
 */
Exercise.storage.setWorkout = function (workout, date, callback, id) {
  var year = date.getFullYear(),
  month = date.getMonth(),
  day = date.getDate() - 1,
  result = null,
  namespace = 'exercise_workouts_' + year;
  
  Exercise.storage.getStorage(namespace, function (storage) {
    var data = storage.get(month);
    if (workout == null) {
      if (data != null && data[day] != undefined && data[day][id] != undefined) {
        if (data[day].length == 1) {
          delete data[day];
        }
        else {
          data[day].splice(id, 1);
        }
      }
    }
    else {
      data = data || {};
      data[day] = data[day] || [];
      if (id == undefined) {
        data[day].push(workout);
        result = data[day].length - 1;
      }
      else {
        data[day][id] = workout;
        result = id;
      }
    }
  
    if (!Exercise.objectSize(data)) {
      storage.del(month);
    }
    else {
      storage.set(month, data);
    }
    if (callback != undefined) {
      callback(result);
    }
  }); 
}

/**
 * Return a period or a set of periodes
 */
Exercise.storage.getPeriod = function (year, callback, id) {
  var namespace = 'exercise_periodics_' + year;
  
  Exercise.storage.getStorage(namespace, function (storage) {
    var periodes = storage.get('periodics');

    // Makes sure dates are dates
    for (var period in periodes) {
      var d;
      if (typeof periodes[period].to == 'string') {
        d = periodes[period].to.split('T')[0].split('-');
        periodes[period].to = new Date(d[0], parseInt(d[1], 10) - 1, parseInt(d[2], 10) + 1);
      }
      if (typeof periodes[period].from == 'string') {
        d = periodes[period].from.split('T')[0].split('-');
        periodes[period].from = new Date(d[0], parseInt(d[1], 10) - 1, parseInt(d[2], 10) + 1);
      }
    }

    if (periodes != null) {
      if (id == undefined) {
        return callback(periodes);
      }
      else {
        return callback(periodes[id]);
      }
    }
    return callback(null);
  });
}

/**
 * Save or delete a period.
 */
Exercise.storage.setPeriod = function (periodic, year, id, oldId) {
  var namespace = 'exercise_periodics_' + year;
    
  Exercise.storage.getStorage(namespace, function (storage) {
    var data = storage.get('periodics');
    if (periodic == null) {
      if (data != null && data[id] != undefined) {
        delete data[id];
      }
    }
    else {
      data = data || {};
      if (oldId != undefined && data[oldId] != undefined) {
        delete data[oldId];
      }
      data[id] = periodic;    
    }

    if (!Exercise.objectSize(data)) {
      storage.del('periodics');
    }
    else {
      storage.set('periodics', data);
    }
  });  
}

/**
 * Initialize the calender.
 */
Exercise.calender.initialize = function () {
  Exercise.calender.$wrapper = $('#calender');
  Exercise.calender.$buttons = $('#calender-buttons', Exercise.calender.$wrapper);
  Exercise.calender.months = [Drupal.t('January'),Drupal.t('February'),Drupal.t('March'),Drupal.t('April'),Drupal.t('May'),Drupal.t('June'),Drupal.t('July'),Drupal.t('August'),Drupal.t('September'),Drupal.t('October'),Drupal.t('November'),Drupal.t('December')];

  // Bind triggers
  $('.next', Exercise.$subHeader).click(function () { 
    Exercise.calender.next();
  });
  $('.previous', Exercise.$subHeader).click(function () { 
    Exercise.calender.previous();
  });
  $('.workout', Exercise.calender.$buttons).click(function () { 
    Exercise.dialog.workoutHelp(); 
  });
  $('.periodic', Exercise.calender.$buttons).click(function () { 
    Exercise.dialog.period($(this)); 
  });
  $('.download', Exercise.calender.$buttons).click(function () { 
    Exercise.dialog.download(); 
  });

  // Set current year and month
  var date = new Date();
  Exercise.calender.currentYear = date.getFullYear();
  Exercise.calender.currentMonth = date.getMonth();
}

/**
 * Show previous calender month.
 */
Exercise.calender.previous = function () {
  if (Exercise.calender.currentMonth == 0) {
    Exercise.calender.currentYear--;
    Exercise.calender.currentMonth = 11;
  }
  else {
    Exercise.calender.currentMonth--;
  }
  Exercise.calender.show();
}

/**
 * Show next calender month.
 */
Exercise.calender.next = function () {
  if (Exercise.calender.currentMonth == 11) {
    Exercise.calender.currentYear++;
    Exercise.calender.currentMonth = 0;
  }
  else {
    Exercise.calender.currentMonth++;
  }
  Exercise.calender.show();
}

/**
 * Display the calender.
 */
Exercise.calender.show = function () {
  $('.buttons', Exercise.$subHeader).addClass('display');
  Exercise.helpPage = 'calender';
  Exercise.dialog.close(); // Close any open dialogs
  Exercise.calender.$wrapper.show();
  Exercise.$header.html(Exercise.calender.months[Exercise.calender.currentMonth] + ' ' + Exercise.calender.currentYear);
  if (Drupal.settings['exercise']['examplePeriodicPlan'] != '') {
    $('.example-plan', Exercise.$subHeader).show().unbind('click').click(function () {
      Exercise.lightbox.showExample(Drupal.t('Example periodic plan'), Drupal.settings['exercise']['examplePeriodicPlan']);
    });
  }
  
  // Clean up last month
  $('td', Exercise.calender.$wrapper).removeClass('day today periodic prev').html('');

  // Find first day in the calender
  var $rows = $('tbody tr', Exercise.calender.$wrapper),
      $prevDay, 
      $day = $('td:first', $rows[0]),
      start = 7 - (parseInt(Drupal.settings['exercise']['weekStart'], 10) - new Date(Exercise.calender.currentYear, Exercise.calender.currentMonth, 1).getDay());
      
  if (start > 6) {
    start -= 7;
  }
  
  var date = new Date(Exercise.calender.currentYear, Exercise.calender.currentMonth - 1, new Date(Exercise.calender.currentYear, Exercise.calender.currentMonth, 0).getDate() - start + 1);
  
  while ($day.is('td')) {
    Exercise.calender.processDay($day, date);

    $prevDay = $day;
    $day = $day.next();
    if (!$day.is('td') && date.getMonth() == Exercise.calender.currentMonth) {
      $day = $prevDay.parent().next().children(':first'); // Try next week
    }
    date.setDate(date.getDate() + 1);
  }

  // Hide empty rows
  Exercise.datePicker.hideEmptyRows($rows);

  // Render periodic plans
  Exercise.storage.getPeriod(Exercise.calender.currentYear, function (periodics) {
    for (var periodic in periodics) {
      Exercise.calender.renderPeriod(periodic, periodics[periodic]);
    }
  });
}

/**
 * Process each day in the calender.
 */
Exercise.calender.processDay = function ($day, date) {
  $day.addClass('day' + (date.getMonth() != Exercise.calender.currentMonth ? ' prev' : '') + (Exercise.datePicker.isToday(date) ? ' today' : '')).data('date', new Date(date.getFullYear(), date.getMonth(), date.getDate())).html('<div class="header"><div class="date">' + date.getDate() + '</div><div class="text"></div></div>').mouseover(function () {
    if ($('.workout', this).length < 3) {
      $('.create', this).show();
    }
  }).mouseout(function () { 
    $('.create', this).hide();
  });
  $('<div title="' + Drupal.t('Create workout') + '" class="create"><div class="round-left"></div><div class="round-right"></div><span>+</span>' + Drupal.t('Create workout') + '</div>').appendTo($day).click(function () {
    Exercise.dialog.workout($(this));
  });

  Exercise.storage.getWorkout(date, function (workouts) {
    if (workouts != undefined) {
      // Add workouts
      for (var n = 0; n < workouts.length; n++) {
        Exercise.calender.renderWorkout($day, n, workouts[n]);
      }
    }
  });
}

/**
 * Renders a workout in the calender on the day it belong to
 */
Exercise.calender.renderWorkout = function ($day, id, workout) {
  var $create = $('.create', $day),
  $workout = $('<div title="' + workout.title + '" class="workout' + (workout.exercises.length || (workout.objective != '' && workout.objective != undefined) ? ' exercises' : '') + '"><div class="round-left"></div><div class="round-right"></div>' + Exercise.overflow(workout.title, 106) + '</div>').data('id', id).click(function () {
    Exercise.dialog.workout($(this));
  }).insertBefore($create);
  
  $('<span title="' + Drupal.t('Edit workout plan') + '"></span>').click(function () {
    Exercise.calender.$clicked = $(this).parent();
    $('.create', $day).hide();
    Exercise.calender.workout();
    return false;
  }).appendTo($workout);
  if ($('.workout', $day).length == 3) {
    $create.hide();
  }
}

/**
 * Copy selected workout.
 */
Exercise.calender.copyWorkout = function () {
  Exercise.storage.getWorkout(Exercise.calender.$clicked.parent().data('date'), function (workout) {
    Exercise.calender.workoutCopy = workout;
    if (Exercise.calender.copyTipDisplayed == undefined) {
      Exercise.calender.copyTipDisplayed = true;
      Exercise.dialog.copyTip();
    }
    else {
      Exercise.dialog.close();
    }
  }, Exercise.calender.$clicked.data('id'));
}

/**
 * Paste copied workout
 */
Exercise.calender.pasteWorkout = function () {
  var $day = Exercise.calender.$clicked.parent(),
  date = $day.data('date'),
  copy = $.extend({}, Exercise.calender.workoutCopy),
  id = Exercise.storage.setWorkout(copy, date, function (id) {
    Exercise.calender.renderWorkout($day, id, copy);
  });

  Exercise.dialog.close();
}

/**
 * Create a new workout.
 */
Exercise.calender.createWorkout = function (displayTip, inStore) {
  var $day = Exercise.calender.$clicked.parent(),
      date = $day.data('date'),
      id = Exercise.calender.$clicked.data('id');

  if (id == undefined) {
    inStore = null;
  }
  else if (inStore == undefined) {
    Exercise.storage.getWorkout(date, function (workout) {
      Exercise.calender.createWorkout(displayTip, workout);
    }, id);
    return;
  }
  
  var newTitle = $('input', Exercise.dialog.$wrapper).val(),
      workout = {
        title: newTitle == undefined ? inStore.title : newTitle ,
        objective: inStore != null ? inStore.objective : '',
        exercises: inStore != null ? inStore.exercises : []
      };
  
  if (id != undefined) {
    Exercise.storage.setWorkout(workout, date, undefined, id);
    var $workout = $('.workout:eq(' + id + ')', $day).html('<div class="round-left"></div><div class="round-right"></div>' + Exercise.overflow(workout.title, 106)).attr('title', workout.title);
    $('<span title="' + Drupal.t('Edit workout plan') + '"></span>').click(function () {
      Exercise.calender.$clicked = $(this).parent();
      $('.create', $day).hide();
      Exercise.calender.workout();
      return false;
    }).appendTo($workout);
  }
  else {
    Exercise.storage.setWorkout(workout, date, function (id) {
      Exercise.calender.renderWorkout($day, id, workout);
    });
  }
  
  if (Exercise.calender.workoutTipDisplayed == undefined && (displayTip == undefined || displayTip)) {
    Exercise.calender.workoutTipDisplayed = true;
    Exercise.dialog.workoutTip();
  }
  else {
    Exercise.dialog.close();
  }
}

/**
 * Generates a short string with ellipsis if the string exceeds max length.
 */
Exercise.overflow = function (string, maxLength, ellipsis) {
  if (ellipsis == undefined || ellipsis == null) {
    ellipsis = '(...)';
  }
  Exercise.$widthTester.html(string);
  if (Exercise.$widthTester.width() > maxLength) {
    string = string.substr(0, string.length - 2) + ellipsis;
  }
  while (Exercise.$widthTester.width() > maxLength) {
    string = string.substr(0, string.length - ellipsis.length - 1) + ellipsis;
    Exercise.$widthTester.html(string);
  }
  return string;
}

/**
 * Delete selected workout.
 */
Exercise.calender.deleteWorkout = function () {
  if (!confirm(Drupal.t('Are you sure that you wish to delete this workout?'))) {
    return;
  }

  var $day = Exercise.calender.$clicked.parent(),
      date = $day.data('date'),
      id = Exercise.calender.$clicked.data('id'),
      $workouts = $('.workout', $day);

  // Remove from html and array
  $workouts.eq(id).remove();
  Exercise.storage.setWorkout(null, date, undefined, id);

  // Update id for other workouts
  $workouts.each(function () {
    var $this = $(this),
    wid = $this.data('id');
    if (wid > id) {
      $this.data('id', wid - 1);
    }
  });
  Exercise.dialog.close();
}

Exercise.calender.validatePeriodYears = function (i, years, period, id, callback) {
  Exercise.storage.getPeriod(years[i], function (periods) {
    for (var p in periods) {
      if (p != id) {
        var collision = true;
        if (period.from < periods[p].from) {
          if (period.to < periods[p].from) {
            collision = false;
          }
        }
        else if (period.from > periods[p].to) {
          collision = false;
        }
        if (collision) {
          Exercise.dialog.error($('.button.from', Exercise.dialog.$wrapper), Drupal.t('Period overlaps with another.'));
          return;
        }
      }
    }
    i++;
    if (i < years.length) {
      Exercise.calender.validatePeriodYears(0, years, period, id, callback);
    }
    else {
      callback();
    }
  });
}

/**
 * Validates a period.
 */
Exercise.calender.validatePeriod = function (years, period, id, callback) {
  if (period.from > period.to) {
    Exercise.dialog.error($('.button.to', Exercise.dialog.$wrapper), Drupal.t('To date must be after from date.'));
    return;
  }

  Exercise.calender.validatePeriodYears(0, years, period, id, callback);
}

Exercise.calender.createPeriodInternal = function (years, period, id) {
  var newId = Exercise.datePicker.formatDate(period.from.getFullYear(), period.from.getMonth(), period.from.getDate(), 'ymd');
  for (var i = 0; i < years.length; i++) {
    if (years[i] >= period.from.getFullYear() && years[i] <= period.to.getFullYear()) {
      Exercise.storage.setPeriod(period, years[i], newId, id);
    }
    else {
      Exercise.storage.setPeriod(null, years[i], id);
    }
  }

  Exercise.calender.renderPeriod(newId, period);

  if (Exercise.calender.periodicTipDisplayed == undefined) {
    Exercise.calender.periodicTipDisplayed = true;
    Exercise.dialog.periodTip();
  }
  else {
    Exercise.dialog.close();
  }
}

/**
 * Create periodic plan.
 */
Exercise.calender.createPeriod = function () {
  var period = {
    objective: $('.objective', Exercise.dialog.$wrapper).val(),
    from: $('.button.from', Exercise.dialog.$wrapper).data('date'),
    to: $('.button.to', Exercise.dialog.$wrapper).data('date')
  },
  id = Exercise.calender.$clicked.data('id'),
  years = Exercise.calender.findPeriodYears(period.from, period.to);

  Exercise.calender.validatePeriod(years, period, id, function () {
    if (id != null) {
      Exercise.calender.wipePeriod(id);
      Exercise.storage.getPeriod(Exercise.calender.currentYear, function (oldPeriod) {
        years = Exercise.calender.combineYears(period, oldPeriod);
        Exercise.calender.createPeriodInternal(years, period, id);
      }, id);
    }
    else {
      Exercise.calender.createPeriodInternal(years, period, id);
    }
  });
}

/**
 * Find all the years this period is in.
 */
Exercise.calender.findPeriodYears = function (from, to) {
  var startYear = from.getFullYear();
  var endYear = to.getFullYear();
  var years = [startYear];
  for (var year = startYear + 1; year <= endYear; year++) {
    years.push(year);
  }
  return years;
}

/**
 * Find all the years from to periods combined
 */
Exercise.calender.combineYears = function (period1, period2) {
  var from = period1.from < period2.from ? period1.from : period2.from;
  var to = period1.to > period2.to ? period1.to : period2.to;
  return Exercise.calender.findPeriodYears(from, to);
};

/**
 * Delete selected period.
 */
Exercise.calender.deletePeriod = function (period, id) {
  if (confirm(Drupal.t('Are your sure you wish to delete this periodic plan?'))) {
    var years = Exercise.calender.findPeriodYears(period.from, period.to)
    for (var i = 0; i < years.length; i++) {
      Exercise.storage.setPeriod(null, years[i], id);
    }
    Exercise.calender.wipePeriod(id);
    Exercise.dialog.close();
  }
}

/**
 * Wipe rendered period from the calender.
 */
Exercise.calender.wipePeriod = function (id) {
  $('.day', Exercise.calender.$wrapper).each(function () {
    var $this = $(this),
    $header = $('.header', $this),
    periodicId = $header.data('id');
    if (periodicId == id) {
      // Remove
      $this.removeClass('periodic')
      $header.data('id', null).unbind('click').attr('title', null).children('.text').html('');
      $('.arrow-left, .arrow-right, .round-left, .round-right', $header).remove();
    }
  });
}

/**
 * Render a periodic plan in the calender.
 */
Exercise.calender.renderPeriod = function (id, period) {
  var month = Exercise.calender.determinePeriodThisMonth(period);
  if (month != null) {
    var $days = $('.day');
    for (var i = month.start; i <= month.end; i++) {
      var $day = $days.eq(i).addClass('periodic'),
          $header = $('.header', $day).data('id', id).click(function () { 
            Exercise.dialog.period($(this)); 
          }).attr('title', period.objective),
          date = $day.data('date');
          
      if (date.getDate() == period.from.getDate() && date.getMonth() == period.from.getMonth() && date.getFullYear() == period.from.getFullYear()) {
        $header.prepend('<div class="round-left"></div>');
      }
      if (date.getDate() == period.to.getDate() && date.getMonth() == period.to.getMonth() && date.getFullYear() == period.to.getFullYear()) {
        $header.prepend('<div class="round-right"></div>');
      }
      if (i == month.start) {
        $('.text', $header).html(Exercise.overflow(period.objective, 114 - $('.date', $header).width() - (month.arrowLeft ? 16 : 0)));
        if (month.arrowLeft) {
          $header.prepend('<div class="arrow-left"></div>');
        }
      }
      else {
        if ($day.hasClass('week-start')) {
          $('.text', $header).html(Exercise.overflow(period.objective, 114 - $('.date', $header).width()));
        }
        if (i == month.end && month.arrowRight) {
          $header.prepend('<div class="arrow-right"></div>');
        }
      }
    }
  }
}

/**
 * Determine first and last day of a period in this month.
 */
Exercise.calender.determinePeriodThisMonth = function (period) {
  var month, first = $('.day:first', Exercise.calender.$wrapper).data('date'),
  last = $('.day:last', Exercise.calender.$wrapper).data('date');

  // Find the first and the last day we're going to color this month
  if (period.from <= first) {
    if (period.to >= first) {
      month = {
        arrowLeft: period.from < first ? true : false,
        start: 0,
        end: period.to >= last ? Exercise.calender.dateDiffInDays(last, first) : Exercise.calender.dateDiffInDays(period.to, first),
        arrowRight: period.to > last ? true : false
      }
    }
  }
  else if (period.from <= last) {
    month = {
      arrowLeft: false,
      start: Exercise.calender.dateDiffInDays(period.from, first),
      end: period.to >= last ? Exercise.calender.dateDiffInDays(last, first) : Exercise.calender.dateDiffInDays(period.to, first),
      arrowRight: period.to > last ? true : false
    }
  }
  return month;
}

/**
 * Return difference in days between date1 and date2.
 */
Exercise.calender.dateDiffInDays = function (date1, date2) {
  return Math.round((date1.getTime() - date2.getTime()) / 86400000);
}


Exercise.calender.getDateWorkout = function (date, periodTo, workouts, callback) {
  Exercise.storage.getWorkout(date, function (dateWorkouts) {
    if (dateWorkouts != undefined) {
      for (var i = 0; i < dateWorkouts.length; i++) {
        workouts.push(dateWorkouts[i]);
        workouts[workouts.length - 1].date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
      }
    }
    
    date.setDate(date.getDate() + 1);
    if (date <= periodTo) {
      Exercise.calender.getDateWorkout(date, periodTo, workouts, callback);
    }
    else {
      callback(workouts);
    }
  });
}

/**
 * Download selected periodic plan
 */
Exercise.calender.downloadPeriod = function () {
  Exercise.storage.getPeriod(Exercise.calender.currentYear, function (period) {
    var date = new Date(period.from.getFullYear(), period.from.getMonth(), period.from.getDate());
      
    // To find all workouts, first we loop through all the months for this period
    Exercise.calender.getDateWorkout(date, period.to, [], function (workouts) {
      if (workouts.length == 0) {
        Exercise.dialog.downloadError();
        return;
      }
  
      $('#edit-json').val(JSON.stringify({
        period: period,
        workouts: workouts,
        name: Exercise.storage.general.getUserName()
      }));

      $('form').submit();
      Exercise.dialog.downloadTip();
    });
  }, $('#periodic', Exercise.dialog.$wrapper).val());
}

/**
 * Show selected workout plan.
 */
Exercise.calender.workout = function () {
  Exercise.calender.$wrapper.hide();
  Exercise.workout.show();
  Exercise.calender.createWorkout(false);
}

/**
 * Initialize dialog.
 */
Exercise.dialog.initialize = function () {
  Exercise.dialog.$wrapper = $('#dialog');
  Exercise.dialog.$buttons = $('#dialog-buttons', Exercise.dialog.$wrapper);
  $('.close', Exercise.dialog.$wrapper).click(Exercise.dialog.close);
}

/**
 * Position and show dialog.
 */
Exercise.dialog.show = function () {
  Exercise.dialog.position();
  Exercise.dialog.$wrapper.fadeIn(100);
}

/**
 * Set the position of the dialog.
 */
Exercise.dialog.position = function () {
  if (Exercise.dialog.$anchor == undefined) {
    Exercise.dialog.$wrapper.css({
      position: 'fixed',
      left: '50%',
      top: '50%',
      marginTop: '-' + (Exercise.dialog.$wrapper.height() / 2) + 'px'
    });
    $('.header, .footer', Exercise.dialog.$wrapper).removeClass('pointer');
  }
  else {
    Exercise.dialog.$wrapper.removeAttr('style');
    var offset = Exercise.dialog.$anchor.offset();
    if (offset.top < Exercise.dialog.$wrapper.height()) {
      Exercise.dialog.$wrapper.css({
        left: offset.left + (Exercise.dialog.$anchor.width() / 2),
        top: offset.top + Exercise.dialog.$anchor.height()
      });
      $('.footer', Exercise.dialog.$wrapper).removeClass('pointer');
      $('.header', Exercise.dialog.$wrapper).addClass('pointer');
    }
    else {
      Exercise.dialog.$wrapper.css({
        left: offset.left + (Exercise.dialog.$anchor.width() / 2),
        top: offset.top - Exercise.dialog.$wrapper.height()
      });
      $('.header', Exercise.dialog.$wrapper).removeClass('pointer');
      $('.footer', Exercise.dialog.$wrapper).addClass('pointer');
    }
  }
}

/**
 * Close dialog.
 */
Exercise.dialog.close = function (callback) {
  if (Exercise.dialog.$wrapper.is(':visible')) {
    Exercise.dialog.$anchor = undefined;
    Exercise.leaveWarning = false;
    Exercise.dialog.$wrapper.fadeOut(100, function () {
      $('.body', Exercise.dialog.$wrapper).children(':not(.close)').not(Exercise.dialog.$buttons).remove();
      $('.ok', Exercise.dialog.$buttons).unbind('click');
      if (typeof callback == 'function') {
        callback();
      }
    });
    return true;
  }
  else if(typeof callback == 'function') {
    callback();
  }
  return false;
}

/**
 * Add close button to error message.
 */
Exercise.dialog.addCloseButton = function ($message) {
  var $this = $message != undefined ? $message : $(this),
  $inner = $('<div>' + $this.html() + '</div>').css({
    paddingTop: $this.css('paddingTop'),
    paddingLeft: $this.css('paddingLeft'),
    paddingBottom: $this.css('paddingBottom'),
    paddingRight: $this.css('paddingRight')
  });
  $this.css('padding', '0 0 0 0').html('').append($inner);
  $('<div class="close"></div>').prependTo($inner).click(function () {
    $(this).parent().parent().slideUp(200, function () {
      $(this).remove();
    });
  });
}

/**
 * Display copy tip dialog.
 */
Exercise.dialog.copyTip = function () {
  Exercise.dialog.close(function () {
    $('<div>' + Drupal.t('You can now press "Create workout" on a day in the calender to paste your workout.') + '</div>').insertBefore(Exercise.dialog.$buttons);
    $('.ok', Exercise.dialog.$buttons).click(Exercise.dialog.close);
    Exercise.dialog.show();
  });
}

/**
 * Display periodic tip dialog.
 */
Exercise.dialog.periodTip = function () {
  Exercise.dialog.close(function () {
    $('<div><p>' + Drupal.t('You can now press "Create workout" on a day in your periodic plan to add a workout.') + '</p><p>' + Drupal.t('Examples of workouts: Football training, strength training, aerobic etc.') + '</p></div>').insertBefore(Exercise.dialog.$buttons);
    $('.ok', Exercise.dialog.$buttons).click(Exercise.dialog.close);
    Exercise.dialog.show();
  });
}

/**
 * Display workout tip dialog.
 */
Exercise.dialog.workoutTip = function () {
  Exercise.dialog.close(function () {
    $('<div><p>' + Drupal.t('You can plan this workout in detail by pressing on the workout bar and then on "Create workout plan" in the dialog that appears.') + '</p></div>').insertBefore(Exercise.dialog.$buttons);
    $('.ok', Exercise.dialog.$buttons).click(Exercise.dialog.close);
    Exercise.dialog.show();
  });
}

/**
 * Display dialog for downloading periodic plans.
 */
Exercise.dialog.download = function () {
  Exercise.dialog.close(function () {
    Exercise.storage.getPeriod(Exercise.calender.currentYear, function (periods) {
      var $options = $([]);
      
      for (var p in periods) {
        var month = Exercise.calender.determinePeriodThisMonth(periods[p]);
        if (month != null) {
          $options = $options.add('<option value="' + p + '">' + periods[p].objective + '</option>');
        }
      }

      if ($options.length) {
        $('<p>' + Drupal.t('Which periodic plan do you wish to download?') + '</p>').insertBefore(Exercise.dialog.$buttons);
        $('<select id="periodic"></select>').append($options).insertBefore(Exercise.dialog.$buttons);
        $('.ok', Exercise.dialog.$buttons).click(Exercise.calender.downloadPeriod);
      }
      else {
        $('<div>' + Drupal.t('There are no periodic plans you can download this month.') + '</div>').insertBefore(Exercise.dialog.$buttons);
        $('.ok', Exercise.dialog.$buttons).click(Exercise.dialog.close);
      }

      Exercise.dialog.show();
    });
  });
}

/**
 * Error dialog when downloading empty periodic plan.
 */
Exercise.dialog.downloadError = function () {
  Exercise.dialog.close(function () {
    $('<div>' + Drupal.t('The plan you are trying to download is empty. You must add some workouts to your plan before you can download it.') + '</div>').insertBefore(Exercise.dialog.$buttons);
    $('.ok', Exercise.dialog.$buttons).click(Exercise.dialog.close);
    Exercise.dialog.show();
  });
}

/**
 * Error dialog when downloading empty workout plan.
 */
Exercise.dialog.downloadWorkoutError = function () {
  Exercise.dialog.close(function () {
    $('<div>' + Drupal.t('The plan you are trying to download is empty. You must add some exercises to your plan before you can download it.') + '</div>').insertBefore(Exercise.dialog.$buttons);
    $('.ok', Exercise.dialog.$buttons).click(Exercise.dialog.close);
    Exercise.dialog.show();
  });
}

/**
 * Display dialog for adding workouts.
 */
Exercise.dialog.workout = function ($this) {
  if (Exercise.calender.$clicked != undefined && Exercise.calender.$clicked[0] == $this[0] && Exercise.dialog.close()) {
    return;
  }
  if ($this.is('span')) {
    Exercise.calender.$clicked = $this.parent();
  }
  else {
    Exercise.calender.$clicked = $this;
  }
  Exercise.dialog.close(function () {
    var date = Exercise.calender.$clicked.parent().data('date'),
    id = Exercise.calender.$clicked.data('id');

    var $input = $('<input value="' + (id == undefined ? Drupal.t('Title workout') : '') + '" type="text"/>').keypress(Exercise.disableSubmit).insertBefore(Exercise.dialog.$buttons);
    Exercise.storage.getWorkout(date, function (workout) { 
      if (workout != null) {
        $input.val(workout.title);
      }
    }, id);

    if (id != undefined) {
      $('<div class="link">' +  (Exercise.calender.$clicked.hasClass('exercises') ? Drupal.t('Edit workout plan') : Drupal.t('Create workout plan')) + '</div>').click(Exercise.calender.workout).appendTo($('<div></div>').insertBefore(Exercise.dialog.$buttons));
      $('<div class="link">' + Drupal.t('Copy workout') + '</div>').click(Exercise.calender.copyWorkout).appendTo($('<div></div>').insertBefore(Exercise.dialog.$buttons));
      $('<div class="link">' + Drupal.t('Delete workout') + '</div>').click(Exercise.calender.deleteWorkout).appendTo($('<div></div>').insertBefore(Exercise.dialog.$buttons));
    }
    else if (Exercise.calender.workoutCopy != undefined) {
      $('<div class="link">' + Drupal.t('Paste workout') + '</div>').click(Exercise.calender.pasteWorkout).appendTo($('<div></div>').insertBefore(Exercise.dialog.$buttons));
    }

    $('.ok', Exercise.dialog.$buttons).click(Exercise.calender.createWorkout)

    Exercise.dialog.$anchor = Exercise.calender.$clicked;
    Exercise.dialog.show();
    if (id == undefined) {
      $input[0].select();
    }
    $input.focus();
  });
}

/**
 * Display help dialog for create workout plans.
 */
Exercise.dialog.workoutHelp = function () {
  Exercise.dialog.close(function () {
    $('<div><p>' + Drupal.t('To create a workout/warm up plan you must first press "Create workout" on one of the days in the calender.') + '</p><p>' + Drupal.t('Next you must press on your new workout and select "Create workout plan".') + '</p>').insertBefore(Exercise.dialog.$buttons);

    $('.ok', Exercise.dialog.$buttons).click(Exercise.dialog.close);
    Exercise.dialog.show();
  });
}

/**
 * Display help dialog for create workout plans.
 */
Exercise.dialog.downloadTip = function () {
  Exercise.dialog.close(function () {
    $('<div>' + Drupal.t('It may take some time for the download to start, please wait.') + '</div>').insertBefore(Exercise.dialog.$buttons);

    $('.ok', Exercise.dialog.$buttons).click(Exercise.dialog.close);
    Exercise.dialog.show();
  });
}

Exercise.dialog.periodLoaded = function (period, id) {
  var $input = $('<input class="objective" value="' + (period != undefined ? period.objective : Drupal.t('Objective for the period')) + '" type="text"/>').keypress(Exercise.disableSubmit).insertBefore(Exercise.dialog.$buttons);

  if (period != undefined) {
    $('<div class="link">' + Drupal.t('Delete the period') + '</div>').click(function () {
      Exercise.calender.deletePeriod(period, id);
    }).appendTo($('<div></div>').insertBefore(Exercise.dialog.$buttons));
  }

  // Add date pickers
  var dates = Exercise.dialog.findPeriodDefaultDates(period),
  $datePickers = $(Drupal.theme('exerciseDialogFromTo', Exercise.datePicker.formatDate(dates.from.getFullYear(), dates.from.getMonth() + 1, dates.from.getDate()), Exercise.datePicker.formatDate(dates.to.getFullYear(), dates.to.getMonth() + 1, dates.to.getDate()))).insertBefore(Exercise.dialog.$buttons);
  $('.from', $datePickers).data('date', dates.from).click(Exercise.datePicker.show);
  $('.to', $datePickers).data('date', dates.to).click(Exercise.datePicker.show);

  $('.ok', Exercise.dialog.$buttons).click(Exercise.calender.createPeriod);
  if (id != undefined) {
    Exercise.dialog.$anchor = Exercise.calender.$clicked;
  }
  Exercise.dialog.show();
  if (period == undefined) {
    $input[0].select();
  }
  $input.focus();
}

/**
 * Display dialog for creating periodic plans.
 */
Exercise.dialog.period = function ($this) {
  if (Exercise.calender.$clicked != undefined && Exercise.calender.$clicked[0] == $this[0] && Exercise.dialog.close()) {
    return;
  }
  Exercise.calender.$clicked = $this;
  Exercise.dialog.close(function () {
    var id = Exercise.calender.$clicked.data('id');

    if (id != undefined) {
      var year = Exercise.calender.$clicked.parent().data('date').getFullYear();
      Exercise.storage.getPeriod(year, function (period) {
        Exercise.dialog.periodLoaded(period, id);
      }, id);
    }
    else {
      Exercise.dialog.periodLoaded(undefined, id);
    }
  });
}

/**
 * Detemine default dates for period date pickers.
 */
Exercise.dialog.findPeriodDefaultDates = function (period) {
  var dates = {},
  today = new Date();
  if (period != undefined) {
    dates.from = period.from;
    dates.to = period.to;
  }
  else if (today.getFullYear() == Exercise.calender.currentYear && today.getMonth() == Exercise.calender.currentMonth) {
    dates.from = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    dates.to = new Date(today.getFullYear(), today.getMonth(), today.getDate());
  }
  else {
    dates.from = new Date(Exercise.calender.currentYear, Exercise.calender.currentMonth, 1);
    dates.to = new Date(Exercise.calender.currentYear, Exercise.calender.currentMonth, 1);
  }
  return dates;
}

/**
 * Display a small error dialog above given object.
 */
Exercise.dialog.error = function ($obj, message) {
  if (!$('.error-dialog').length) {
    var offset = $obj.offset(),
    $dialog = $(Drupal.theme('exerciseDialogError', message)).appendTo('#wrapper');
    $dialog.css({
      top: offset.top - $dialog.height() + 10,
      left: offset.left + 30
    });
    setInterval(function () {
      $dialog.fadeOut(function () {
        $(this).remove();
      });
    }, 3000);
  }
}

/**
 * Display tip below this.
 */
Exercise.dialog.showTip = function () {
  var $this = $(this),
  $dialog = $('#tip-dialog'),
  adjust, offset = $this.offset();
  if (offset.left - $('#wrapper').offset().left < 480) {
    $dialog.addClass('left');
    adjust = 43;
  }
  else {
    adjust = 276;
  }
  $dialog.css({
    top: offset.top + 20,
    left: offset.left - adjust + ($this.width() / 2)
  });

  $('.text', $dialog).html($this.next().html());
  $dialog.show();
}

/**
 * Hide tip.
 */
Exercise.dialog.hideTip = function () {
  $('#tip-dialog').removeClass('left').hide();
}

/**
 * Show watermark
 */
Exercise.dialog.watermarkShow = function () {
  var $this = $(this);
  if ($this.val() == '') {
    $this.val($this.data('watermark'));
  }
}

/**
 * Hide watermark.
 */
Exercise.dialog.watermarkHide = function () {
  var $this = $(this);
  if ($this.val() == $this.data('watermark')) {
    $(this).val('');
  }
}

/**
 * Theme functions for dialog.
 */
Drupal.theme.prototype.exerciseDialogFromTo = function (from, to) {
  return '<table>'
  + '<tr><th>' + Drupal.t('From') + ':</th><td><div class="button from"><div class="start"></div><div class="text">' + from + '</div><div class="pick"></div><div class="end"></div></div></td></tr>'
  + '<tr><th>' + Drupal.t('To') + ':</th><td><div class="button to"><div class="start"></div><div class="text">' + to + '</div><div class="pick"></div><div class="end"></div></div></td></tr>'
  + '</table>';
}
Drupal.theme.prototype.exerciseDialogError = function (message) {
  return '<div class="error-dialog"><div class="header"></div><div class="text">' + message + '</div><div class="footer"></div></div>'
}

/**
 * Initialize the workout functionality.
 */
Exercise.workout.initialize = function () {
  Exercise.workout.$wrapper = $('#workout');
  Exercise.workout.$noExercises = $('.no-exercises', Exercise.workout.$wrapper);
  Exercise.workout.$buttons = $('#workout-buttons', Exercise.workout.$wrapper);
  Exercise.workout.$tbody = $('tbody', Exercise.workout.$wrapper);

  $('.cancel', Exercise.workout.$buttons).click(Exercise.workout.cancel);
  $('.objective input', Exercise.workout.$wrapper).keypress(Exercise.disableSubmit);

  $('.pick-exercises', Exercise.workout.$wrapper).click(Exercise.workout.library);
}

/**
 * Show the workout plan.
 */
Exercise.workout.show = function () {
  Exercise.$header.html(Drupal.t('Workout plan'));
  Exercise.helpPage = 'workout';
  $('.buttons', Exercise.$subHeader).removeClass('display');
  if (Drupal.settings['exercise']['exampleWorkoutPlan'] != '') {
    $('.example-plan', Exercise.$subHeader).show().unbind('click').click(function () {
      Exercise.lightbox.showExample(Drupal.t('Example workout plan'), Drupal.settings['exercise']['exampleWorkoutPlan']);
    });
  }
  Exercise.workout.date = Exercise.calender.$clicked.parent().data('date');
  Exercise.workout.id = Exercise.calender.$clicked.data('id');
  
  Exercise.storage.getWorkout(Exercise.workout.date, function (workout) {
    // Set objective
    $('.objective input', Exercise.workout.$wrapper).val(workout.objective == undefined ? '' : workout.objective);  

    if ($('.loading', Exercise.workout.$noExercises)[0] == undefined) {
      Exercise.workout.addExercises();
    }
    else {
      // Add exercises when we're done loading
      Exercise.workout.showExercises = true;
    }
    Exercise.workout.$wrapper.show();
    Exercise.library.scrollBar();
  }, Exercise.workout.id);
}

/**
 * Got to the exercise library.
 */
Exercise.workout.library = function () {
  Exercise.workout.$wrapper.hide();
  Exercise.library.show();
}

/**
 * Display exercises for current workout.
 */
Exercise.workout.addExercises = function () {
  if (Exercise.workout.id != undefined) {
    Exercise.storage.getWorkout(Exercise.workout.date, function (workout) {
      var exercises = Exercise.library.details(workout.exercises);
      for (var i = 0; i < exercises.length; i++) {
        if (exercises[i].image == undefined) {
          exercises[i].nid = 0;
          exercises[i].image = Drupal.settings.exercise.modulePath + '/theme/images/exercise-custom.png';
        }
        Exercise.workout.add(exercises[i]);
      }
    }, Exercise.workout.id);
  }
  else {
    Exercise.workout.$noExercises.show();
  }

  $('.download', Exercise.workout.$buttons).click(Exercise.workout.download);
  $('.save', Exercise.workout.$buttons).click(Exercise.workout.save);
}

/**
 * Add exercise row.
 */
Exercise.workout.add = function (exercise) {
  if (exercise.why == undefined) {
    exercise.why = '';
  }
  var $row = $(Drupal.theme('exerciseWorkoutRow', exercise)).appendTo(Exercise.workout.$tbody);

  if (exercise.nid == 0) {
    // Custom exercise
    $('td:first', $row).html('<textarea rows="4" cols="15">' + (exercise.title == Drupal.t('Create exercise') ? '' : exercise.title )+ '</textarea>');
  }
  $('textarea', $row).keypress(Exercise.disableSubmit);

  $('div.details', $row).data('nid', exercise.nid).click(function () {
    Exercise.lightbox.showDetails(exercise.nid);
  });
  $('.move-up', $row).click(Exercise.workout.moveUp).hover(Exercise.workout.moveMouseover, Exercise.workout.moveMouseout);
  $('.move-down', $row).click(Exercise.workout.moveDown).hover(Exercise.workout.moveMouseover, Exercise.workout.moveMouseout);
  $('.remove', $row).click(Exercise.workout.remove);

  if ($('tr', Exercise.workout.$tbody).length != 1) {
    Exercise.workout.$noExercises.hide();
  }
}

Exercise.workout.moveMouseover = function () {
  $(this).addClass('hover');
}

Exercise.workout.moveMouseout = function () {
  $(this).removeClass('hover');
}

/**
 * Order exercise one row up.
 */
Exercise.workout.moveUp = function () {
  var $this = $(this),
  $row = $this.parent().parent(),
  $prev = $row.prev();
  if ($prev.is('tr') && !$prev.hasClass('no-exercises')) {
    $this.removeClass('hover');
    $prev.insertAfter($row);
    $('.move-up', $prev).addClass('hover');
    Exercise.leaveWarning = true;
  }
}

/**
 * Order exercise one row down.
 */
Exercise.workout.moveDown = function () {
  var $this = $(this),
  $row = $this.parent().parent(),
  $next = $row.next();
  if ($next.is('tr') && !$next.hasClass('no-exercises')) {
    $this.removeClass('hover');
    $next.insertBefore($row);
    $('.move-down', $next).addClass('hover');
    Exercise.leaveWarning = true;
  }
}

/**
 * Remove exercise row.
 */
Exercise.workout.remove = function () {
  $(this).parent().parent().remove();

  if ($('tr', Exercise.workout.$tbody).length == 1) {
    Exercise.workout.$noExercises.show();
  }
  Exercise.leaveWarning = true;
  Exercise.library.scrollBar();
}

/**
 * Save workout plan.
 */
Exercise.workout.save = function () {
  var exercises = [],
  objective = $('.objective input', Exercise.workout.$wrapper).val();
  $('tr', Exercise.workout.$tbody).each(function () {
    var $this = $(this);
    if ($this.hasClass('no-exercises')) {
      return;
    }
    var nid = $('div.details', $this).data('nid');

    exercises.push({
      nid: nid,
      title: nid == 0 ? $('td:first textarea', this).val() : $('td:first div', this).html(),
      image: $('img', $this).attr('src'),
      amount: $('.amount', $this).val(),
      why: $('.why', $this).val()
    });
  });

  if (exercises.length != 0 || objective != '') {
    Exercise.calender.$clicked.addClass('exercises');
  }
  else {
    Exercise.calender.$clicked.removeClass('exercises');
  }

  Exercise.storage.getWorkout(Exercise.workout.date, function (workout) {
    workout.objective = objective;
    workout.exercises = exercises;
    Exercise.storage.setWorkout(workout, Exercise.workout.date, undefined, Exercise.workout.id);
    
    Exercise.workout.cancel();
  }, Exercise.workout.id);
}

/**
 * Close workout plan.
 */
Exercise.workout.cancel = function () {
  $('tr:not(.no-exercises)', Exercise.workout.$tbody).remove();
  Exercise.workout.$wrapper.hide();

  $('.buttons', Exercise.$subHeader).addClass('display');
  Exercise.helpPage = 'calender';
  if (Drupal.settings['exercise']['examplePeriodicPlan'] != '') {
    $('.example-plan', Exercise.$subHeader).show().unbind('click').click(function () {
      Exercise.lightbox.showExample(Drupal.t('Example periodic plan'), Drupal.settings['exercise']['examplePeriodicPlan']);
    });
  }
  Exercise.$header.html(Exercise.calender.months[Exercise.calender.currentMonth] + ' ' + Exercise.calender.currentYear);
  Exercise.calender.$wrapper.show();
  Exercise.leaveWarning = false;
}

/**
 * Download the current workout plan
 */
Exercise.workout.download = function () {
  var exercises = [];

  $('tr', Exercise.workout.$tbody).each(function () {
    var $this = $(this);
    if ($this.hasClass('no-exercises')) {
      return;
    }
    var nid = $('div.details', $this).data('nid');

    exercises.push({
      title: nid == 0 ? $('td:first textarea', this).val() : $('td:first div', this).html(),
      image: $('img', $this).attr('src'),
      amount: $('.amount', $this).val(),
      why: $('.why', $this).val()
    });
  });

  if (exercises.length == 0) {
    Exercise.dialog.downloadWorkoutError();
    return;
  }

  Exercise.storage.getWorkout(Exercise.workout.date, function (workout) {
    $('#edit-json').val(JSON.stringify({
      workouts: [{
        title: workout.title,
        objective: $('.objective input', Exercise.workout.$wrapper).val(),
        exercises: exercises,
        date: Exercise.workout.date
      }],
      name: Exercise.storage.general.getUserName()
    }));
    var leaveWarning = Exercise.leaveWarning;
    Exercise.leaveWarning = false;
    $('form').submit();
    setTimeout(function () {
      Exercise.leaveWarning = leaveWarning;
    }, 100);
    Exercise.dialog.downloadTip();
  }, Exercise.workout.id);
}

/**
 * Theme functions for workout.
 */
Drupal.theme.prototype.exerciseWorkoutRow = function (exercise) {
  return '<tr>'
  + '<td class="name no-border-left-right"><div>' + exercise.title + '</div></td>'
  + '<td class="image no-border-left-right"><img src="' + exercise.image + '" alt="" width="100" height="75"/></td>'
  + '<td class="details no-border-left"><div title="Details" class="details"></div></td>'
  + '<td class="no-border-right"><textarea cols="30" rows="4" class="amount">' + exercise.amount + '</textarea></td>'
  + '<td><textarea cols="15" rows="4" class="why">' + exercise.why + '</textarea></td>'
  + '<td class="move no-border-right"><div title="Move up" class="move-up"></div><div title="Move down" class="move-down"></div></td>'
  + '<td class="no-border-left-right"><div title="Remove" class="remove"></div></td>'
  + '</tr>';
}

/**
 * Initialize the exercise library.
 */
Exercise.library.initialize = function () {
  Exercise.library.$wrapper = $('#library');
  Exercise.library.$filters = $('#library-filters', Exercise.library.$wrapper);
  Exercise.library.$exercises = $('#library-exercises', Exercise.library.$wrapper);
  Exercise.library.$buttons = $('#library-buttons', Exercise.library.$wrapper);
  Exercise.library.selected = [];

  // Buttons
  $('.cancel', Exercise.library.$buttons).click(Exercise.library.cancel);

  // Load filters and exercises
  $.getJSON(Drupal.settings['exercise']['url'] + '/xhr/exercises', Exercise.library.insertData);
}

/**
 * Display new messages.
 */
Exercise.addMessages = function (messages) {
  var types = ['status', 'warning', 'error'];
  for (var i = 0; i < types.length; i++) {
    if (messages[types[i]] != undefined) {
      for (var j = 0; j < messages[types[i]].length; j++) {
        Exercise.dialog.addCloseButton($('<div class="messages ' + types[i] + '">' + messages[types[i]][j] + '</div>').appendTo('#messages'));
      }
    }
  }
}

/**
 * Stick buttons to bottom when scrolling.
 */
Exercise.library.scrollBar = function () {
  var height;
  if ($.browser.msie && $.browser.version == 8.0) {
    height = $(window).height() + 4;
  }
  else if ($.browser.opera && $.browser.version >= 9.50) {
    height = window.innerHeight;
  }
  else {
    height = $(window).height();
  }

  if (height < $(document).height()) {
    Exercise.library.$buttons.add(Exercise.workout.$buttons).addClass('bottom');
  }
  else {
    Exercise.library.$buttons.add(Exercise.workout.$buttons).removeClass('bottom');
  }
}

/**
 * Insert loaded data.
 */
Exercise.library.insertData = function (data) {
  Exercise.addMessages(data[2]);
  // Filters
  var $filtersWrapper = $('td', Exercise.library.$filters);
  Exercise.library.filters = data[0];
  for (var i = 0; i < data[0].length; i++) {
    var $filter = $('<select class="filter-' + i + '"><option value="0" class="all">' + Drupal.t('All') + '</select>').data('id', i).appendTo($filtersWrapper);
    if (i == 0) {
      Exercise.library.filterOptions($filter, 0);
    }
    else {
      $filter.hide();
    }
    $filter.change(Exercise.library.filter);
  }
  // Exercises
  if (!Drupal.settings.exercise.disableCustomExercise) {
    // Add custom exercise
    Exercise.library.add(0, {
      nid: 0,
      title: Drupal.t('Create exercise'),
      image: Drupal.settings.exercise.modulePath + '/theme/images/exercise-custom.png',
      amount: '',
      categories: [0]
    });
  }
  Exercise.library.length = data[1].length;
  if (!data[1].length) {
    Exercise.library.loaded();
  }
  else {
    $.each(data[1], Exercise.library.add);
  }
}

/**
 * Display the exercise library.
 */
Exercise.library.show = function () {
  Exercise.$header.html(Drupal.t('Exercise library'));
  Exercise.helpPage = 'library';
  if ($('.loading', Exercise.library.$exercises)[0] == undefined) {
    Exercise.library.showFilters();
  }
  else {
    Exercise.library.showExercises = true;
  }
  
  $(window).scroll(Exercise.library.scroll).scroll();
  Exercise.library.$wrapper.show();
  Exercise.library.scrollBar();
}

Exercise.library.scroll = function () {
  var $window = $(window),
  height = $window.height(),
  scrollTop = $window.scrollTop(),
  first = scrollTop - $('table:visible img:first', Exercise.library.$exercises).offset().top,
  from = 7 * (first / 147);
  $('table:visible img', Exercise.library.$exercises).slice(from < 0 ? 0 : from, 7 * ((first + height + 147) / 147)).trigger('scroll');
}

/**
 * Display filters.
 */
Exercise.library.showFilters = function () {
  $('table', Exercise.library.$exercises).show();
  Exercise.library.$filters.show();
  $('select:visible:last', Exercise.library.$filters).change();
  $('.insert', Exercise.library.$buttons).click(Exercise.library.insert);
}

/**
 * Add an exercise to the library.
 */
Exercise.library.add = function (i, e) {
  // Use timeout so we don't hang browsers.
  setTimeout(function () {
    // If there is no image, use default image
    if (e.image == '') {
      e.image = Drupal.settings.exercise.modulePath + '/theme/images/exercise-default.png';
    }
    
    // Make sure title isn't to long
    e.shortTitle = Exercise.overflow(e.title, 100);

    // Insert table
    var $table = $(Drupal.theme('exerciseLibraryExercise', e)).appendTo(Exercise.library.$exercises).data('categories', e.categories).hide();
    $('.details', $table).click(function () {
      Exercise.lightbox.showDetails(e.nid);
    });

    $('img', $table).data('src', e.image).bind('scroll', function () {
      var $this = $(this);
      $this.attr('src', $this.data('src')).unbind('scroll');
    }).error(function () {
      $(this).attr('src', Drupal.settings.exercise.modulePath + '/theme/images/exercise-default.png');
    });
    $('.add', $table).data('exercise', e).click(Exercise.library.select);

    Exercise.$loadingBars.width(i * 200 / Exercise.library.length);

    if (i == Exercise.library.length - 1) {
      Exercise.library.loaded();
    }
  }, i);
}

/**
 * All exercises has been loaded.
 */
Exercise.library.loaded = function () {
  $('.loading', Exercise.library.$exercises).remove();
  if (Exercise.library.showExercises) {
    Exercise.library.showFilters();
  }

  $('.loading', Exercise.workout.$noExercises).remove();
  if (Exercise.workout.showExercises) {
    Exercise.workout.addExercises();
  }
  Exercise.library.scrollBar();
}

/**
 * Display right filters and exercises when a filter changes.
 */
Exercise.library.filter = function () {
  var $this = $(this),
  $next = $this.next(),
  category = $this.val();

  // Show/hide filters
  while($next.is('select')) {
    Exercise.library.filterOptions($next, category);
    if ($next.children('option').length > 1) {
      $next.show();
    }
    else {
      $next.hide();
    }
    $next = $next.next();
  }

  if (category == '0') {
    // Try to get previous category
    var $prev = $this.prev();
    if ($prev.is('select')) {
      category = $prev.val();
    }
    else {
      // Show all exercises if there is no category
      $('table', Exercise.library.$exercises).show();
      Exercise.library.scrollBar();
      return;
    }
  }

  // Show/hide exercises
  var $show = $([]),
  $hide = $([]);
  $('table', Exercise.library.$exercises).each(function () {
    var $this = $(this),
    categories = $this.data('categories');
    for (var i = 0; i < categories.length; i++) {
      if (categories[i] == category) {
        $show = $show.add($this);
        return;
      }
    }
    if ($('.add', $this).data('exercise').nid == 0) {
      $show = $show.add($this);
      return;
    }
    $hide = $hide.add($this);
  });

  $show.show();
  $hide.hide();
  Exercise.library.scrollBar();
  $(window).scroll();
}

/**
 * Change options for a filter.
 */
Exercise.library.filterOptions = function ($filter, parent) {
  var id = $filter.data('id'),
  filters = Exercise.library.filters[id];

  // Remove old options
  $('option:not(.all)', $filter).remove();

  for (var i = 0; i < filters.length; i++) {
    if (filters[i].parent == parent) {
      $filter.append('<option value="' + filters[i].tid + '">' + filters[i].name + '</option>');
    }
  }
}

/**
 * Select exercise and put it in the selected list.
 */
Exercise.library.select = function () {
  var $this = $(this).unbind('click').click(Exercise.library.deselect);

  Exercise.library.selected.push($this.data('exercise'));

  $this.addClass('selected');
  Exercise.leaveWarning = true;
}

/**
 * Deselect exercise and remove it from the selected.
 */
Exercise.library.deselect = function () {
  var nid = $(this).removeClass('selected').unbind('click').click(Exercise.library.select).data('exercise').nid;
  for (var i = 0; i < Exercise.library.selected.length; i++) {
    if (Exercise.library.selected[i].nid == nid) {
      Exercise.library.selected.splice(i, 1);
      break;
    }
  }
}

/**
 * Insert selected exercises in the workout plan.
 */
Exercise.library.insert = function () {
  for (var i = 0; i < Exercise.library.selected.length; i++) {
    Exercise.workout.add(Exercise.library.selected[i]);
  }
  Exercise.library.cancel();
}

/**
 * Clear selected exercises and show the workout plan.
 */
Exercise.library.cancel = function () {
  Exercise.library.selected = [];
  $('.selected', Exercise.library.$exercises).removeClass('selected').unbind('click').click(Exercise.library.select);
  Exercise.library.$wrapper.hide();
  Exercise.$header.html(Drupal.t('Workout plan'));
  Exercise.helpPage = 'workout';
  Exercise.workout.$wrapper.show();
  Exercise.library.scrollBar();
  $(window).unbind('scroll')
}

/**
 * Find details for exercises.
 */
Exercise.library.details = function (exercises) {
  if (exercises.length) {
    $('table', Exercise.library.$exercises).each(function () {
      var exercise = $('.add', this).data('exercise');
      for (var i = 0; i < exercises.length; i++) {
        if (exercises[i].nid == exercise.nid && exercise.nid != 0) {
          exercises[i].image = exercise.image;
          exercises[i].title = exercise.title;
        }
      }
    });
  }
  return exercises;
}

/**
 * Theme function for an library exercise.
 */
Drupal.theme.prototype.exerciseLibraryExercise = function (exercise) {
  return '<table class="exercise">'
  + '<tr><th colspan="2" title="' + exercise.title + '">' + exercise.shortTitle + '</th></tr>'
  + '<tr><td colspan="2"><img src="' + Drupal.settings.exercise.modulePath + '/theme/images/exercise-default.png" alt="" title="' + exercise.title + '" width="100" height="75"/></td></tr>'
  + '<tr><td><div title="' + Drupal.t('Press to view details for this exercise.') + '" class="details"/></div></td>'
  + '<td><div title="' + Drupal.t('Press to add this exercise to your plan.') + '" class="add"></div></td></tr>'
  + '</table>';
}

/**
 * Initialize lightbox functionality.
 */
Exercise.lightbox.initialize = function () {
  Exercise.lightbox.$overlay = $('#lightbox-overlay');
  Exercise.lightbox.$content = $('#lightbox-content', Exercise.lightbox.$lightbox);
  Exercise.lightbox.$close = $('#lightbox-close span', Exercise.$lightbox);

  // Details
  Exercise.lightbox.$details = $('#details', Exercise.lightbox.$content);
  Exercise.lightbox.$detailsTitle = $('h4 span', Exercise.lightbox.$details);
  Exercise.lightbox.$detailsLoader = $('h4 img', Exercise.lightbox.$details);
  Exercise.lightbox.$detailsIllustration = $('.illustration .inner', Exercise.lightbox.$details);
  Exercise.lightbox.$detailsDescription = $('.description .inner', Exercise.lightbox.$details);
  Exercise.lightbox.$detailsMuscles = $('.muscles .inner', Exercise.lightbox.$details);

  // Help
  Exercise.lightbox.$help = $('#help', Exercise.lightbox.$content);
  Exercise.lightbox.$helpTitle = $('h4', Exercise.lightbox.$help);
  Exercise.lightbox.$helpLoader = $('img', Exercise.lightbox.$help);
}

/**
 * Clear content and close details lightbox.
 */
Exercise.lightbox.closeDetails = function () {
  Exercise.lightbox.$details.add(Exercise.lightbox.$overlay).add(Exercise.lightbox.$content).hide();

  Exercise.lightbox.$detailsLoader.show();
  Exercise.lightbox.$detailsTitle.html('');
  Exercise.lightbox.$detailsIllustration.html('');
  Exercise.lightbox.$detailsDescription.html('');
  Exercise.lightbox.$printDetails.remove();
  $('th:eq(2)', Exercise.lightbox.$details).show().prev().removeClass('no-border-right');
  $('td:eq(2)', Exercise.lightbox.$details).show().prev().removeClass('no-border-right');
  $(':not(:first)', Exercise.lightbox.$detailsMuscles).remove();
  Exercise.lightbox.$content.css('top', '100px');
}

/**
 * Show details lightbox.
 */
Exercise.lightbox.showDetails = function (nid) {
  // Bind close button and display lightbox
  Exercise.lightbox.$close.unbind('click').click(Exercise.lightbox.closeDetails);
  Exercise.lightbox.$printDetails = $('<span class="print">' + Drupal.t('Print') + '</span>').click(function () {
    window.print();
    return false;
  }).prependTo('#lightbox-close');
  Exercise.lightbox.$details.add(Exercise.lightbox.$overlay).add(Exercise.lightbox.$content).show();
  Exercise.lightbox.$content.css('top', ($(window).scrollTop() + 100) + 'px');
  Exercise.lightbox.$overlay.click(Exercise.lightbox.closeDetails);

  if (nid == 0) {
    // Custom exercise
    Exercise.lightbox.insertDetails({
      title: Drupal.t('Create exercise'),
      image: Drupal.settings.exercise.modulePath + '/theme/images/exercise-custom-big.png',
      body: Drupal.t("This is a special exercise that will show up without a name in your workout plan. This is your own custom exercise and you can name it whatever you want. This exercise is useful if you can't find the exercise you are looking for."),
      illustration: '',
      muscles: []
    });
  }
  else {
    // Load exercise details
    $.getJSON(Drupal.settings['exercise']['url'] + '/xhr/details/' + nid, Exercise.lightbox.insertDetails);
  }
}

/**
 * Insert content in the details lightbox.
 */
Exercise.lightbox.insertDetails = function (exercise) {
  Exercise.lightbox.$detailsTitle.html(exercise.title);
  if (exercise.illustration == '') {
    exercise.illustration = '<img src="' + exercise.image + '" alt="" width="300" height="240"/>';
  }
  Exercise.lightbox.$detailsIllustration.html(exercise.illustration);
  $('img', Exercise.lightbox.$detailsIllustration).error(function () {
    $(this).attr('src', Drupal.settings.exercise.modulePath + '/theme/images/exercise-default-big.png');
  });
  Exercise.lightbox.$detailsDescription.html(exercise.body);
  for (var i = 0; i < exercise.muscles.length; i++) {
    Exercise.lightbox.$detailsMuscles.append('<img src="' + Drupal.settings.exercise.taxonomyImagePath + exercise.muscles[i] + '" alt=""/>');
  }
  if (!exercise.muscles.length) {
    $('th:eq(2)', Exercise.lightbox.$details).hide().prev().addClass('no-border-right');
    $('td:eq(2)', Exercise.lightbox.$details).hide().prev().addClass('no-border-right');
  }
  Exercise.lightbox.$detailsLoader.hide();
}

Exercise.lightbox.showHelp = function () {
  var title;
  switch(Exercise.helpPage) {
    default:
    case 'calender':
      title = Drupal.t('Calender');
      break;
    case 'workout':
      title = Drupal.t('Workout');
      break;
    case 'library':
      title = Drupal.t('Library');
      break;
  }
  Exercise.lightbox.$helpTitle.html(Drupal.t('Help') + ' - ' + title);
  $('.text', Exercise.lightbox.$help).load(Drupal.settings['exercise']['url'] + '/xhr/help/' + Exercise.helpPage, function () {
    Exercise.lightbox.$helpLoader.hide();
  });

  Exercise.lightbox.$close.unbind('click').click(Exercise.lightbox.closeHelp);
  Exercise.lightbox.$help.add(Exercise.lightbox.$overlay).add(Exercise.lightbox.$content).show();
  Exercise.lightbox.$overlay.unbind('click').click(Exercise.lightbox.closeHelp);
}

Exercise.lightbox.closeHelp = function () {
  Exercise.lightbox.$help.add(Exercise.lightbox.$overlay).add(Exercise.lightbox.$content).hide();
  Exercise.lightbox.$helpLoader.show();
  $('.text', Exercise.lightbox.$help).html('');
}

Exercise.lightbox.showExample = function (title, plan) {
  Exercise.lightbox.$helpTitle.html(title);
  Exercise.lightbox.$helpLoader.hide();
  Exercise.lightbox.$close.unbind('click').click(Exercise.lightbox.closeHelp);
  Exercise.lightbox.$overlay.unbind('click').click(Exercise.lightbox.closeHelp);
  $('.text', Exercise.lightbox.$help).html('<img style="display: block; margin: 0 auto;" src="' + plan + '" alt=""/>');
  Exercise.lightbox.$help.add(Exercise.lightbox.$overlay).add(Exercise.lightbox.$content).show();
}

/**
 * Initialize the date picker.
 * This is a copy of the calender and some functions here are used by both
 * the calender and the date picker.
 */
Exercise.datePicker.initialize = function () {
  Exercise.datePicker.$overlay = $('<div id="date-picker-overlay"></div>').appendTo('#wrapper').click(Exercise.datePicker.close);
  Exercise.datePicker.$wrapper = $('<div id="date-picker"></div>').appendTo('#wrapper');
  $('<div class="previous">&lt;</div>').appendTo(Exercise.datePicker.$wrapper).click(Exercise.datePicker.previous);
  $('<div class="next">&gt;</div>').appendTo(Exercise.datePicker.$wrapper).click(Exercise.datePicker.next);
  Exercise.datePicker.$info = $('<div class="info"></div>').appendTo(Exercise.datePicker.$wrapper);

  var $table = $('table', Exercise.calender.$wrapper).clone().appendTo(Exercise.datePicker.$wrapper);
  $('th', $table).each(function () {
    var $this = $(this);
    $this.html($this.html().substring(0, 2));
  });
}

/**
 * Display datepicker below current element.
 */
Exercise.datePicker.show = function () {
  Exercise.datePicker.$clicked = $(this);
  Exercise.leaveWarning = true;

  // Set stored date
  var date = Exercise.datePicker.$clicked.data('date');
  Exercise.datePicker.currentYear = date.getFullYear();
  Exercise.datePicker.currentMonth = date.getMonth();
  Exercise.datePicker.currentDay = Exercise.datePicker.currentYear + '' + Exercise.datePicker.currentMonth + date.getDate();

  // Position date picker
  var offset = Exercise.datePicker.$clicked.offset();
  Exercise.datePicker.$wrapper.css({
    top: offset.top + 30,
    left: offset.left + 7
  });

  Exercise.datePicker.render();
  Exercise.datePicker.$overlay.show();
  Exercise.datePicker.$wrapper.fadeIn(150);
}

/**
 * Renders the date picker with the current month.
 */
Exercise.datePicker.render = function () {
  // Set name of month
  Exercise.datePicker.$info.html(Exercise.calender.months[Exercise.datePicker.currentMonth] + ' ' + Exercise.datePicker.currentYear);

  // Get data for current month
  var month = Exercise.datePicker.getMonth(Exercise.datePicker.currentYear, Exercise.datePicker.currentMonth);

  // Clean up last month
  $('tbody td', Exercise.datePicker.$wrapper).removeClass('day current').html('');

  // Find rows
  var $rows = $('tbody tr', Exercise.datePicker.$wrapper),

  // Find first day of month in the calender
  $previousDay, $day = $('td:nth-child('+ month.first +')', $rows[0]);
  for (var i = 1; i <= month.length; i++) {
    $day.addClass('day').data('day', i).html(i).click(Exercise.datePicker.pick);
    if (Exercise.datePicker.currentYear + '' + Exercise.datePicker.currentMonth + i == Exercise.datePicker.currentDay) {
      $day.addClass('current');
    }
    $previousDay = $day;
    $day = $day.next();
    if ($day[0] == undefined) {
      $day = $previousDay.parent().next().children(':first');
    }
  }

  // Hide empty rows
  Exercise.datePicker.hideEmptyRows($rows);
}

/**
 * Pick clicked day.
 */
Exercise.datePicker.pick = function () {
  var day = $(this).data('day'),
  date = new Date(Exercise.datePicker.currentYear, Exercise.datePicker.currentMonth, day)

  $('.text', Exercise.datePicker.$clicked).html(Exercise.datePicker.formatDate(Exercise.datePicker.currentYear, Exercise.datePicker.currentMonth + 1, day))
  Exercise.datePicker.$clicked.data('date', date)
  Exercise.datePicker.close();
}

/**
 * Close date picker.
 */
Exercise.datePicker.close = function () {
  Exercise.datePicker.$overlay.hide();
  Exercise.datePicker.$wrapper.fadeOut(150);
}

/**
 * Show previous calender month in date picker.
 */
Exercise.datePicker.previous = function () {
  if (Exercise.datePicker.currentMonth == 0) {
    Exercise.datePicker.currentYear--;
    Exercise.datePicker.currentMonth = 11;
  }
  else {
    Exercise.datePicker.currentMonth--;
  }
  Exercise.datePicker.render();
}

/**
 * Show next calender month in date picker.
 */
Exercise.datePicker.next = function () {
  if (Exercise.datePicker.currentMonth == 11) {
    Exercise.datePicker.currentYear++;
    Exercise.datePicker.currentMonth = 0;
  }
  else {
    Exercise.datePicker.currentMonth++;
  }
  Exercise.datePicker.render();
}

/**
 * Returns an object containing the first day and the number of days in the month.
 */
Exercise.datePicker.getMonth = function (year, month) {
  var data = {
    start: 0
  },
  date = new Date(year, month + 1, 0);

  // Get number of days this month
  data.length = date.getDate();

  // Get first day of the month
  date.setDate(1);
  data.first = date.getDay() + 1;
  data.first -= Drupal.settings.exercise.weekStart;
  if (data.first < 1) {
    data.first += 7
  }

  return data;
}

/**
 * Checks if date is today.
 */
Exercise.datePicker.isToday = function (date) {
  if (Exercise.datePicker.today == undefined) {
    Exercise.datePicker.today = new Date();
  }
  return Exercise.datePicker.today.getFullYear() == date.getFullYear() && Exercise.datePicker.today.getMonth() == date.getMonth() && Exercise.datePicker.today.getDate() == date.getDate();
}

/**
 * Hide empty rows in the calender.
 */
Exercise.datePicker.hideEmptyRows = function ($rows) {
  var $td = $('td:first', $rows[4]);
  if (!$td.hasClass('day') || $td.hasClass('prev')) {
    $rows.eq(4).hide();
  }
  else {
    $rows.eq(4).show();
  }
  $td = $('td:first', $rows[5]);
  if (!$td.hasClass('day') || $td.hasClass('prev')) {
    $rows.eq(5).hide();
  }
  else {
    $rows.eq(5).show();
  }
}

/**
 * Format a date.
 */
Exercise.datePicker.formatDate = function (year, month, day, format) {
  if (format == undefined) {
    format = Drupal.settings['exercise']['dateFormat'];
  }
  
  if (month < 10) {
    month = '0' + month;
  }
  if (day < 10) {
    day = '0' + day;
  }
  
  return format.replace('d', day).replace('m', month).replace('y', year);
}

/**
 * Prevents form submit when the enter key is pressed.
 */
Exercise.disableSubmit = function (e) {
  Exercise.leaveWarning = true;
  if (e.keyCode == 13/* ENTER */) {
    e.preventDefault();
  }
}

/**
 * Extend object so we can find the size of objects.
 */
Exercise.objectSize = function (object) {
  var property, i = 0;
  for (property in object) {
    i++;
  }
  return i;
}

// Initialize the exercise planner when the document is loaded.
$(document).ready(function () { Exercise.initialize(); });