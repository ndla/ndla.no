<?php
$path = base_path() . drupal_get_path('module', 'exercise');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>
  <body>
    <div id="header">
      <div class="inner">
        <?php if (variable_get('exercise_logo', '') != ''): ?>
          <div class="logo">
            <a href="<?php print url('<front>') ?>">
              <img src="<?php print variable_get('exercise_logo', '') ?>" alt=""/>
            </a>
          </div>
        <?php endif ?>
        <h1><?php print t('Exercise planner') ?></h1>
      </div>
    </div>
    <div id="wrapper">
      <div id="sub-header">
        <div class="name"></div>
        <div class="help"></div>
        <div class="example-plan">
          <div class="left"></div><div class="text"><?php print t('Show example plan') ?></div><div class="right"></div>
        </div>
        <div class="header">
          <div class="buttons previous"></div>
          <h2><?php print ($title == t('Exercise planner') ? t('Enable javascript') : $title) ?></h2>
          <div class="buttons next"></div>
        </div>
      </div>
      <div id="messages">
        <?php print $messages ?>
      </div>
      <?php print $content ?>
    </div>
    <div id="lightbox-overlay"></div>
    <div id="lightbox-content">
      <div id="details">
        <h4>
          <span></span>
          <img src="<?php print $path ?>/theme/images/ajax-loader.gif" alt="Loading"/>
        </h4>
        <div class="content illustration">
          <h5><?php print t('Illustration') ?></h5>
          <div class="inner">
          </div>
        </div>
        <div class="content muscles">
          <h5><?php print t('Muscles') ?></h5>
          <div class="inner">
            <img src="<?php print $path; ?>/theme/images/muscles-outline.png" alt=""/>
          </div>
        </div>
        <div class="content description">
          <h5><?php print t('Description') ?></h5>
          <div class="inner">
          </div>
        </div>
       </div>
      <div id="help">
        <h4></h4>
        <img src="<?php print $path ?>/theme/images/ajax-loader.gif" alt="Loading"/>
        <div class="text">
        </div>
      </div>
      <div id="lightbox-close">
        <span><?php print t('Close') ?></span>
      </div>
    </div>
    <?php print $closure ?>
  </body>
</html>