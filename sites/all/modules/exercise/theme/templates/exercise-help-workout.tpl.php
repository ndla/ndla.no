<p><?php print t('Here you start by filling in a workout objective. Next you press the "Pick exercises/activities" link to access the exercise library. There you will select all the exercises you wish to use in your workout plan.') ?></p>
<h5><?php print t('The workout plan consists of five main columns') ?></h5>
<h6><?php print t('What') ?></h6>
<p><?php print t('In this column you see the exercise\'s title, an illustration of the exercise and a button with a magnifying glass. By pressing this button you can view details for the exercise, such as the procedure for the exercise.') ?></p>
<h6><?php print t('How long') ?></h6>
<p><?php print t('In this column you enter the estimated duration of the exercise. For example this can be "10 minutes" or "30 seconds".') ?></p>
<h6><?php print t('How') ?></h6>
<p><?php print t('In this column you can enter the training amount for the exercise. This can for example be "3 sets of 10 repetitions" or "10 km".') ?></p>
<h6><?php print t('Why') ?></h6>
<p><?php print t('In this column you justify, in your own words, why you picked this exercise.') ?></p>
<h6><?php print t('Edit') ?></h6>
<p><?php print t('At the end you can change the order of the exercises in the workout plan by using the buttons with arrows up/down. You can remove an exercise from the workout plan by pressing the button with the cross.') ?></p>
<p><?php print t('Press "Show example plan" for an example on how a workout plan might look.') ?></p>
<p><?php print t('You can download your workout plan by pressing the "Download" button.') ?></p>
<p><?php print t('Press "Save" when you are done and wish to save your workout plan and return to the calender. If you press cancel all your changes are lost.') ?></p>
