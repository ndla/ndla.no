<div id="login" class="frame">
  <?php if (!variable_get('exercise_allow_anonymous_users', 0)): ?>
    <?php
    if(!variable_get('ndla_utils_auth_enable', TRUE)) {
      echo '<p>' . t('Login has been disabled for the momet.') .'</p>';
    }
    else {
    ?>
    <p><?php print t('You must <a href="@url">login</a> in order to use this application.', array('@url' => url('user', array('query' => array('destination' => $_GET['q']))))) ?></p>
    <?php } ?>
  <?php else: ?>
    <p><?php print t('You must <a href="@url">login</a> if you want your exercise plans to get stored.', array('@url' => url('user', array('query' => array('destination' => $_GET['q']))))) ?></p>
    <p><?php print t('You can use the application <a href="#" class="continue">without logging in</a> but it\'s not recommended.') ?></p>
  <?php endif ?>
</div>