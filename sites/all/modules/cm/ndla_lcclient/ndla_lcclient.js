/**
 * @file
 * @ingroup ndla_msclient
 */
var learningCurve = learningCurve || {};

learningCurve.init = function() {
  var defText, defTitle, marked,
    chosen = userData.getData('page', 'learningcurve');
  if (chosen !== null && typeof chosen.mark != 'undefined' && chosen.mark) {
    defText = Drupal.t('Unmark');
    defTitle = Drupal.t('Remove from learning basket');
    marked = ' marked';
  }
  else {
    defText = Drupal.t('Mark');
    defTitle = Drupal.t('Add to learning basket');
    marked = '';
  }
  learningCurve.$mark = $('<li><a href="#" class="learningcurve' + marked + '" title="' + defTitle + '"><span class="icon"></span><span class="text">' + defText + '</span></a></li>')
  learningCurve.$mark.children('a').click(function () {
    var $this = $(this);
    if (!$this.hasClass('marked')) {
      $this.addClass('marked').attr('title', Drupal.t('Remove from learning basket')).children('.text').text(Drupal.t('Unmark'));
      userData.saveData('page', 'learningcurve', {mark: true, title: $(document).attr('title')});
    }
    else {
      $this.removeClass('marked').attr('title', Drupal.t('Add to learning basket')).children('.text').text(Drupal.t('Mark'));
      userData.removeData('page', 'learningcurve');
    }
    return false;
  });
  $('#node-actions').append(learningCurve.$mark);
}

userData.ready(function () { 
  learningCurve.init(); 
});