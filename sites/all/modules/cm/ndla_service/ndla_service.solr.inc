<?php
function ndla_service_get_facets() {
  $enabled_facets = apachesolr_get_enabled_facets();
  $ids = array();
  foreach($enabled_facets as $module) {
    foreach($module as $id => $facet) {
      $ids[$id] = $id;
    }
  }  
  $facets = module_invoke_all('apachesolr_facets');
  foreach($facets as $id => $facet) {
    if(!in_array($id, $ids)) {
      unset($facets[$id]); 
    }
  }

  return $facets;
}

function ndla_service_get_config() {
  drupal_set_header('Content-Type: text/javascript; charset=utf-8');
  $solr = apachesolr_get_solr();
  $lang_list = language_list();
  $data = array();
  //Fetch available facets and if the facet is a vid - fetch the taxonomy terms
  //as IDs aren't relevant for users, they want words.
  $facets = ndla_service_get_facets();
  $taxonomy = array();
  if(!empty($facets)) {
    $data['facets'] = $facets;
    foreach($facets as $name => $facet) {
      if(strpos($name, "_vid_") !== FALSE && $name != 'im_vid_1') {
        $vid = preg_replace("/[^0-9]/", "", $name);
        $tree = taxonomy_get_tree($vid);
        foreach($tree as $tid => $term) {

          $term = (array)$term;
          if(module_exists('i18ntaxonomy')) {
            foreach($lang_list as $l) {
              $term['title'] = $term['name'];
              $term['title_'.$l->language] = i18ntaxonomy_translate_term_name($term['tid'], $term['name'], $l->language);
            }
          } else {
              $term['title'] = $term['name'];
          }
          $taxonomy[$vid]['terms'][$tid] = (array)$term;
        }
        if($vid == 100016) {
          $taxonomy[$vid]['flat_structure'] = TRUE;
        }
      }
    }
    $data['facets']['_taxonomy'] = $taxonomy;
  }
  $data['params'] = ndla_service_get_solr_params();
  $data['url'] = url("", array('absolute' => TRUE, 'language' => ''));
  
  //We need to pickup the human readable names of the content types and their translation.

  $types = node_get_types();
  foreach($types as $type) {
    $data['content_types'][$type->type]['title'] = $type->name; //Default - if search.ndla.no doesnt handle any of our languages
    foreach($lang_list as $l) {
      $data['content_types'][$type->type]['title_' . $l->language] = t($type->name, array(), $l->language);
    }
  }
  print json_encode($data, JSON_FORCE_OBJECT);
  exit();
}

/**
 * Let modules add their specifik params
 */
function ndla_service_get_solr_params() {
  $params = array();
  $query = apachesolr_drupal_query();

  $params += apachesolr_search_basic_params($query);
  if ($keys) {
    $params += apachesolr_search_highlighting_params($query);
    $params += apachesolr_search_spellcheck_params($query);
  }
  else {
    // No highlighting, use the teaser as a snippet.
    $params['fl'] .= ',teaser';
  }
  if (module_exists('upload')) {
    $params['fl'] .= ',is_upload_count';
  }
  apachesolr_search_add_facet_params($params, $query);
  apachesolr_search_add_boost_params($params, $query, apachesolr_get_solr());
  
  // Allow modules to alter the query prior to statically caching it.
  // This can e.g. be used to add available sorts.
  foreach (module_implements('apachesolr_prepare_query') as $module) {
    $function_name = $module . '_apachesolr_prepare_query';
    $function_name($query, $params, $caller);
  }

  // This hook allows modules to modify the query and params objects.
  apachesolr_modify_query($query, $params, $caller);

  // Final chance for the caller to modify the query and params. The signature
  // is: CALLER_finalize_query(&$query, &$params);
  $function = $caller . '_finalize_query';
  if (function_exists($function)) {
    $function($query, $params);
  }
  
  return $params;
}
