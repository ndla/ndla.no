Drupal.behaviors.ndla_county = function() {
  if ($('div#county').length) {
    var url = Drupal.settings.basePath + "ndla_county/get";
    $.ajax({
      url: url,
      success: function(data) {
        $('div#county').replaceWith(data);
      }
    });
  }
}
