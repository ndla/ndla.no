<div class="qrcode">
  <?php print $qrcode ?>
  <br />
  <span class="headline">
    <span class="title"><?php print $title ?></span> / <span class="type"><?php print $type ?></span>
  </span>
  <a href="<?php print $url ?>"><?php print $url ?></a>
</div>