(function($) {
  $(document).ready(function() {
    $("input[type='checkbox']").click(function() {
      $('.download-frame a').each(update_link);
    });    
    $('.download-frame a').each(update_link);
  });

  function update_link(counter, link_element) {
    if(link_element.original_link === undefined) {
      link_element.original_link = $(link_element).attr('href');
    }
    var options = [];
    $("input[type='checkbox']").each(function(counter, input) {
      if($(input).attr('checked')) options.push($(input).attr('id'));
    });
    
    if(options.length > 0) {
      $(link_element).attr('href', link_element.original_link + '?' + options.join('=1&') + '=1'); 
    } else {
      $(link_element).attr('href', link_element.original_link); 
    }
  }
}.call(window, jQuery))
