<?php
/**
 * @file
 * @ingroup utdanning_oai_export
 */

$lom = array();

$lom["Vocabulary"][1]  = array("atomic", "collection", "networked", "hierarchical", "linear");
$lom["Vocabulary"][2]  = array(1, 2, 3, 4);
$lom["Vocabulary"][3]  = array("draft", "final", "revised", "unavailable");
$lom["Vocabulary"][4]  = array("author", "publisher", "unknown", "initiator", "terminator", "validator", "editor", "graphical designer", "technical implementer", "content provider", "technical validator", "educational validator", "script writer", "instructional designer", "subject matter expert");
$lom["Vocabulary"][5]  = array("creator", "validator");
$lom["Vocabulary"][6]  = array("operating system", "browser");
$lom["Vocabulary"][7]  = array($lom["Vocabulary"][6][0] => array("pc-dos", "ms-windows", "ms-xp", "ms-vista", "macos", "macosx", "unix", "linux", "multi-os", "none"), $lom["Vocabulary"][6][1] => array("any", "netscape communicator", "ms-internet explorer", "opera", "amaya", "safari", "firefox", "chrome"));
$lom["Vocabulary"][8]  = array("active", "expositive", "mixed");
$lom["Vocabulary"][9]  = array("Tester og oppgaver", "Drill, enkle �vinger og spill", "Simuleringer, fors�k/eksperiment og praktisk arbeid", "�pne aktiviteter", "Ordlister, oppslagsverk eller vokabularer", "Veiledningsmateriell og undervisningsopplegg", "Informasjonsressurser", "Verkt�y");
$lom["Vocabulary"][10] = array("very low", "low", "medium", "high", "very high");
$lom["Vocabulary"][11] = array("low", "medium", "high", "very high");
$lom["Vocabulary"][12] = array("L�rer", "Forfatter", "L�rende", "Tilrettelegger");
$lom["Vocabulary"][13] = array("barnehage", "1. - 2. �rstrinn", "3. - 4. �rstrinn", "5. - 7. �rstrinn", "8. - 10. �rstrinn", "VG1", "VG2", "VG3", "VG4", "bachelor", "master/ph.d", "etter- og videreutdanning");
$lom["Vocabulary"][14] = array("very easy", "easy", "medium", "difficult", "very difficult");
$lom["Vocabulary"][15] = array("ja", "nei");
$lom["Vocabulary"][16] = array("ispartof", "haspart", "isversionof", "hasversion", "isformatof", "hasformat", "references", "isreferencedby", "isbasedon", "isbasisfor", "requires", "isrequiredby");
$lom["Vocabulary"][17] = array("grep", "vitenskapsdisipliner", "basis", "discipline", "idea", "prerequisite", "educational objective", "accessibility restrictions", "educational level", "skill level", "security level", "competency");

$lom["lom"]["1"] = array("name" => "General", "attributeGroup" => "general", "presence type" => "Required", "size" => 1, "data type" => "Aggregate element");
$lom["lom"]["1"]["1.1"] = array("name" => "Identifier", "attributeGroup" => "identifier", "presence type" => "Required", "size" => 10, "data type" => "Aggregate element");
$lom["lom"]["1"]["1.1"]["1.1.1"] = array("name" => "Catalog", "attributeGroup" => "catalog", "presence type" => "Required", "size" => 1, "data type" => "CharacterString");
$lom["lom"]["1"]["1.1"]["1.1.2"] = array("name" => "Entry", "attributeGroup" => "entry", "presence type" => "Required", "size" => 1, "data type" => "CharacterString");	
$lom["lom"]["1"]["1.2"] = array("name" => "Title", "attributeGroup" => "title", "presence type" => "Required", "size" => 1, "data type" => "LangString");
$lom["lom"]["1"]["1.3"] = array("name" => "Language", "attributeGroup" => "language", "presence type" => "Required", "size" => 10, "data type" => "CharacterString");
$lom["lom"]["1"]["1.4"] = array("name" => "Description", "attributeGroup" => "description", "presence type" => "Required", "size" => 10, "data type" => "LangString");
$lom["lom"]["1"]["1.5"] = array("name" => "Keyword", "attributeGroup" => "keyword", "presence type" => "Recommended", "size" => 10, "data type" => "LangString");
$lom["lom"]["1"]["1.6"] = array("name" => "Coverage", "attributeGroup" => "coverage", "presence type" => "Optional", "size" => 10, "data type" => "LangString");
$lom["lom"]["1"]["1.7"] = array("name" => "Structure", "attributeGroup" => "structure", "presence type" => "Optional", "size" => 1, "data type" => "Vocabulary", "value" => 1);
$lom["lom"]["1"]["1.8"] = array("name" => "Aggregation Level", "attributeGroup" => "aggregationLevel", "presence type" => "Optional", "size" => 1, "data type" => "Vocabulary", "value" => 2);

$lom["lom"]["2"] = array("name" => "Life Cycle", "attributeGroup" => "lifeCycle", "presence type" => "Required", "size" => 1, "data type" => "Aggregate element");
$lom["lom"]["2"]["2.1"] = array("name" => "Version", "attributeGroup" => "version", "presence type" => "Recommended", "size" => 1, "data type" => "LangString");
$lom["lom"]["2"]["2.2"] = array("name" => "Status", "attributeGroup" => "status", "presence type" => "Optional", "size" => 1, "data type" => "Vocabulary", "value" => 3);
$lom["lom"]["2"]["2.3"] = array("name" => "Contribute", "attributeGroup" => "contribute", "presence type" => "Recommended", "size" => 30, "data type" => "Aggregate element");
$lom["lom"]["2"]["2.3"]["2.3.1"] = array("name" => "Role", "attributeGroup" => "role", "presence type" => "Recommended", "size" => 1, "data type" => "Vocabulary", "value" => 4);
$lom["lom"]["2"]["2.3"]["2.3.2"] = array("name" => "Entity", "attributeGroup" => "entity", "presence type" => "Recommended", "size" => 40, "data type" => "CharacterString");
$lom["lom"]["2"]["2.3"]["2.3.3"] = array("name" => "Date", "attributeGroup" => "date", "presence type" => "Recommended", "size" => 1, "data type" => "DateTime");

$lom["lom"]["3"] = array("name" => "Meta-Metadata", "attributeGroup" => "metaMetadata", "presence type" => "Required", "size" => 1, "data type" => "Aggregate element");
$lom["lom"]["3"]["3.1"] = array("name" => "Identifier", "attributeGroup" => "identifier", "presence type" => "Required", "size" => 1, "data type" => "Aggregate element");
$lom["lom"]["3"]["3.1"]["3.1.1"] = array("name" => "Catalog", "attributeGroup" => "catalog", "presence type" => "Required", "size" => 1, "data type" => "CharacterString");
$lom["lom"]["3"]["3.1"]["3.1.2"] = array("name" => "Entry", "attributeGroup" => "entry", "presence type" => "Required", "size" => 1, "data type" => "CharacterString");
$lom["lom"]["3"]["3.2"] = array("name" => "Contribute", "attributeGroup" => "contribute", "presence type" => "Required", "size" => 10, "data type" => "Aggregate element");
$lom["lom"]["3"]["3.2"]["3.2.1"] = array("name" => "Role", "attributeGroup" => "role", "presence type" => "Required", "size" => 1, "data type" => "Vocabulary", "value" => 5);
$lom["lom"]["3"]["3.2"]["3.2.2"] = array("name" => "Entry", "attributeGroup" => "entry", "presence type" => "Required", "size" => 10, "data type" => "CharacterString");
$lom["lom"]["3"]["3.2"]["3.2.3"] = array("name" => "Date", "attributeGroup" => "date", "presence type" => "Required", "size" => 1, "data type" => "DateTime");
$lom["lom"]["3"]["3.3"] = array("name" => "Metadata Schema", "attributeGroup" => "metadataSchema", "presence type" => "Required", "size" => 10, "data type" => "CharacterString");
$lom["lom"]["3"]["3.4"] = array("name" => "Language", "attributeGroup" => "language", "presence type" => "Required", "size" => 1, "data type" => "CharacterString");

$lom["lom"]["4"] = array("name" => "Technical", "attributeGroup" => "technical", "presence type" => "Required", "size" => 1, "data type" => "Aggregate element");
$lom["lom"]["4"]["4.1"] = array("name" => "Format", "attributeGroup" => "format", "presence type" => "Recommended", "size" => 40, "data type" => "CharacterString");
$lom["lom"]["4"]["4.2"] = array("name" => "Size", "attributeGroup" => "size", "presence type" => "Recommended", "size" => 1, "data type" => "CharacterString");
$lom["lom"]["4"]["4.3"] = array("name" => "Location", "attributeGroup" => "location", "presence type" => "Required", "size" => 10, "data type" => "CharacterString");
$lom["lom"]["4"]["4.4"] = array("name" => "Requirement", "attributeGroup" => "requirement", "presence type" => "Optional", "size" => 1, "data type" => "Aggregate element");
$lom["lom"]["4"]["4.4"]["4.4.1"] = array("name" => "OrComposite", "attributeGroup" => "orComposite", "presence type" => "Optional", "size" => 40, "data type" => "Aggregate element");
$lom["lom"]["4"]["4.4"]["4.4.1"]["4.4.1.1"] = array("name" => "Type", "attributeGroup" => "type", "presence type" => "Optional", "size" => 1, "data type" => "Vocabulary", "value" => 6);
$lom["lom"]["4"]["4.4"]["4.4.1"]["4.4.1.2"] = array("name" => "Name", "attributeGroup" => "name", "presence type" => "Optional", "size" => 1, "data type" => "Vocabulary", "value" => 7);
$lom["lom"]["4"]["4.4"]["4.4.1"]["4.4.1.3"] = array("name" => "Minimum Version", "attributeGroup" => "minimumVersion", "presence type" => "Optional", "size" => 1, "data type" => "CharacterString");
$lom["lom"]["4"]["4.4"]["4.4.1"]["4.4.1.4"] = array("name" => "Maximum Version", "attributeGroup" => "maximumVersion", "presence type" => "Optional", "size" => 1, "data type" => "CharacterString");
$lom["lom"]["4"]["4.5"] = array("name" => "Installation Remarks", "attributeGroup" => "installationRemarks", "presence type" => "Optional", "size" => 1, "data type" => "LangString");
$lom["lom"]["4"]["4.6"] = array("name" => "Other Platform Requirements", "attributeGroup" => "otherPlatformRequirements", "presence type" => "Optional", "size" => 1, "data type" => "LangString");
$lom["lom"]["4"]["4.7"] = array("name" => "Duration", "attributeGroup" => "duration", "presence type" => "Optional", "size" => 1, "data type" => "Duration");

$lom["lom"]["5"] = array("name" => "Educational", "attributeGroup" => "educational", "presence type" => "Recommended", "size" => 1, "data type" => "Aggregate element");
$lom["lom"]["5"]["5.1"] = array("name" => "Interactivity Type", "attributeGroup" => "interactivityType", "presence type" => "Optional", "size" => 1, "data type" => "Vocabulary", "value" => 8);
$lom["lom"]["5"]["5.2"] = array("name" => "Learning Resource Type", "attributeGroup" => "learningResourceType", "presence type" => "Recommended", "size" => 1, "data type" => "Vocabulary", "value" => 9);
$lom["lom"]["5"]["5.3"] = array("name" => "Interactivity Level", "attributeGroup" => "interactivityLevel", "presence type" => "Optional", "size" => 1, "data type" => "Vocabulary", "value" => 10);
$lom["lom"]["5"]["5.4"] = array("name" => "Semantic Density", "attributeGroup" => "semanticDensity", "presence type" => "Optional", "size" => 1, "data type" => "Vocabulary", "value" => 11);
$lom["lom"]["5"]["5.5"] = array("name" => "Intended End User Role", "attributeGroup" => "intendedEndUserRole", "presence type" => "Recommended", "size" => 10, "data type" => "Vocabulary", "value" => 12);
$lom["lom"]["5"]["5.6"] = array("name" => "Context", "attributeGroup" => "context", "presence type" => "Recommended", "size" => 10, "data type" => "Vocabulary", "value" => 13);
$lom["lom"]["5"]["5.7"] = array("name" => "Typical Age Range", "attributeGroup" => "typicalAgeRange", "presence type" => "Optional", "size" => 5, "data type" => "LangString");
$lom["lom"]["5"]["5.8"] = array("name" => "Difficulty", "attributeGroup" => "difficulty", "presence type" => "Optional", "size" => 1, "data type" => "Vocabulary", "value" => 14);
$lom["lom"]["5"]["5.9"] = array("name" => "Typical Learning Time", "attributeGroup" => "typicalLearningTime", "presence type" => "Recommended", "size" => 1, "data type" => "Duration");
$lom["lom"]["5"]["5.10"] = array("name" => "Description", "attributeGroup" => "description", "presence type" => "Optional", "size" => 10, "data type" => "LangString");
$lom["lom"]["5"]["5.11"] = array("name" => "Language", "attributeGroup" => "language", "presence type" => "Optional", "size" => 10, "data type" => "LangString");

$lom["lom"]["6"] = array("name" => "Rights", "attributeGroup" => "rights", "presence type" => "Required", "size" => 1, "data type" => "Aggregate element");
$lom["lom"]["6"]["6.1"] = array("name" => "Cost", "attributeGroup" => "cost", "presence type" => "Recommended", "size" => 1, "data type" => "Vocabulary", "value" => 15);
$lom["lom"]["6"]["6.2"] = array("name" => "Copyright and Other Restrictions", "attributeGroup" => "copyrightAndOtherRestrictions", "presence type" => "Required", "size" => 1, "data type" => "Vocabulary", "value" => 15);
$lom["lom"]["6"]["6.3"] = array("name" => "Description", "attributeGroup" => "description", "presence type" => "Required", "size" => 1, "data type" => "LangString");

$lom["lom"]["7"] = array("name" => "Relation", "attributeGroup" => "relation", "presence type" => "Optional", "size" => 100, "data type" => "Aggregate element");
$lom["lom"]["7"]["7.1"] = array("name" => "Kind", "attributeGroup" => "kind", "presence type" => "Optional", "size" => 100, "data type" => "Vocabulary", "value" => 16);
$lom["lom"]["7"]["7.2"] = array("name" => "Resource", "attributeGroup" => "resource", "presence type" => "Optional", "size" => 1, "data type" => "Aggregate element");
$lom["lom"]["7"]["7.2"]["7.2.1"] = array("name" => "Identifier", "attributeGroup" => "identifier", "presence type" => "Optional", "size" => 10, "data type" => "Aggregate element");
$lom["lom"]["7"]["7.2"]["7.2.1"]["7.2.1.1"] = array("name" => "Catalog", "attributeGroup" => "catalog", "presence type" => "Optional", "size" => 1, "data type" => "CharacterString");
$lom["lom"]["7"]["7.2"]["7.2.1"]["7.2.1.2"] = array("name" => "entry", "attributeGroup" => "entry", "presence type" => "Optional", "size" => 1, "data type" => "CharacterString");
$lom["lom"]["7"]["7.2"]["7.2.2"] = array("name" => "Description", "attributeGroup" => "description", "presence type" => "Optional", "size" => 1, "data type" => "LangString");

$lom["lom"]["8"] = array("name" => "Annotation", "attributeGroup" => "annotation", "presence type" => "Optional", "size" => 30, "data type" => "Aggregate element");
$lom["lom"]["8"]["8.1"] = array("name" => "Entity", "attributeGroup" => "entity", "presence type" => "Optional", "size" => 1, "data type" => "CharacterString");
$lom["lom"]["8"]["8.2"] = array("name" => "Date", "attributeGroup" => "date", "presence type" => "Optional", "size" => 1, "data type" => "DateTime");
$lom["lom"]["8"]["8.3"] = array("name" => "Description", "attributeGroup" => "description", "presence type" => "Optional", "size" => 1, "data type" => "LangString");

$lom["lom"]["9"] = array("name" => "Classification", "attributeGroup" => "classification", "presence type" => "Recommended", "size" => 40, "data type" => "Aggregate element");
$lom["lom"]["9"]["9.1"] = array("name" => "Purpose", "attributeGroup" => "purpose", "presence type" => "Recommended", "size" => 1, "data type" => "Vocabulary", "value" => 17);
$lom["lom"]["9"]["9.2"] = array("name" => "Taxon Path", "attributeGroup" => "taxonPath", "presence type" => "Recommended", "size" => 15, "data type" => "Aggregate element");
$lom["lom"]["9"]["9.2"]["9.2.1"] = array("name" => "Source", "attributeGroup" => "source", "presence type" => "Recommended", "size" => 1, "data type" => "LangString");
$lom["lom"]["9"]["9.2"]["9.2.2"] = array("name" => "Taxon", "attributeGroup" => "taxon", "presence type" => "Recommended", "size" => 15, "data type" => "Aggregate element");
$lom["lom"]["9"]["9.2"]["9.2.2"]["9.2.2.1"] = array("name" => "Id", "attributeGroup" => "id", "presence type" => "Recommended", "size" => 1, "data type" => "CharacterString");
$lom["lom"]["9"]["9.2"]["9.2.2"]["9.2.2.2"] = array("name" => "Entry", "attributeGroup" => "entry", "presence type" => "Recommended", "size" => 1, "data type" => "LangString");
$lom["lom"]["9"]["9.3"] = array("name" => "Description", "attributeGroup" => "description", "presence type" => "Recommended", "size" => 1, "data type" => "LangString");
$lom["lom"]["9"]["9.4"] = array("name" => "Keyword", "attributeGroup" => "keyword", "presence type" => "Optional", "size" => 1, "data type" => "LangString");

$lom["version"] = "NORLOMv1.1";
$GLOBALS['lom'] = $lom;
?>