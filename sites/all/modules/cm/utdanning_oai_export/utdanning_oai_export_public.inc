<?php
/**
* @file
* @ingroup utdanning_oai_export
* Public functions for utdanning_oai_export.
*
* @see utdanning_oai_export.module
*    
* @author hrm
*
* @version 1.0
*
*@license
 *Copyright (C) Utdanning.no
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the LISENCE.txt file or the
 *   GNU General Public License for more details.
*/

/**
 * Enter description here...
 *
 * @return unknown
 */
function utdanning_oai_export_admin_form() {
	$form['setting'] = array(
		'#title' => t('Settings'),
		'#description' => t('General settings for this module.'),
		'#type' => 'fieldset',
		'#access' => user_access('administer utdanning_oai_export'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
	);
	
	$form['setting']['utdanning_oai_export_URI'] = array(
		'#title' => t('The URI for the repository'),
		'#type' => 'textfield',
		'#access' => user_access('administer utdanning_oai_export'),
		'#description' => t('Type in an URI where the repository could be accessed.'),
		'#default_value' => variable_get('utdanning_oai_export_URI', UTDANNING_OAI_EXPORT_DEFAULT_URI),
		'#field_prefix' => '<span class="path">http://'.$_SERVER["SERVER_NAME"].base_path().'</span>',
		'#size' => 25,
	);
	
	$default_value = variable_get('utdanning_oai_export_lom_profile', FALSE);
	if($default_value === FALSE) {
		$default_value = 0;
	}

	if(is_dir(UTDANNING_OAI_EXPORT_DEFAULT_LOM_PATH)) {
		$dir = scandir(UTDANNING_OAI_EXPORT_DEFAULT_LOM_PATH);
		while($dir[0] == "." || $dir[0] == ".." || strpos($dir[0], ".") == 0) {
			array_shift($dir);
		}
		$profiles = array_combine($dir, $dir);
	}

	$form['setting']['utdanning_oai_export_lom_profile'] = array(
		'#title' => t('Profiles'),
		'#type' => 'select',
		'#default_value' => $default_value,
		'#options' => array(t('LOM profiles') => $profiles),
		'#description' => t('Select the correct profile for you\'re country.'),
		'#required' => FALSE,
	);

	return system_settings_form($form);
}

/**
 * Enter description here...
 *
 * @return array $form
 */
function utdanning_oai_export_admin_map_profile() {
	$form['utdanning_oai_export_fields'] = array(
		'#title' => t('Fields'),
		'#description' => t('Configuration for the mapping of NORLOM-elements.'),
		'#type' => 'fieldset',
		'#access' => user_access('administer utdanning_oai_export'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
	);

	$resource = db_query("SELECT id,profile FROM utdanning_oai_export_settings ORDER BY profile ASC");
	$profiles = array(0 => t('- None -'));
	
	while($profile_data = db_fetch_array($resource))
	{
		$profiles[$profile_data['id']] = $profile_data['profile'];
	}
	
	$form['utdanning_oai_export_fields']['profile'] = array(
		'#title' => t('Profiles'),
		'#type' => 'select',
		'#default_value' => 0,
		'#options' => array(t('Profiles') => $profiles),
		'#description' => t('Select the profile you want to delete.'),
		'#required' => FALSE,
	);
	
	$form['utdanning_oai_export_fields']['new_profile'] = array(
		'#title' => t('Create new profile'),
		'#type' => 'textfield',
		'#description' => t('Enter the name of the new profile. Preffered names are the same as the content-types.'),
		'#maxlength' => 50,
		'#required' => FALSE,
		'#size' => 20,
	);
		
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save changes'),
	);
	
	return $form;
}

/**
 * Enter description here...
 *
 * @param array $form
 * @param array $form_state
 */
function utdanning_oai_export_admin_map_profile_submit($form, &$form_state)
{
	if(isset($form_state['values']['new_profile']) && $form_state['values']['new_profile'] != "") {
		if(db_query("INSERT INTO {utdanning_oai_export_settings} (profile) VALUES('%s')", check_plain($form_state['values']['new_profile']))) {
			drupal_set_message(t("Profile @profile have been created.", array('@profile' => check_plain($form_state['values']['new_profile']))));
			menu_rebuild();
			drupal_set_message(t('Menus have been rebuild.'),'warning');
		}
	}
	else if(isset($form_state['values']['profile']) && $form_state['values']['profile'] != 0) {
		if(db_query("DELETE FROM {utdanning_oai_export_settings} WHERE id = %d LIMIT 1", check_plain($form_state['values']['profile']))) {
			drupal_set_message(t('Selected profile has been deleted.'));
			menu_rebuild();
			drupal_set_message(t('Menus have been rebuild.'),'warning');
		}
		//print "<pre>".print_r($form_state, true)."</pre>";
	}
}

/**
 * Enter description here...
 *
 * @return array $form
 */
function utdanning_oai_export_admin_mapping() {
	$form['utdanning_oai_export_fields'] = array(
		'#title' => t('Fields'),
		'#description' => t('Config for the mapping of NORLOM-elements.'),
		'#type' => 'fieldset',
		'#access' => user_access('administer utdanning_oai_export'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
	);

	$db_result = db_query("SELECT * FROM {utdanning_oai_export_settings} WHERE id = '%d' LIMIT 1", check_plain(arg(4)));
	if($db_result) {
		$values = db_fetch_array($db_result);
	}
	else {
		$values = FALSE;
	}
	
	$form['utdanning_oai_export_fields']['numberPerPage'] = array(
		'#title' => t('Number per page'),
		'#type' => 'textfield',
		'#default_value' => $values['numPage'],
		'#description' => t('The maximum number of records to issue per response.
						If result set is larger than this number, a resumption token will be issued.'),
		'#maxlength' => 3,
		'#required' => TRUE,
		'#size' => 5,
	);
	
	$form['utdanning_oai_export_fields']['profile_name'] = array(
		'#title' => t('Profile name'),
		'#type' => 'textfield',
		'#default_value' => $values['profile'],
		'#description' => t('Here you can change the name of this profile.'),
		'#maxlength' => 50,
		'#required' => TRUE,
		'#size' => 20,
	);
	
	$form['utdanning_oai_export_fields']['content_type'] = array(
		'#title' => t('Content type'),
		'#type' => 'select',
		'#default_value' => $values['content_type'],
		'#options' => array(t('Content types') => node_get_types('names')),
		'#description' => t('Select the content-type you wish to use for this profile.'),
		'#required' => TRUE,
	);
	
	_utdanning_oai_export_formTraverser($form, unserialize($values['data']));
	
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save changes'),
	);
	
	return $form;
}

/**
 * A validate function for field-mapping
 *   It checks for unique profile names. Instead of the user getting a nasty errormessage
 *   it will be presented with a text that describes the problem.
 *
 * @param array $form
 * @param array $form_state
 */
function utdanning_oai_export_admin_mapping_validate($form, &$form_state)
{
	$db_resource = db_query("SELECT profile FROM utdanning_oai_export_settings WHERE id != '%d'", check_plain(arg(4)));
	while($db_array = db_fetch_array($db_resource)) {
		if($db_array['profile'] == check_plain($form_state['values']['profile_name'])) {
			form_set_error('profile_name', t('The profile name have to be unique!'));
		}
	}
}

/**
 * A submit function for field-mapping
 *
 * @param array $form
 * @param array $form_state
 */
function utdanning_oai_export_admin_mapping_submit($form, &$form_state)
{
	$values = array();	
	foreach($form_state['values'] AS $key => $value)
	{
		if(strncmp($key, "lom_", 4) == 0) $values[str_replace('_', '.', substr($key, 4))] = $value;
	}

	$db_resource = db_query("UPDATE {utdanning_oai_export_settings} 
							 SET profile = '%s', numPage = '%s', content_type = '%s', data = '%s' 
							 WHERE id = '%s'", 
							 check_plain($form_state['values']['profile_name']), 
							 check_plain($form_state['values']['numberPerPage']), 
							 check_plain($form_state['values']['content_type']), 
							 serialize($values), 
							 check_plain(arg(4)));
	if($db_resource) {
		drupal_set_message(t("Mapping saved successfully"));
		menu_rebuild();
		drupal_set_message(t('Menus have been rebuild.'),'warning');
	}
}

/**
 * Enter description here...
 *
 * @param array $items
 */
function utdanning_oai_export_form_profiles(&$items) {
	$query_data = db_query("SELECT id,profile FROM {utdanning_oai_export_settings}");
	if($query_data) {
		while($values = db_fetch_array($query_data)) {
			$items['admin/settings/oai_export/profile/'.$values['id']] = array(
				'title' => $values['profile'],
				'description' => t('Change settings for the @profile.', array('@profile' => $values['profile'])),
				'access arguments' => array('administer utdanning_oai_export'),
				'page callback' => 'drupal_get_form',
				'page arguments' => array('utdanning_oai_export_admin_mapping'),
				'type' => MENU_LOCAL_TASK,
			);
		}
	}	
}

/**
 * Returns an array of supported metadataPrefixes
 *
 * @return array
 */
function utdanning_oai_export_metadataPrefixes()
{
	return array('norlom');
}
