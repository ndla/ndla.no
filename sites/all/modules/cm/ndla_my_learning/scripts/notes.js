NdlaNotes = {};
NdlaNotes.notes = [];

NdlaNotes.init = function(loggedIn) {
  var notes = userData.getData('page', 'notes');
  
  if(notes != null) {
    NdlaNotes.notes = notes;
  }

  $block = $('<div class="block notes"><ul><li title="' + Drupal.t('Add note') + '">' + Drupal.t('Add note') + '</li></ul><div id="notes_form"><textarea id="note-editor-new"></textarea><br><input type="button" value="' + Drupal.t('Add note') + '"></div></div>').appendTo('.region-learning');
  
  NdlaNotes.add_editor('#notes_form textarea');
  
  $('li', $block).click(function () {
    $('#notes_form').slideToggle();
  });
  
  $(NdlaNotes.notes).each(function(){
    NdlaNotes.add_note(this);
  });
  
	$('#notes_form input').click(function(){
    var text = tinyMCE.get('note-editor-new').getContent();
    tinyMCE.get('note-editor-new').setContent('');
	  $('#notes_form').slideToggle(function(){
	    var note = {
	      title: $(document).attr('title'),
        subject: Drupal.settings.ndla_mssclient.fag,
        contenttype: Drupal.settings.ndla_mssclient.contenttype,
        text: text,
      };
      NdlaNotes.notes.push(note);
      NdlaNotes.add_note(note);
      userData.saveData('page', 'notes', NdlaNotes.notes);
	    $('#notes_form textarea').val('');
    });
	});
}

NdlaNotes.add_note = function(note) {
  $('.li-stickers').show();
  var id = $('.ndla_note').length;
  $('.region-stickers .block').before("<div class='ndla_note n" + id + "'><span class=''>" + note.text + "</span><textarea id=note-editor-" + id + "></textarea><div class='note-actions'><a class='note-edit n" + id + "' href='#' onclick='NdlaNotes.edit_note(this);'>" + Drupal.t('Edit') + "</a><a class='note-delete n" + id + "' href='#' onclick='NdlaNotes.remove_note(this);'>" + Drupal.t('Delete') + "</a><a class='note-save n" + id + "' href='#' onclick='NdlaNotes.save_note(this);'>" + Drupal.t('Save') + "</a><a class='note-cancel n" + id + "' href='#' onclick='NdlaNotes.cancel_note(this);'>" + Drupal.t('Cancel') + "</a></div></div>");
}

NdlaNotes.remove_note = function(element) {
  id = $(element).attr('class').replace(/[^0-9]*/, '');
  $('div.n' + id).remove();
  NdlaNotes.notes.splice(id, 1);
  userData.saveData('page', 'notes', NdlaNotes.notes);
  
  for(i = id;i<NdlaNotes.notes.length + 1;i++) {
    var prev = i - 1;
    $('div.n' + i).removeClass('n' + i).addClass('n' + prev);
    $('a.n' + i).removeClass('n' + i).addClass('n' + prev);
  }
}

NdlaNotes.edit_note = function(element) {
  id = $(element).attr('class').replace(/[^0-9]*/, '');
  $('.ndla_note.n' + id + ' span').hide();
  $('.ndla_note.n' + id + ' .note-actions a.note-edit').hide();
  $('.ndla_note.n' + id + ' .note-actions a.note-delete').hide();
  $('.ndla_note.n' + id + ' .note-actions a.note-save').show();
  $('.ndla_note.n' + id + ' .note-actions a.note-cancel').show();  
  $('.ndla_note.n' + id + ' textarea').val(NdlaNotes.notes[id].text).show().focus();
  NdlaNotes.add_editor('.ndla_note.n' + id + ' textarea');
}

NdlaNotes.cancel_note = function(element) {
  id = $(element).attr('class').replace(/[^0-9]*/, '');
  $('.ndla_note.n' + id + ' .note-actions a.note-save').hide();
  $('.ndla_note.n' + id + ' .note-actions a.note-cancel').hide();

  tinyMCE.get('note-editor-' + id).destroy();
  $('.ndla_note.n' + id + ' textarea').hide();
  
  $('.ndla_note.n' + id + ' span').show();
  $('.ndla_note.n' + id + ' .note-actions a.note-edit').show();
  $('.ndla_note.n' + id + ' .note-actions a.note-delete').show();
}

NdlaNotes.save_note = function(element) {
  
  id = $(element).attr('class').replace(/[^0-9]*/, '');
  
  text = tinyMCE.get('note-editor-' + id).getContent();
  tinyMCE.get('note-editor-' + id).destroy();
  $('.ndla_note.n' + id + ' textarea').hide();
  
  NdlaNotes.notes[id].text = text;
  $('.ndla_note.n' + id + ' .note-actions a.note-save').hide();
  $('.ndla_note.n' + id + ' .note-actions a.note-cancel').hide();
  $('.ndla_note.n' + id + ' span').html(text).show();
  $('.ndla_note.n' + id + ' .note-actions a.note-edit').show();
  $('.ndla_note.n' + id + ' .note-actions a.note-delete').show();
  userData.saveData('page', 'notes', NdlaNotes.notes);g
}

NdlaNotes.add_editor = function(selector) {
  tinymce.init({
    selector: selector,
    menubar : false,
    plugins: ["link"],
    toolbar: "bold italic | link image",
    statusbar : false
  });
}

userData.ready(function (loggedIn) {
  NdlaNotes.init(loggedIn);
});