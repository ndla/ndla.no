NdlaReminders = {};
NdlaReminders.reminders = [];

NdlaReminders.init = function(loggedIn) {
  var reminders = userData.getData('page', 'reminder');
  
  if(reminders != null) {
    NdlaReminders.reminders = reminders;
  }
  
  $block = $('<div class="block reminders"><ul><li title="' + Drupal.t('Add reminder') + '">' + Drupal.t('Add reminder') + '</li></ul><div id="current">' + Drupal.t('Current reminders') + ':<ul></ul></div><div id="reminder_form"><div id="reminder_calendar"></div><br><textarea></textarea><br><input type="button" value="' + Drupal.t('Add reminder') + '"></div></div>').appendTo('.region-learning');
  $('li', $block).click(function () {
    $('#reminder_form').slideToggle();
  });
  
  if(loggedIn) {
    g_jsDatePickImagePath = Drupal.settings.basePath + "sites/all/modules/ndla_reminders/libraries/jsdatepick-calendar/img/";
    
    reminder = new JsDatePick({
	  	useMode:1,
	  	isStripped:true,
	  	target:"reminder_calendar",
	  	imgPath: Drupal.settings.basePath + "sites/all/modules/ndla_reminders/libraries/jsdatepick-calendar/img/"
	  });
	
	  $('#reminder_form input').click(function(){
	    var obj = reminder.getSelectedDay();
	  	if(obj == false) {
	  	  alert(Drupal.t('No date has been selected.'));
	  	  return;
	    }
	    var text = $('#reminder_form textarea').val();
      var date = new Date(obj.year, (obj.month - 1), obj.day);
	  	var timestamp = date.getTime() / 1000;
	  	$('#reminder_form').slideToggle(function(){
        var reminder = {
	  	    title: $(document).attr('title'),
          subject: Drupal.settings.ndla_mssclient.fag,
          contenttype: Drupal.settings.ndla_mssclient.contenttype,
          text: text,
          timestamp: timestamp
        };
        NdlaReminders.reminders.push(reminder);
        NdlaReminders.show_reminder(reminder);
	  	  userData.saveData('page', 'reminder', NdlaReminders.reminders);
	  	  $('#reminder_form textarea').val('');
    	});
	  });  
  }

  $(NdlaReminders.reminders).each(function(key, reminder) {
    NdlaReminders.show_reminder(reminder);
  });
}

userData.ready(function (loggedIn) {
  NdlaReminders.init(loggedIn);
});

NdlaReminders.show_reminder = function(reminder) {
  $('.reminders #current').show();
  var date = new Date(Number(reminder.timestamp) * 1000);
  $('.reminders #current ul').append('<li>' + date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear() + "</li>");
}