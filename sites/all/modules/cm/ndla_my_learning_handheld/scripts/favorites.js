NdlaFavorites = {};

NdlaFavorites.init = function(loggedIn) {
  if(!loggedIn) {
    return;
  }
  var $button = $('<i class="icon-bookmark-empty"></i>');
  $('#my-learning-panel').append($button);
  var marked = userData.getData('page', 'favorite');
  if(marked != null) {
    $button.removeClass('icon-bookmark-empty').addClass('icon-bookmark');
  }
  $button.click(function() {
    NdlaFavorites.mark($(this));
  });
};

NdlaFavorites.mark = function($button) {
  var isMarked = $button.hasClass('icon-bookmark');
  if(!isMarked) {
    $button.removeClass('icon-bookmark-empty').addClass('icon-bookmark');
    userData.saveData('page', 'favorite', {
      title: $(document).attr('title'),
      subject: Drupal.settings.ndla_mssclient.fag,
      contenttype: Drupal.settings.ndla_mssclient.contenttype
    });
  } else {
    $button.removeClass('icon-bookmark').addClass('icon-bookmark-empty');
    userData.removeData('page', 'favorite');
  }
};

userData.ready(function (loggedIn) {
  NdlaFavorites.init(loggedIn);
});
