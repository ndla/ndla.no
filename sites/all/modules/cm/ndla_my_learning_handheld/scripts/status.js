NdlaStatus = {};

NdlaStatus.init = function(loggedIn) {
  if(!loggedIn) {
    return;
  }
  $button = $('<i class="icon-dashboard"></i>');
  $('#my-learning-panel').append($button);
  var color = userData.getData('page', 'learningStatus');
  $button.click(function() {
    var html = "<ul><li class='green'><i class='icon-circle'></i>" + Drupal.t('Learned and understood') + "</li><li class='yellow'><i class='icon-circle'></i>" + Drupal.t('Perceived, but must be repeated') + "</li><li class='red'><i class='icon-circle'></i>" + Drupal.t('Not sufficiently understood') + "</li></ul>";
    NdlaPanel.toggle_dropdown(Drupal.t('Learning status'), html, 'status');
    $('#my-learning-panel-dropdown fieldset.subject_dropdown li').click(NdlaStatus.mark);
  });
  if(color != null) {
    $button.addClass(color.color);
  }
};

NdlaStatus.mark = function() {
  var color = $(this).attr('class');
  if($('#my-learning-panel .icon-dashboard').hasClass(color)) {
    $('#my-learning-panel .icon-dashboard').removeClass('red yellow green');
    userData.removeData('page', 'learningStatus');
  } else {
    $('#my-learning-panel .icon-dashboard').removeClass('red yellow green').addClass(color);
    userData.saveData('page', 'learningStatus', {
      color: color,
      title: $(document).attr('title'),
      subject: Drupal.settings.ndla_mssclient.fag,
      contenttype: Drupal.settings.ndla_mssclient.contenttype
    });
  }
  NdlaPanel.toggle_dropdown('', '', 'status');
};

userData.ready(function (loggedIn) {
  NdlaStatus.init(loggedIn);
});
