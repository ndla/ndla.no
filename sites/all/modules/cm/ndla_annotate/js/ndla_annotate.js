NdlaAnnotate = {};

NdlaAnnotate.notes = [];
NdlaAnnotate.thread_cancel = false;
var rangyCssApplier;

NdlaAnnotate.init = function() {
  rangy.init();
  rangyCssApplier = rangy.createCssClassApplier("ndla-annotate-selection-new", {normalize: true});

  var region = $('.region-learning .block:first');
  var url = Drupal.settings.ndla_utils.base_url + Drupal.settings.basePath;

  /* Add button */
  var html = "<div id='ndla-annotate-add-note'><img src='" + url + "sites/all/modules/ndla_annotate/img/write.png'> <a>" + Drupal.t('Add note') + "</a></div>";
  region.html(html);
  
  var notes = userData.getData('page', 'ndla_annotation');
  if(notes != null) {
    $(notes).each(function(pid, thread){
      NdlaAnnotate.add_thread(pid);
      $(thread).each(function(aid, note){
        if(note.area != undefined && note.area != '') {
          NdlaAnnotate.restore_selection(pid, note);
        }
        NdlaAnnotate.add_note(pid, note, true);
      });
    });
  }
  
  /* Add note */
  $('#ndla-annotate-add-note a').mousedown(NdlaAnnotate.insert_note);
  $('.ndla_annotate_textselect').show();
}

NdlaAnnotate.restore_selection = function(pid, note) {
  var range = rangy.createRange();
  var caseSensitive = false;
  var searchScopeRange = rangy.createRange();
  searchScopeRange.selectNodeContents(document.body);
  var options = {
      caseSensitive: caseSensitive,
      wholeWordsOnly: false,
      withinRange: searchScopeRange,
      direction: "forward"
  };
  range.selectNodeContents(document.body);
  rangyCssApplier.undoToRange(range);
  regexp = new RegExp(note.area.replace('&nbsp;',' ').replace(/[^A-Z0-9\s]/ig, '.').replace(/\s+/g, '\\s*'), 'g');
  var select_in_area = null;
  while (range.findText(regexp, options)) {
      rangyCssApplier.applyToRange(range);
      select_in_area = range.commonAncestorContainer;
      range.collapse(false);
  }
  $('.ndla-annotate-selection-new').removeClass('ndla-annotate-selection-new').addClass('ndla-annotate-selection-area-' + pid);
  var range = rangy.createRange();
  var caseSensitive = false;
  var searchScopeRange = rangy.createRange();
  searchScopeRange.selectNodeContents(select_in_area);
  options = {
      caseSensitive: caseSensitive,
      wholeWordsOnly: false,
      withinRange: searchScopeRange,
      direction: "forward"
  };
  range.selectNodeContents(document.body);
  rangyCssApplier.undoToRange(range);
  if (searchScopeRange.findText(note.selection.replace(' ', '\s*'), options)) {
      rangyCssApplier.applyToRange(searchScopeRange);
      searchScopeRange.collapse(false);
      $('.ndla-annotate-selection-new').removeClass('ndla-annotate-selection-new').addClass('ndla-annotate-selection pid' + pid);
  }
}

NdlaAnnotate.add_thread = function(pid) {
  var region = $('.region-learning .block:first');
  var button = $('#ndla-annotate-add-note');
  thread = "<div class='ndla-annotate-thread pid" + pid + "'><div class='ndla-annotate-arrow'>&nbsp;</div><div class='ndla-annotate-block ndla-annotate-answer'><textarea></textarea><div class='ndla-annotate-actions'><input type='button' value='" + Drupal.t('Answer') + "'><a class='ndla-annotate-cancel'>" + Drupal.t('Cancel') + "</a></div></div></div>";
  //region.append(thread);
  button.after(thread);
  NdlaAnnotate.notes.push([]);
  NdlaAnnotate.add_thread_listeners(pid);
}

NdlaAnnotate.add_note = function(pid, note, init) {
  var aid = NdlaAnnotate.notes[pid].length;
  var location = $('.ndla-annotate-thread.pid' + pid + ' .ndla-annotate-answer');
  html = "<div class='ndla-annotate-note ndla-annotate-block aid" + aid + "'><div class='ndla-annotate-display'><div class='ndla-annotate-text'>" + note.text + "</div><div class='ndla-annotate-actions'><a class='ndla-annotate-edit'>" + Drupal.t('Edit') + "</a><a class='ndla-annotate-delete'>" + Drupal.t('Delete') + "</a><span class='ndla-annotate-date'>" + note.time + "</span></div></div><div class='ndla-annotate-form'><textarea></textarea><div class='ndla-annotate-actions'><input type='button' value='" + Drupal.t('Save') + "'><a class='ndla-annotate-cancel'>" + Drupal.t('Cancel') + "</a></div></div></div>";
  location.before(html);
  NdlaAnnotate.notes[pid].push({});
  NdlaAnnotate.notes[pid][aid] = note;
  NdlaAnnotate.add_note_listeners(pid, aid);
  NdlaAnnotate.resize();
  if(!init) {
    userData.saveData('page', 'ndla_annotation', NdlaAnnotate.notes);
  }
}

NdlaAnnotate.add_thread_listeners = function(pid) {
  var thread = $('.ndla-annotate-thread.pid' + pid);

  /* Select */
  thread.click(NdlaAnnotate.select_thread);

  /* Cancel */
  thread.find('.ndla-annotate-answer .ndla-annotate-cancel').click(NdlaAnnotate.cancel_thread);

  /* Answer */
  //thread.find('.ndla-annotate-answer input[type=button]').click(NdlaAnnotate.answer_thread);
}

NdlaAnnotate.select_thread = function() {
  if($(this).hasClass('selected') || NdlaAnnotate.thread_cancel == true) {
    NdlaAnnotate.thread_cancel = false;
    return;
  }
  $('.ndla-annotate-thread.selected').removeClass('selected');
  $(this).addClass('selected');
  
  $('.ndla-annotate-thread.pidnew').remove();
  $('.ndla-annotate-answer textarea').val('');
  $('.ndla-annotate-display').show();
  $('.ndla-annotate-form').hide();
  
  $('.ndla-annotate-selection.selected').removeClass('selected');
  var pid = NdlaAnnotate.get_id(this);
  $('.ndla-annotate-selection.pid' + pid).addClass('selected');
  
  NdlaAnnotate.resize();
}

NdlaAnnotate.cancel_thread = function() {
  $('.ndla-annotate-thread.selected').removeClass('selected');
  $('.ndla-annotate-selection.selected').removeClass('selected');
  $('.ndla-annotate-display').show();
  $('.ndla-annotate-form').hide();
  NdlaAnnotate.resize();
  NdlaAnnotate.thread_cancel = true;
}

//NdlaAnnotate.answer_thread = function() {
//  var answer = $(this).parents('.ndla-annotate-answer');
//  var thread = $(this).parents('.ndla-annotate-thread');
//  var pid = NdlaAnnotate.get_id(thread);
//  var text = answer.find('textarea').val();
//  answer.find('textarea').val('');
//
//  $.post(Drupal.settings.basePath + 'sites/all/modules/ndla_annotate/NdlaAnnotateServer.php',
//    {'pid' : pid, 'text' : text},
//    NdlaAnnotate.set_note, "json");
//}
//
//NdlaAnnotate.set_note = function(data) {
//  var note = {
//    'text' : data.text,
//    'selection' : '',
//    'diff' : '',
//    'time' : data.time
//  };
//  NdlaAnnotate.add_note(data.pid, data, false);
//}

NdlaAnnotate.add_note_listeners = function(pid, aid) {
  var note = $('.pid' + pid + ' .aid' + aid);

  /* Edit */
  note.find('.ndla-annotate-edit').click(NdlaAnnotate.edit);

  /* Delete */
  note.find('.ndla-annotate-delete').click(NdlaAnnotate.remove);

  /* Save */
  note.find('.ndla-annotate-form input[type=button]').click(NdlaAnnotate.save);

  /* Cancel */
  note.find('.ndla-annotate-cancel').click(NdlaAnnotate.cancel);
}

NdlaAnnotate.remove = function(){
  var note = $(this).parents('.ndla-annotate-note');
  var thread = $(this).parents('.ndla-annotate-thread');
  var aid = NdlaAnnotate.get_id(note);
  var pid = NdlaAnnotate.get_id(thread);
  if(aid == 0) {
    if(thread.find('.ndla-annotate-note').length > 1) {
      if(!confirm(Drupal.t('Deleting this annotation will also delete related answers. Do you want to delete it?'))) {
        return;
      }
    }
    $('.ndla-annotate-selection.pid' + pid).removeClass('ndla-annotate-selection selected pid' + pid);
    thread.remove();
    for(i = pid;i<NdlaAnnotate.notes.length;i++) {
      var prev = i - 1;
      $('.ndla-annotate-thread.pid' + i).removeClass('pid' + i).addClass('pid' + prev);
    }
    NdlaAnnotate.array_remove(NdlaAnnotate.notes, pid);
  } else {
    note.remove();
    for(i = aid;i<NdlaAnnotate.notes[pid].length;i++) {
      var prev = i - 1;
      $('.ndla-annotate-note.aid' + i).removeClass('aid' + i).addClass('aid' + prev);
    }
    NdlaAnnotate.array_remove(NdlaAnnotate.notes[pid], aid);
  }
  NdlaAnnotate.resize();
  userData.saveData('page', 'ndla_annotation', NdlaAnnotate.notes);
}

NdlaAnnotate.edit = function() {
  var note = $(this).parents('.ndla-annotate-note');
  var display = note.find('.ndla-annotate-display');
  var form = note.find('.ndla-annotate-form');
  var text = display.find('.ndla-annotate-text').html().trim();
  form.find('textarea').val(text);
  display.hide();
  form.show();
  NdlaAnnotate.resize();
}

NdlaAnnotate.cancel = function() {
  var note = $(this).parents('.ndla-annotate-note');
  var display = note.find('.ndla-annotate-display');
  var form = note.find('.ndla-annotate-form');
  display.show();
  form.hide();
  NdlaAnnotate.resize();
}

NdlaAnnotate.save = function() {
  var note = $(this).parents('.ndla-annotate-note');
  var thread = $(this).parents('.ndla-annotate-thread');

  var aid = NdlaAnnotate.get_id(note);
  var pid = NdlaAnnotate.get_id(thread);
  var text = note.find('textarea').val();

  $.post(Drupal.settings.basePath + 'sites/all/modules/ndla_annotate/NdlaAnnotateServer.php',
    {'aid' : aid, 'pid' : pid, 'text' : text},
    NdlaAnnotate.update_note, "json");

  note.find('.ndla-annotate-text').html(text);
  var display = note.find('.ndla-annotate-display');
  var form = note.find('.ndla-annotate-form');
  display.show();
  form.hide();
  NdlaAnnotate.resize();
}

NdlaAnnotate.update_note = function(data) {
  var aid = data.aid;
  var pid = data.pid;
  $('.pid'+ pid + ' .aid' + aid + ' .ndla-annotate-date').html(data.time);
  NdlaAnnotate.notes[pid][aid].text = data.text;
  NdlaAnnotate.notes[pid][aid].time = data.time;
  NdlaAnnotate.notes[pid][aid].timestamp = data.timestamp;
  userData.saveData('page', 'ndla_annotation', NdlaAnnotate.notes);
}

NdlaAnnotate.insert_note = function() {
  /* Reset */
  $('.ndla-annotate-thread.pidnew').remove();
  $('.ndla-annotate-thread.selected').removeClass('selected');
  $('.ndla-annotate-selection.selected').removeClass('selected');
  $('.ndla-annotate-answer textarea').val('');
  $('.ndla-annotate-display').show();
  $('.ndla-annotate-form').hide();
  
  /* Hidden insert thread */
  var html = "<div class='ndla-annotate-thread pidnew selected'><div class='ndla-annotate-arrow'>&nbsp;</div><div class='ndla-annotate-block'><textarea></textarea><div class='ndla-annotate-actions'><input type='button' value='" + Drupal.t('Save') + "'/><input type='hidden' name='selection'><input type='hidden' name='area'><a class='ndla-annotate-cancel'>" + Drupal.t('Cancel') + "</a></div></div></div>";
  var button = $('#ndla-annotate-add-note');
  button.after(html);
  
  /* Get content */
  var thread = $('.ndla-annotate-thread.pidnew');

  /* Get selected content and area */
  var selection = ndla_textselect_get_selection().replace(/<[^>]*>/g, '').replace(/\s+/g, ' ');
  expand_selection();
  var area = ndla_textselect_get_selection().replace(/<[^>]*>/g, '').replace(/\s+/g, ' ');
  collpase_selection();
  
  /* Add selection */
  rangyCssApplier.applyToSelection();
  
  thread.find('input[name=selection]').val(selection);
  thread.find('input[name=area]').val(area);

  /* Set selection class */
  $('.ndla-annotate-selection-new').removeClass('ndla-annotate-selection-new').addClass('ndla-annotate-selection selected pidnew');

  /* Add listeners */
  
  /* New note cancel */
  $('.ndla-annotate-thread.pidnew .ndla-annotate-cancel').click(NdlaAnnotate.cancel_note);

  /* Save note */
  $('.ndla-annotate-thread.pidnew input[type=button]').click(NdlaAnnotate.save_note);
  
  NdlaAnnotate.resize();
}

NdlaAnnotate.cancel_note = function() {
  $('.ndla-annotate-thread.pidnew').remove();
  $('.ndla-annotate-selection.pidnew').removeClass('ndla-annotate-selection selected pidnew');
  NdlaAnnotate.resize();
}

NdlaAnnotate.save_note = function() {
  var thread = $('.ndla-annotate-thread.pidnew');
  var text = thread.find('textarea').val();
  var selection = thread.find('input[name=selection]').val();
  var area = thread.find('input[name=area]').val();
  $('.ndla-annotate-selection.selected').removeClass('selected');

  $.post(Drupal.settings.basePath + 'sites/all/modules/ndla_annotate/NdlaAnnotateServer.php',
    {'text' : text, 'selection' : selection, 'area' : area},
    NdlaAnnotate.insert_thread, "json");
    
  thread.remove();
}

NdlaAnnotate.insert_thread = function(data) {
  pid = NdlaAnnotate.notes.length;
  $('.pidnew').removeClass('pidnew').addClass('pid' + pid);
  NdlaAnnotate.add_thread(pid);
  var note = {
    'title' : $(document).attr('title'),
    'text' : data.text,
    'selection' : data.selection,
    'area' : data.area,
    'time' : data.time,
    'timestamp' : data.timestamp,
  };
  NdlaAnnotate.add_note(pid, note, false);
}

NdlaAnnotate.resize = function() {
  $('.ndla-annotate-thread').css('top', '0px');
  var error = 0;
  var offset_top = 0;
  $('.ndla-annotate-thread:visible').each(function(){
    var offset = $(this).offset().top;
    if(offset < offset_top || offset_top == 0) {
      offset_top = offset;
    }
  });
  if(offset_top == 0) {
    $('.region-learning .block:first').css('height', null);
    return; 
  }
  var offset_bottom = offset_top;
  $('.ndla-annotate-thread:visible').addClass('resize-me');
  while($('.ndla-annotate-thread.resize-me').length > 0 && error < 100) {
    var offset_min = 0;
    var pid_min = -1;
    $('.ndla-annotate-thread.resize-me').each(function(){
      var pid = NdlaAnnotate.get_id(this);
      if($('.ndla-annotate-selection.pid' + pid).length) {
        offset = $('.ndla-annotate-selection.pid' + pid).offset().top;
      } else {
        offset = offset_top;
      }
      if(offset < offset_min || pid_min == -1) {
        offset_min = offset;
        pid_min = pid;
      }
    });
    var element = $('.ndla-annotate-thread.pid' + pid_min);
    element.removeClass('resize-me');
    if(offset_min < offset_bottom) {
      offset_min = offset_bottom;
    }
    x = offset_min - element.offset().top;
    element.css('top', x + 'px');
    offset_bottom = offset_min + element.height() + 15;
    error++;
  }
  x = offset_bottom - $('.region-learning .block:first').offset().top - 15;
  if(x > 0) {
    $('.region-learning .block:first').css('height', x  + 'px');
  } else {
    $('.region-learning .block:first').css('height', null);
  }
}

NdlaAnnotate.get_id = function(element) {
  return $(element).attr('class').match(/id([^\s]*)/)[1];
}

NdlaAnnotate.array_remove = function(array, start) {
  array.splice(start, 1);
};

ndla_annotate_insert = function() {
  NdlaAnnotate.insert_note();x
}

userData.ready(function () {
  NdlaAnnotate.init();
});

function expand_selection() {
  var sel;
  // Check for existence of window.getSelection() and that it has a
  // modify() method. IE 9 has both selection APIs but no modify() method.
  if (window.getSelection && (sel = window.getSelection()).modify) {
    sel = window.getSelection();
    if (!sel.isCollapsed) {
      // Detect if selection is backwards
      var range = document.createRange();
      range.setStart(sel.anchorNode, sel.anchorOffset);
      range.setEnd(sel.focusNode, sel.focusOffset);
      var backwards = range.collapsed;
      range.detach();
      // modify() works on the focus of the selection
      var endNode = sel.focusNode, endOffset = sel.focusOffset;
      sel.collapse(sel.anchorNode, sel.anchorOffset);
      if (backwards) {
        direction = ['backward', 'forward'];
      } else {
        direction = ['forward', 'backward'];
      }
      for (var i=0; i<5; i++) {
        sel.modify("move", direction[1], "character");
      }
      sel.extend(endNode, endOffset);
      for (var i=0; i<5; i++) {
        sel.modify("extend", direction[0], "character");
      }
    }
  } else if ( (sel = document.selection) && sel.type != "Control") {
    var textRange = sel.createRange();
    if (textRange.text) {
      textRange.moveStart("character", -5);
      textRange.moveEnd("character", 5);
      //textRange.expand("word");
      //// Move the end back to not include the word's trailing space(s),
      //// if necessary
      //while (/\s$/.test(textRange.text)) {
      //    textRange.moveEnd("character", -1);
      //}
      textRange.select();
    }
  }
}

function collpase_selection() {
  var sel;
  // Check for existence of window.getSelection() and that it has a
  // modify() method. IE 9 has both selection APIs but no modify() method.
  if (window.getSelection && (sel = window.getSelection()).modify) {
    sel = window.getSelection();
    if (!sel.isCollapsed) {
      // Detect if selection is backwards
      var range = document.createRange();
      range.setStart(sel.anchorNode, sel.anchorOffset);
      range.setEnd(sel.focusNode, sel.focusOffset);
      var backwards = range.collapsed;
      range.detach();
      // modify() works on the focus of the selection
      var endNode = sel.focusNode, endOffset = sel.focusOffset;
      sel.collapse(sel.anchorNode, sel.anchorOffset);
      if (backwards) {
        direction = ['backward', 'forward'];
      } else {
        direction = ['forward', 'backward'];
      }
      for (var i=0; i<5; i++) {
        sel.modify("move", direction[0], "character");
      }
      sel.extend(endNode, endOffset);
      for (var i=0; i<5; i++) {
        sel.modify("extend", direction[1], "character");
      }
    }
  } else if ( (sel = document.selection) && sel.type != "Control") {
    var textRange = sel.createRange();
    if (textRange.text) {
      textRange.moveStart("character", 5);
      textRange.moveEnd("character", -5);
      //textRange.expand("word");
      //// Move the end back to not include the word's trailing space(s),
      //// if necessary
      //while (/\s$/.test(textRange.text)) {
      //    textRange.moveEnd("character", -1);
      //}
      textRange.select();
    }
  }
}