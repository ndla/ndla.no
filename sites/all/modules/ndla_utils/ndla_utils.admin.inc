<?php

/**
 * Settings page for the NDLA2010 theme front page
 */
function ndla_utils_ndla2010_theme_admin() {
  $content_types = array();
  $result = db_query("SELECT tid AS id, name FROM {term_data} WHERE vid = 100004");
  while ($term = db_fetch_object($result)) {
    $content_types[$term->id] = $term->name;
  }
  $form['ndla_utils_ndla2010_theme_actuality'] = array(
    '#type' => 'fieldset',
    '#title' => t('Actuality'),
    '#collapsible' => FALSE
  );
  $form['ndla_utils_ndla2010_theme_actuality']['ndla_utils_ndla2010_theme_actuality_content_type'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#options' => $content_types,
    '#default_value' => variable_get('ndla_utils_ndla2010_theme_actuality_content_type', 0),
    '#description' => t('Content type to mark as Actuality on front pages.')
  );
  $form['ndla_utils_ndla2010_theme_feedback_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Feedback URL'),
    '#default_value' => variable_get('ndla_utils_ndla2010_theme_feedback_url', 'http://feedback.ndla.no'),
    '#description' => t('URL to the feedback api server.')
  );
  $form['ndla_utils_ndla2010_theme_front_page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Front Page'),
    '#collapsible' => FALSE
  );
  $form['ndla_utils_ndla2010_theme_front_page']['ndla_utils_ndla2010_theme_front_page_tweets'] = array(
    '#type' => 'textfield',
    '#title' => t('Tweets'),
    '#default_value' => variable_get('ndla_utils_ndla2010_theme_front_page_tweets', '4'),
    '#description' => t('Number of twitter messages to display on the front page.')
  );
  $form['ndla_utils_ndla2010_theme_front_page']['ndla_utils_ndla2010_theme_front_page_nb_slogan'] = array(
    '#type' => 'textfield',
    '#title' => t('Bokmål slogan'),
    '#default_value' => variable_get('ndla_utils_ndla2010_theme_front_page_nb_slogan', ''),
    '#description' => t('The slogan to display at the top of the front page when langauge is set to norwegian bokmål.')
  );
  $form['ndla_utils_ndla2010_theme_front_page']['ndla_utils_ndla2010_theme_front_page_nn_slogan'] = array(
    '#type' => 'textfield',
    '#title' => t('Nynorsk slogan'),
    '#default_value' => variable_get('ndla_utils_ndla2010_theme_front_page_nn_slogan', ''),
    '#description' => t('The slogan to display at the top of the front page when langauge is set to norwegian nynorsk.')
  );
  $form['ndla_utils_ndla2010_theme_front_page']['ndla_utils_ndla2010_theme_front_page_en_slogan'] = array(
    '#type' => 'textfield',
    '#title' => t('English slogan'),
    '#default_value' => variable_get('ndla_utils_ndla2010_theme_front_page_en_slogan', ''),
    '#description' => t('The slogan to display at the top of the front page when langauge is set to english.')
  );

  $path = base_path() . drupal_get_path('theme', 'ndla2010');
  $default_applications = array(
    (object) array(
      'title' => 'Deling',
      'description' => 'Del læringsressurser!',
      'image' => $path . '/img/icon-delingsarena.png',
      'href' => 'http://deling.ndla.no/'
    ),
    (object) array(
      'title' => 'Samskrive',
      'description' => 'Skriv sammen!',
      'image' => $path . '/img/icon-samskrive.png',
      'href' => 'http://samskrive.ndla.no/'
    ),
    (object) array(
      'title' => 'Digital kompetanse',
      'description' => 'Den femte basisferdighet!',
      'image' => $path . '/img/icon-digital-kompetanse.png',
      'href' => 'node/16381'
    )
  );
  for ($i = 0; $i < 10; $i++) {
    $default_values = variable_get('ndla_utils_ndla2010_theme_front_page_application_' . $i, array(
      'nb_title' => $default_applications[$i]->title,
      'nn_title' => $default_applications[$i]->title,
      'en_title' => $default_applications[$i]->title,
      'nb_description' => $default_applications[$i]->description,
      'nn_description' => $default_applications[$i]->description,
      'en_description' => $default_applications[$i]->description,
      'image' => $default_applications[$i]->image,
      'href' => $default_applications[$i]->href
      )
    );
    $field_title = !empty($default_values['nb_title']) ? " (" . $default_values['nb_title'] . ")" : ' (' . t('empty') . ')';
    $form['ndla_utils_ndla2010_theme_front_page']['ndla_utils_ndla2010_theme_front_page_application_' . $i] = array(
      '#type' => 'fieldset',
      '#title' => t('Application !num', array('!num' => ($i + 1) . $field_title)),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
      'nb_title' => array(
        '#type' => 'textfield',
        '#title' => t('Bokmål title'),
        '#default_value' => $default_values['nb_title'],
        '#required' => FALSE,
      ),
      'nn_title' => array(
        '#type' => 'textfield',
        '#title' => t('Nynorsk title'),
        '#default_value' => $default_values['nn_title'],
        '#required' => FALSE,
      ),
      'en_title' => array(
        '#type' => 'textfield',
        '#title' => t('English title'),
        '#default_value' => $default_values['en_title'],
        '#required' => FALSE,
      ),
      'nb_description' => array(
        '#type' => 'textfield',
        '#title' => t('Bokmål description'),
        '#default_value' => $default_values['nb_description'],
        '#required' => FALSE,
      ),
      'nn_description' => array(
        '#type' => 'textfield',
        '#title' => t('Nynorsk description'),
        '#default_value' => $default_values['nn_description'],
        '#required' => FALSE,
      ),
      'en_description' => array(
        '#type' => 'textfield',
        '#title' => t('English description'),
        '#default_value' => $default_values['en_description'],
        '#required' => FALSE,
      ),
      'image' => array(
        '#type' => 'textfield',
        '#title' => t('Image'),
        '#default_value' => $default_values['image'],
        '#required' => FALSE,
      ),
      'href' => array(
        '#type' => 'textfield',
        '#title' => t('URL'),
        '#default_value' => $default_values['href'],
        '#required' => FALSE,
      )
    );
  }

  $first = $second = $third = '';
  if ($first_nid = variable_get('ndla_utils_ndla2010_theme_new_content_first', '')) {
    $node = ndla_utils_load_node($first_nid);
    $first = $node->title . ' [nid:' . $first_nid . ']';
  }
  if ($second_nid = variable_get('ndla_utils_ndla2010_theme_new_content_second', '')) {
    $node = ndla_utils_load_node($second_nid);
    $second = $node->title . ' [nid:' . $second_nid . ']';
  }
  if ($third_nid = variable_get('ndla_utils_ndla2010_theme_new_content_third', '')) {
    $node = ndla_utils_load_node($third_nid);
    $third = $node->title . ' [nid:' . $third_nid . ']';
  }

  $form['ndla_utils_ndla2010_theme_course_pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Course Pages'),
    '#collapsible' => FALSE
  );
  $form['ndla_utils_ndla2010_theme_course_pages']['ndla_utils_ndla2010_theme_user_guide'] = array(
    '#type' => 'textfield',
    '#title' => t('User guide'),
    '#description' => t('Begin typing the name of the node to search for it.'),
    '#default_value' => variable_get('ndla_utils_ndla2010_theme_user_guide', ''),
    '#required' => FALSE,
    '#autocomplete_path' => "ndla-utils/ndla2010-theme/xhr/nodes"
  );
  $form['#submit'] = array('ndla_utils_ndla2010_theme_admin_submit');
  return system_settings_form($form);
}

function ndla_utils_ndla2010_theme_new_content_node_types_admin() {
  $node_types = array();
  foreach (node_get_types() as $node_type_name => $node_type) {
    $node_types[$node_type_name] = $node_type->name;
  }
  $form['ndla_utils_ndla2010_theme_new_content_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#default_value' => variable_get('ndla_utils_ndla2010_theme_new_content_node_types', array()),
    '#description' => t('Select which node types to display on front pages.'),
    '#options' => $node_types
  );
  return system_settings_form($form);
}

/**
 * The administration form found on admin/settings/ndla_auth
 */
function ndla_utils_auth_setting() {

  $form = array();

  $form['ndla_utils_auth_enable'] = array(
    '#title' => t('Enable auth services'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('ndla_utils_auth_enable', FALSE),
  );

  $form['ndla_utils_auth_server_url'] = array(
    '#title' => t('Server URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('ndla_utils_auth_server_url', 'http://auth.ndla.no'),
  );

  $form['ndla_utils_auth_login_box_url'] = array(
    '#title' => t('Login box URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('ndla_utils_auth_login_box_url', 'loginbox.php'),
  );

  $form['ndla_utils_auth_auth_roam_url'] = array(
    '#title' => t('Auth roam URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('ndla_utils_auth_roam_auth_url', 'roamauth.js.php'),
  );

  return system_settings_form($form);
}

/**
 * Admin-page-callback for selecting on which nodes the imagecache-dropdown should be enabled.
 */
function ndla_utils_node_imagecache_setting() {
  $types = node_get_types();
  $form = array();

  foreach ($types as $id => $data) {
    $selectable_types[$id] = $data->name;
  }

  $form['ndla_utils_node_imagecache_enabled'] = array(
    '#title' => t('Enable ImageCache dropdown on'),
    '#type' => 'checkboxes',
    '#options' => $selectable_types,
    '#default_value' => variable_get('ndla_utils_node_imagecache_enabled', array()),
  );

  return system_settings_form($form);
}


/**
 * Helper function to uasort
 */
function _ndla_utils_sort_types_cmp($a, $b) {
  if (isset($a->weight)) {
    $a = $a->weight;
    $b = $b->weight;
  }
  if ($a == $b) {
    return 0;
  }
  return ($a < $b) ? -1 : 1;
}


/**
 * Admin page callback for creating draggable form for ordering node types.
 */
function ndla_utils_node_type_order_setting(&$form_state) {
  $form = array();

  $types = node_get_types();

  $stored_weights = variable_get("ndla_utils_node_type_order", array());
  foreach ($types as $id => &$type) {
    $type->weight = (isset($stored_weights[$id]) ? $stored_weights[$id] : 0);
  }
  uasort($types, '_ndla_utils_sort_types_cmp');

  $form['weights'] = array('#tree' => TRUE);
  foreach ($types as $id => $type) {
    $form['names'][$id] = array('#value' => $type->name);
    
    $form['weights'][$id] = array('#type' => 'weight', '#default_value' => $type->weight, '#delta' => 50);
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));

  return $form;
}


function theme_ndla_utils_node_type_order_setting($form) {
  $header = array(t('Name'), t('Weight'));
  $rows = array();
  foreach (element_children($form['names']) as $id) {
    // Don't take form control structures.
    if (is_array($form['names'][$id])) {
      $form['weights'][$id]['#attributes']['class'] = 'node-type-order-weight';
      $rows[] = array(
        'data' => array(drupal_render($form['names'][$id]), drupal_render($form['weights'][$id])),
        'class' => 'draggable',
      );
    }
  }

  $output = theme('table', $header, $rows, array('id' => 'node-type-order'));
  $output .= drupal_render($form);

  drupal_add_tabledrag('node-type-order', 'order', 'sibling', 'node-type-order-weight', NULL, NULL, TRUE);

  return $output;
}

function ndla_utils_node_type_order_setting_submit($form, &$form_state) {
  // Make a copy to be sure not to break anything inside FAPI
  $node_list = &$form_state['values']['weights'];

  uasort($node_list, '_ndla_utils_sort_types_cmp');
  variable_set("ndla_utils_node_type_order", $node_list);
}

/**
 * Sets up form to control properties for the role 'desk'.
 * 
 * This function is a callback for drupal_get_form in hook_menu
 * 
 * @return Array with form elements
 */
function ndla_utils_desk_form() {
  $form = array();
  
  // Available options
  $collapsed_options = array(
    'collapsed' => t('Collapsed'),
    'expanded' => t('Expanded'),
    'default' => t('Drupal standard'),
  );
    
  //Create fieldset
  $form['desk_collapse_fieldset'] = array(
    '#title' => 'Collapse/Expand',
    '#type' => 'fieldset',
    '#description' => 'Options for how expandable fields in the edit node page are shown for desk',
    '#collapsable' => FALSE,
  );
  
  // Control ingress field collapsed/expanded for desk.
  $form['desk_collapse_fieldset']['ndla_utils_desk_ingress_collapsed'] = array(
    '#type' => 'select',
    '#title' => t('Ingress'),
    '#options' => $collapsed_options,
    '#default_value' => variable_get('ndla_utils_desk_ingress_collapsed', 'default'),
  );
  
  // Control author field collapsed/expanded for desk.
  $form['desk_collapse_fieldset']['ndla_utils_desk_author_collapsed'] = array(
    '#type' => 'select',
    '#title' => t('Author'),
    '#options' => $collapsed_options,
    '#default_value' => variable_get('ndla_utils_desk_author_collapsed', 'default'),
  );
  
  // Control page title field collapsed/expanded for desk.
  $form['desk_collapse_fieldset']['ndla_utils_desk_page_title_collapsed'] = array(
    '#type' => 'select',
    '#title' => t('Page title'),
    '#options' => $collapsed_options,
    '#default_value' => variable_get('ndla_utils_desk_page_title_collapsed', 'default'),
  );
  
  // Control meta tag field collapsed/expanded for desk.
  $form['desk_collapse_fieldset']['ndla_utils_desk_meta_tags_collapsed'] = array(
    '#type' => 'select',
    '#title' => t('Meta tag'),
    '#options' => $collapsed_options,
    '#default_value' => variable_get('ndla_utils_desk_meta_tags_collapsed', 'default'),
  );
  
  return system_settings_form($form);
}

function ndla_utils_admin_fag_image() {
  $imagecache_presets = array('' => t('Choose'));
  foreach (imagecache_presets() as $index => $preset) {
    $imagecache_presets[$preset['presetname']] = $preset['presetname'];
  }
  
  $form['ndla_utils_ndla2010_theme_fag_header_imagecache'] = array(
    '#title' => t('Imagecache preset for fag images'),
    '#type' => 'select',
    '#multiple' => FALSE,
    '#options' => $imagecache_presets,
    '#default_value' => variable_get('ndla_utils_ndla2010_theme_fag_header_imagecache', ''),
  );
  
  return system_settings_form($form);
}

/**
 * Form for the cookie warning settings
 **/
function ndla_utils_cookie_warning() {
  $form = array();

  $form['ndla_utils_cookie_warning_en'] = array(
    '#title' => t('Enter text for the cookie warning popup in English'),
    '#type' => 'textarea',
    '#default_value' => variable_get('ndla_utils_cookie_warning_en', ''),
    '#maxlength' => '600',
  );
  $form['ndla_utils_cookie_warning_nb'] = array(
    '#title' => t('Norwegian'),
    '#type' => 'textarea',
    '#default_value' => variable_get('ndla_utils_cookie_warning_nb', ''),
    '#maxlength' => '600',
  );
  $form['ndla_utils_cookie_warning_nn'] = array(
    '#title' => t('Nynorsk'),
    '#type' => 'textarea',
    '#default_value' => variable_get('ndla_utils_cookie_warning_nn', ''),
    '#maxlength' => '600',
  );

  return system_settings_form($form);
}

function ndla_utils_slideshow_admin() {
  $presaved = ndla_utils_load_slideshow_nodes(variable_get('ndla_utils_num_slides', 3));

  $num_slides = array();
  for($i = 1; $i < 21; $i++) {
    $num_slides[$i] = $i;
  }

  $form['#tree'] = TRUE;
  $form['ndla_utils_num_slides'] = array(
    '#type' => 'select',
    '#title' => t('Number of slides to display on frontpage'),
    '#options' => $num_slides,
    '#default_value' => variable_get('ndla_utils_num_slides', 3),
  );

  $num = intval(variable_get('ndla_utils_num_slides', 3));
  for($i = 0; $i < $num; $i++) {
    $complete_node = "$('#nid-$i').val";
    $complete_image = "$('#image-nid-$i').val";
    
    if(!empty($presaved[$i]['nid'])) {
      $title = db_fetch_object(db_query("SELECT title FROM {node} WHERE nid = %d", $presaved[$i]['nid']))->title;
      $presaved[$i]['nid'] = trim($title) . " [nid:" . $presaved[$i]['nid'] . "]";
    }
    if(!empty($presaved[$i]['image_nid'])) {
      $title = db_fetch_object(db_query("SELECT title FROM {node} WHERE nid = %d", $presaved[$i]['image_nid']))->title;
      $presaved[$i]['image_nid'] = trim($title) . " [nid:" . $presaved[$i]['image_nid'] . "]";
    }

    // List of courses
    $courses = ndla_utils_courses();
    $course_list[0] = t('None');
    foreach($courses as $course) {
      $nid = $course->nid;
      if($nid) {
        $course_list[$nid] = $course->name;
      }
    }

    
    $form['nodes'][$i] = array(
      '#type' => 'fieldset',
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
      '#title' => t('Node @num', array('@num' => ($i+1))),
      'nid' => array(
        '#id' => 'nid-' . $i,
        '#type' => 'textfield',
        '#title' => t('Node'),
        '#default_value' => $presaved[$i]['nid'],
        '#suffix' => l(t('Browse'), 'contentbrowser', array('attributes' => array('rel' => 'lightframe[|width:1000;height:800]'), 'query' => array('output' => 'drupal', 'oncomplete' => $complete_node))),
      ),
      'context' => array(
        '#title' => t('Course context'),
        '#type' => 'select',
        '#description' => t('If you want the link to open with a subject context, select a subject here.'),
        '#default_value' => 0,
        '#options' => $course_list,
      ),
      'image_nid' => array(
        '#id' => 'image-nid-' . $i,
        '#type' => 'textfield',
        '#title' => t('Image'),
        '#default_value' => $presaved[$i]['image_nid'],
        '#suffix' => l(t('Browse'), 'contentbrowser', array('attributes' => array('rel' => 'lightframe[|width:1000;height:800]'), 'query' => array('output' => 'drupal', 'oncomplete' => $complete_image, 'nodereference' => 'field_ingress_bilde'))),
        '#description' => t('Custom image. If left blank the nodes teaser image will be used.'),
      ),
      'timestamp' => array(
        '#type' => 'hidden',
        '#value' => !empty($presaved[$i]['timestamp']) ? $presaved[$i]['timestamp'] : time(),
      ),
    );

    foreach(language_list() as $prefix => $lang) {
      $form['nodes'][$i]['titles'][$prefix] = array(
        '#title' => t('Title in @lang', array('@lang' => $lang->native)),
        '#type' => 'textfield',
        '#description' => t('Custom title, if left empty the node title in @lang will be used,', array('@lang' => $lang->native)),
        '#default_value' => $presaved[$i]['titles'][$prefix],
      );
      $form['nodes'][$i]['teasers'][$prefix] = array(
        '#title' => t('Description in @lang', array('@lang' => $lang->native)),
        '#type' => 'textarea',
        '#description' => t('Custom teaser, if left empty the node teaser in @lang will be used,', array('@lang' => $lang->native)),
        '#default_value' => $presaved[$i]['teasers'][$prefix]
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function ndla_utils_slideshow_admin_submit($form, $form_state) {
  $nodes_to_save = array();

  //Store the num slides to display.
  variable_set('ndla_utils_num_slides', check_plain($form_state['values']['ndla_utils_num_slides']));

  if(empty($form_state['values']['nodes'])) {
    return;
  }
  
  foreach($form_state['values']['nodes'] as $index => $node_data) {
    $image_nid = $nid = NULL;
    
    if(preg_match("/nid:([0-9]+)/", $node_data['nid'], $matches)) {
      $nid = trim(check_plain($matches[1]));
    }
    if(preg_match("/nid:([0-9]+)/", $node_data['image_nid'], $matches)) {
      $image_nid = trim(check_plain($matches[1]));
    }

    $context = $node_data['context'];

    if($nid && $node = node_load($nid)) {
      if(empty($image_nid) && !empty($node->field_ingress_bilde[0]['nid'])) {
        $node_data['image_nid'] = $node->field_ingress_bilde[0]['nid'];
      }
      else {
        $node_data['image_nid'] = $image_nid;
      }
      
      if(empty($node_data['image_nid'])) {
        drupal_set_message(t('Node @index (@nid) is missing an image. The node will not be visible in the slideshow.', array('@nid' => $nid, '@index' => $index+1)), 'error');
      }
      
      if($node->tnid) {
        $result = db_query("SELECT nid FROM {node} WHERE tnid = %d", $node->tnid);
        while($row = db_fetch_object($result)) {
          $node_lang = node_load($row->nid);
          //Set the titles if they left them empty
          if(isset($node_data['titles'][$node_lang->language]) && empty($node_data['titles'][$node_lang->language])) {
            $node_data['titles'][$node_lang->language] = trim($node_lang->title);
          }
          //Set the descriptions
          if(isset($node_data['teasers'][$node_lang->language]) && empty($node_data['teasers'][$node_lang->language])) {
            if(!empty($node_lang->field_ingress[0]['value'])) {
              $node_data['teasers'][$node_lang->language] = trim(strip_tags($node_lang->field_ingress[0]['value']));
            }
            else {
              $node_data['teasers'][$node_lang->language] = trim(strip_tags(node_teaser($node_lang->body, $node_lang->format)));
            }
          }
        }
      }
      $node_data['nid'] = $nid;
      $nodes_to_save[] = $node_data;
    }
  }

  if(count($nodes_to_save)) {
    ndla_utils_save_slideshow_nodes($nodes_to_save);
  }
}

function ndla_utils_save_slideshow_nodes($nodes_to_save) {
  $current_time = time()+61;
  foreach($nodes_to_save as $slide) {
    $current_time--;
    $nid = $slide['nid'];
    $timestamp = $slide['timestamp'];
    $slide['timestamp'] = $current_time;
    $data = serialize($slide);

    $update = db_fetch_object(db_query("SELECT nid FROM {ndla_utils_slideshow} WHERE nid = %d AND timestamp = %d", $nid, $timestamp))->nid;
    if($update) {
      db_query("DELETE FROM {ndla_utils_slideshow} WHERE nid = %d AND timestamp = %d", $nid, $timestamp);
    }

    db_query("INSERT INTO {ndla_utils_slideshow} (nid, timestamp, data) VALUES(%d, %d, '%s')", $nid, $current_time, $data);
  }
}

function ndla_utils_slideshow_admin_delete() {
  drupal_set_title(t('Slideshow settings: Delete from archive'));
  $result = pager_query("SELECT nid, timestamp, data FROM {ndla_utils_slideshow} ORDER BY timestamp DESC", 20);
  $form = array();
  $ids = array();
  while($row = db_fetch_object($result)) {
    $id = $row->nid . "_" . $row->timestamp;
    $nodes[$id] = unserialize($row->data);
    $ids[$id] = '';
    $form[$id]['nid'] = array(
      '#type' => 'markup',
      '#value' => $nodes[$id]['nid'],
    );
    $form[$id]['timestamp'] = array(
      '#type' => 'markup',
      '#value' => format_date($nodes[$id]['timestamp']),
    );
    
    foreach(language_list() as $prefix => $lang) {
      if(isset($nodes[$id]['titles'][$prefix])) {
        $form[$id]['title_' . $prefix] = array(
          '#type' => 'markup',
          '#value' => $nodes[$id]['titles'][$prefix],
        );
      }
    }
  }
  $form['checkboxes'] = array(
    '#type' => 'checkboxes',
    '#options' => $ids,
  );
  
  $form['#theme'] = 'ndla_utils_slideshow';
  $form['buttons'] = array(
    'delete' => array(
      '#type' => 'submit',
      '#value' => t('Delete from archive'),
    )
  );
  return $form;
}

function ndla_utils_slideshow_admin_delete_submit($form, $form_state) {
  if(!empty($form_state['values']['checkboxes'])) {
    $ids = array_filter($form_state['values']['checkboxes']);
    foreach($ids as $id) {
      list($nid, $timestamp) = explode("_", $id);
      db_query("DELETE FROM {ndla_utils_slideshow} WHERE nid = %d AND timestamp = %d", $nid, $timestamp);
    }
    drupal_set_message(t('@count items deleted', array('@count' => count($ids))));
  }
  else {
    drupal_set_message(t('No items selected'));
  }
}

function theme_ndla_utils_slideshow($form) {
  $rows = array();

  foreach(element_children($form['checkboxes']) as $id) {
    $row = array(); 
    $row[] = drupal_render($form['checkboxes'][$id]); 
    $row[] = drupal_render($form[$id]['nid']); 
    
    foreach(language_list() as $prefix => $lang) {
      if(!empty($form[$id]['title_' . $prefix])) {
        $row[] = drupal_render($form[$id]['title_' . $prefix]);
      }
    }
    $row[] = drupal_render($form[$id]['timestamp']);
    
    $rows[] = $row;
  }

  if(count($rows)) {
    $header = array(theme('table_select_header_cell') , t('Node id'));
    foreach(language_list() as $prefix => $lang) {
      if(!empty($form[$id]['title_' . $prefix])) {
        $header[] = t('Title in @lang', array('@lang' => $lang->native));
      }
    }
    $header[] = t('Created');
  }
  else {
    $header = array(t('Node id'), t('Created')); 
    $row = array();
    $row[] = array(
      'data' => t('No items were found'),
      'colspan' => 2,
      'style' => 'text-align:center',
    );
    $rows[] = $row;
  }
 
  $output = theme('table', $header, $rows);
  return $output . drupal_render($form) . "<p>" . theme('pager') . "</p>";
}

function ndla_utils_gtm_admin_form() {
  $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
  $title = t("Dont add GTM on the following pages");
  $form = array();
  $form['ndla_utils_gtm'] = array(
    '#title' => t('Google Tag Manager ID'),
    '#default_value' => variable_get('ndla_utils_gtm', 'GTM-P5SK7W'),
    '#type' => 'textfield',
  );

  $form['ndla_utils_gtm_skip_pages'] = array(
    '#type' => 'textarea',
    '#title' => $title,
    '#default_value' => $pages,
    '#description' => $description,
    '#rows' => 30,
    '#default_value' => ndla_utils_gtm_skip_pages(),
  );
  return system_settings_form($form);
}

function ndla_utils_https_admin_form() {
  $form = array();
  $form['ndla_https_domains'] = array(
    '#type' => 'textarea',
    '#title' => 'NDLA domains which support https',
    '#description' => t('Enter one domain per line without http(s)://'),
    '#default_value' => implode("\n", ndla_utils_get_https_domains()),
    '#rows' => 30,
  );
  
  return system_settings_form($form);
}
