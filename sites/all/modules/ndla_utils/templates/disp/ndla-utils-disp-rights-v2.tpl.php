<div id="rights-block">
  <div id="node-rights">
    <?php //print theme('ndla_utils_disp_rights_content', $node, $creative_commons)?>
  </div>
  <?php
    if(!empty($nids_inserted)):
  ?>
  <div id="ndla_mediabrowser_inserted_nodes">
    <?php foreach($nids_inserted as $license => $data): ?>
    <?php //print theme('contentbrowser_related_data_block_content', $nids_inserted);?>
    <?php 
      $license_data = get_license_info($license);
      if(!empty($license_data['images'][0])) {
        if($license == 'nolicense') {
          $license_data['license_name'] = t('No license');
        }
        print "<a title='" . $license_data['license_name'] . "' class='grouped-license tooltip'>";
        print $license_data['images'][0];
        print "</a>";
        print "<div class='tooltip-data'>" . theme('contentbrowser_related_data_block_content_v2', $data) . "</div>";
      }
    ?>
    <?php endforeach; ?>
  </div>
  <?php endif; ?>
</div>