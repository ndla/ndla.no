<label for="embed-code"><?php print t('Embed code') ?>:</label>
<textarea rows="2" cols="30"  name="embed-code" id="embed-code" onclick="$(this).focus().select()">
<?php
if(module_exists('ndla_content_purifier') && $node->type != 'h5p_content') {
  _ndla_content_view_embed($node);
}
else if($node->type == 'h5p_content') {
  print "<!-- " . t('Embed code from http://ndla.no.') . " -->\n";
  print "<!-- " . t('Alter the width and height attributes if needed.') . " -->\n";
  print "<iframe src='" . url('node/' . $node->nid . '/embed', array('absolute' => TRUE)) . "' width='670px' height='550px'></iframe>";
 }
else { ?>
<style type='text/css'>
  .right {
    clear: right;
    float: right;
    margin-right: -25em;
    position: relative;
    width: 15em;
  }

  .frame {
    background-color: #EBEBEB;
    color: #666666;
    margin-bottom: 10px;
    margin-left: 10px;
    padding: 15px;
  }

  .wrap_icon.no_icon {
    min-height: 0;
    padding-top: 1px;
  }

  .UMB_image.UMB_float_none {
    float: none;
  }

  iframe, img {
    border: none;
  }

  .UMB_image.UMB_float_right.imageframe {
    margin-left: 5px;
    float: right;
    background-color: #EBEBEB;
    color: #666666;
    margin-bottom: 20px;
  }

  .read-more, .re-collapse {
    display: none;
  }
</style>
<div class="ndla-container" style="float:left;width:45.5em">
  <h1><?php print $node->title ?></h1>
  <div class="ndla-image" style="border-bottom:1px solid #EBEBEB;float:left;margin:0 10px 10px 0;width:auto">
    <?php
  $imgq = "select f.filepath, nr.title from {files} f INNER JOIN {image} img on f.fid = img.fid INNER JOIN  {node_revisions} nr on nr.nid = %d where img.nid = %d and img.image_size = '%s'";
  $ires = db_query($imgq, $node->field_ingress_bilde[0]['nid'], $node->field_ingress_bilde[0]['nid'], 'normal');
  $iresarr = db_fetch_array($ires);
    if(!empty($iresarr['filepath'])) {
      if (strpos($iresarr['filepath'],'http') === FALSE) {
        print '<img src="'.url(NULL, array('absolute' => TRUE, 'language' => '')).'/'.$iresarr['filepath'].'" alt="'.$iresarr['title'].'" />';
      }
      else {
        print '<img src="'.$iresarr['filepath'].'" alt="'.$iresarr['title'].'" />';
      }
    }
?>
  </div>
  <div class="ndla-teaser" style="color:#666666;font-size:1.2em;font-style:italic">
    <?php print $node->field_ingress[0]['value'] ?>
  </div>
  <div class="ndla-content" style="width:30em;clear:both">
    <div id="ndla-<?php print $node->nid ?>" style="display: none">
      <a href="#" onclick="document.getElementById('ndla-<?php print $node->nid ?>').style.display='none';document.getElementById('ndla-more-<?php print $node->nid ?>').style.display='block';return false"><?php print t('Show less') ?></a>
      <?php print ndla_utils_replace_styles(ndla_utils_resolve_embeds(ndla_utils_resolve_biblio($node->body))) ?>
    </div>
    <a href="#" onclick="document.getElementById('ndla-<?php print $node->nid ?>').style.display='block';this.style.display='none';return false" id="ndla-more-<?php print $node->nid ?>"><?php print t('Show more') ?></a>
    <?php print theme('ndla_utils_license', $node) ?>
  </div>
</div>
<?php } ?>
</textarea>
<a href="#" onclick="$(this).prev().focus().select();return false"><?php print t('Select embed code') ?></a>
