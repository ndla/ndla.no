<?php if ($total > 0): ?>
<div class="author-solr-header">
  <h2><?php print t('Content related to') . " <em>$name</em>" ?></h2>
  <span class="author-solr-search-user">
    <a href="<?php print $link ?>"><?php print $total . ' ' . t('results') . '</a> '.t('by').' <em>' . $name; ?></em>
  </span>
  <?php /*<a class="author-solr-clear" href="<?php print $link ?>"><?php print "$total " . t('results') ?> </a> */ ?>
</div>

<?php foreach ($blocks as $block): ?>
<div class="author-solr">
  <?php print $block ?>
</div>
<?php endforeach; ?>

<?php endif; ?>