<div class='editorial-news'>
  <?php
    if(count($news)) {
      print "<div class='news-items'>";
      foreach($news as $index => $content) {
        $last = (count($news)-1 == ($index)) ? ' last ' : '';
        print "<div class='news-item " . (($index %2) ? "odd" : "even") ."$last '>";
        if ($content->image) {
          print "<div class='editorial-news-image-wrapper'>";
          print "<a href='" . url('node/' . $content->nid) . "'>";
          print "<img src='" . $base_path . $content->image . "' />";
          print "</a>";
          print "</div>";
        }
        $teaser = preg_replace('/\[.*\]/', '', strip_tags($content->teaser));
        print "<h2><a href='" . url('node/' . $content->nid) ."'>" . $content->title . "</a></h2>";
        print "<p>" . $teaser . "</p>";
        print "</div>";
      }
      print "</div>"; //end news-items
      print "<div class='clearfix'></div>";
    }
?>
</div>
