<?php
/**
 * $embed_vars
 */
 
 $url = url(NULL, array('absolute' => TRUE, 'language' => ''));
?>

<div style="border: 1px solid rgb(245, 245, 245); padding: 5px 5px 0; font-size: 80%; background-color: rgb(249, 249, 249); color: rgb(102, 102, 102); height: 30px; width: 45.5em; border: solid 1px #ebebeb">
  <div style="float: left;">
    <strong style="float:left"><?php print t('Originator'); ?>: </strong>
    <?php if(isset($embed_vars['opphav']) && $embed_vars['opphav']):; ?>
      <ul style="list-style-type: none; float: left; margin: 0; padding:0; display: inline;">
        <?php foreach($embed_vars['opphav'] as $nid =>  $title):; ?>
          <li style="display: inline; padding: 0 2px 0 2px">
            <a href="<?php print url(NULL,array('absolute' => TRUE)) . '/aktor/' . $nid; ?>" target="_blank" style="color: #666666"><?php print $title; ?></a>
          </li>
        <?php endforeach; ?>
      </ul>
  
    <?php else: ?>
      <?php print t('No originator'); ?>
    <?php endif; ?>
  </div>

  <div style="float: left; margin-left: 15px; max-width: 600px;">
    <strong style="float:left"><?php print t('Usufruct'); ?>: </strong>
    <?php if(isset($embed_vars["cc"])):
      if(!empty($embed_vars['cc']) && !in_array($embed_vars["cc"], array('copyrighted', 'gnu', 'publicdomain'))): ?>
        <a title="<?php print $embed_vars["cc_title"]; ?>" href="http://creativecommons.org/licenses/<?php print $embed_vars["cc"]; ?>/3.0/no/"><img style="border: none" src="<?php print $url; ?>/sites/all/modules/creativecommons_lite/images/buttons_small/<?php print $embed_vars["cc"]; ?>.png" alt="Creative Commons license icon" /></a>
      <?php elseif(!empty($embed_vars['cc']) && in_array($embed_vars["cc"], array('copyrighted'))): ?>
        <a title="<?php print $embed_vars["cc_title"]; ?>" href="http://no.wikipedia.org/wiki/Copyright"><img style="border: none" src="<?php print $url; ?>/sites/all/modules/creativecommons_lite/images/buttons_small/<?php print $embed_vars["cc"]; ?>.png" alt="Copyright icon" /></a>
      <?php elseif(!empty($embed_vars['cc']) && in_array($embed_vars["cc"], array('gnu'))): ?>
        <a title="<?php print $embed_vars["cc_title"]; ?>" href="http://www.gnu.org/licenses/gpl.html"><img style="border: none" src="<?php print $url; ?>/sites/all/modules/creativecommons_lite/images/buttons_small/<?php print $embed_vars["cc"]; ?>.png" alt="Copyright icon" /></a>
      <?php else: ?>
        <strong><?php print $embed_vars["cc_title"]; ?></strong>
      <?php endif; ?>
    <?php else: ?>
      <strong><?php print t('Not Licensed')?></strong>
    <?php endif; ?>
  </div>

    <?php
      $nodeid = $embed_vars['nid'] ? $embed_vars['nid'] : $embed_vars['node']->nid;
    ?>
    
  <div style="float: right; margin-right: 5px; margin-top: 5px;max-width: 600px;">
    <a title="Les på NDLA" href="<?php print url(NULL,array('absolute' => TRUE)).'/node/'.$nodeid; ?>" target="_blank">
      <img style="border: none; height: 15px" alt="Nasjonal Digital Læringsarena" src="<?php print $url; ?>/sites/all/themes/ndla2010/img/ndlarektangular16h.png" />
    </a>
  </div>
</div>