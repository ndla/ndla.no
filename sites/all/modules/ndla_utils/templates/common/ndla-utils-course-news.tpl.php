<div class='editorial-news'>
  <?php
  if(count($news)) {
    $content = $news[0];
    unset($news[0]);
    print "<div class='news-container'>";
    ?>
    <div class='news-item item-1'>
      <?php if(!empty($content->full)): ?>
      <?php print $content->full; ?>
      <?php else: ?>

      <?php if ($content->image): ?>
        <div class="editorial-news-image-wrapper item-0">
          <a href='<?php print url('node/' . $content->nid); ?>'>
            <img src="<?php print $base_path . $content->image ?>" />
          </a>
        </div>
      <?php endif; ?>

      <div class='blue-background'>
        <h2><a href="<?php print url('node/' . $content->nid) ?>"><?php print $content->title ?></a></h2>
        <?php
          $teaser = preg_replace('/\[.*\]/', '', strip_tags($content->teaser))
        ?>
        <p><?php print truncate_utf8($teaser, 200, TRUE) . " ... "; ?></p>
      </div>
    <?php endif; ?>
    </div><!-- end main item -->
    <?php
    if(count($news)) {
      unset($content);
      print "<div class='right-items'>";
      foreach($news as $index => $content) {
        $last_three = (count($news) == 3 && $index == 3) ? TRUE : FALSE;
        print "<div class='news-item " . (($last_three) ? "last_three " : "") . "small-$index " . (($index %2) ? "odd" : "even") ."'>";
        if ($content->image && $index < 3) {
          print "<div class='editorial-news-image-wrapper'>";
          print "<a href='" . url('node/' . $content->nid) . "'>";
          print "<img src='" . $base_path . $content->image . "' />";
          print "</a>";
          print "</div>";
        }
        $teaser = preg_replace('/\[.*\]/', '', strip_tags($content->teaser));
        if($last_three) {
          $teaser = truncate_utf8($teaser, 125, TRUE);
        }
        else {
          $teaser = truncate_utf8($teaser, 100, TRUE);
        }
        // Check is last char is a '.'. In that case dont display the '...'
        if(substr($teaser, -1) != '.') {
          $teaser .= '...';
        }
        print "<h2><a href='" . url('node/' . $content->nid) ."'>" . $content->title . "</a></h2>";
        print "<p>" . $teaser . '</p>'
        ."<p class='readmore'>"
          .l("<i class='icon-circle-arrow-right'></i>"
          .t('Read more'), 'node/' . $content->nid, array('html' => TRUE))
        ."</p>";
        print "</div>";
      }
      print "</div>"; //end right-items
      print "<div class='clearfix'></div>";
    }
    print "</div>";
  }
?>
</div>
