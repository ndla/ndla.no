<?php

/**
 * @file
 * @ingroup ndla_utils
 * @brief
 *  Various menu callbacks
 */

/**
 * Callback for the grep page. Lists all nodes which is connected to that competence aim.
 * 
 * @param object $node 
 * @return string HTML
 */
function ndla_utils_node_grep_callback($node) {
  if ($node->type != 'fag') {
    drupal_not_found();
  }

  _ndla_utils_setup_js_settings($node);

  $nodes = _ndla_utils_node_listing($node, arg(2));
  $heading = '<div id="grep_header"><h1 class="listing-title">' . t('Competence aim') . '</h1><h2 class="aim-title">' . ndla_ontopia_connect_fetch_aim_title(arg(3),$node->language) . '</h2></div>';

  return '<div id="fag-subpage">' . $heading . $nodes['content'] . '</div>';
}

/**
 * @todo Document me.
 */
function _ndla_utils_has_type($type_name, $arr) {
  $hasit = '';
  foreach ($arr as $key => $data) {
    if ($data['title'] == $type_name) {
      $hasit = $key;
      break;
    }
  }
  return $hasit;
}

/**
 * Returns a block array which shows a listing of nodes.
 * @todo More cleanup needed.
 *
 * @param $node
 *  Object. The current node which we are viewing. Only nodes with of type 'fag' will be accepted.
 * @param $list_type
 *  String. "grep" or "topics"
 * @return
 *  Array.
 */
function _ndla_utils_node_listing($node, $list_type) {
  global $language;
  $nodes = array();
  $uuid = NULL;
  $grepview = true;
  if ($node->type != 'fag' || empty($list_type)) {
    return array();
  }

  switch ($list_type) {
    case 'topics':
      $tid = arg(3);
      if (!is_numeric($tid)) {
        return array();
      }

      $nodes = _ndla_utils_get_topic_nodes($node, $tid);
      break;

    case 'grep':
      $uuid = arg(3);
      $nodes = _ndla_ontopia_connect_get_ressurslisting($uuid);
      break;
  } //end switch

  $types = node_get_types('names');
  $current_types = array();

  $relation_types = array();
  $node_types = array();

  //Pick up the last arguments for creating the links.
  $args = array(arg(0), arg(1), arg(2), arg(3));
  $last_arg = arg(4);
  if (!empty($last_arg) && !is_numeric($last_arg)) {
    $args[] = $last_arg;
  }

  //error_log("NODES: ".print_r($nodes['array']['ndla'],TRUE));
  // filter out unpublished nodes on prod
  if (is_array($nodes['array']['ndla']) && count($nodes['array']) > 0) {
    foreach ($nodes['array']['ndla'] as $assoc => $assocdata) {
      $published_data = array();
      foreach ($assocdata['psis']['data'] as $nodetype => $nodedata) {
        for ($m = 0; $m < count($nodedata); $m++) {
          $obj = new StdClass();
          $obj->nid = $nodedata[$m]['nodeid'];
          $obj->vid = $nodedata[$m]['vid'];
          if (ndla_utils_load_node($obj->nid)) {
            // Only display nodes in the list that exists. This will make sure
            // only nodes published to production will show up there
            array_push($published_data, $nodedata);
          }
        }
      }//end for

      $assocdata['psis']['data'][$nodetype] = $published_data;
    }
  }

  if (is_array($nodes['array']['ndla']) && count($nodes['array']) > 0) {
    foreach ($nodes['array']['ndla'] as $assoc => $assocdata) {
      if (!array_key_exists($assoc, $relation_types)) {
        $relation_types[$assoc] = array("title" => $assocdata['tittel'], "count" => 0);
      }

      //$relation_types[$assoc]['count'] = count($assocdata['psis']);
      $relation_types[$assoc]['count'] = 0;
      foreach ($assocdata['psis']['data'] as $nodetype => $reselm) {
        for ($n = 0; $n < count($reselm); $n++) {
          // error_log("TyPE: ".$nodetype);
          //error_log("RESELM: ".print_r($reselm,TRUE));
          if ($nodetype != '' && ($reselm[$n]['spraak'] == '' || $reselm[$n]['spraak'] == $language->language)) {
            $relation_types[$assoc]['count']++;

            $hasit = _ndla_utils_has_type(trim($nodetype), $node_types);
            if ($hasit != '' && trim($nodetype) != "") {
              if ($hasit != $reselm[$n]['type_tid']) {
                $node_types[$hasit]['title'] = trim($nodetype);
                $node_types[$hasit]['full_title'] = $reselm[$n]['type_fullname'];
              }
              $node_types[$hasit]['count']++;
            }
            else {
              $node_types[$reselm[$n]['type_tid']] = array("title" => trim($nodetype), "full_title" => $reselm[$n]['type_fullname'], "count" => 1);
            }
          }
        }//end for elms
      } //end foreach 2
    } //end foreach 1
  }


  if (is_array($nodes) && count($nodes['array']) == 0) {
    foreach ($nodes as $item) {
      //If we are performaing a relation search and we are viewing the a certain curriculum, only
      //show nodes with that curriculum and relation
      if (is_numeric($rel_search) && $uuid && $item->grep[$uuid]['number'] == $last_arg) {
        $current_types[$item->type]++;
      }
      else if ($rel_search === NULL) {
        $current_types[$item->type]++;
      }
    }
  }

  //Start building the content...
  if ($list_type == 'grep') {
    $content = '<div id="grepsearchfilter">';
    if (count($relation_types) > 0 && count($node_types) > 0) {
      $content .= '<ul class="clearfix" style="margin-bottom: 6px">';
      $content .= '<li><p class="grep-listing-label">' . t('Filter by relations') . ': </p>';
      $list = "";
      foreach ($relation_types as $key => $reldata) {
        if ($reldata['count'] > 0) {
          $list .= '<li class="grep-listing-checkbox"><input type="checkbox" class="relationfilter" name="' . $key . '" onclick="filterGrepListing(\'' . $language->language . '\')" checked="checked" id="grep-' . $key . '" /><label for="grep-' . $key . '">' . ucfirst($reldata['title']) . ' <span class="grep-count">(' . $reldata['count'] . ')</span></label></li>';
        }
      }
      if ($list != "") {
        $content .= '<ul class="grep-listing-checkboxes">' . $list . '</ul>';
      }
      $content .= "</li></ul>";
    }
    $content .= "<div style='clear: both'></div>";
    if (count($node_types) > 0) {
      $content .= '<ul class="clearfix" style="margin-bottom: 10px">';
      $content .= '<li><p class="grep-listing-label">' . t('Filter by content types') . ': </p>';
      // build sublist
      $list = "";
      foreach ($node_types as $key => $reldata) {
        if ($reldata['count'] > 0) {
          $list .= '<li class="grep-listing-checkbox"><input type="checkbox" class="typefilter" name="type_' . $key . '" onclick="filterGrepListing(\'' . $language->language . '\')" checked="checked" id="grep-type-' . $key . '" /><label for="grep-type-' . $key . '">' . $reldata['full_title'] . ' <span class="grep-count">(' . $reldata['count'] . ')</span></label></li>';
        }
      }
      // if we have list items, put them in an ul
      if ($list != "") {
        $content .= '<ul class="grep-listing-checkboxes">' . $list . '</ul>';
      }
      $content .= "</li></ul>";
    }
    $content .= "<div style='clear: both;'></div>";

    $nlistdata = ndla_utils_get_sorting();
    $content .= '</div><!-- END grepsearchfilter -->';

    if (count($nodes['array']) > 0) {

      $content .= '<div id="resource_content">';
      $content .= '<h2 class="grep-listing-header">' . t('Related content') . '</h2>';
      foreach ($nodes['array']['ndla'] as $assoc => $assocdata) {
        foreach ($assocdata['psis']['data'] as $ntype => $ntypedata) {
          for ($o = 0; $o < count($ntypedata); $o++) {
            if (!array_key_exists($ntype, $nlistdata)) {
              $nlistdata[$ntype] = array();
            }
            array_push($nlistdata[$ntype], $ntypedata[$o]);
          }
        }
      }


      $contentlist = '';
      $contentitems = 0;
      foreach ($nlistdata as $type => $nodelistdata) {
        for ($p = 0; $p < count($nodelistdata); $p++) {
          if ($nodelistdata[$p]['tittel'] != '' && ($nodelistdata[$p]['spraak'] == '' || $nodelistdata[$p]['spraak'] == $language->language)) {
            $contentitems++;
            $contentlist .= '<div class="node clear-block ' . $assoc . ' nodetype_' . $nodelistdata[$p]['type_tid'] . ' grep-list-item" id="node-' . $nodelistdata[$p]['nodeid'] . '">';
            $contentlist .= '<div class="ingressView">';

            if ($nodelistdata[$p]['spraak'] == '') {
              $contentlist .= '<h2><a title="' . $nodelistdata[$p]['tittel'] . '" href="' . str_replace('spraak', $language->language, $nodelistdata[$p]['url']) . '">' . $nodelistdata[$p]['tittel'] . '</a>';
              $contentlist .= '<span class="type">(' . $nodelistdata[$p]['type_name'] . ')</span>';
              $contentlist .= '</h2>';
            }
            else if ($nodelistdata[$p]['spraak'] == $language->language) {
              $contentlist .= '<h2><a title="' . $nodelistdata[$p]['tittel'] . '" href="' . str_replace('spraak', $nodelistdata[$p]['spraak'], $nodelistdata[$p]['url']) . '">' . $nodelistdata[$p]['tittel'] . '</a>';
              $contentlist .= '<span class="type">(' . $nodelistdata[$p]['type_name'] . ')</span>';
              $contentlist .= '</h2>';
            }


            if ($nodelistdata[$p]['node_ingress'] != '' || $nodelistdata[$p]['image_url_thumbnail'] != '' || $nodelistdata[$p]['image_url_original'] != '') {
              $contentlist .= '<div class="content">';

              if ($nodelistdata[$p]['image_url_thumbnail'] != '' || $nodelistdata[$p]['image_url_original'] != '') {
                $contentlist .= theme('imagecache', 'Liten', '' . $nodelistdata[$p]['image_url_original'] . '', '' . $nodelistdata[$p]['image_title'] . '', '' . $nodelistdata[$p]['image_title'] . '');
                $contentlist .= '<div class="form-item"><a href="/' . $nodelistdata[$p]['image_url_original'] . '">' . t('Original size') . '</a></div>';
              }

              if ($nodelistdata[$p]['node_ingress'] != '') {
                $contentlist .= $nodelistdata[$p]['node_ingress'];
              }

              $contentlist .= '</div>';
            }
            $contentlist .= '</div></div>';
          }
        }//end for
      }//end foreach

      $content .= '<div class="hits">' . t('@count resources', array('@count' => $contentitems)) . '</div>';
      $content .= $contentlist . '</div>';
      $content .= '<input type="hidden" name="grep-listing-data" value="" />';
      $content .= '<input type="hidden" name="nodetype-arr" value="" />';
      $content .= '<script type="text/javascript"> setGrepListingJSON(\'' . $nodes['json'] . '\',\'' . $nodes['typearr'] . '\')</script>';


      return array('content' => $content);
    } // end if(count($nodes['array']) > 0)
  } //end if($list_type == 'grep'
  else {
    $content = '<div id="searchfilter"><ul class="clearfix">';

    //Dont display Content (0)
    if (count($nodes) != 0 && count($nodes['array']) == 0) {
      $content .= '<li>' . l(t('Content') . ' (' . count($nodes) . ')', arg(0) . '/' . arg(1) . '/' . arg(2) . '/' . arg(3) . ($rel_search !== NULL ? "/$rel_search" : ""), array('attributes' => array('class' => 'topic-content-filter', 'node-type' => 'all'))) . '</li>';
    }

    foreach ($current_types as $name => $count) {
      $content .= '<li>' . l($types[$name] . " ($count)", arg(0) . '/' . arg(1) . '/' . arg(2) . '/' . arg(3) . "/$name" . ($rel_search !== NULL ? "/$rel_search" : ""), array('attributes' => array('class' => 'topic-content-filter', 'node-type' => $name))) . "</li>";
    }

    $current_type = arg(4);
    if (!is_numeric($current_type) && !empty($current_type) && !in_array($current_type, array_keys($current_types))) {
      $content .= "<li>" . $types[$current_type] . " (0)</li>";
    }

    $content .= "</ul>";
    $content .= "<div style='clear: both; padding-top: 6px'></div>";

    if (is_array($nodes) && count($nodes['array']) == 0) {
      //Sort the nodes...
      $nodes = ndla_utils_sort_nodes($nodes);

      foreach ($nodes as $node) {
        if (!is_numeric($last_arg) || $last_arg !== NULL && $node->grep[$uuid]['number'] == $last_arg) {
          //If the user has a filter on a specific content type
          if (!is_numeric($current_type) && !empty($current_type)) {
            if ($node->type == $current_type) {
              $content .= node_view($node, true, false);
            }
          }

          //No filter, show them all...
          else {
            $content .= '<div class="' . $node->type . ' topic-content-node">' . node_view($node, true, false) . '</div>';
          }
        }

        //No relation filter...
        else if ($rel_search === NULL && ($node->type == $last_arg || $uuid == $last_arg)) {
          if ((arg(4) == '') || ($node->type == arg(4))) {
            $content .= node_view($node, true, false);
          }
        }
      } //end foreach

      $title = array();
      if ($types[arg(4)]) {
        $title[] = $types[arg(4)];
      }

      if ($rel_search == 0 && is_numeric($rel_search)) {
        $title[] = t("Partly covers");
      }
      else if ($rel_search == 1) {
        $title[] = t("Covers all");
      }
      else if ($rel_search == 2) {
        $title[] = t("More than covers");
      }

      $content = "<h2>" . implode(", ", $title) . "</h2>" . $content;
    } //end if (is_array($nodes) && count($nodes['array']) == 0
    else {
      $content = '';
    }

    if (strpos($content, '<div id="searchfilter">') !== FALSE) {
      $content .= '</div><!-- END searchfilter -->';
    }

    return array('content' => $content);
  } //end if grep
}

/**
 * _itslearning_embed_get_details
 * @param $nid => node id
 * @param $type ??
 * @return urlencoded html
 */
function _ndla_utils_itslearning_embed_get_details($nid) {
  require_once (drupal_get_path('module', 'ndla_utils') . '/Latin1UTF8.inc');
  $baseUrl = url(NULL, array('absolute' => TRUE));
  $imgURL = ndla_utils_generate_url();
  $node = node_prepare(node_load($nid));
  if ($node->type == 'flashnode' || $node->type == 'image' || $node->type == 'video') {
    $variables = ndla_utils_get_embed_vars($node);
    require_once (drupal_get_path('module', 'utdanning_theme_functions') . '/utdanning_theme_functions_embed.inc');
    $html = node_view($node, FALSE, TRUE, FALSE);
    $html .= get_node_embed_copyright($variables);
  }
  else if($node->type == 'audio') {
    $variables = ndla_utils_get_embed_vars($node);
    require_once (drupal_get_path('module', 'utdanning_theme_functions') . '/utdanning_theme_functions_embed.inc');
    $html = '<h1>' . $node->title . '</h1>';
    $html .= theme('ndla_utils_disp_media_embed', $node->type, ndla_utils_get_embed_vars($node), FALSE);
  }
  else if ($node->type == 'amendor_ios' || $node->type == 'amendor_ios_task') {
    $html = theme('amendor_ios_external_embed', $node);
    $lisens = ndla_utils_buildLicense($node);
    $html .= $lisens;
  }
  else if ($node->type == 'amendor_electure') {
    $html = theme('amendor_electure_external_embed', $node);
    $lisens = ndla_utils_buildLicense($node);
    $html .= $lisens;
  }
  else if ($node->type == 'h5p_content') {
    $html = "<iframe src='" . url('node/' . $node->nid . '/embed', array('absolute' => TRUE)) . "' width='670px' height='550px'></iframe>";
    $lisens = ndla_utils_buildLicense($node);
    $html .= $lisens;
  }
  else {
    $scripturl = str_replace('/nb', '', $baseUrl);
    $scripturl = str_replace('/nn', '', $scripturl);
    $scripturl = str_replace('/en', '', $scripturl);
    $html .= '<script type="text/javascript" src="' . $scripturl . '/ndla_utils/itslearning_js"></script>';
    $html .= '<link type="text/css" rel="stylesheet" media="all" href="' . $scripturl . '/modules/system/system.css" />';

    $html .= "<style type='text/css'>
      .right {
        clear: right;
        float: right;
        margin-right: -210px;
        position: relative;
        width: 194px;
      }

      .frame {
        background-color: #EBEBEB;
        color: #666666;
        margin-bottom: 10px;
        padding: 10px;
      }
      
      .full {
        clear: both;
        float: left;
        width: 630px;
        position: relative;
      }
      
      .contentbrowser_caption {
          font-size: 0.8em;
          display: block;
      }

      .wrap_icon.no_icon {
        min-height: 0;
        padding-top: 1px;
      }

      .UMB_image.UMB_float_none {
        float: none;
      }

      iframe, img {
        border: none;
      }

      .UMB_image.UMB_float_right.imageframe {
        margin-left: 5px;
        float: right;
        background-color: #EBEBEB;
        color: #666666;
        margin-bottom: 20px;
      }

      .read-more, .re-collapse {
        display: none;
      }
    </style>";

    $html .= '<div id="ndla_container" style="float:left; width:630px;">';
    $html .= '<h1>' . $node->title . '</h1>';
    if ($node->field_ingress_bilde[0]['nid'] != null) {
      $imgq = "select f.filepath, nr.title from {files} f INNER JOIN {image} img on f.fid = img.fid INNER JOIN  {node_revisions} nr on nr.nid = %d where img.nid = %d and img.image_size = '%s'";
      $ires = db_query($imgq, $node->field_ingress_bilde[0]['nid'], $node->field_ingress_bilde[0]['nid'], 'normal');
      $iresarr = db_fetch_array($ires);
      $html .= '<div style="border-bottom:1px solid #EBEBEB; float:left; margin:0 10px 10px 0; width:auto;">';

      if (strpos($iresarr['filepath'], 'http') === FALSE) {
        $html .= '<img src="' . $imgURL . '/' . $iresarr['filepath'] . '" alt="' . $iresarr['title'] . '" />';
      }
      else {
        $html .= '<img src="' . $iresarr['filepath'] . '" alt="' . $iresarr['title'] . '" />';
      }

      $html .= '</div>';
    }

    $html .= '<div style="color: #666666; font-size: 1.2 em; font-style: italic">';
    $html .= $node->field_ingress[0]['value'];
    $html .= '</div>';
    $html .= '<div id="node_content"  style="width: 420px">';
    $data['nid'] = $nid;
    //$html .= _resolveEmbeds($node->teaser);
    $html .= '<div id="body_' . $nid . '" style="display: none">';
    $html .= '<br /><div style="clear: both" /><a href="javascript:"';
    $html .= " onclick=\"document.getElementById('body_" . $nid . "').style.display = 'none';document.getElementById('body_mer_" . $nid . "').style.display = 'block'\">Les mindre</a><br /><br />";
    $html .= ndla_utils_replace_styles(ndla_utils_resolve_biblio($node->body)) . '</div>';
    $html .= '</div><br />';
    $html .= '</div><br />';
    $html .= '<div style="clear: both" /><a href="javascript:"';
    $html .= " onclick=\"document.getElementById('body_" . $nid . "').style.display = 'block';document.getElementById('body_mer_" . $nid . "').style.display = 'none'\" id=\"body_mer_" . $nid . "\">Les mer</a><br />";

    $lisens = ndla_utils_buildLicense($node);

    if ($type == 'textarea') {
      $html = '<div id="media-embed"><strong>' . t('Embed') . '</strong><br /><textarea rows="1" cols="30"  name="embed-text" id="embed-text" onClick="this.focus();this.select();">' . $html . $lisens . '</textarea><br />';

      $html .= '<a href="#" onclick="$(\':input[name=embed-text]\').focus();$(\':input[name=embed-text]\').select(); return false;">' . t('Select embed code') . '</a>';
    }
    else {
      $html .= '</div></div>';
      $html = ndla_utils_resolve_biblio($html . $lisens);
    }
  }

  $pattern = '/(http)((:\/\/|%3A%2F%2F)(?:[^\.]*\.){0,2}?ndla\.)/si';
  $html = preg_replace($pattern, 'https$2', $html);

//$html = str_replace('http','https',$html);
  $burl = str_replace('/nb', '', $baseUrl);
  $burl = str_replace('/nn', '', $burl);
  $burl = str_replace('/en', '', $burl);
  $burl = str_replace('http://', '', $burl);

  $html = str_replace($burl, 'ndla.no', $html);
  print $html;
}

function _ndla_utils_itslearning_get_js() {
  $itslearning_file = file_create_path('js') . '/drupal.itslearning.js';
  if (!file_exists($itslearning_file)) {
    $content = file_get_contents('misc/jquery.js');
    $content .= ";\n";
    $content .= '(function ($) {';
    $content .= "\n";
    $content .= file_get_contents('misc/drupal.js');
    $content .= "\n";
    $content .= file_get_contents('misc/collapse.js');
    $content .= "\n";
    $content .= '}(jQuery));';
    $content .= "\n";
    $content .= '$.noConflict();';
    file_put_contents($itslearning_file, $content);
  }
  print file_get_contents($itslearning_file);
}

function ndla_utils_grep_callback($group_id) {
 $result = ndla_ontopia_connect_get_greplisting($group_id);
 print $result;

 exit();
}

/**
 * Prints an iframe containing a youtube clip.
 *
 * @param $video_code
 *  The youtube video id
 */
function ndla_utils_print_youtube_iframe($video_code) {
  $attributes = array(
    "width='" . NDLA_UTILS_YOUTUBE_WIDTH . "'",
    "height='" . NDLA_UTILS_YOUTUBE_HEIGHT . "'",
    "title='YouTube video player'",
    "class='youtube-player'",
    "type='text/html'",
    "frameborder='0'",
    "allowFullScreen",
    "src='http://www.youtube.com/embed/$video_code'",
  );

  $attributes = implode(" ", $attributes);
  $full_url = "<a href='http://www.youtube.com/watch?v=$code' target='_blank'>" . t('View on YouTube.com') . "</a>";
  $out = "<iframe $attributes>" . t('Your browser does not support iframes.') . $full_url . "</iframe>";
  print $out;
}

/**
 * Get user guide html
 *
 * @param nid
 *  nid for the htmlvideo node we want to show
 */
function ndla_utils_user_guide($nid) {
  $light_node = htmlvideo_get_light_node($nid);
  drupal_set_header('Content-Type: application/xhtml+xml; charset=utf-8');
  print htmlvideo_display($light_node, array('width' => 611, 'height' => 344, 'autoplay' => isset($_GET['autoplay'])));
}


function ndla_utils_facebook_admin() {
  $form = array(
    'ndla_utils_facebook_js' => array(
      '#type' => 'textarea',
      '#title' => t('Javascript'),
      '#default_value' => variable_get("ndla_utils_facebook_js", "<div id=\"fb-root\"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = \"//connect.facebook.net/en_GB/all.js#xfbml=1\";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>"),
    ),
    
    'ndla_utils_facebook_html_frontpage' => array(
      '#type' => 'textarea',
      '#title' => t('HTML for the frontpage'),
      '#default_value' => variable_get("ndla_utils_facebook_html_frontpage", "<div class=\"fb-like-box\" data-href=\"https://www.facebook.com/ndla.no\" data-width=\"300\" data-show-faces=\"false\" data-stream=\"true\" data-border-color=\"#eaeaea\" data-header=\"false\"></div>"),
    ),
    'ndla_utils_facebook_html_fag' => array(
      '#type' => 'textarea',
      '#title' => t('HTML for subjects'),
      '#default_value' => variable_get("ndla_utils_facebook_html_fag", "<div class=\"fb-like-box\" data-href=\"https://www.facebook.com/ACCOUNT_NAME\" data-width=\"292\" data-show-faces=\"false\" data-stream=\"false\" data-header=\"true\"></div>"),
      '#description' => t('ACCOUNT_NAME will be replaced with the real the account name Ie) naturfag'),
    ),
  );

  $form['nda_utils_facebook_fag_accounts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subject Facebook accounts'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#tree' => TRUE,
  );
  
  $groups_result = db_query("SELECT nid, title FROM {node} WHERE type = 'fag'");
  $fag_accounts = variable_get('nda_utils_facebook_fag_accounts', array());
  while($row = db_fetch_object($groups_result)) {
    $form['nda_utils_facebook_fag_accounts'][$row->nid] = array(
      '#type' => 'textfield',
      '#title' => t('Account for @name', array('@name' => $row->title)),
      '#default_value' => (!empty($fag_accounts[$row->nid])) ? $fag_accounts[$row->nid] : '',
    );
  }
  
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  return system_settings_form($form);
}

function ndla_utils_googleplus_admin() {
  $res = db_query("SELECT nid, title FROM {node} WHERE type = 'fag' ORDER BY title ASC");
  $groups = array();
  while($row = db_fetch_object($res)) {
    $groups[$row->nid] = $row->title;
  }
  
  $form['ndla_utils_google_ndla_badge'] = array(
    '#type' => 'textarea',
    '#title' => t('Google+ badge code'),
    '#description' => t('Google+ code for ndla.no'),
    '#default_value' => variable_get('ndla_utils_google_ndla_badge', ''),
  );
  foreach($groups as $nid => $title) {
    $form[$title] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => FALSE,
      '#title' => t('Code for @title', array('@title' => $title)),
      'ndla_utils_google_badge_nid_' . $nid => array(
        '#title' => t('Code'),
        '#type' => 'textarea',
        '#tree' => FALSE,
        '#default_value' => variable_get('ndla_utils_google_badge_nid_' . $nid, ''),
      ),
    );
  }
  
  return system_settings_form($form);
}

function ndla_utils_view_embed($nid, $print = TURE) {
  $out = '';
  $content = '';
  $title = NULL;
  if(is_numeric($nid)) {
    $node = node_load($nid);
    if($node) {
      $title = $node->title;
      $vars = ndla_utils_get_embed_vars($node);
      $content = node_view($node, FALSE, TRUE, FALSE);
      module_load_include('inc', 'ndla_utils_disp', 'ndla_utils_disp.blocks');
      $usufruct = _ndla_utils_disp_rights_content($node, 2);
    }
  }
  
  $css = drupal_get_css();
  $js = drupal_get_js();

  //Upgrade jquery
  $js = str_replace('/misc/jquery.js', '/sites/all/themes/ndla2010/js/jquery-1.7.2.min.js', $js);
  //Remove ndla_auth
  $js = preg_replace("/<script.*?ndla_auth_js.*?<\/script>/", "", $js);
  //Remove annotate etc
  $js = preg_replace("/<script.*?ndla_my_learning.*?<\/script>/", "", $js);
  $html = theme('ndla_utils_embed_page', $title, $content, $js, $css, $usufruct);
  if($print) {
    print $html;
  } else {
    return $html;
  }
}

function ndla_utils_view_slideshow_history() {
  global $language, $base_path;
  $out = "";
  $nodes = ndla_utils_load_slideshow(TRUE);
  foreach($nodes as $node) {
    $out .= "<div class='news-item'>";
    $out .= "<h2>" . l($node->title, 'node/' . $node->nid) . "</h2>";
    $out .= "<p>" . l("<img src='" . $base_path . $node->image . "'/>", 'node/' . $node->nid, array('html' => TRUE)) . "</p><p>" .  $node->teaser . "</p>";
    $out .= "</div>";
  }
  
  $out .= theme('pager');
  return $out;
}
