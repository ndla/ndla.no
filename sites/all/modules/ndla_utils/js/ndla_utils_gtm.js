/**
 * Javascript for adding the Google Tag Manager Javascript
 */

var added = false;
Drupal.behaviors.ndla_utils_gtm = function() {
  if(!added) {
    var gtm_id = Drupal.settings.gtm_id;
    jQuery('body').prepend('<noscript><iframe src="//www.googletagmanager.com/ns.html?id=' + gtm_id + '" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>' + '<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\': new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],' + 'j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=\'//www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);'+'})(window,document,\'script\',\'dataLayer\',\'' + gtm_id + '\');<\/script>');
    added = true;
  }
};