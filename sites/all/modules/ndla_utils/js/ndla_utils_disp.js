// The lightbox module don't find the correct page height. We fix it here...
// http://drupal.org/node/1285208
var Lightbox = Lightbox || {};
Drupal.behaviors.frontpage_slide = function() {
  if($('.slideshow-feeds').length > 0) {
    $('.slideshow-feeds').cycle({ 
        fx:     'fade', 
        timeout: 8000, 
        delay:  -1000,
        pager: '.front-slideshow-nav',
    });
  
    $('.play-pause a').bind('click', function() {
      if($(this).hasClass('pause')) {
        $('.slideshow-feeds').cycle('pause');
        $('.play-pause .play').show().focus();
        $('.play-pause .pause').hide();
      }
      else if($(this).hasClass('play')) {
        $('.slideshow-feeds').cycle('resume');
        $('.play-pause .pause').show().focus();
        $('.play-pause .play').hide();
      }
    });
  }
};
Lightbox.oldGetPageSize = Lightbox.getPageSize;

Lightbox.getPageSize = function() {
  var pageSizes = Lightbox.oldGetPageSize();
  pageSizes[1] = Math.max(
    Math.max(document.body.scrollHeight, document.documentElement.scrollHeight),
    Math.max(document.body.offsetHeight, document.documentElement.offsetHeight),
    Math.max(document.body.clientHeight, document.documentElement.clientHeight)
  );
  return pageSizes;
}

// Pause the slideshow 
viewsSlideshowSingleFramePause = function (settings) {
  //make Resume translatable
  var resume = '<img src="' + Drupal.settings.basePath + 'sites/all/themes/ndla2010/img/slideshow_play.png" alt="" title="" width="20" height="20">';

  $(settings.targetId).cycle('pause');
  if (settings.controls > 0) {
    $('#views_slideshow_singleframe_playpause_' + settings.vss_id)
      .addClass('views_slideshow_singleframe_play')
      .addClass('views_slideshow_play')
      .removeClass('views_slideshow_singleframe_pause')
      .removeClass('views_slideshow_pause')
      .html(resume);
  }
  settings.paused = true;
}

// Resume the slideshow
viewsSlideshowSingleFrameResume = function (settings) {
  var pause = '<img src="' + Drupal.settings.basePath + 'sites/all/themes/ndla2010/img/slideshow_pause.png" alt="" title="" width="20" height="20">';
	
  $(settings.targetId).cycle('resume');
  if (settings.controls > 0) {
    $('#views_slideshow_singleframe_playpause_' + settings.vss_id)
      .addClass('views_slideshow_singleframe_pause')
      .addClass('views_slideshow_pause')
      .removeClass('views_slideshow_singleframe_play')
      .removeClass('views_slideshow_play')
      .html(pause);
  }
  settings.paused = false;
}

/**
 * This will write over a core drupal js function
 * for collapsible fields. Drupal uses listeners
 * and r3adspeaker reloads the page and the
 * listeners is lost. This adds the click events
 * to the html instead of listeners
 */
Drupal.behaviors.collapse = function (context) {
  $('fieldset.collapsible > legend:not(.collapse-processed)', context).each(function() {
    var fieldset = $(this.parentNode);
    // Expand if there are errors inside
    if ($('input.error, textarea.error, select.error', fieldset).size() > 0) {
      fieldset.removeClass('collapsed');
    }

    // Turn the legend into a clickable link and wrap the contents of the fieldset
    // in a div for easier animation
    var text = this.innerHTML;
    $(this).empty().append($('<a href="javascript:;" onclick="collapse_fieldset(this);">'+ text +'</a>'))
      .after($('<div class="fieldset-wrapper"></div>')
      .append(fieldset.children(':not(legend):not(.action)')))
      .addClass('collapse-processed');
  });
  
  $('.click_and_close legend a').bind('click', function() {
    try {
      ndla_tinymce_set_height();
    }
    catch(err) { ; }
  });
};

collapse_fieldset = function(element) {
  var fieldset = $(element).parents('fieldset:first')[0];
  // Don't animate multiple times
  if (!fieldset.animating) {
    fieldset.animating = true;
    Drupal.toggleFieldset(fieldset);
  }
  return false;
}