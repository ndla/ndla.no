$(document).ready(function() {
  if (!Drupal.settings.user_is_logged_in) {
    if (typeof ndla_auth !== 'undefined') {
      ndla_auth(function(a) {
        if (a.is_authenticated()) {
            $('body').removeClass('password-protected');
        } else {
          ndla_utils_set_login_text();
        }
      });
    } else {
      ndla_utils_set_login_text();
    }
  }
  function ndla_utils_set_login_text() {
    $('<h1>' + Drupal.t('Login required') + '</h1><p>' + Drupal.t('You must log in to NDLA to view this page.') + '</p>').insertAfter('#top-bla-menu');
  }
});
