Drupal.behaviors.person_exists = function() {
  $('#edit-title').blur(function() {
    $('#duplicate-warning').remove();
    $('#edit-title').removeClass("error");
    var title = $('#edit-title').val();
    if(title == '')
      return;
    $.ajax({
      url: Drupal.settings.basePath + 'node/exists/person/' + title,
      //data: title,
      success: function(data) {
        if(data.num != 0) {
          $('#headerSeparator').before(data.warning);
          $('#edit-title').addClass("error");
        }
      },
      dataType: 'json'
    });
  });
}