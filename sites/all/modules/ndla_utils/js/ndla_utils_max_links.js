var ndla_utils_max_links_init = false;
Drupal.behaviors.ndla_utils_max_links = function(){
  if(ndla_utils_max_links_init == false) {
    $('.max-links-show-more a').click(function() {
      $(this).parent().find('a').toggle();
      $(this).parent().parent().find('.max-links-hidden').toggle();
    });
  }
  ndla_utils_max_links_init = true;
}