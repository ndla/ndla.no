(function($) {

  function escapeHtml(text) {
    if (typeof text !== 'undefined') {
      return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
    } else {
      return '';
    }
  }

  var counter = 0;
  $(function() {

    var languages = Drupal.settings['topic_site_languages'];

    var alanguages = Array();
    var counter = 0;
    for (lang_code in languages) {
      alanguages[counter++] = languages[lang_code]['iso_639_2'];
    }

    var delay = (function() {
      var timer = 0;
      return function(callback, ms){
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
      };
    })();

    var currentXHR = false;

    var displayKeyword = function($keyword) {
      $('.ndla-ord-keyword-popup').hide();
      $keyword.parent().find('.ndla-ord-keyword-popup').css('display', 'inline-block').fadeIn();

      $keyword.parent().find('.ndla-ord-keyword-popup .ndla-ord-keyword-lang input').each(function() {
        if ($(this).hasClass('ndla-ord-' + Drupal.settings.topic_site_languages[Drupal.settings.node_language]['iso_639_2'])) {
          $input = $(this);
          return false;
        }
      });

      if ($input.val() == '') {
        $input.val($keyword.find('.tagger-plugin-keyword-title').text());
        $input.trigger('change');
      }
      setTimeout(function() {$input.focus();}, 0); // I don't know why I need this timeout, but without it focus doesn't work
      $('.ndla-ord-autocomplete').fadeOut(100);
      if (currentXHR) currentXHR.abort();
    }

    var getPrioritiezedName = function(names) {
      var i;
      prioritiezed_languages = Array('http://psi.oasis-open.org/iso/639/#nob', 'http://psi.topic.ndla.no/#language-neutral'); // TODO: Hardcoded. Me don't like
      name = false;
      for (i in prioritiezed_languages) {
        lang = prioritiezed_languages[i];
        if (typeof names[lang] !== 'undefined') {
          name = names[lang];
          break;
        }
      }
      if (name === false) for(name in names) break;
      return name;
    }

    var autocomplete = function($input, $autocomplete, callback) {
      $input.keyup(function(event) {
        var $that = $(this);
        delay(function() {
          var search = $that.val();

          $autocomplete_body = $autocomplete.find('table tbody');

          if (search != '') {
            var search_url = '/sites/all/modules/ndla_ord/autocomplete.php'; // TODO: Hardcoded. Fix

            if (currentXHR) currentXHR.abort();
            currentXHR = $.get(search_url, {search: search, 'lang[]': alanguages}, function(data) {

              o = JSON.parse(data);
              var html = '';
              var types = '';

              $autocomplete_body.empty();

              for (i in o) {
                types = o[i].type;

                lang_str = '';
                for (k in o[i]) {
                  if (k.substr(0, 6) == 'title_' && k != 'title_language_neutral') {
                    lang_str += 'data-topic_name_' + k.substr(6) + '="' + escapeHtml(o[i][k]) + '" ';
                  }
                }

                class_str = '';
                counter = 0;
                types = '';
                for (k in o[i].types) {
                  if (typeof o[i].types[k] !== 'undefined') {
                    class_str += 'data-topic_class_id_' + counter + '="' + escapeHtml(o[i].types[k].type_id) + '" ';
                    class_name = getPrioritiezedName(o[i].types[k].names);
                    class_str += 'data-topic_class_name_' + counter + '="' + escapeHtml(class_name) + '" ';
                    types += ', ' + class_name;
                    counter++;
                  }
                }
                if (types) types = types.substr(2);

                $word = $('<tr class="ndla-ord-autocomplete-word' + (i == 0 ? ' ndla-ord-autocomplete-word-hover' : '') + '" ' + lang_str + class_str + 'data-topic_name="' + escapeHtml(o[i].title) + '" data-topic_id="' + escapeHtml(o[i].id) + '" data-topic_visible="' + o[i].visible + '"></tr>');
                $autocomplete_body.append($word);

                for (lang_code in languages) {
                  if (languages[lang_code]['course_language'] || languages[lang_code]['required']) $word.append('<td>' + (typeof o[i]['title_' + languages[lang_code]['iso_639_2']] !== 'undefined' ? o[i]['title_' + languages[lang_code]['iso_639_2']] : '') + '</td>');
                }

                $word.append('<td>' + types + '</td>');

                $word.hover(
                  function(event) {
                    $(this).parent().find('.ndla-ord-autocomplete-word').removeClass('ndla-ord-autocomplete-word-hover');
                    $(this).addClass('ndla-ord-autocomplete-word-hover');
                  },
                  function(event) {
                    $(this).removeClass('ndla-ord-autocomplete-word-hover');
                    if ($(this).parent().find('.ndla-ord-autocomplete-word-hover').length == 0) {
                      $($(this).parent().find('.ndla-ord-autocomplete-word')[0]).addClass('ndla-ord-autocomplete-word-hover');
                    }
                  }
                );

                $word.click(function() {
                  callback($(this), $input, $autocomplete);
                  $autocomplete.fadeOut(100);
                });
              }
              $autocomplete.fadeIn(500);
            });
          } else {
            $autocomplete.fadeOut(500);
          }

        }, 300);
      });


    };

 
    var initialize = function($plugin, params) {
      var c = tagger.getPluginId($plugin);
      $div = $('<div class="ndla-ord-autocomplete"><div class="ndla-ord-scroller"><table><thead><tr></tr></thead><tbody></tbody></table></div>' + ($plugin.extra('new-tags') == '1' ? '<button type="button">' + Drupal.t('New keyword') + '</button>' : '') + '</div>');
      var $textfield = $('#tagger-plugin-textfield-' + c);
      $textfield.after($div);
      autocomplete($textfield, $div, function($keyword) {
        c = tagger.getPluginId($keyword);
        var extra = {};

        extra['visible'] = $keyword.extra('topic_visible');
        extra['topic_id'] = $keyword.extra('topic_id');

        extras = $keyword.extra();
        counter = 0;
        for (key in extras) {
          if (key.substr(0, 15) == 'topic_class_id_') {
            extra['class_id_' + counter] = extras[key];
            extra['class_name_' + counter] = extras['topic_class_name_' + counter];
            counter++;
          }
        }

        for (lang_code in languages) {
          extra['lang_' + languages[lang_code]['iso_639_2']] = $keyword.extra('topic_name_' + languages[lang_code]['iso_639_2']);
        }

        tagger.addKeyword($('#tagger-plugin-' + c), $keyword.extra('topic_name'), params, extra);
      });

      for (i in languages) {
        if (languages[i]['required'] || languages[i]['course_language']) $div.find('thead tr').append('<td>' + languages[i]['title'] + '</td>');
      }

      $div.find('thead tr').append('<td>' + Drupal.t('Category') + '</td>');

      $div.find('button').click(function() {
        $plugin = tagger.getPlugin($(this));
        $keyword = tagger.addKeyword($plugin);
        displayKeyword($keyword);
      });

    };

    $('.ndla-ord-keywords').bind('initialized', function() {
      initialize(tagger.getPlugin($(this)));
    });

    $('.ndla-ord-keywords').bind('mouseenterkeyword', function(event, $keyword) {
      c = tagger.getPluginId($keyword);
      $keyword.addClass('hover');
      $keyword.parent().find('.ndla-ord-keyword-popup-' + c).css('display', 'inline-block').fadeIn();
    });

    $('.ndla-ord-keywords').bind('mouseleavekeyword', function(event, $keyword) {
      c = tagger.getPluginId($keyword);
      $keyword.removeClass('hover');
      $keyword.parent().find('.ndla-ord-keyword-popup-' + c).fadeOut(10);
    });

    $('.ndla-ord-keywords').bind('keyboard-enter', function(event, $plugin) {
      $collection = $plugin.find('.ndla-ord-autocomplete table tbody tr');
      if ($collection.length >= 1) {
        $($collection[0]).click();
      }
    });

    // This will not run for classes, and it should not.
    $('.ndla-ord-keywords').bind('addkeyword', function(event, $keyword, data) {
      counter++;
      c = tagger.getPluginId($keyword);

      width = $keyword.width();
      height = $keyword.height() + 1;
      if (typeof data === 'undefined') data = {visible: true};

      $span = $('<span class="ndla-ord-keyword-popup ndla-ord-keyword-popup-' + c + '" style="width: 400px; margin-left: -' + width + 'px; margin-top: ' + height + 'px;"></span>');
      $keyword.append($span);
      for (lang_code in languages) {
        $input = $('<input type="text" class="ndla-ord-keyword-lang-input ndla-ord-' + languages[lang_code]['iso_639_2'] + '" value="' + escapeHtml(data['lang_' + languages[lang_code]['iso_639_2']]) + '">');
        $div = $('<div class="ndla-ord-keyword-lang' + (languages[lang_code]['required'] ? ' ndla-ord-keyword-required' : '') + (!languages[lang_code]['course_language'] && !languages[lang_code]['required']  ? ' ndla-ord-keyword-hidden' : '') + '"><div class="ndla-ord-keyword-title">' + languages[lang_code]['title'] + ': ' + (languages[lang_code]['required'] ? '<span class="form-required">*</span>' : '') + '</div></div>');
        $span.append($div);
        $div.append($input);

        $input.change(function(e) {
          tagger.refresh();
        });

        if (languages[lang_code]['required']) {
          if ($input.val() == '') $input.parents('.tagger-plugin-keyword').addClass('ndla-ord-error');
          $input.change(function(e) {
            if ($(this).val() == '') {
              $(this).parents('.tagger-plugin-keyword').addClass('ndla-ord-error');
            } else {
              $(this).parents('.tagger-plugin-keyword').removeClass('ndla-ord-error');
            }
          });
        }
      }

      $span.append('<input type="checkbox" class="ndla-ord-keyword-visibility" id="ndla-ord-keyword-visibility-' + counter + '"><label for="ndla-ord-keyword-visibility-' + counter + '">' + Drupal.t('Visible in views?') + '</label><div style="display: none;">' + Drupal.t('Category') + ':</div><span style="display: none;" class="ndla-ord-class-wrapper"></span><input type="hidden" class="ndla-ord-class-id" value="' + escapeHtml(data['class_id']) + '"><input type="hidden" class="ndla-ord-topic-id" value="' + escapeHtml(data['topic_id']) + '">');

      $class = $('<input type="text" class="ndla-ord-class" value="' + escapeHtml(data['class']) + '">');
      $span.find('.ndla-ord-class-wrapper').append($class);

      $class.bind('initialized', function() {
        initialize(tagger.getPlugin($(this)), {
          locked: false
        });
      });

      $class.bind('addkeyword', function(event, $keyword, data) {
        $input = $('<input type="hidden" class="ndla-ord-class-id" value="' + escapeHtml(data['topic_id']) + '" />')
        $keyword.append($input);
      });

      $class.tagger({
        new_tags: false
      });

      for (i in data) {
        if (i.substr(0, 9) == 'class_id_') {
          var extra = {};

          extra['visible'] = 1;
          extra['topic_id'] = data[i];

          y = i.substr(9);
          tagger.addKeyword($class, data['class_name_' + y], {locked: true}, extra);
        }
      }

      if (data.visible == false) {
          $keyword.addClass('ndla-ord-hidden');
      }
      $keyword.find('.ndla-ord-keyword-visibility').attr('checked', (data.visible == true));

      $keyword.find('.ndla-ord-keyword-visibility').change(function() {
        if ($(this).attr('checked')) {
          $keyword.removeClass('ndla-ord-hidden');
        } else {
          $keyword.addClass('ndla-ord-hidden');
        }
        tagger.refresh();
      });

      $span.click(function(event) {
        tagger.blurKeyword();
        event.stopPropagation();
      });

      if (!tagger.initializing && !data['id']) displayKeyword($keyword);
    });

    $('.ndla-ord-keywords').bind('save', function(event, $keywords) {
      $keywords.each(function() {

        for (i in $(this).extra()) {
          $(this).removeAttr('data-' + i);          
        }

        for (lang_code in languages) {
          $(this).extra('lang_' + languages[lang_code]['iso_639_2'], $(this).find('.ndla-ord-' + languages[lang_code]['iso_639_2']).val());
        }

        $(this).extra('visible', $(this).find('.ndla-ord-keyword-visibility').is(':checked') ? 1 : 0);
        $(this).extra('topic_id', $(this).find('.ndla-ord-topic-id').val());
        $class_names = $(this).find('.ndla-ord-class-wrapper .tagger-plugin-keyword-title');
        $class_ids = $(this).find('.ndla-ord-class-wrapper .ndla-ord-class-id');
        i = 0;
        $that = $(this);
        $class_ids.each(function() {
          class_id = $($class_ids[i]).val();
          class_name = $($class_names[i]).text();
          $that.extra('class_id_' + i, class_id);
          $that.extra('class_name_' + i, class_name);
          i++;
        });
      });
    });
  });
}(jQuery));

$(function() {
  tagger.initializing = true;
  $('.ndla-ord-keywords').tagger();
  tagger.initializing = false;
});
