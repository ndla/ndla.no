
Drupal.behaviors.ndla_ord_form = function(context) {
	$("#allKeyWords input[type=checkbox]").each(function(){
		if($(this).is(':checked')){
			var thisid = $(this).attr('id');
			thisarr = thisid.split("-");
			var thisnum = parseInt(thisarr[2])+1; 
			$("#singleKeyWords_"+thisnum).hide();
		}
	});   
};

function removeKeyword() {
	var counter = 0;
	$("#allKeyWords input[type=checkbox]").each(function(){
		if($(this).is(':checked')){
			var thisid = $(this).attr('id');
			thisarr = thisid.split("-");
			var thisnum = parseInt(thisarr[2])+1; 
			$("#singleKeyWords_"+thisnum).hide();
		}
		else{
			counter++;
		}
	});
	
	if(counter < 3){
		alert(Drupal.t("Remember to add new keywords for the ones you removed,in order to meet the requirement of 3 keywords"));
	}
	return;
}
