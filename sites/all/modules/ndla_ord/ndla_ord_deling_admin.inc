<?php
// $Id$  ndla_ord_deling_admin.inc, v 1.0 2013/05/22 14:00 minimalismore Exp $
/** \addtogroup ndla_ord */

/**
 * @file
 * @ingroup ndla_ord
 * @brief
 *  Provides ndla_ord
 */


function ndla_ord_deling_admin_module_settings(){
  $form = array();
  $form['ndla_ord_deling_topic_service_url'] = array(
      '#title' => t('Base url of the topic server'),
      '#type' => 'textfield',
      '#default_value' => variable_get('ndla_ord_deling_topic_service_url',  ''),
      '#description' => t('Enter the topic service url for this installation i.e http://topics.ndla.no. Omit trailing slash'),
  );
  
  $form['submit']=array(
      '#type' => 'submit',
      '#value' => t('Save')
  );
  
  return $form;
}

/**
 * Implementation of hook_validate for ndla_ontopia_connect_admin_settings
 */
function ndla_ord_deling_admin_module_settings_validate($form,  &$form_state) {

  if (empty($form_state['values']['ndla_ord_deling_topic_service_url'])) {
    form_set_error('ndla_ord_deling_topic_service_url', t('The topic service url for this installation cannot be empty'));
  }
 
}


/**
 * Implementation of hook_submit for ndla_ontopia_connect_admin_settings
 */
function ndla_ord_deling_admin_module_settings_submit($form,  &$form_state) {
  foreach ($form_state['values'] as $key => $value) {
    variable_set($key, $value);
  }
  drupal_set_message(t('The settings were saved.'), 'status', TRUE);
}
