var H5P = H5P || {};

(function ($) {
  // Make changes in NDLA smart editor trigger window resize, so resize
  // calculations in H5P works properly.
  $(document).ready(function () {

    // Wiris needs Drupal.settings, therefore using the parent windows
    if (window.Drupal === undefined) {
      window.Drupal = window.top.Drupal;
    }

    $('.node-form').on('click', ".ndla-form-step, #form-step-previous, #form-step-next", function (event) {
      // We are in H5P.jQuery world here, trigger will work.
      $(window).trigger('resize');
    });

    if (typeof CKEDITOR !== 'undefined') {
      // Add wiris plugin
      CKEDITOR.plugins.addExternal('ckeditor_wiris', Drupal.settings.h5peditor.wirisPath);

      CKEDITOR.on('dialogDefinition', function (event) {

        // Add Node support to Link plugin.
        if (event.data.name === 'link') {
          var plugin = CKEDITOR.plugins.link;
          var editor = event.editor;
          var dialogDefinition = event.data.definition;
          var infoTab = dialogDefinition.getContents('info');

          // Alter link type select to include Node
          var linkType = infoTab.get('linkType');
          linkType.items = [['Node', 'nid']];
          linkType.default = 'nid';

          // Hook into linkType's onChange
          var linkTypeOnChange = linkType.onChange;
          linkType.onChange = function () {
            linkTypeOnChange.apply(this, arguments);

            // Adds support for Node option
            var dialog = this.getDialog();
            var nodeElement = dialog.getContentElement('info', 'node').getElement();
            if (this.getValue() === 'nid') {
              nodeElement.show();
            }
            else {
              nodeElement.hide();
            }
          };

          // Keep track of current nid
          var nid;

          // Hook into linkType's setup
          var linkTypeSetup = linkType.setup;
          linkType.setup = function () {
            // Look to see if a link with NDLA nid is selected
            var element = plugin.getSelectedLink(editor);
            if (element && element.hasAttribute('data-ndla-nid')) {
              nid = element.getAttribute('data-ndla-nid');
            }
            else {
              nid = null;
            }

            // Open Node dialog if a nid is set.
            this.setValue('nid');
          };

          // Add node field to tab
          infoTab.add({
            type: 'vbox',
            id: 'node',
            padding: 1,
            children: [
              {
                type: 'text',
                id: 'nodeId',
                label: 'Node ID',
                setup: function () {
                  if (nid) {
                    this.setValue(nid);
                  }
                },
                validate: function () {
                  var value = this.getValue();
                  if (value === '' || (/\D/).test(value)) {
                    return 'Node ID can only contain digits';
                  }
                },
                commit: function (data) {
                  if (data.type === 'nid') {
                    data.type = 'url'; // Nid is always url!
                  }

                  // Update URL and protocol to match nodeid
                  nid = this.getValue();
                  var dialog = this.getDialog();
                  dialog.getContentElement('info', 'url').setValue(nid ? '/node/' + nid : '/');
                  var schema = (Drupal.settings.ndla_utils.https) ? 'https://' : 'http://';
                  dialog.getContentElement('info', 'protocol').setValue(schema);
                }
              },
              {
                type: 'button',
                id: 'contentbrowser',
                label: window.top.Drupal.t('Browse') + '...',
                onClick: function () {
                  var dialog = this.getDialog();

                  // Return callback for contentbrowser
                  runkode = function (nodeId) {
                    var nodeIdField = dialog.getContentElement('info', 'nodeId');
                    nodeIdField.setValue(nodeId);
                  };

                  // Open contentbrowser
                  var base = window.location.protocol + '//' + window.location.host + H5PEditor.baseUrl;
                  var cb = window.open(base + 'contentbrowser?output=nid', "_blank", "resizable=yes");
                }
              }
            ]
          });

          // Hook into the dialogs onOk
          var onOk = dialogDefinition.onOk;
          dialogDefinition.onOk = function() {
            onOk.apply(this, arguments);

            // Find the link in question
            var element = plugin.getSelectedLink(editor);
            if (element && element.hasAttribute('href')) {
              if (nid) {
                element.setAttribute('data-ndla-nid', nid);
              }
              else if (element.hasAttribute('data-ndla-nid')) {
                element.removeAttribute('data-ndla-nid');
              }
            }
          };
        }
      });
    }
  });
})(H5P.jQuery);
