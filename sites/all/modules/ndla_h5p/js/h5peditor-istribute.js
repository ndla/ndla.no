var H5PEditor = H5PEditor || {};

H5PEditor.widgets.video = H5PEditor.Istribute = (function ($, H, E) {
  function C(parent, field, params, setValue) {
    this.field = field;
    this.params = params;
    this.setValue = setValue;
    this.uploading = false;

    this.changes = []; // What does it do?
  };

  C.prototype.appendTo = function ($wrapper) {
    var that = this;

    this.$item = $(this.createHtml()).appendTo($wrapper);
    this.$errors = this.$item.children('.errors');

    var $iframe;
    C.loadEasyXDM(function () {
      var path = Drupal.settings.basePath + 'ndla-h5p/istribute-url';
      if (that.params !== undefined && that.params[0].key !== undefined) {
        path += '/' + that.params[0].key;
      }

      $.ajax({
        url: path,
        success: function (data) {
          new easyXDM.Socket({
          remote: data.url,
          container: that.$item.children('.video')[0],
      		onMessage: function (message, origin) {
            var cmd = message.split(/ /);

            if ($iframe === undefined) {
              $iframe = that.$item.find('iframe');
            }

            switch(cmd[0]) {
              case 'isuploading':
                // Prevent submit?
                that.uploading = true;
                break;

              case 'isnotuploading':
                // Take a break
                that.uploading = false;
                break;

              case 'dialogify':
                $iframe.css('height', cmd[2] + 'px');
                break;

              case 'undialogify':
                $iframe.css('height', '90px');
                break;

              case 'setvalue':
                var key = message.substring(9);
                var schema = (Drupal.settings.ndla_utils.https) ? 'https://' : 'http://';
                that.params = [
                  {
                    path: schema + 's3-eu-west-1.amazonaws.com/istribute/' + data.appId + '/' + key + '/360.mp4',
                    mime: 'video/mp4',
                    key: key
                  }
                ];

                that.setValue(that.field, that.params);

                for (var i = 0; i < that.changes.length; i++) {
                  that.changes[i](that.params[0]);
                }

                break;
            }
          },
          props: {
            allowtransparency: true,
            style: {
              border: 0,
              padding: 0,
              margin: 0,
              width: '100%',
              height: '90px'
            }
          }
        });
        },
        dataType: 'json',
        error: function(jqXHR, textStatus, errorThrown) {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
        }
      });
    });
  };

  C.prototype.createHtml = function () {
    var label = '';
    if (this.field.label !== 0) {
      label = '<span class="h5peditor-label">' + (this.field.label === undefined ? this.field.name : this.field.label) + '</span>';
    }

    return E.createItem(this.field.type, label + '<div class="video"></div>');
  };

  C.prototype.validate = function () {
    if (this.uploading) {
      return false;
    }
    return true;
  };

  C.prototype.remove = function () {
    this.$item.remove();
  };

  C.loadEasyXDM = function (callback) {
    if (window['easyXDM'] !== undefined) {
      callback();
      return;
    }

    if (C.easyXDMQueue !== undefined) {
      C.easyXDMQueue.push(callback);
      return;
    }

    C.easyXDMQueue = [callback];
    var schema = (Drupal.settings.ndla_utils.https) ? 'https://' : 'http://';
    $.getScript(schema + 'api.istribute.com/v1/video/uploader/lib/easyXDM/easyXDM.min.js', function () {
      for (var i = 0; i < C.easyXDMQueue.length; i++) {
        C.easyXDMQueue[i]();
      }
      delete C.easyXDMQueue;
    });
  };

  return C;
})(H5P.jQuery, H5P, H5PEditor);