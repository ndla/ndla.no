<?php


/**
 * @file
 *  cookbook.recipe.inc php file
 *  Contains functions related to displaying a recipe for the Drupal module
 *  cookbook.
 */

/**
 * Function to generate the cookbooks recipe page.
 *
 * @param int $category
 *  Selected category id.
 * @param string $search
 *  Keywords to filter title on.
 * @param string $order
 *  Field and direction to order recipes by.
 * @param int $page
 *  Which page of results to display.
 * @param int $nid
 *  The id of the recipe to display.
 *
 * @return string
 *  Themed output.
 */
function cookbook_recipe($category, $search, $order, $page, $nid) {
  // TODO: This function is long, it should be split up
  global $language;

  cookbook_add_js();
  drupal_add_js(
    array(
      'cookbook' => array(
        'disableFractions' => variable_get('cookbook_disable_fractions', 0)
      )
    ), 'setting'
  );
  drupal_add_js(drupal_get_path('module', 'cookbook') . '/theme/javascripts/cookbook-recipe.js');

  // Check for translations
  if (variable_get('language_content_type_recipe', 0) == TRANSLATION_ENABLED) {
    // Get the node's tnid
    $tnid = db_result(db_query('SELECT tnid FROM node WHERE nid = %d', $nid));
    if ($tnid) {
      // Check for possible translations
      $translations = translation_node_get_translations($tnid);
      if (isset($translations[$language->language])) {
        // Use translation
        $recipe = node_load($translations[$language->language]->nid);
      }
      else {
        drupal_set_message(t('No !language translation found for this recipe.', array('!language' => strtolower($language->name))), 'warning');
      }
    }
  }

  if (!isset($recipe)) {
    $recipe = node_load($nid);
  }

  // Display front page if the recipe doesn't exist
  if (!$recipe) {
    drupal_set_message(t('Selected recipe does not exist!'), 'error');
    return cookbook_front();
  }

  // Images
  if (isset($recipe->cookbook_recipe_images)) {
    foreach ($recipe->cookbook_recipe_images as $image) {
      if (is_numeric($image->source)) {
        $image->nid = $image->source;
        $image->source = file_create_url(db_result(db_query("SELECT f.filepath FROM {files} f JOIN {image} i ON i.nid = %d AND i.image_size = '%s' AND f.fid = i.fid", $image->source, variable_get('cookbook_image_node_recipe_size', '_original'))));
      }
    }
  }
  else {
    // Use default image
    $default_image = new stdClass();
    $default_image->id = 0;
    $default_image->source = base_path() . drupal_get_path('module', 'cookbook') . '/theme/images/default-recipe-big.png';
    $recipe->cookbook_recipe_images[] = $default_image;
  }

  // Load partial recipe
  if ($recipe->cookbook_recipe_partial_nid != NULL) {
    $recipe->cookbook_recipe_partial = db_fetch_object(db_query("SELECT n.vid, n.title, cr.steps FROM {node} n JOIN {cookbook_recipes} cr ON n.nid = %d AND cr.vid = n.vid", $recipe->cookbook_recipe_partial_nid));
  }

  // Ingredients
  if (variable_get('cookbook_use_ingredient_nodes', 0) == 1) {
    $recipe->cookbook_recipe_ingredients = array();
    $query = "SELECT cri.ingredient_nid, cri.unit_id, n.title, cri.amount, cri.display,
      ci.energy, ci.protein, ci.fat, ci.carbohydrate,
      ci.saturedtransfattyacids, ci.cismonounsaturated,
      ci.cispolyunsaturatedfattyacids, ci.cholesterol, ci.starch,
      ci.monodisaccharides, ci.dietaryfiber, ci.addedsugar, ci.alcohol,
      ci.calcium, ci.iron, ci.sodium, ci.potassium, ci.magnesium, ci.zinc,
      ci.selenium, ci.phosphorus, ci.vitamina, ci.vitamind, ci.vitamine,
      ci.thiamine, ci.riboflavin, ci.niacinequivalents, ci.vitaminb6,
      ci.folate, ci.vitaminc, ci.water, ci.transfattyacids, cu.name, cu.grams
      FROM {cookbook_recipe_ingredients} cri
      LEFT JOIN {node} n ON cri.ingredient_nid = n.nid
      LEFT JOIN {cookbook_ingredients} ci ON ci.vid = n.vid
      LEFT JOIN {cookbook_units} cu ON cri.unit_id = cu.id
      WHERE cri.recipe_vid = %d
      ORDER BY cri.weight";

    // Add partial ingredients before recipe
    if ($recipe->cookbook_recipe_partial != NULL && $recipe->cookbook_recipe_partial_pos == '0') {
      cookbook_recipe_ingredients($recipe, db_query($query, $recipe->cookbook_recipe_partial->vid), TRUE);
    }

    cookbook_recipe_ingredients($recipe, db_query($query, $recipe->vid));

    // Add partial ingredients after recipe.
    if ($recipe->cookbook_recipe_partial != NULL && $recipe->cookbook_recipe_partial_pos == '1') {
      cookbook_recipe_ingredients($recipe, db_query($query, $recipe->cookbook_recipe_partial->vid), TRUE);
    }

    foreach (cookbook_get_nutrients () as $nutrient => $attr) {
      if ($recipe->cookbook_recipe_nutrients[$nutrient] != -1) {
        if ($nutrient == 'energy') {
          $recipe->cookbook_recipe_nutrients[$nutrient] = round($recipe->cookbook_recipe_nutrients[$nutrient], 0);
        }
        else {
          $recipe->cookbook_recipe_nutrients[$nutrient] = round($recipe->cookbook_recipe_nutrients[$nutrient], 2);
        }
      }
    }
  }
  else {
    cookbook_load_partial_list($recipe, 'ingredients');
  }

  // Add partial recipe's steps to recipe steps
  cookbook_load_partial_list($recipe);
  $recipe->cookbook_recipe_steps = check_plain($recipe->cookbook_recipe_steps);
 
  // Get dictionary terms and synonyms and highlight them in the steps.
  $query = "SELECT ts.name as synonym, td.name, td.description FROM {term_synonym} ts RIGHT JOIN {term_data} td ON ts.tid = td.tid WHERE td.vid = %d";
  $query_args = array(variable_get('cookbook_dictionary', NULL));

  if (module_exists('i18ntaxonomy')) {
    $query .= " AND td.language = '%s'";
    $query_args[] = i18n_get_lang();
  }

  $path = base_path() . drupal_get_path('module', 'cookbook');

  // Find dictionary words and put in placeholders.
  $counter = 0;
  $placeholders = array();
  $checked = array();
  $result = db_query($query, $query_args);
  while ($term = db_fetch_array($result)) {
    if (!$checked[$term['name']]) {
      $recipe->cookbook_recipe_steps = preg_replace("'\b(" . str_replace(array('-', '+'), array('\-', '\+'), $term['name']) . ")\b'i", '<span class="term">' . "$1" . '</span>~' . $counter . '~', $recipe->cookbook_recipe_steps, -1, $matches);
      if ($matches != 0) {
        $placeholders['~' . $counter . '~'] = '<span class="term-description' . (strlen($term['description']) > 200 ? ' term-description-xl' : '') . '">' . $term['description'] . '</span><img src="' . $path . '/theme/images/term-tip-pointer.png" class="term-pointer" alt=""/>';
        $counter++;
      }
      $checked[$term['name']] = TRUE;
    }
    if ($term['synonym'] != NULL && !$checked[$term['synonym']]) {
      $recipe->cookbook_recipe_steps = preg_replace("'\b(" . str_replace(array('-', '+'), array('\-', '\+'), $term['synonym']) . ")\b'i", '<span class="term">' . "$1" . '</span>~' . $counter . '~', $recipe->cookbook_recipe_steps, -1, $matches);
      if ($matches != 0) {
        $placeholders['~' . $counter . '~'] = '<span class="term-description' . (strlen($term['description']) > 200 ? ' term-description-xl' : '') . '">' . $term['description'] . '</span><img src="' . $path . '/theme/images/term-tip-pointer.png" class="term-pointer" alt=""/>';
        $counter++;
      }
      $checked[$term['synonym']] = TRUE;
    }
  }

  // Replace placeholders
  foreach ($placeholders as $placeholder => $value) {
    $recipe->cookbook_recipe_steps = str_replace($placeholder, $value, $recipe->cookbook_recipe_steps);
  }

  // Taxonomy
  foreach ($recipe->taxonomy as $taxonomy) {
    if ($taxonomy->vid == variable_get('cookbook_time_consumption', NULL)) {
      $recipe->cookbook_recipe_time = $taxonomy->name;
      $recipe->cookbook_recipe_time_tid = $taxonomy->tid;
    }
    elseif ($taxonomy->vid == variable_get('cookbook_difficulty', NULL)) {
      $recipe->cookbook_recipe_diff = $taxonomy->name;
      $recipe->cookbook_recipe_diff_tid = $taxonomy->tid;
    }
  }

  // Translate taxonomy
  $vocabulary_id = variable_get('cookbook_categories', NULL);
  $translate = variable_get('i18ntaxonomy_vocabulary', array($vocabulary_id => 0));
  if ($translate[$vocabulary_id] == 1) {
    $recipe->cookbook_recipe_time = i18nstrings("taxonomy:term:$recipe->cookbook_recipe_time_tid:name", $recipe->cookbook_recipe_time);
    $recipe->cookbook_recipe_diff = i18nstrings("taxonomy:term:$recipe->cookbook_recipe_diff_tid:name", $recipe->cookbook_recipe_diff);
  }

  // Fetch custom block
  $block = variable_get('cookbook_block', NULL);
  if (isset($block)) {
    $block_ids = explode('-', $block);
    $block = module_invoke($block_ids[0], 'block', 'view', $block_ids[1]);
  }
  
  // Load additional data for related recipes
  if (isset($recipe->cookbook_recipe_relations) && count($recipe->cookbook_recipe_relations)) {
    $vids = '';
    foreach ($recipe->cookbook_recipe_relations as $related) {
      if ($vids !== '') {
        $vids .= ',';
      }
      $vids .= $related->vid;     
    }
    
    $recipe->cookbook_recipe_relations = cookbook_get_recipe_results(db_query(
        "SELECT nr.nid, nr.title, nr.vid, nr.body, time.tid as time_tid, time.name as time, diff.tid as diff_tid, diff.name as diff
          FROM {node_revisions} nr
          JOIN (
            SELECT tn.vid, tn.tid, td.name FROM {term_node} tn
            JOIN {term_data} td ON td.vid = %d AND td.tid = tn.tid
          ) time ON time.vid = nr.vid
          JOIN (
            SELECT tn.vid, tn.tid, td.name FROM {term_node} tn
            JOIN {term_data} td ON td.vid = %d AND td.tid = tn.tid
          ) diff ON diff.vid = nr.vid
          WHERE nr.vid IN (%s)",
        variable_get('cookbook_time_consumption', NULL),
        variable_get('cookbook_difficulty', NULL),
        $vids));
  }
  
  return theme('cookbook_recipe', $recipe, $category, $search, $order, $page, $block);
}

function cookbook_recipe_ingredients(&$recipe, $result, $partial = FALSE) {
  $first = TRUE;
  while ($ingredient = db_fetch_object($result)) {
    if ($first && $partial && $ingredient->amount >= 0) {
      $head = new stdClass();
      $head->display = $recipe->cookbook_recipe_partial->title;
      $head->amount = -1;
      $recipe->cookbook_recipe_ingredients[] = $head;
    }

    if ($ingredient->amount >= 0 && $ingredient->ingredient_nid != '0') {
      if ($ingredient->name == NULL) {
        $ingredient->name = 'g';
        $multiplier = $ingredient->amount * 0.01;
      }
      else {
        $multiplier = $ingredient->amount * $ingredient->grams * 0.01;
      }

      foreach (cookbook_get_nutrients () as $nutrient => $attr) {
        if ($ingredient->$nutrient == -1) {
          $recipe->cookbook_recipe_nutrients[$nutrient] = -1;
        }
        elseif ($recipe->cookbook_recipe_nutrients[$nutrient] != -1) {
          $recipe->cookbook_recipe_nutrients[$nutrient] += $ingredient->$nutrient * $multiplier;
        }
      }
    }

    $recipe->cookbook_recipe_ingredients[] = $ingredient;
    $first = FALSE;
  }
  if ($partial) {
    $end = new stdClass();
    $end->display = '@';
    $end->ingredient_nid = 0;
    $end->amount = 0;
    $recipe->cookbook_recipe_ingredients[] = $end;
  }
}

/**
 * Function to get a recipe's history.
 *
 * @param int $category
 *  Selected category id.
 * @param string $search
 *  Keywords to filter title on.
 * @param string $order Field
 *  and direction to order recipes by.
 * @param int $page
 *  Which page of results to display.
 * @param int $nid
 *  The node identifier of the recipe to display.
 *
 * @return string
 *  Themed output.
 */
function cookbook_history($category, $search, $order, $page, $nid) {
  cookbook_add_js();

  $recipe = db_fetch_object(db_query(
        "SELECT n.nid, n.vid, n.title, cr.history, cr.video
      FROM {node} n
      JOIN {cookbook_recipes} cr ON n.nid = %d AND cr.vid = n.vid",
        $nid)
  );
  return theme('cookbook_history', $recipe, $category, $search, $order, $page);
}

/**
 * Function to get a recipe's video.
 *
 * @param int $category
 *  Selected category id.
 * @param string $search
 *  Keywords to filter title on.
 * @param string $order Field
 *  and direction to order recipes by.
 * @param int $page
 *  Which page of results to display.
 * @param int $nid
 *  The node identifier of the recipe to display.
 *
 * @return string
 *  Themed output.
 */
function cookbook_video($category, $search, $order, $page, $nid) {
  cookbook_add_js();

  $recipe = db_fetch_object(db_query(
        "SELECT n.nid, n.vid, n.title, cr.history, cr.video
      FROM {node} n
      JOIN {cookbook_recipes} cr ON n.nid = %d AND cr.vid = n.vid",
        $nid)
  );
  return theme('cookbook_video', $recipe, $category, $search, $order, $page);
}