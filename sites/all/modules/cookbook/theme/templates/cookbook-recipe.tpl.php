<?php
?>
<div class="block">
  <div class="content">
    <h3 id="recipe-header"><?php print check_plain($recipe->title) ?></h3>
  </div>
</div>
<div class="block">
  <h2><?php print t('Recipe') ?></h2>
  <?php if ($recipe->cookbook_recipe_history != ''): ?>
    <h2 class="link short-margin">
      <a href="<?php print url('cookbook/history/' . $category . '/' . $search . '/' . $order . '/' . $page . '/' . $recipe->nid) ?>" class="fading"><?php print t('History') ?></a>
    </h2>
  <?php endif;
  if ($recipe->cookbook_recipe_video != ''): ?>
    <h2 class="link short-margin">
      <a href="<?php print url('cookbook/video/' . $category . '/' . $search . '/' . $order . '/' . $page . '/' . $recipe->nid) ?>" class="fading"><?php print t('Video') ?></a>
    </h2>
  <?php endif ?>
  <div class="content">
    <div id="mainbar">
      <div id="recipe-image">
        <span id="recipe-image-overlay">
          <span class="previous">&lt;</span>
          <?php print t('Picture') ?>
          <span class="num">1</span>
          <?php print t('of') . ' ' . count($recipe->cookbook_recipe_images) ?>
          <span class="next">&gt;</span>
        </span>
        <span id="recipe-image-overlay-2"></span>
        <div>
          <?php foreach ($recipe->cookbook_recipe_images as $image): ?>
            <img src="<?php print check_url($image->source) ?>" alt="" width="654"/>
          <?php endforeach ?>
        </div>
      </div>
      <div id="teaser"><?php print check_plain($recipe->body) ?></div>
      <!-- RSPEAK_START -->
      <div id="ingredients">
        <h3><?php print t('Ingredients') ?></h3>
        <?php if (variable_get('cookbook_use_ingredient_nodes', 0) == 1):
            if (is_array($recipe->cookbook_recipe_ingredients)):
            $new_list = TRUE;
            foreach ($recipe->cookbook_recipe_ingredients as $ingredient):
              if ($ingredient->amount * $recipe->cookbook_recipe_servings < 0):
                if (!$new_list): ?>
                  </ul>
                <?php endif;
                $new_list = TRUE ?>
                <h4><?php print check_plain($ingredient->display) ?></h4>
              <?php else:
                if ($ingredient->display == '@'):
                  if (!$new_list): ?>
                    </ul><ul>
                  <?php endif;
                  continue;
                endif;
                if ($new_list):
                  $new_list = FALSE ?>
                  <ul>
                <?php endif ?>
                <li>
                  <?php if ($ingredient->ingredient_nid != '0'): ?>
                    <span class="ingredient-amount"><?php print round($ingredient->amount * $recipe->cookbook_recipe_servings, 3) ?></span>
                    <span class="hidden"><?php print $ingredient->amount ?></span>
                    <?php print check_plain($ingredient->name); ?>
                  <?php elseif ($ingredient->amount != '0'): ?>
                    <span class="ingredient-amount"><?php print round($ingredient->amount * $recipe->cookbook_recipe_servings, 3) ?></span>
                    <span class="hidden"><?php print $ingredient->amount ?></span>
                  <?php endif;
                  print check_plain(strtolower($ingredient->display)) ?>
                </li>
              <?php endif;
            endforeach;
            if (!$new_list): ?>
              </ul>
            <?php endif; endif;
          else:
            $new_list = TRUE;
            foreach (explode('@', $recipe->cookbook_recipe_ingredients) as $ingredient):
              if ($ingredient != ''):
                $ingredient = explode(';', $ingredient);
                if (count($ingredient) == 2):
                  if ($ingredient[0] == '1'):
                    if (!$new_list): ?>
                      </ul>
                    <?php endif;
                    $new_list = TRUE; ?>
                    <h4><?php print check_plain($ingredient['1']) ?></h4>
                  <?php elseif ($ingredient[0] == '2'): ?>
                    </ul><ul>
                  <?php else:
                    if ($new_list):
                      $new_list = FALSE ?>
                      <ul>
                    <?php endif; ?>
                    <li>
                      <?php $parts = explode(' ', $ingredient['1']);
                      if (is_numeric($parts[0])): ?>
                        <span class="ingredient-amount"><?php print $parts[0] ?></span><span class="hidden"><?php print $parts[0] / $recipe->cookbook_recipe_servings ?></span>
                        <?php array_shift($parts); print check_plain(implode(' ', $parts));
                      else:
                        print check_plain($ingredient['1']);
                      endif ?>
                    </li>
                  <?php endif;
                else:
                  if ($new_list):
                    $new_list = FALSE ?>
                    <ul>
                  <?php endif ?>
                  <li><?php print check_plain($ingredient['0']) ?></li>
                <?php endif;
              endif;
            endforeach;
            if (!$new_list): ?>
              </ul>
            <?php endif ?>
          <?php endif ?>
      </div>
      <div id="steps">
        <h3><?php print t('Steps') ?></h3>
        <?php $new_list = TRUE;
        foreach (explode('@', $recipe->cookbook_recipe_steps) as $step):
          if ($step != ''):
            $step_split = explode(';', $step, 2);
            if (count($step_split) == 2 && is_numeric($step_split[0])):
              if ($step_split[0] == '1'):
                if (!$new_list): ?>
                  </ol>
                <?php endif;
                $new_list = TRUE ?>
                <h4><?php print $step_split[1] ?></h4>
              <?php elseif ($step_split[0] == 2): ?>
                </ol><ol>
              <?php else:
                if ($new_list):
                  $new_list = FALSE ?>
                  <ol>
                <?php endif ?>
                <li><span><?php print $step_split[1] ?></span></li>
              <?php endif;
            else:
              if ($new_list):
                $new_list = FALSE ?>
                <ol>
              <?php endif ?>
              <li><span><?php print $step ?></span></li>
            <?php endif;
          endif;
        endforeach;
        if (!$new_list): ?>
          </ol>
        <?php endif; ?>
      </div>
      <!-- RSPEAK_STOP -->
    </div>
    <div id="sidebar">
      <div id="time-diff" class="sidebar">
        <div>
          <?php print taxonomy_image_display($recipe->cookbook_recipe_time_tid) ?><br/>
          <?php print check_plain($recipe->cookbook_recipe_time); ?>
        </div>
        <div>
          <?php print taxonomy_image_display($recipe->cookbook_recipe_diff_tid) ?><br/>
          <?php print check_plain($recipe->cookbook_recipe_diff); ?>
        </div>
      </div>
      <div id="servings" class="sidebar">
        <h3><?php print t('Number of servings') ?></h3>
        <input type="text" name="servings" value="<?php print $recipe->cookbook_recipe_servings ?>" size="1" class="textfield" id="number-of-servings"/>
      </div>
      <?php if (variable_get('cookbook_use_ingredient_nodes', 0) == 1): ?>
        <div id="nutrition-facts" class="sidebar">
          <h3><?php print t('Nutrition facts') ?></h3>
          <div>
            <table>
              <?php $counter = 0; foreach (cookbook_get_nutrients (FALSE) as $name => $nutrient):
                if ($recipe->cookbook_recipe_nutrients[$name] != -1): ?>
                  <tr title="<?php print $recipe->$name . ' ' . $nutrient[1] ?>"<?php if ($counter > 3): print ' class="additional"'; endif ?>>
                    <td class="option">
                      <span><?php print $nutrient[0] ?></span>
                    </td>
                    <td class="value"><?php print $recipe->cookbook_recipe_nutrients[$name] . ' ' . $nutrient[2] ?></td>
                  </tr>
                <?php endif; $counter++;
              endforeach ?>
            </table>
          </div>
          <p class="show-more"><?php print t('Show more') ?></p>
          <p><?php print t('Estimated values per serving.') ?></p>
          <?php $ref = variable_get('cookbook_nutrition_data_reference', '');
          if ($ref != ''): ?>
            <p><?php print $ref ?></p>
          <?php endif ?>
        </div>
      <?php endif;
      if (isset($block)): ?>
        <div id="custom-block" class="sidebar">
          <?php if ($block['subject'] != ''): ?>
            <h3><?php print $block['subject'] ?></h3>
          <?php endif ?>
          <?php if ($block['content'] != ''): ?>
            <div><?php print $block['content'] ?></div>
          <?php endif ?>
        </div>
      <?php endif ?>
    </div>
    <div id="print"></div>
    <?php if (isset($recipe->cookbook_recipe_relations) && count($recipe->cookbook_recipe_relations)): ?>
      <div id="results" class="related">
        <h3><?php print t('Related recipes') ?></h3>
        <?php foreach ($recipe->cookbook_recipe_relations as $related): ?>
          <div class="result-item shadow-bottom-right">
            <div class="result-image">
              <img src="<?php print check_url($related->image) ?>" width="116" height="87" alt=""/>
            </div>
            <div class="result-difficulty">
              <?php print taxonomy_image_display($related->diff_tid) ?><br/>
              <?php print check_plain($related->diff) ?>
            </div>
            <div class="result-time-consumption">
              <?php print taxonomy_image_display($related->time_tid) ?><br/>
              <?php print check_plain($related->time) ?>
            </div>
            <div class="result-text">
              <h3>
                <a href="<?php print url('cookbook/recipe/' . $category . '/' . $search . '/' . $order . '/' . $page . '/' . $related->nid) ?>" class="reverse-fading"><?php print $related->title ?></a>
              </h3>
              <p><?php print check_plain($related->body) ?></p>
            </div>
          </div>
        <?php endforeach ?>
      </div>
    <?php endif; ?>
  </div>
  <div class="tab-bottom">
    <a href="<?php print url('cookbook') ?>" class="fading"><?php print t('Front page') ?></a>
  </div>
  <div class="tab-bottom">
    <a href="<?php print url('cookbook/results/' . $category . '/' . $search . '/' . $order . '/' . $page) ?>" class="fading"><?php print t('Back to recipes') ?></a>
  </div>
</div>