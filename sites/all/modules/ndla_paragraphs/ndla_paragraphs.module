<?php

/**
 * @file ndla_paragraphs.module
 * @ingroup ndla_paragraphs
 * @brief module for multiple paragraphs in body
 */
function ndla_paragraphs_init() {
  $args = arg();
  if ((isset($args[1]) && $args[1] == 'add') || (isset($args[2]) && $args[2] == 'edit')) {
    drupal_add_css(drupal_get_path('module', 'ndla_paragraphs') . '/css/ndla_paragraphs_edit.css');
  }
}

/**
 * Implementation of hook_menu()
 */
function ndla_paragraphs_menu() {
  $items = array();
  //Menu callback for #ahah add-more function
  $items['ndla_paragraphs/add'] = array(
    'title' => 'Add NDLA Paragraph',
    'page callback' => 'ndla_paragraphs_add',
    'access arguments' => array('access content'),
  );
  $items['ndla_paragraphs/preview/build'] = array(
    'title' => 'Preview of NDLA Paragraphs',
    'page callback' => 'ndla_paragraphs_preview_build',
    'access arguments' => array('access content'),
  );
  $items['ndla_paragraphs/preview/get/%'] = array(
    'title' => 'Preview of NDLA Paragraphs',
    'page arguments' => array(3),
    'page callback' => 'ndla_paragraphs_preview_get',
    'access arguments' => array('access content'),
  );
  $items['ndla_paragraphs/install'] = array(
    'title' => 'NDLA Paragraph Install',
    'page callback' => 'ndla_paragraph_install',
    'page argument' => array(2),
    'access arguments' => array('administer site configuration'),
  );
  $items['admin/settings/ndla_paragraphs'] = array(
    'title' => 'NDLA Paragraphs',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ndla_paragraphs_admin_form'),
    'access arguments' => array('administer site configuration'),
  );
  return $items;
}

/**
 * Implemenation of hook_form_alter()
 */
function ndla_paragraphs_form_alter(&$form, $form_state, $form_id) {
  if (isset($form['type']) && $form_id == $form['#node']->type . "_node_form") {
    $omit = variable_get('ndla_paragraphs_omit', array());
    $omit = array_filter($omit);
    if (!in_array($form['#node']->type, $omit)) {
      drupal_add_js(drupal_get_path('module', 'ndla_paragraphs') . '/js/ndla_paragraphs.js');
      $form['ndla_paragraphs'] = ndla_paragraphs_node_form($form_state, $form['#node']);

      // Dont force right input format if node belong to a chinese course
      $current_course = ndla_utils_disp_get_context();
      $courses = array(127756,138654);
      if(!in_array($current_course['course'],$courses)) {
        $form['#after_build'][] = 'ndla_paragraphs_set_default_input_format_for_right';
      }
      if (isset($form['body_field']))
        unset($form['body_field']);
      if (isset($form['body_filter']))
        unset($form['body_filter']);
    }
  }
}

/**
 * The form which will be inserted into the node-form.
 */
function ndla_paragraphs_node_form(&$form_state, $node) {
  $form = array(
    '#type' => 'fieldset',
    '#title' => t('Body'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'paragraphs' => array(
      '#prefix' => '<div id="ndla_paragraphs_wrapper">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
    ),
  );

  /* Re-arrange */
  if (!empty($form_state['post']['paragraphs'])) {
    foreach ($form_state['post']['paragraphs'] as $paragraph) {
      if(empty($paragraph['deleted'])) {
        list($weight, $element) = _ndla_paragraphs_create_form_item($paragraph);
        $form['paragraphs'][$weight] = $element;
      }
    }
  }

  /* Load from db */
  else if (!empty($node->paragraphs)) {
    foreach ($node->paragraphs as $paragraph) {
      list($weight, $element) = _ndla_paragraphs_create_form_item($paragraph);
      $form['paragraphs'][$weight] = $element;
    }
  }
  
  else {
    $new = array(
      'weight' => sizeof(element_children($form['paragraphs'])),
    );
    list($weight, $element) = _ndla_paragraphs_create_form_item($new);
    $form['paragraphs'][$weight] = $element; 
  }
  
  /* Add new */
  if($form_state['post']['op'] == t('Add paragraph') && empty($form_state['post']['ndla_paragraph_ahah_action'])) {
    $new = array(
      'weight' => sizeof(element_children($form['paragraphs'])),
    );
    list($weight, $element) = _ndla_paragraphs_create_form_item($new);
    $form['paragraphs'][$weight] = $element;
  }

  ksort($form['paragraphs']);

  $omitted_cts = variable_get('ndla_paragraphs_omit_right', array());
  if (isset($omitted_cts[$node->type]) && $omitted_cts[$node->type]) {
    $paras = $form['paragraphs'];
    foreach ($paras as $index => $data) {
      if (isset($data['right']) && is_numeric($index)) {
        unset($form['paragraphs'][$index]['right']);
      }
    }
  }
  //Nasty.
  $form['paragraphs'][0]['#suffix'] = "<script>$('#ndla-paragraph-ahah-action').val('');</script>";
  
  $num_paragraphs = count(element_children($form['paragraphs']));  
  //Remove up-button on the first paragraph
  unset($form['paragraphs'][0]['buttons']['up']);
  //Remove down-button on the last paragraph
  unset($form['paragraphs'][($num_paragraphs-1)]['buttons']['down']);
  //Remove all buttons if there are only 1 paragraph.
  if($num_paragraphs == 1) {
    unset($form['paragraphs'][0]['buttons']);
  }
  
  
  $form['create'] = array(
    '#prefix' => '<table class="ndla-paragraphs-table-wrapper"><tr><td class="ndla-paragraphs-add-wrapper">',
    '#suffix' => '</td>',
    '#type' => 'button',
    '#value' => t('Add paragraph'),
    '#id' => 'add-paragraph-button',
    '#attributes' => array('class' => 'wysiwyg-detach'),
    '#ahah' => array(
      'path' => 'ndla_paragraphs/add',
      'wrapper' => 'ndla_paragraphs_wrapper',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['preview'] = array(
    '#prefix' => '<td class="ndla-paragraphs-preview-wrapper">',
    '#suffix' => '</td></tr></table>',
    '#type' => 'item',
    '#value' => '<input type="button" onclick="ndla_paragraphs_preview();" value ="' . t('Preview') . '"><a id="hidden-lightbox" rel="lightframe[|width:635px; height:500px; scrolling: auto;]" href="' . url('ndla_paragraphs/preview/get/x') . '"></a>'
  );

  $form['ndla_paragraph_ahah_action'] = array(
    '#type' => 'hidden',
    '#value' => NULL,
    '#id' => 'ndla-paragraph-ahah-action',
  );
  
  $form['ndla_paragraph_ahah_element'] = array(
    '#type' => 'hidden',
    '#value' => NULL,
    '#id' => 'ndla-paragraph-ahah-element',
  );

  return $form;
}

/**
 * Function for creating a paragraphs element in the node form.
 */
function _ndla_paragraphs_create_form_item($paragraph = NULL, $number = 1) {
  $left = !empty($paragraph['left']['field']) ? $paragraph['left']['field'] : '';
  $right = !empty($paragraph['right']['field']) ? $paragraph['right']['field'] : '';
  $left_format = !empty($paragraph['left']['format']) ? $paragraph['left']['format'] : '2';
  $right_format = !empty($paragraph['right']['format']) ? $paragraph['right']['format'] : variable_get('ndla_paragraphs_right_format', 2);
  $item = array(
    '#title' => t('Paragraph'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#attributes' => array('class' => 'paragraph'),
    'weight' => array(
      '#type' => 'hidden',
      '#default_value' => $paragraph['weight'],
      '#attributes' => array('class' => 'weight-value'),
    ),
    'left' => array(
      'field' => array(
        '#type' => 'textarea',
        '#title' => t('Body'),
        '#default_value' => $left,
      ),
    ),
    'right' => array(
      '#title' => t('Right'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed'  => FALSE,
      '#attributes' => array('class' => (empty($right)) ? 'collapse_when_ready' : ''),
      'field' => array(
        '#type' => 'textarea',
        '#title' => t('Right column'),
        '#default_value' => $right,
      ),
    ),
  );

  $item['left']['format'] = filter_form($left_format, NULL, array('format_left_' . $paragraph['weight']));
  $item['right']['format'] = filter_form($right_format, NULL, array('format_right_' . $paragraph['weight']));
  
  $item['buttons'] = array(
    'up' => array(
      '#type' => 'button',
      '#value' => t('Move up'),
      '#attributes' => array('class' => 'paragraph-move-up'),      
    ),
    'down' => array(
      '#type' => 'button',
      '#value' => t('Move down'),
      '#attributes' => array('class' => 'paragraph-move-down'),
    ),
    'delete' => array(
      '#type' => 'button',
      '#value' => t('Delete'),
      '#attributes' => array('class' => 'paragraph-delete'),
    ),
  );
  return array($paragraph['weight'], $item);
}

function ndla_paragraphs_move($direction = 'down', $current_weight = 0) {
  $current_weight = intval($current_weight);
  // The form is generated in an include file which we need to include manually.
  include_once 'modules/node/node.pages.inc';
  // We're starting in step #3, preparing for #4.
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  // Step #4.
  $form = form_get_cache($form_build_id, $form_state);
  // Preparing for #5.
  $args = $form['#parameters'];
  $form_id = array_shift($args);

  $form_state['post'] = $form['#post'] = $_POST;

  //Recalculate the weight
  if(isset($form_state['post']['paragraphs'][$current_weight])) {
    if($direction == 'down') {
      $form_state['post']['paragraphs'] = _ndla_paragraphs_move_down($form_state['post']['paragraphs'], $current_weight);
    }
    else {
      $form_state['post']['paragraphs'] = _ndla_paragraphs_move_up($form_state['post']['paragraphs'], $current_weight);
    }
    foreach($form_state['post']['paragraphs'] as $index => $dummy) {
      $form_state['post']['paragraphs'][$index]['weight'] = $index;
    }
  }

  $form['#programmed'] = $form['#redirect'] = FALSE;
  // Step #5.
  drupal_process_form($form_id, $form, $form_state);
  // Step #6 and #7 and #8.
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  //NDLA specifics...
  $dummy = theme('status_messages');
  // Step #9.  
  $paragraphs_form = $form['ndla_paragraphs']['paragraphs'];
  unset($paragraphs_form['#prefix'], $paragraphs_form['#suffix']);
  $output = drupal_render($paragraphs_form);
  // Final rendering callback.
  print drupal_to_js(array('data' => $output, 'status' => TRUE));
  exit();
}

function _ndla_paragraphs_move_down($paragraphs, $index) {
  if(isset($paragraphs[$index])) {
    if(isset($paragraphs[$index+1])) {
      $occupied = $paragraphs[$index+1];
      $occupied['weight'] = $index;
      $moved = $paragraphs[$index];
      $moved['weight'] = $index+1;
      $paragraphs[$index] = $occupied;
      $paragraphs[$index+1] = $moved;
      return $paragraphs;
    }
    else {
      return $paragraphs;
    }
  }
  else {
    return $paragraphs;
  }
}

function _ndla_paragraphs_move_up($paragraphs, $index) {
  if(isset($paragraphs[$index])) {
    if(isset($paragraphs[$index-1])) {
      $occupied = $paragraphs[$index-1];
      $occupied['weight'] = $index;
      $moved = $paragraphs[$index];
      $moved['weight'] = $index-1;
      $paragraphs[$index] = $occupied;
      $paragraphs[$index-1] = $moved;
      return $paragraphs;
    }
    else {
      return $paragraphs;
    }
  }
  else {
    return $paragraphs;
  }
}

/**
 * Called via #ahah. Rebuilds the body part of the node form.
 */
function ndla_paragraphs_add() {
  $move = array('down', 'up');
  if(!empty($_POST['ndla_paragraph_ahah_action']) && in_array($_POST['ndla_paragraph_ahah_action'], $move)) {
    ndla_paragraphs_move($_POST['ndla_paragraph_ahah_action'], $_POST['ndla_paragraph_ahah_element']);
  }

  // The form is generated in an include file which we need to include manually.
  include_once 'modules/node/node.pages.inc';
  // We're starting in step #3, preparing for #4.
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  // Step #4.
  $form = form_get_cache($form_build_id, $form_state);
  // Preparing for #5.
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  
  if(!empty($_POST['ndla_paragraph_ahah_action']) && $_POST['ndla_paragraph_ahah_action'] == 'delete') {
    $element = $_POST['ndla_paragraph_ahah_element'];
    $form_state['post']['paragraphs'][$element]['deleted'] = 1;
  }
  
  $form['#programmed'] = $form['#redirect'] = FALSE;
  // Step #5.
  drupal_process_form($form_id, $form, $form_state);
  // Step #6 and #7 and #8.
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  //NDLA specifics...
  $dummy = theme('status_messages');
  // Step #9.  
  $paragraphs_form = $form['ndla_paragraphs']['paragraphs'];
  unset($paragraphs_form['#prefix'], $paragraphs_form['#suffix']);
  $output = drupal_render($paragraphs_form);
  // Final rendering callback.
  print drupal_to_js(array('data' => $output, 'status' => TRUE));
  exit();
}

/**
 * Implementation of hook_content_extra_fields().
 * Allows the admin to change the order of fields in the node edit form.
 *
 * @param $type_name
 *  The content type
 *
 * @return
 *  Array of fields
 */
function ndla_paragraphs_content_extra_fields($type_name) {
  $weight = 5;
  $fields = array();
  $fields['ndla_paragraphs'] = array(
    'label' => t('NDLA Paragraphs'),
    'description' => t('Multiple body paragraphs'),
    'weight' => $weight,
  );
  return $fields;
}

/**
 * Implementation of hook_theme()
 */
function ndla_paragraphs_theme() {
  $theme = array(
    'ndla_paragraphs_paragraph' => array(
      'arguments' => array('body' => NULL, 'column' => NULL),
      'template' => 'templates/ndla-paragraphs-paragraph',
    ),
  );
  return $theme;
}

/**
 * Implementation of hook_nodeapi()
 */
function ndla_paragraphs_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  $omit = variable_get('ndla_paragraphs_omit', array());
  $omit = array_filter($omit);
  if (!in_array($node->type, $omit)) {
    switch ($op) {
      /**
       * Special fix for multichoice module. It validates that
       * body is set, but now body shall be paragraphs. So this
       * removes incorrect validation and revalidates.
       */
      case 'validate':
        $valid = FALSE;
        foreach ($node->paragraphs as $paragraph) {
          if (!empty($paragraph['left']['field']) || !empty($paragraph['right']['field'])) {
            $valid = TRUE;
            break;
          }
        }
        if ($valid) {
          $errors = form_get_errors();
          form_set_error(NULL, '', TRUE);
          if (is_array($errors)) {
            foreach ($errors as $field => $message) {
              if ($field != 'body' && $message != t('Question text is empty.')) {
                form_set_error($field, $message, $first);
              }
            }
          }
          $messages = drupal_get_messages('error', TRUE);
          if (is_array($messages['error'])) {
            $index = array_search(t('Question text is empty.'), $messages['error']);
            if ($index !== FALSE) {
              unset($messages['error'][$index]);
            }

            foreach ($messages['error'] as $error) {
              drupal_set_message($error, 'error');
            }
          }
        }
        break;
      case 'presave':
        _ndla_paragraphs_view($node);
        $node->teaser = node_teaser($node->body);
        $node->format = 2;
        break;
      case 'insert':
      case 'update':
        _ndla_paragraphs_delete_revision($node);
        _ndla_paragraphs_save_revision($node);
        _ndla_paragraphs_view($node);
        break;
      case 'delete revision':
        _ndla_paragraphs_delete_revision($node);
        break;
      case 'delete':
        _ndla_paragraphs_delete($node);
        break;
      case 'load':
        _ndla_paragraphs_load($node);
        _ndla_paragraphs_view($node);
        break;
      case 'view':
        drupal_add_css(drupal_get_path('module', 'ndla_paragraphs') . '/css/ndla_paragraphs.css');

        break;
    }
  }
}

function _ndla_paragraphs_view(&$node) {
  if (isset($node->paragraphs)) {
    $html = "";
    foreach ($node->paragraphs as $paragraph) {
      if(empty($paragraph['deleted'])) {
        $left = $paragraph['left']['field'];
        $right = '';
        if (isset($paragraph['right'])) {
          $right = $paragraph['right']['field'];
        }
        $html .= theme('ndla_paragraphs_paragraph', $left, $right);
      }
    }
  }
  else {
    $node->paragraphs[] = array('left' => array('field' => $node->body));
    $html = theme('ndla_paragraphs_paragraph', $node->body, '');
  }
  $node->body = $html;
}

function _ndla_paragraphs_load(&$node) {
  $node->paragraphs = array();
  $query = "SELECT body_data, right_data, body_format, right_format FROM {ndla_paragraphs} WHERE nid = %d AND vid = %d ORDER BY weight, pid ASC";
  $result = db_query($query, $node->nid, $node->vid);
  $key = 0;
  while ($row = db_fetch_object($result)) {
    $node->paragraphs[] = array(
      'left' => array('field' => $row->body_data, 'format' => $row->body_format),
      'right' => array('field' => $row->right_data, 'format' => $row->right_format),
      'weight' => $key,
    );
    $key++;
    $node->{'format_left_' . $key} = $row->body_format;
    $node->{'format_right_' . $key} = $row->right_format;
  }
}

function _ndla_paragraphs_save_revision($node) {
  foreach ($node->paragraphs as $key => $paragraph) {
    $left = $paragraph['left']['field'];
    $right = $paragraph['right']['field'];

    $right_format = $node->{'format_right_' . $key};
    $left_format = $node->{'format_left_' . $key};

    $weight = $paragraph['weight'];
    if (!_ndla_paragraphs_empty($left) || !_ndla_paragraphs_empty($right)) {
      db_query('INSERT INTO {ndla_paragraphs} (nid, vid, body_data, right_data, body_format, right_format, weight) VALUES (%d, %d, "%s", "%s", %d, %d, %d)', $node->nid, $node->vid, $left, $right, $left_format, $right_format, $weight);
    }
  }
}

function _ndla_paragraphs_empty($text) {
  $text = str_replace('&nbsp;', '', $text);
  $text = str_replace(' ', '', $text);
  $text = preg_replace('/^<p>/', '', $text);
  $text = preg_replace('/<\/p>$/', '', $text);
  return empty($text);
}

function _ndla_paragraphs_delete($node) {
  db_query('DELETE FROM {ndla_paragraphs} WHERE nid = %d', $node->nid);
}

function _ndla_paragraphs_delete_revision($node) {
  db_query('DELETE FROM {ndla_paragraphs} WHERE nid = %d AND vid = %d', $node->nid, $node->vid);
}

function ndla_paragraphs_preview_get($cid) {
  $data = cache_get($cid);
  cache_clear_all($cid, 'cache');
  echo $data->data;
}

function ndla_paragraphs_preview_build() {
  $cid = 'ndla_paragraphs:' . uniqid();
  global $language, $base_path;
  $data = array();
  foreach ($_REQUEST as $key => $value) {
    if (preg_match('/edit-paragraphs-[\d]*-(left|right)-field/', $key)) {
      preg_match('/edit-paragraphs-([\d]*)-(left|right)-field/', $key, $matches);
      $data[$matches[1]][$matches[2]] = urldecode($value);
    }
  }
  $body = "";
  foreach ($data as $paragraph) {
    if (!isset($paragraph['right'])) {
      $paragraph['right'] = '';
    }
    $body .= theme('ndla_paragraphs_paragraph', $paragraph['left'], $paragraph['right']);
  }
  $node = new stdClass();
  $node->status = 1;
  $node->body = $body;

  $node->field_ingress[0]['safe'] = urldecode($_REQUEST['ingress_text']);
  $node->field_ingressvispaasiden[0]['value'] = $_REQUEST['ingress_use'];
  if(preg_match('/\[nid:([\d]*)\]$/', $_REQUEST['ingress_image'], $matches)) {
    $node->field_ingress_bilde[0]['nid'] = $matches[1];
  }

  $content = node_view($node, FALSE, TRUE, FALSE);

  $scripts = drupal_get_js();
  $styles = drupal_get_css();

  $content = "<html><head> $scripts $styles </head><body> $content </body></html>";
  cache_set($cid, $content);
  echo $cid;
}

function ndla_paragraphs_admin_form() {
  $types = node_get_types();
  $formats = filter_formats();
  $format_options = array();
  foreach($formats as $format) {
    $format_options[$format->format] = $format->name;
  }
  
  $type_options = array();
  foreach ($types as $data_name => $type) {
    $type_options[$data_name] = $type->name;
  }

  $form['ndla_paragraphs_omit_right'] = array(
    '#title' => t('Omit the right paragraph on the following content types'),
    '#type' => 'checkboxes',
    '#options' => $type_options,
    '#default_value' => variable_get('ndla_paragraphs_omit_right', array()),
  );

  $form['ndla_paragraphs_omit'] = array(
    '#title' => t('Omit paragraphs on the following content types'),
    '#type' => 'checkboxes',
    '#options' => $type_options,
    '#default_value' => variable_get('ndla_paragraphs_omit', array()),
    '#description' => t('WARNING: Add or remove one content type at the time since this can take some time. Make sure you have a database backup in case the scripts time out.')
  );
  
  $form['ndla_paragraphs']['right_format'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings for the right paragraph'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    'ndla_paragraphs_right_format' => array(
      '#title' => t('TinyMCE preset / Input format'),
      '#tree' => FALSE,
      '#type' => 'select',
      '#options' => $format_options,
      '#default_value' => variable_get('ndla_paragraphs_right_format', 2),
      '#description' => t('The format is used for selecting TinyMCE preset. Select the format which corresponds to the preset you want to use.'),
    ),
    'ndla_paragraphs_right_true_format' => array(
      '#title' => t('After save, which format will the content be rendered as'),
      '#tree' => FALSE,
      '#type' => 'select',
      '#options' => $format_options,
      '#default_value' => variable_get('ndla_paragraphs_right_true_format', 2),
      '#description' => t('The format is used for selecting TinyMCE preset. Select the format which corresponds to the preset you want to use.'),
    ),
  );
  
  $form['#validate'][] = 'ndla_paragraphs_admin_form_submit';
  return system_settings_form($form);
}

function ndla_paragraphs_admin_form_submit($form, $form_state) {
  ini_set('memory_limit', '1024M');
  ini_set('max_execution_time', 600);

  $omit_old = variable_get('ndla_paragraphs_omit', array());
  $omit_old = array_filter($omit_old);

  $omit_new = $form_state['values']['ndla_paragraphs_omit'];
  $omit_new = array_filter($omit_new);

  $add = array_diff($omit_new, $omit_old);
  $remove = array_diff($omit_old, $omit_new);

  foreach ($add as $type) {
    $nquery = "SELECT nid FROM {node} WHERE type = '%s' ORDER BY nid";
    $nresult = db_query($nquery, $type);
    while ($nrow = db_fetch_object($nresult)) {
      $nid = $nrow->nid;
      $rquery = "SELECT vid FROM {node_revisions} WHERE nid = '%d'";
      $rresult = db_query($rquery, $nid);
      while ($rrow = db_fetch_object($rresult)) {
        $vid = $rrow->vid;
        $pquery = "SELECT body_data, right_data FROM {ndla_paragraphs} WHERE vid = '%d' ORDER BY weight, pid ASC";
        $presult = db_query($pquery, $vid);
        $body = "";
        while ($prow = db_fetch_object($presult)) {
          $body .= $prow->body_data . $prow->right_data;
        }
        $teaser = node_teaser($body);
        db_query("UPDATE {node_revisions} SET body = '%s', teaser = '%s' WHERE vid = '%d'", $body, $teaser, $vid);
      }
      db_query("DELETE FROM {ndla_paragraphs} WHERE nid = '%d'", $nid);
    }
  }

  foreach ($remove as $type) {
    $nquery = "SELECT nid FROM {node} WHERE type = '%s' ORDER BY nid";
    $nresult = db_query($nquery, $type);
    while ($nrow = db_fetch_object($nresult)) {
      $nid = $nrow->nid;
      $rquery = "SELECT vid, body, teaser, format FROM {node_revisions} WHERE nid = '%d'";
      $rresult = db_query($rquery, $nid);
      while ($rrow = db_fetch_object($rresult)) {
        $vid = $rrow->vid;
        db_query('INSERT INTO {ndla_paragraphs} (nid, vid, body_data, right_data, body_format, right_format) VALUES (%d, %d, "%s", "", %d, %d)', $nid, $vid, $rrow->body, $rrow->format, $rrow->format);
        $body = theme('ndla_paragraphs_paragraph', $rrow->body, '');
        $teaser = node_teaser($body);
        db_query("UPDATE {node_revisions} SET body = '%s', teaser = '%s' WHERE vid = '%d'", $body, $teaser, $vid);
      }
    }
  }
}

function ndla_paragraphs_set_default_input_format_for_right($form) {
  foreach(element_children($form['ndla_paragraphs']['paragraphs']) as $index) {
    foreach(element_children($form['ndla_paragraphs']['paragraphs'][$index]['right']['format']) as $key) {
      if(!empty($form['ndla_paragraphs']['paragraphs'][$index]['right']['format'][$key]['#value'])) {
        $form['ndla_paragraphs']['paragraphs'][$index]['right']['format'][$key]['#value'] = variable_get('ndla_paragraphs_right_format', 2);
        //$form['ndla_paragraphs']['paragraphs'][$index]['right']['format']['#prefix'] = "<div style='display:none;'>";
        //$form['ndla_paragraphs']['paragraphs'][$index]['right']['format']['#suffix'] = "</div>";
      }
    }
  }

  return $form;
}
