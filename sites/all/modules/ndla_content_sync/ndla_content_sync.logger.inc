<?php
/**
 *
 */

function ndla_content_sync_truncate() {
  db_query("TRUNCATE {ndla_content_sync_log}");
}

function ndla_content_sync_log_write($message, $type, $severity = 'info') {
  db_query("INSERT INTO {ndla_content_sync_log} (severity, type, message, time) VALUES('%s', '%s', '%s', '%d')", $severity, $type, $message, time());
}

function ndla_content_sync_log_get_num_errors() {
  $result = db_query("SELECT COUNT(*) AS num FROM {ndla_content_sync_log} WHERE type = 'error'");
  return db_fetch_object($result)->num;
}

function ndla_content_sync_get_options($field = 'severity') {
  $result = db_query("SELECT $field FROM {ndla_content_sync_log} GROUP BY $field");
  while($row = db_fetch_object($result)) {
    $options[$row->{$field}] = $row->{$field};
  }

  return $options;
}

function ndla_content_sync_get_time_options() {
  $options = array();
  return $options;
}

function ndla_content_sync_get_log() {
  $filters = array('1=1');

  foreach(array('type', 'severity') as $field) {
    if (!empty($_REQUEST[$field])) {
      $data = array_map('check_plain', $_REQUEST[$field]);
      $filters[] = $field  . " IN ('" . implode("','", $data) . "')";
    }
  }

  $sort_order = "ASC";
  $sort_field = "time";
  $sort_field_map = array(
    t('Severity') => 'severity',
    t('Type') => 'type',
    t('Time') => 'time',
  );

  if(!empty($_REQUEST['sort'])) {
    $sort_order = check_plain($_REQUEST['sort']);
  }
  if(!empty($_REQUEST['order']) && !empty($sort_field_map[$_REQUEST['order']])) {
    $sort_field = $sort_field_map[$_REQUEST['order']];
  }

  $query = "SELECT message, severity, type, time FROM {ndla_content_sync_log} WHERE " . implode(" AND ", $filters) . " ORDER BY $sort_field $sort_order";
  $result = pager_query($query, 50, 0);

  $rows = array();
  while($row = db_fetch_object($result)) {
    $row->time = date('Y-m-d H:i:s', $row->time);
    $rows[] = $row;
  }

  return $rows;
}


function ndla_content_sync_log_form() {
  if($_REQUEST['op'] == t('Clear the log')) {
    ndla_content_sync_truncate();
    drupal_set_message(t('Log cleared and sent to a better place.'));
    drupal_goto('admin/settings/ndlasync/log');
  }
  drupal_add_css(drupal_get_path('module', 'ndla_content_sync') . '/css/ndla-content-sync.css');
  $form = array();
  $form['#method'] = 'GET';
  $log_entries = ndla_content_sync_get_log();

  if(!empty($log_entries)) {
    $header = array(
      array('data' => t('Message'), 'class' => 'message'),
      array(
        'data' => t('Severity'),
        'class' => 'severity',
        'field' => 'severity'
      ),
      array('data' => t('Type'), 'class' => 'type', 'field' => 'type'),
      array('data' => t('Time'), 'class' => 'time', 'field' => 'time')
    );
    $table_rows = array();

    foreach ($log_entries as $entry) {
      $new_row = array();
      $new_row['message'] = array(
        'data' => $entry->message,
        'class' => 'message'
      );
      $new_row['severity'] = array(
        'data' => $entry->severity,
        'class' => 'severity'
      );
      $new_row['type'] = array('data' => $entry->type, 'class' => 'type');
      $new_row['time'] = array('data' => $entry->time, 'class' => 'time');
      $table_rows[] = array('data' => $new_row);
    }

    $result_table = theme('table', $header, $table_rows);

    $severity = ndla_content_sync_get_options('severity');
    $type = ndla_content_sync_get_options('type');

    if (!empty($severity)) {
      $form['severity'] = array(
        '#title' => t('Severity'),
        '#type' => 'checkboxes',
        '#options' => $severity,
        '#default_value' => !empty($_REQUEST['severity']) ? array_map('check_plain', $_REQUEST['severity']) : array(),
      );
    }

    if (!empty($type)) {
      $form['type'] = array(
        '#title' => t('Log type'),
        '#type' => 'checkboxes',
        '#options' => $type,
        '#default_value' => !empty($_REQUEST['type']) ? array_map('check_plain', $_REQUEST['type']) : array(),
      );
    }

    $form['truncate'] = array(
      '#type' => 'submit',
      '#value' => t('Clear the log'),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Filter'),
    );

    $form['#entries'] = $result_table;
  }
  $form['#theme'] = 'ndla_content_sync_log';
  return $form;
}