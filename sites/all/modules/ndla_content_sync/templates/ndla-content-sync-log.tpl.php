<div class="log-info">
  <?php
  $pending_node_list = variable_get("ndla_content_sync_pending_node_list", array());
  print "<p> " . t('Latest synchronization was performed (with nodes transferred)') . ': ' . date('Y-m-d H:i:s', variable_get("ndla_content_sync_last_sync", 0)) . "</p>";
  if($num_errors = ndla_content_sync_log_get_num_errors()) {
    print "<p class='error'>" . t("There are @num errors in the log.", array('@num' => $num_errors)) . "</p>";
  }
  if(!empty($pending_node_list)) {
    echo "<p>" . t('There are @count nodes in the pending node list.', array('@count' => count($pending_node_list))) . "</p>";
  }
  else {
    echo "<p>" . t('No nodes in the pending node list.') . "</p>";
  }
  ?>
  <div>
    <?php echo drupal_render($form['truncate']); ?>
  </div>
</div>
<div style='float: left; width: 150px'>
  <?php echo drupal_render($form['type']); ?>
</div>
<div style='float: left; width: 150px;'>
  <?php echo drupal_render($form['severity']); ?>
</div>
<div style='clear: both'></div>

<div>
  <?php echo drupal_render($form['submit']); ?>
</div>

<div class="entries">
  <?php if(empty($form['#entries'])): ?>
    <p>Nothing in the log.</p>
  <?php else: ?>
    <?php print $form['#entries']; ?>
  <?php endif; ?>
</div>

<div class="pager">
  <?php print theme('pager'); ?>
</div>
