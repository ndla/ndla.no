<?php
/**
 * @file
 * MathJax module.
 */

/**
 * Implements hook_help().
 */
function mathjax_help($path, $arg) {
  switch ($path) {
    // Main module help for the mathjax module.
    case 'admin/help#mathjax':
      return '<p>' . t('MathJax allows you to include mathematics in your web pages, either using TeX and LaTeX notation, or as MathML, and you can even use both in the same document. Go to the !website for more information.', array('!website' => l(t('MathJax website'), 'http://www.mathjax.org/'))) . '</p>';
  }
}

/**
 * Implements hook_init().
 */
function mathjax_init() {
  // Load Mathjax only on specified nodes.
  if (mathjax_active() && variable_get('mathjax_enabled', TRUE) && !drupal_match_path('filter/tips', $_GET['q'])) {
    drupal_add_js(drupal_get_path('module', 'mathjax') . '/mathjax.js');

    // Pass variables to mathjax.js
    $mathjax_path = variable_get('mathjax_use_cdn', TRUE) ? 'cdn' : base_path() . 'sites/all/libraries/mathjax' . '/MathJax.js?config=TeX-AMS-MML_HTMLorMML';
    drupal_add_js(array(
      'mathjax' => array(
        'path' => $mathjax_path,
      ),
    ), 'setting');
  }
}

/**
 * Verify that MathJax should be active for the current URL.
 */
function mathjax_active() {
  $pages = variable_get('mathjax_pages', "admin*\nnode/add/*\nnode/*/edit");
  $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
  // Compare the lowercase internal and lowercase path alias (if any).
  $page_match = drupal_match_path($path, $pages);
  if ($path != $_GET['q']) {
    $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
  }
  if (variable_get('mathjax_active_type', 'disable') == 'disable') {
    return !$page_match;
  }
  else {
    return $page_match;
  }
}

/**
 * Implements hook_perm().
 */
function mathjax_perm() {
  return array('administer mathjax');
}

/**
 * Implements hook_menu().
 */
function mathjax_menu() {
  $items = array();
  $items['admin/settings/mathjax'] = array(
    'title' => 'MathJax',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mathjax_global_settings'),
    'access arguments' => array('administer mathjax'),
    'description' => 'Configure global settings for MathJax.',
  );
  return $items;
}

/**
 * Configure global settings for MathJax.
 */
function mathjax_global_settings() {
  $form['mathjax']['mathjax_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Global switch'),
    '#default_value' => variable_get('mathjax_enabled', TRUE),
    '#description' => t('Check this box to enable MathJax for the entire site.'),
  );
  $form['mathjax']['mathjax_use_cdn'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use MathJax Content Delivery Network (CDN)'),
    '#default_value' => variable_get('mathjax_use_cdn', TRUE),
    '#description' => t('Check this box to load MathJax source from MathJax servers (recommended).'),
  );
  $form['mathjax']['active'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page specific activation settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['mathjax']['active']['mathjax_active_type'] = array(
    '#type' => 'radios',
    '#title' => t('Enable MathJax on specific pages'),
    '#options' => array('disable' => 'Enable on every page except the listed pages.', 'enable' => 'Enable on the listed pages only.'),
    '#default_value' => variable_get('mathjax_active_type', 'disable'),
  );
  $form['mathjax']['active']['mathjax_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#default_value' => variable_get('mathjax_pages', "admin*\nnode/add/*\nnode/*/edit"),
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.",
        array(
          '%blog' => 'blog',
          '%blog-wildcard' => 'blog/*',
          '%front' => '<front>',
        )
    ),
  );
  return system_settings_form($form);
}

/**
 * Implements hook_filter().
 */
function mathjax_filter($op, $delta = 0, $format = -1, $text = '', $cache_id = 0) {
  switch ($op) {
    case 'list':
      return array(
        0 => t('MathJax filter'),
      );
    case 'description':
      return t('JavaScript display engine for mathematics using LaTeX.');
    case 'prepare':
      // Format the equation input according to the different display styles.
      $text = preg_replace('/\$\$(.*?)\$\$/m', '[mathjax:block \1]', $text);
      $text = preg_replace('/\$(.*?)\$/m', '[mathjax:inline \1]', $text);
      return $text;
    case 'process':
      return $text;
  }
}

/**
 * Implements hook_filter_tips().
 */
function mathjax_filter_tips($delta, $format, $long = FALSE) {
  if (!$long) {
    return t('Equations entered between $...$ are rendered using !mathjax', array('!mathjax' => l(t('MathJax'), 'http://www.mathjax.org/')));
  }
  else {
    $tips = '<p>' . t('Mathematics that is written in TeX or LaTeX format is indicated using math delimiters that surround the mathematics, telling MathJax what part of your page represents mathematics and what is normal text. There are two types of equations: ones that occur within a paragraph (in-line mathematics), and larger equations that appear separated from the rest of the text on lines by themselves (displayed mathematics).') . '</p>';
    $tips .= '<p>' . t('The default math delimiters are $$...$$ for displayed mathematics, and $...$ for in-line mathematics. The longer notation can also be used: [mathjax:inline ...] for in-line mathematics, and [mathjax:block ...] for displayed mathematics.') . '</p>';
    $tips .= '<p>' . t('Go to the MathJax documentation website for a list of supported LaTeX commands: !url', array('!url' => l('http://www.mathjax.org/docs/1.1/tex.html#supported-latex-commands', 'http://www.mathjax.org/docs/1.1/tex.html#supported-latex-commands'))) . '</p>';
    return $tips;
  }
}
