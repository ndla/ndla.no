<?php
/**
* Implementation of hook_install().
*/
function istribute_install() {
  drupal_install_schema('istribute');
}

/**
* Implementation of hook_uninstall().
*/
function istribute_uninstall() {
  drupal_uninstall_schema('istribute');
  // TODO: Remove variables from variable table.
}

function istribute_schema() {
  $schema = array(
    'istribute_videos' => array(
      'description' => 'Store per-revision Istribute video id for {node}',
      'fields' => array(
        'nid' => array(
          'description' => 'The primary identifier for a node.', 
          'type' => 'int', 
          'unsigned' => TRUE, 
          'not null' => TRUE,
        ),
        'vid' => array(
          'description' => 'Revision number.', 
          'type' => 'int', 
          'unsigned' => TRUE, 
          'not null' => TRUE,
        ),
        'videoid' => array(
          'type' => 'varchar', 
          'length' => 60, 
          'not null' => TRUE
        ),
        'status' => array(
          'type' => 'varchar',
          'length' => 50,
          'not null' => TRUE,
          'default' => '',
        ),
        'vars' => array(
          'type' => 'text',
          'size' => 'medium',
          'not null' => TRUE,
        ),
        'serverdata' => array(
          'description' => 'Tells us if the server is synchronized with metadata such as title',
          'type' => 'int',
          'length' => 1,
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('nid','vid'),
      'indexes' => array(
        'istr_videoid' => array('videoid'),
        'istr_serverdata' => array('serverdata'),
      ),
    ),
    'istribute_flashvideo_import' => array(
      'description' => 'Store if the Istribute video field is enabled for the various node types.',
      'fields' => array(
        'nid' => array(
          'description' => 'The primary identifier for a node.', 
          'type' => 'int', 
          'unsigned' => TRUE, 
          'not null' => TRUE,
        ),
        'created' => array(
          'description' => 'The time the video was requested to be uploaded',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
	),
        'attempted' => array(
          'description' => 'Time of the last upload attempt',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
	),
        'filename' => array(
          'description' => 'Filename that is to be uploaded, relative to the files root',
          'type' => 'varchar',
          'length' => 150,
          'not null' => TRUE,
        ),
        'message' => array(
          'description' => 'Information about the job performed',
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ),
      ),
      'primary key' => array('nid'),
      'indexes' => array(
        'istr_flv_imp' => array('created','attempted'),
      ),
    ),
    'istribute_log' => array(
      'description' => 'Logs what is done for video files',
      'fields' => array(
        'iid' => array(
          'description' => 'Primary key for logged events',
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'nid' => array(
          'description' => 'The primary identifier for a node.', 
          'type' => 'int', 
          'unsigned' => TRUE, 
          'not null' => TRUE,
        ),
        'performed' => array(
          'description' => 'The time the action was performed',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
	),
        'message' => array(
          'description' => 'Information about the action performed',
          'type' => 'text',
          'size' => 'medium',
          'not null' => TRUE,
        ),
        'vars' => array(
          'description' => 'Holds variables for translation of message',
          'type' => 'text',
          'size' => 'medium',
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('iid'),
      'indexes' => array(
        'istr_log' => array('nid'),
      ),
    ),
  );
  return $schema;
}

/**
* Implementation of hook_enable().
*/
function istribute_enable() {
}

/**
* Implementation of hook_disable().
*/
function istribute_disable() {
}
