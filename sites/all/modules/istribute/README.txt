-- SUMMARY --

The Istribute video module for Drupal enables integration with the high performance Istribute
video CDN.


-- REQUIREMENTS --

None

-- INSTALLATION --

* Install as usual.


-- CONTACT --

* Seria AS, www.seria.no
