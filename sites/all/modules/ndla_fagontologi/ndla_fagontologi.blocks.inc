<?php
/**
 * Created by PhpStorm.
 * User: rolfguescini
 * Date: 19.10.14
 * Time: 17:11
 */
function ndla_fagontologi_block($op = 'list', $delta = 0, $edit = array()) {
  global $language;
  switch ($op) {
    case 'list':
      return array(
        'ndla_fagontologi_topics' => array(
          'info' => t('NDLA Fagontologi: Topics'),
          'weight' => 3,
          'status' => 1,
          'region' => 'right'
        ),
      );
    case 'view':
      if($delta == 'ndla_fagontologi_topics') {
        $args = arg();
        if($args[1] == intval($args[1]) && $args[2] != "edit") {

          $node = node_load($args[1]);

          if (isset($node->nodeTopics)) {
            drupal_add_css(drupal_get_path('module', 'ndla_fagontologi') . "/css/ndla_fagontologi.blocks.css");
error_log(print_r($node->nodeTopics,1));
            foreach ($node->nodeTopics as $topicId => $topicData) {
              if(count($topicData['names']) > 0){
                $nameLanguage = NDLAFagontologiClient::getNameLanguage($topicData['names'],NdlaGrepLanguages::iso639_1_iso639_2($language->language));
                foreach($topicData['names'] as $nameObject) {
                  if(property_exists($nameObject,$nameLanguage)) {
                    $nameArray = get_object_vars($nameObject);
                    $link_name = utf8_decode($nameArray[$nameLanguage]);
                    $topicList[] = l($link_name,
                      'search/apachesolr_search', array('query' => array(ndla_fagontologi_topics_ids_solr_key() . '[]' => $topicId)));

                  }
                }

              }
            }

            if (isset($topicList)) {
              return array(
                'title' => t('Is about'),
                'content' => theme('ndla_fagontologi_topics', $topicList),
              );
            }

          }//end if fagontologi
        }//end if not node form

        break;
      }
  }
}