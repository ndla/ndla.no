/**
 * Created by rolfguescini on 27.10.14.
 */
$(document).ready(function() {
    $("#edit-kompetansemaalontologi").wrap('<span id="fagontologi-input-wrapper" class="tagger-plugin"></span>');
    $("#edit-kompetansemaalontologi").after('<span id="fagontologi-input-content"></span><span id="fagontologi-hidden-wrapper"></span>');
    var jsonString = $("#edit-chosenCurriculaTopics").val();

    var jsonNameString = $("#edit-chosenCurriculaTopicNames").val();
    var chosenJson = null;
    var chosenJSONames = null;
    if((typeof jsonString != 'undefined' && jsonString.length > 0) && (typeof jsonNameString != 'undefined' && jsonNameString.length > 0)){
        chosenJson = $.evalJSON(jsonString);
        chosenJSONames = $.evalJSON(jsonNameString);
        for(var i = 0; i < chosenJson.length; i++){
            var topicId = chosenJson[i];
            var topicName = chosenJSONames[topicId].name;

            var  $topicBodyPill = $('<span class="fagontologi-begrep chosenTopic" data-chosen-topic-id="'+topicId+'" id="'+topicId+'">'+topicName+'</span>');
            $topicBodyPill.appendTo($("#fagontologi-input-content"));

            $topicBodyPill.bind('click',function(){
                var topicId = $(this).attr('id');
                var jsonString = $("#edit-chosenCurriculaTopics").val();
                var chosenJson = null;
                if(jsonString.length > 0){
                    chosenJson = $.evalJSON(jsonString);
                }
                else{
                    chosenJson = []
                }

                var array = removeFromArray(chosenJson,topicId)
                var nameArray = removeFromAssocArray(chosenJSONames,topicId)
                $("#edit-chosenCurriculaTopicNames").val(JSON.stringify(nameArray));
                $("#edit-chosenCurriculaTopics").val(JSON.stringify(array));
                $(this).remove();
            });
        }
    }

});

function showCurriculaOntology(serviceurl,language) {

    $("#edit-kompetansemaalontologi-wrapper").before('<div id="show_CurriculumOntology_window" class="ndla_CurriculumOntology_window"></div>');
    $("#show_CurriculumOntology_window").html('<div class="aim_window_loader_'+language+'"><div class="progress progress-striped active"><div class="bar" style="width: 40%;"></div></div></div>');
    $("#show_CurriculumOntology_window").fadeIn(750);
    $.ajaxSetup({
        scriptCharset: "utf-8" ,
        contentType: "application/x-www-form-urlencoded; charset=utf-8"
    });

    var urlArray = serviceurl.trim().split("/");
    if(typeof urlArray[3] == 'undefined' || urlArray[3] == ''){
        serviceurl = "/"+urlArray[1]+"/ndla_fagontologi_get_fag/1"

        $.ajax({
            type: 'GET',
            url: serviceurl,
            dataType: 'json',
            success: function(data) {
                $("#show_CurriculumOntology_window").html();
                var select = '<label>'+Drupal.t("Choose subject matter")+'</label><br /><select name="courseSelect" id="courseSelect"><option value="">'+Drupal.t('Choose')+'</option>';
                $.each(data,function(courseId, title){
                    select += '<option value="'+courseId+'">'+title+'</option>';
                });
                select += '</select>';
                var $selectObject = $(select);
                $("#show_CurriculumOntology_window").html($selectObject);
                $selectObject.bind('change',function(){
                    var courseId = $(this).val();
                    var newServiceurl = "/"+urlArray[1]+"/"+urlArray[2]+"/"+courseId;
                    $("#edit-chosenCourse").val(courseId);
                    $("#fagOntologiOpener").attr("onClick","showCurriculaOntology('/"+language+"/ndla_fagontologi_get_curriculumOntology/"+courseId+"','"+language+"');return false;");
                    showCurriculaOntology(newServiceurl,language)
                });


            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                $("#ndla_fagontologi_errors").html(Drupal.t('<p>An error occured when fetching the data for the competence aim ontology. Unable to fetch data.</p>'));
            }

        });

    }
    else{
        $.ajax({
            type: 'GET',
            url: serviceurl,
            dataType: 'text',
            success: function(data) {
                $(".aim_window_loader_"+language+"").fadeOut(500);
                $("#show_CurriculumOntology_window").html();
                $("#show_CurriculumOntology_window").html(data);

                $(".tabbable").after('<div id="show_CurriculumOntology_close_button"></div>');
                $("#show_CurriculumOntology_close_button").bind('click',function(){
                    $("#show_CurriculumOntology_window").html();
                    $("#show_CurriculumOntology_window").fadeOut(500);
                });


                $("a[data-main-toggle]").bind('mouseover', "a[data-main-toggle]", function () {
                    var mainId = $(this).attr('data-main-toggle');
                    var offset = $(this).offset();
                    var windowOffset = $("#show_CurriculumOntology_window").offset();
                    var theOffset = (offset.top - windowOffset.top)

                    $("#mainAreaLevelPopUp_"+mainId).css("top",theOffset);
                    $("#mainAreaLevelPopUp_"+mainId).fadeIn(100)
                });

                $("div.courseListInActive").bind('click',function(){
                    var idArr = $(this).attr('id').split("_");
                    var curriculumCount = idArr[1];
                    var newServiceurl = "/"+urlArray[1]+"/"+urlArray[2]+"/"+urlArray[3]+"/"+curriculumCount;
                    showCurriculaOntology(newServiceurl,language)
                });

                $("a[data-main-toggle]").bind('mouseout', "a[data-main-toggle]", function () {
                    var mainId = $(this).attr('data-main-toggle');
                    $("#mainAreaLevelPopUp_"+mainId).fadeOut(100)
                });

                $("a[data-main-toggle]").bind('click',function(){
                    var aimId = $(this).attr('data-main-toggle');
                    var lastAimId = $(this).parent().parent().find("li.active > a").attr('data-main-toggle');
                    $(this).parent().parent().find("li.active").removeClass('active');
                    $(this).parent().addClass('active');
                    $("#"+lastAimId).css("display","none");
                    $("#"+aimId).css("display","block");
                })

                $("a[data-aim-toggle]").bind('click',function(){
                    var aimId = $(this).attr('data-aim-toggle');
                    $("#"+aimId+"_aimCollapse").css("display","block");
                    $("#"+aimId+"_aimCollapse").css("height","200px");
                    $(this).parent().parent().parent().find("a[data-aim-toggle]").each(function(){
                        if($(this).attr('data-aim-toggle') != aimId) {
                            $("#"+$(this).attr('data-aim-toggle')+"_aimCollapse").css("height","0px");
                        }
                    });

                });

                $("a[data-topic-id]").bind('click',function(){
                    var topicId = $(this).attr('data-topic-id');
                    var topicText = $(this).text();
                    topicId = topicId.substring(topicId.lastIndexOf("#")+1);
                    var jsonString = $("#edit-chosenCurriculaTopics").val();
                    var jsonNameString = $("#edit-chosenCurriculaTopicNames").val();
                    var chosenJson = null;
                    var chosenJsonNames = null;
                    if(jsonString.length > 0){
                        chosenJson = $.evalJSON(jsonString);
                    }
                    else{
                        chosenJson = []
                    }

                    if(jsonNameString.length > 0){
                        chosenJsonNames = $.evalJSON(jsonNameString);
                    }
                    else{
                        chosenJsonNames = {}
                    }


                    if($(this).hasClass("checkedTopic")) {
                        $(this).removeClass("checkedTopic");
                        $("#"+topicId).remove();
                        var array = removeFromArray(chosenJson,topicId)
                        var nameArray = removeFromAssocArray(chosenJsonNames,topicId)
                        $("#edit-chosenCurriculaTopics").val(JSON.stringify(array));
                        $("#edit-chosenCurriculaTopicNames").val(JSON.stringify(nameArray));

                    }
                    else{
                        if(!hasDuplicate(topicId)) {
                            $(this).addClass("checkedTopic");
                            chosenJson.push(topicId);
                            $("#edit-chosenCurriculaTopics").val(JSON.stringify(chosenJson));
                            if(typeof chosenJsonNames[topicId] == 'undefined'){
                                chosenJsonNames[topicId] = new Object();
                            }
                            chosenJsonNames[topicId].name = topicText;

                            $("#edit-chosenCurriculaTopicNames").val(JSON.stringify(chosenJsonNames));

                            var  $topicPill = $('<span class="fagontologi-begrep chosenTopic" data-chosen-topic-id="'+topicId+'" id="'+topicId+'">'+$(this).text()+'</span>');
                            $topicPill.appendTo($("#fagontologi-input-content"));

                            $topicPill.bind('click',function(){
                                var topicId = $(this).attr('id');
                                var jsonString = $("#edit-chosenCurriculaTopics").val();
                                var jsonNameString = $("#edit-chosenCurriculaTopicNames").val();
                                var chosenJson = null;
                                var chosenJsonNames = null;
                                if(jsonString.length > 0){
                                    chosenJson = $.evalJSON(jsonString);
                                }
                                else{
                                    chosenJson = []
                                }

                                if(jsonNameString.length > 0){
                                    chosenJsonNames = $.evalJSON(jsonNameString);
                                }
                                else{
                                    chosenJsonNames = {}
                                }

                                var array = removeFromArray(chosenJson,topicId)
                                var nameArray = removeFromAssocArray(chosenJsonNames,topicId)
                                $("#edit-chosenCurriculaTopics").val(JSON.stringify(array));
                                $("#edit-chosenCurriculaTopicNames").val(JSON.stringify(nameArray));
                                $(this).remove();
                            });

                        }
                        else{
                            alert(Drupal.t("The topic was already added"));
                        }
                    }


                });




            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                $("#ndla_fagontologi_errors").html(Drupal.t('<p>An error occured when fetching the data for the competence aim ontology. Unable to fetch data.</p>'));
            }

        });
    }

}

function hasDuplicate(topicId){
    var exists = false;
    var jsonString = $("#edit-chosenCurriculaTopics").val();
    var chosenJson = null;
    if(jsonString.length > 0){
        chosenJson = $.evalJSON(jsonString);
        for(var i = 0; i < chosenJson.length; i++) {
            if(chosenJson[i] == topicId){
                exists = true;
                break;
            }
        }
    }

    return exists;
}


function removeFromArray(arr,id) {
    var result = new Array();
    for(var i = 0; i < arr.length; i++){
        if(arr[i] != id){
            result.push(arr[i]);
        }
    }
    return result;
}

function removeFromAssocArray(arr,id) {
    var result = {}
    $.each(arr,function(i, value){
        if(i == id) {
            delete arr[i]
        }

    });

    return arr;
}