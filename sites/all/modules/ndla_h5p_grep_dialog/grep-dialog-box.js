/** @namespace H5P */
var H5P = H5P || {};

/**
 * Grep Dialog Box module
 * @class
 * @external {jQuery} $ H5P.jQuery
 */
H5P.GrepDialogBox = (function ($) {
  var CURRICULUM = 0;
  var COMPETENCE_AIM_SET = 1;
  var COMPETENCE_AIM = 2;

  var isIE9 = function () {
    return (/MSIE 9/i).test(navigator.userAgent);
  };

  // Create case insensitive search
  $.extend($.expr[":"], {
    "containsIN": function(elem, i, match, array) {
      return (elem.textContent || elem.innerText || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
    }
  });

  /**
   * Initialize module.
   * @param {Object} params Object containing parameters
   * @param {Array} filterIdList Array containing ids to filter on
   * @returns {Object} GrepDialogBox GrepDialogBox instance
   */
  function GrepDialogBox(params) {
    this.isCreated = false;
    this.hasBottomBar = false;
    this.selectedCompetenceAims = [];
    this.aimCounter = 0;

    // l10n
    this.params = $.extend({}, {
      chooseGoalText: 'Choose goal from list',
      goalsAddedText: 'Number of goals added:',
      grepDialogDone: 'Done',
      filterGoalsPlaceholder: 'Filter on words...',
      noResultsFound: "No matching goals were found..."
    }, params);
  }

  /**
   * Creates the dialog box and attaches it to wrapper
   * @param {jQuery} $wrapper
   */
  GrepDialogBox.prototype.attach = function ($wrapper) {
    this.$wrapper = $wrapper;
    if (!this.isCreated) {
      this.createDialogView();
    }
    this.createLoadingScreen();
    this.createBottomBar();
  };

  /**
   * Creates the dialog
   * @returns {H5P.GoalsPage.GrepDialogBox}
   */
  GrepDialogBox.prototype.createDialogView = function () {
    this.$curriculumDialogContainer = $('<div>', {
      'class': 'h5p-curriculum-popup-container'
    });

    // Create a semi-transparent background for the popup
    $('<div>', {
      'class': 'h5p-curriculum-popup-background'
    }).appendTo(this.$curriculumDialogContainer);

    this.$curriculumDialog = $('<div>', {
      'class': 'h5p-curriculum-popup'
    }).appendTo(this.$curriculumDialogContainer);

    this.createHeader().appendTo(this.$curriculumDialog);
    this.createSearchBox().appendTo(this.$curriculumDialog);

    this.$curriculumView = $('<div>', {
      'class': 'h5p-curriculum-view'
    }).appendTo(this.$curriculumDialog);

    if (isIE9()) {
      this.$curriculumDialogContainer.addClass('ie9');
    }

    this.isCreated = true;

    this.$curriculumDialogContainer.appendTo(this.$wrapper);

    return this;
  };

  /**
   * Creates header
   */
  GrepDialogBox.prototype.createHeader = function () {
    var $header = $('<div>', {
      'class': 'h5p-curriculum-header'
    });

    $('<div>', {
      'class': 'h5p-curriculum-header-text',
      'html': this.params.chooseGoalText
    }).appendTo($header);
    this.createExit().appendTo($header);

    return $header;
  };

  /**
   * Creates an exit button for the dialog box
   */
  GrepDialogBox.prototype.createExit = function () {
    var self = this;
    return $('<div>', {
      'class': 'h5p-curriculum-popup-exit'
    }).click(function () {
      self.removeDialogBox();
    });
  };

  /**
   * Removes the dialog box
   */
  GrepDialogBox.prototype.removeDialogBox = function () {
    this.$curriculumDialogContainer.remove();
  };

  /**
   * Creates a throbber at the curriculum view.
   */
  GrepDialogBox.prototype.createLoadingScreen = function (selectedItem) {
    var self = this;

    self.$throbber = $('<div>', {
      'class': 'h5p-throbber-container'
    }).appendTo(self.$curriculumView);


    H5P.JoubelUI.createThrobber().appendTo(self.$throbber);
  };

  /**
   * Appends list entry
   * @method appendListEntry
   * @param  {H5P.jQuery}    $wrapper       Container which holds entries
   * @param  {string}        name           Name
   * @param  {number}        type           Curriculum aim set or aim
   * @param  {string}        curriculumName Name of curriculum
   */
  GrepDialogBox.prototype.appendListEntry = function ($wrapper, name, type, curriculumName) {
    var self = this;
    var classString = 'h5p-view-list-entry ' + (type === CURRICULUM ? 'h5p-curriculum-instance' : (type === COMPETENCE_AIM_SET ? 'h5p-competence-aim-set-instance' : 'h5p-competence-aim-instance'));

    return $('<div>', {
      'class': classString,
      'html': '<div class="name">' + name + '</div>',
      'data-id': self.aimCounter++
    }).click(function () {
      var $entry = $(this);
      var isCompetenceAim = $entry.hasClass('h5p-competence-aim-instance');
      var id = $entry.data('id');

      if (!isCompetenceAim && $('.h5p-list-search-mode').length !== 0) {
        return false;
      }

      $entry.toggleClass('selected');

      if (isCompetenceAim) {
        if ($entry.hasClass('selected')) {
          // Aim selected
          self.selectedCompetenceAims[id] = {
            value: name,
            description: curriculumName
          };
        }
        else {
          delete self.selectedCompetenceAims[id];
        }
        self.updateBottomBar();
      }
      return false;
    }).appendTo($wrapper);
  };

  /**
   * Recursively add competence aim sets
   * @method function
   * @param  {[Object]} sets         Array of sets
   * @param  {H5P.jQuery} $wrapper   parent DOM node
   * @param  {number} type           Aim set or aim
   * @param  {string} curriculumName Name of curriculum
   * @param  {string} prefix         prefix
   */
  GrepDialogBox.prototype.appendCompetenceAimSets = function (sets, $wrapper, type, curriculumName, prefix) {
    if(sets === undefined || sets.length === 0) {
      return;
    }

    for (var i = 0; i < sets.length; i++) {
      var set = sets[i];
      var name = H5P.GrepAPI.getLanguageNeutral(set.names);
      var p = (prefix !== undefined ? (prefix === name ? '' : '<div class="prefix">' + prefix + '</div>') : '');
      var $set = this.appendListEntry($wrapper, p + name, type, curriculumName);
      this.appendCompetenceAimSets((set.competenceAims ? set.competenceAims : set.competenceAimSets), $set, (set.competenceAims ? COMPETENCE_AIM : COMPETENCE_AIM_SET), curriculumName);
    }
  };

  /**
   * Populates the dialog box view
   * @param {Array} List of curriculums
   * @returns {H5P.GoalsPage.GrepDialogBox}
   */
  GrepDialogBox.prototype.populateDialogView = function (curriculums) {
    var self = this;

    self.$throbber.remove();

    var $viewListContainer = $('<div>', {
      'class': 'h5p-view-list-container'
    });

    for (var i = 0; i < curriculums.length; i++) {
      var curriculum = curriculums[i];
      var curriculumName = H5P.GrepAPI.getLanguageNeutral(curriculum.names);
      var $curriculum = this.appendListEntry($viewListContainer, curriculumName, CURRICULUM);
      // For now, skip the first competence aim set
      for (var j = 0; j < curriculum.competenceAimSets.length; j++) {
        this.appendCompetenceAimSets(curriculum.competenceAimSets[j].competenceAimSets, $curriculum, COMPETENCE_AIM_SET, curriculumName, H5P.GrepAPI.getLanguageNeutral(curriculum.competenceAimSets[j].names));
      }
    }

    $viewListContainer.appendTo(this.$curriculumView);
    return this;
  };

  /**
   * Adds a bottom bar to the dialog.
   */
  GrepDialogBox.prototype.createBottomBar = function () {
    var self = this;
    if (!this.hasBottomBar) {
      self.$bottomBar = $('<div>', {
        'class': 'h5p-bottom-bar'
      });

      self.$bottomBarText = $('<div>', {
        'class': 'h5p-bottom-bar-text',
        'html': self.params.goalsAddedText + ' 1'
      }).appendTo(self.$bottomBar);

      self.$bottomBarButton = H5P.JoubelUI
        .createSimpleRoundedButton(self.params.grepDialogDone)
        .addClass('h5p-bottom-bar-button')
        .click(function () {
          $(this).trigger('dialogFinished', [self.selectedCompetenceAims]);
          self.removeDialogBox();
        }).appendTo(self.$bottomBar);
    }
  };

  /**
   * Updates bottom bar, either changing the text or remove the bottom bar.
   */
  GrepDialogBox.prototype.updateBottomBar = function () {
    var self = this;

    if (!this.hasBottomBar) {
      self.$bottomBar.appendTo(self.$curriculumDialog);
      this.hasBottomBar = true;
      self.$curriculumView.addClass('has-bottom-bar');
    }

    self.$bottomBarText.html(self.params.goalsAddedText + ' ' +  Object.keys(self.selectedCompetenceAims).length);
  };

  /**
   * Returns the button for finishing the dialog box
   * @returns {jQuery} this.$bottomBarButton Finish button
   */
  GrepDialogBox.prototype.getFinishedButton = function () {
    return this.$bottomBarButton;
  };

  /**
   * Adds competence aim to list of selected competence aims and updates bottom bar accordingly
   * @param {Object} selectedCompetenceAim Selected competence aim
   * @param {jQuery} selectedElement Selected element corresponding to selected competence aim
   */
  GrepDialogBox.prototype.addCompetenceAim = function (selectedCompetenceAim, selectedElement) {
    var self = this;
    if (self.selectedCompetenceAims.indexOf(selectedCompetenceAim) === -1) {
      self.selectedCompetenceAims.push(selectedCompetenceAim);
      // Add selected class
      selectedElement.addClass('selected');
      selectedCompetenceAim.selected = true;
    } else {
      self.selectedCompetenceAims.splice(self.selectedCompetenceAims.indexOf(selectedCompetenceAim), 1);
      // Remove selected class
      selectedElement.removeClass('selected');
      selectedCompetenceAim.selected = false;
    }

    this.updateBottomBar();
  };

  /**
   * Removes competence aim from selected competence aims list and updates bottom bar
   * @param {Object} selectedCompetenceAim Textual representation of comeptence aim
   */
  GrepDialogBox.prototype.removeCompetenceAim = function (selectedCompetenceAim) {
    if (this.selectedCompetenceAims.indexOf(selectedCompetenceAim) > -1) {
      this.selectedCompetenceAims.splice(this.selectedCompetenceAims.indexOf(selectedCompetenceAim), 1);
    }
    selectedCompetenceAim.selected = false;
    this.updateBottomBar();
  };

  /**
   * Creates a search box inside wrapper
   * @returns {jQuery} $searchContainer Search container
   */
  GrepDialogBox.prototype.createSearchBox = function () {
    var self = this;

    self.currentSearch = '';

    var $searchContainer = $('<div>', {
      'class': 'h5p-curriculum-search-container'
    });

    this.$searchInput = $('<input>', {
      'type': 'text',
      'class': 'h5p-curriculum-search-box',
      'placeholder': this.params.filterGoalsPlaceholder
    }).keyup(function () {
      // Filter curriculum names on key up
      var input = $(this).val().trim();

      if (self.currentSearch === input) {
        return;
      }
      self.currentSearch = input;

      $('.h5p-list-search-hit').removeClass('h5p-list-search-hit');

      if (input.length === 0) {
        $('.h5p-view-list-container').removeClass('h5p-list-search-mode');
      }
      else {
        $('.h5p-view-list-container').addClass('h5p-list-search-mode');
        $('.h5p-competence-aim-instance .name:containsIN(' + input + ')').each(function () {
          var $element = $(this);
          $element.html($element.text().replace(input, '<span class="hit">' + input + '</span>'));
          $element.addClass('h5p-list-search-hit');
          $element.parents('.h5p-view-list-entry').addClass('h5p-list-search-hit');
        });
      }
    }).appendTo($searchContainer);

    return $searchContainer;
  };

  /**
   * Sets error message in grep dialog box
   * @param {String} msg String explaining the error
   */
  GrepDialogBox.prototype.setErrorMessage = function (msg) {
    this.$curriculumView.children().remove();
    this.$curriculumView.html(msg);
  };

  return GrepDialogBox;

}(H5P.jQuery));
