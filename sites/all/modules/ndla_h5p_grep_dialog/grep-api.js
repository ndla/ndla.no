var H5P = H5P || {};

/**
 * GrepAPI module
 * @class
 * @external {jQuery} $ H5P.jQuery
 */
H5P.GrepAPI = (function ($, EventDispatcher) {
  var COMPETENCE_AIM_SET = 0;

  var ERROR_CONNECTION = 'Could not connect to the Internet.';

  var standardGrepUrl = '//mycurriculum.ndla.no/v1/users/ndla/curriculums';

  var UNWANTED_COMPETENCE_AIM_SET_LEVELS = ['aarstrinn2', 'aarstrinn4', 'aarstrinn7', 'aarstrinn10'];

  /**
   * Initialize module.
   * @param {String} targetGrepUrl Url used to get json from
   * @returns {Object} GrepAPI GrepAPI instance
   */
  function GrepAPI(targetGrepUrl) {
    // Initialize event inheritance
    EventDispatcher.call(this);

    this.curriculums = [];
    if (this.grepUrl === undefined) {
      this.grepUrl = standardGrepUrl;
    } else {
      this.grepUrl = targetGrepUrl;
    }
  }

  // Extends the event dispatcher
  GrepAPI.prototype = Object.create(EventDispatcher.prototype);
  GrepAPI.prototype.constructor = GrepAPI;

  /**
   * Prefethes relevant data
   * @method init
   * @param  {[String]}   Array of IDs
   * @param  {Function} callback  invoked on errors
   */
  GrepAPI.prototype.init = function (filterIdList, callback) {
    var self = this;

    function filterHandled(index) {
      filterIdList.splice(index, 1);
      // Finished fetching all data?
      if (filterIdList.length === 0) {
        self.trigger('data-loaded');
      }
    }

    self.getGrepData('', function (error, data) {
      if (error) {
        return callback(error);
      }

      var curriculums = [];

      for (var i = 0; i < data.curriculums.length; i++) {
        var curriculum = data.curriculums[i];
        curriculums[curriculum.id] = curriculum;
      }
      // For all curriculums, fetch the details
      var numFetched = 0;
      for (var i = filterIdList.length-1; i >= 0; i--) {

        var filter = filterIdList[i];

        // Supports filters with the following format: <id>/<subid-a>+<subid-b>...
        var filteredSplit = filter.split('/');
        var id = filteredSplit[0];
        var subIds = filteredSplit.length > 1 ? filteredSplit[1].split('+') : undefined;

        var curriculum = curriculums[id];
        if (curriculum && curriculum.link) {
          if (self.curriculums[curriculum.id] === undefined) {
            self.getGrepData(curriculum.link, function (error, data, subIds) {
              if (self.removeUnwantedData(data.curriculum, subIds)) {
                self.curriculums[data.curriculum.id] = data.curriculum;
              }
              filterHandled(i);
            }, subIds);
          }
          else {
            filterHandled(i);
          }
        }
        else {
          filterHandled(i);
        }
      }
    });
  };

  /**
   * Removes competence aim sets for levels not relevant, and those filtered away bu subid
   * @method removeUnwantedData
   * @param  {Object}           curriculum Curriculum containing competence aim sets
   * @returns {boolean} If curriculum still have competence aim sets, true is returned. Else false
   */
  GrepAPI.prototype.removeUnwantedData = function (curriculum, subIds) {
    for (var i = curriculum.competenceAimSets.length-1; i >= 0; i--) {
      var set = curriculum.competenceAimSets[i];

      if (subIds !== undefined) {
        // Check if subId filters this one away
        // This will override the UNWANTED_COMPETENCE_AIM_SET_LEVELS filter
        var keepMe = false;
        for (var j = 0; j < subIds.length; j++) {
          var suffix = '_' + subIds[j];
          keepMe |= set.id.indexOf(suffix, set.length - suffix.length) !== -1;
        }

        if (!keepMe) {
          curriculum.competenceAimSets.splice(i, 1);
        }
      }
      else if (set.levels !== undefined && set.levels.length !== 0) {
        var keepMe = false;
        for (var j = 0; j < set.levels.length; j++) {
          keepMe |=  (UNWANTED_COMPETENCE_AIM_SET_LEVELS.indexOf(set.levels[j].id) === -1);
        }
        if (!keepMe) {
          curriculum.competenceAimSets.splice(i, 1);
        }
      }
    }
    return curriculum.competenceAimSets.length !== 0;
  };

  /**
   * Fetches data from url and updates the dialog view of provided grep dialog box
   * @param {String} dataUrl url
   * @param {function} callback
   * @returns {H5P.GrepAPI}
   */
  GrepAPI.prototype.getGrepData = function (dataUrl, callback, subId) {
    var self = this;

    if (dataUrl === undefined || dataUrl === '') {
      dataUrl = self.grepUrl;
    }

    // Need to fix grep-api bugs:
    dataUrl = dataUrl.replace('https:', '').replace('http:', '');

    // Find IE version
    function isIE() {
      var myNav = navigator.userAgent.toLowerCase();
      return (myNav.indexOf('msie') !== -1) ? parseInt(myNav.split('msie')[1], 10) : false;
    }

    var xhrFinished = function (error, data) {
      if (error) {
        return callback(error);
      }
      callback(undefined, data, subId);
    }

    // is IE version less than 9
    if (isIE() && isIE() <= 9) {
      // Use XDomainRequest
      if (window.XDomainRequest) {
        /*global XDomainRequest */
        var xdr = new XDomainRequest();
        xdr.open("get", dataUrl);

        xdr.onerror = function () {
          //Error Occured
          xhrFinished(ERROR_CONNECTION);
        };

        // Success
        xdr.onload = function (xdr) {
          xhrFinished(undefined, JSON.parse(xdr.target.responseText));
        };

        setTimeout(function () {
          xdr.send();
        }, 0);
      }
      return this;
    }

    $.support.cors = true;

    $.ajax({
      url: dataUrl,
      dataType: 'json',
      success: function (data) {
        xhrFinished(undefined, data);
      },
      error: function () {
        xhrFinished(ERROR_CONNECTION);
      }
    });
  };

  /**
   * Get language neutral name
   * @param {Object} names Array of names in different languages
   * @static
   * @returns {string} languageNeutralName Language neutral name
   */
  GrepAPI.getLanguageNeutral = function (names) {
    var languageNeutralName = '';
    names.forEach(function (nameInstance) {
      if (nameInstance.isLanguageNeutral) {
        // Set curriculum name to language neutral name
        languageNeutralName = nameInstance.name;
      } else if (languageNeutralName === '') {
        // If there is no language neutral name, set curriculum name to available name
        languageNeutralName = nameInstance.name;
      }
    });

    return languageNeutralName;
  };

  /**
   * Get curriculum from jsonData with optional filter
   * @param {Array} filterIdList Array containing curriculum IDs to filter on
   * @returns {Array} dataList Array containing filtered curriculums
   */
  GrepAPI.prototype.getCurriculumList = function (filterIdList) {
    var list = [];
    for (var i = 0; i < filterIdList.length; i++) {
      var id = filterIdList[i].split('/')[0];
      if (this.curriculums[id]) {
        list.push(this.curriculums[id]);
      }
    }
    return list;
  };

  return GrepAPI;

})(H5P.jQuery, H5P.EventDispatcher);
