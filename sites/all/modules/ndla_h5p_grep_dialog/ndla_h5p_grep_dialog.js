/*global H5P*/

if (H5P && H5P.externalDispatcher) {
  H5P.externalDispatcher.on('domChanged', function (event) {
    // Create plugin
    if (this instanceof H5P.DocumentationTool) {
      var plugin = new H5P.DocumentationToolGrepPlugin(this, event.data.$target);
      plugin.attach();
    }
  });
}

H5P.DocumentationToolGrepPlugin = (function ($, GrepDialogBox) {
  var USER_DEFINED_GOAL = 0;

  /**
   * Initialize module
   * @param {H5P.DocumentationTool} documentationTool
   * @constructor
   */
  function DocumentationToolGrepPlugin(documentationTool, $wrapper) {
    this.documentationTool = documentationTool;
    this.$wrapper = $wrapper;
  }

  DocumentationToolGrepPlugin.prototype.initGoalsPages = function () {
    var filterIds = [];
    var self = this;
    this.documentationTool.pageInstances.forEach(function (page) {
      if (page.libraryInfo.machineName === "H5P.GoalsPage") {
        var goalsPage = new GoalsPage(page, self.grepApi);
        goalsPage.attach();
        filterIds = filterIds.concat(goalsPage.getFilterIdList());
      }
    });

    return filterIds;
  };

  DocumentationToolGrepPlugin.prototype.attach = function () {
    var self = this;

    this.grepApi = new H5P.GrepAPI();
    var filterIds = self.initGoalsPages();

    // Fetch all data from API for all filters setup:
    this.grepApi.init(filterIds);
  };

  /**
   * Get lists with filtered ids
   * @returns {Array} filterIdList
   */
  function GoalsPage(page, grepApi) {
    var self = this;

    self.filteredIdList = [];
    self.prefetched = false;
    self.curriculums;

    // Set default behavior.
    page.params = $.extend(true, {
      chooseGoalText: 'Choose goal from list',
      specifyGoalText: 'Specification',
      grepDialogDone: 'Done',
      filterGoalsPlaceholder: "Filter on words...",
      commaSeparatedCurriculumList: "",
      noResultsFound: "No matching goals were found..."
    }, page.params);

    // Setup filtered IDs:
    if (page.params.commaSeparatedCurriculumList !== undefined &&
        page.params.commaSeparatedCurriculumList.length) {
      page.params.commaSeparatedCurriculumList.split(',').forEach(function (filterId, filterIndex) {
        self.filteredIdList[filterIndex] = filterId.trim().toUpperCase();
      });
    }

    grepApi.on('data-loaded', function () {
      self.grepDataLoaded = true;
      self.curriculums = grepApi.getCurriculumList(self.filteredIdList);
      if (self.dialogInstance !== undefined) {
        self.dialogInstance.populateDialogView(self.curriculums)
      }
    });

    self.attach = function () {
      var self = this;
      if (self.filteredIdList.length !== 0) {
        var $goalButtonArea = $('.goals-define', page.$inner);

        // Create predefined goal using GREP API
        H5P.JoubelUI.createSimpleRoundedButton(page.params.chooseGoalText)
          .addClass('goals-search')
          .click(function () {
            self.createGrepDialogBox();
          }).prependTo($goalButtonArea);
      }
    };

    self.getFilterIdList = function () {
      return self.filteredIdList;
    };

    self.createGrepDialogBox = function () {
      self.dialogInstance = new GrepDialogBox(page.params, grepApi.getCurriculumList(self.filteredIdList));
      self.dialogInstance.attach(page.$inner.parent().parent().parent());
      if (self.curriculums !== undefined) {
        self.dialogInstance.populateDialogView(self.curriculums);
      }
      self.dialogInstance.getFinishedButton().on('dialogFinished', function (event, data) {
        data.forEach(function (competenceAim) {
          // Set competence aim type to 0 to prevent specifications of goals
          if (!competenceAim.goalType) {
            competenceAim.goalType = USER_DEFINED_GOAL;
          }
          page.addGoal(competenceAim);
        });
      });
    };
  };

  return DocumentationToolGrepPlugin;

}(H5P.jQuery, H5P.GrepDialogBox));
