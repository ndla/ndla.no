/**
 * @file
 * @ingroup utdanning_hacks 
 */
function utdanning_hacks_temalist(nid, tid) {

    var path = "utdanning_hacks/tema/addremove/js/checked/" + tid;

    $.ajax(
        {

            type: "POST",
            url: Drupal.settings.basePath+"?q="+path,
            dataType: "text",
            data :"checkednid="+nid,
            success: function(xml)
            {

            },
            error: function(xml)
            {
                alert("Error!\n\nCould not open a connection to server.\nTry again or contact system administrator.\n\n");
            }
        });


}

