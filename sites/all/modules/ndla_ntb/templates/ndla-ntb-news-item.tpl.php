<?php
  global $base_path;
  $image_hml = "";
  if(count($news['images'])) {
    foreach($news['images'] as $image) {
      if($teaser) {
        $image_html .= "<div class='ndla-ntb-thumbnail'><img src='" . $base_path . $image['thumbnail'] . "' /></div>";
      }
      else {
        $image_html .= "<div class='ndla-ntb-image'><img src='" . $base_path . $image['medium'] . "' />";
        if($image['caption']) {
          $image_html .= "<p class='ndla-ntb-caption'>" . $image['caption'] . "</p>";
        }
        $image_html .= "</div>";
      }
    }
  }

  $date = $news['docdata']['date.issue'];
  $time = date("H:i d.m.Y", strtotime($date));
  
  $article = "";
  $headline = "";

  if($teaser) {
    $time = "<span class='ndla_ntb_date_small'>" . $time . "</span>";
    $article = $news['content'][0];
    $article = preg_replace("/(^.*:)/", "", $article);
    $article .= "<br />" . $time;
    $headline = l($news['headline'], 'ndla_ntb/' . $news['ntbid']);
    
    $article = $article; 

  }
  else {
    $article = implode("", $news['content']);
    $article = preg_replace("/(^.*:)/", "<b>\\1</b>", $article);
    $article = $image_html . $article;
    print "<span class='ndla_ntb_date_large'>" . t('Published'). ": " . $time . "</span>";
  }
  
  if($headline) {
    print $image_html . "<h3 class='ndla-ntb-headline'>" . $headline . "</h3>";
  }
  
  print "<div class='ndla-ntb-article'>" . $article . "</div>";
