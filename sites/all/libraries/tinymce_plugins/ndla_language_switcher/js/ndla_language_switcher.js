tinyMCEPopup.requireLangPack();

function init(ed) {
	lang1 = tinyMCEPopup.getWindowArg('lang1');
  document.getElementById('lang1').value = lang1;
	lang2 = tinyMCEPopup.getWindowArg('lang2');
  document.getElementById('lang2').value = lang2;
}

function createLanguageSwitcher() {
  tinyMCEPopup.editor.execCommand('mceCreateElementLangSwitcher', document.getElementById('lang1').value, document.getElementById('lang2').value);
  tinyMCEPopup.close();
}

tinyMCEPopup.onInit.add(init);