// Plugin static class
(function() {

	tinymce.create('tinymce.plugins.UtdanningObjects', {

	getInfo : function() {
		return {
			longname : 'Utdanning objects',
			author : 'ole',
			authorurl : 'utdanning.no',
			
			version : tinyMCE.majorVersion + "." + tinyMCE.minorVersion
		};
	},

	init : function(ed, url) {
		// Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('mceExample');
			// Register example button

		ed.addCommand('mceUtdanning_addsimpleframe', function() {
			tinyMCE.execCommand('mceBeginUndoLevel');
			var select = tinyMCE.activeEditor.selection.getNode();
			var selected = tinyMCE.activeEditor.selection.getContent();
				
			//If somthing is selected
			//alert(tinyMCE.selectedInstance.selection.getSelectedHTML());
				
			if(selected.length > 3){
				var test= test_if_firstdiv(select);	
				if(test==false){
					while(select.className != "frame"){
						select=select.parentNode;
					}
						
					//var html='<div class="wrapicon no_icon"><div class="spacer">&nbsp;</div><p>'+selected+'</p></div>';  //<strong>Sammarbeidsoppgave:</strong>
					//var html='<div class="wrapicon no_icon"><p>'+selected+'</p></div>';  //<strong>Sammarbeidsoppgave:</strong>
					//var html='<div class="wrapicon no_icon"><p>'+selected+'</p></div>';  //<strong>Sammarbeidsoppgave:</strong>
					//tinyMCE.execInstanceCommand(tinyMCE.activeEditor.id, 'mceInsertContent', false, html);
				}
				else{
					//var html='<div class="frame"><div class="wrapicon no_icon"><div class="spacer">&nbsp;</div><p>'+selected+'</p></div></div>';  //<strong>Sammarbeidsoppgave:</strong>
					//var html='<div class="frame"><div class="wrapicon no_icon"><p>'+selected+'</p></div></div>';  //<strong>Sammarbeidsoppgave:</strong>
				  var html='<div class="frame"><p>'+selected+'</p></div>';  //<strong>Sammarbeidsoppgave:</strong>
					tinyMCE.execInstanceCommand(tinyMCE.activeEditor.id, 'mceInsertContent', false, html);
				}
					

				//If nothing is selected
			}
			else{		
				var test= test_if_firstdiv(select);	
				if(test==false){
					while(select.className != "frame"){
						select=select.parentNode;
					}
					
					//select.innerHTML=select.innerHTML+'<div class="wrapicon no_icon"><div class="spacer">&nbsp;</div><p>Teksten begynner her</p></div>'; //<strong>Sammarbeidsoppgave:</strong>
					//select.innerHTML=select.innerHTML+'<div class="wrapicon no_icon"><p>Teksten begynner her</p></div>'; //<strong>Sammarbeidsoppgave:</strong>
					//select.innerHTML=select.innerHTML+'<div class="wrapicon no_icon"><p>Teksten begynner her</p></div>'; //<strong>Sammarbeidsoppgave:</strong>
				}
				else if(test==true){
					//var html='<div class="frame"><div class="wrapicon no_icon"><div class="spacer">&nbsp;</div><p>Teksten begynner her</p></div></div>&nbsp;'; //<strong>Sammarbeidsoppgave:</strong>
					//var html='<div class="frame"><div class="wrapicon no_icon"><p>Teksten begynner her</p></div></div>&nbsp;'; //<strong>Sammarbeidsoppgave:</strong>
					var html='<div class="frame"><p>Teksten begynner her</p></div>&nbsp;'; //<strong>Sammarbeidsoppgave:</strong>
					tinyMCE.execInstanceCommand(tinyMCE.activeEditor.id, 'mceInsertContent', false, html);
				}
			}

			tinyMCE.execCommand('mceEndUndoLevel');
			return true;
		});

		ed.addButton('utdanning_addsimpleframe', {
			title : 'Add frame',
			cmd : 'mceUtdanning_addsimpleframe',
			image : url + '/img/object.gif'
		});
	},
	
	/* not ported */
	handleNodeChange : function(editor_id, node, undo_index, undo_levels, visual_aid, any_selection) {
		if (node == null)
			return;
			
		var inst = tinyMCE.getInstanceById(editor_id);
		
		if (node.parentNode.nodeName.toLowerCase() == "ol" || node.parentNode.nodeName.toLowerCase() == "ul") {
			inst.bruktListe = true;
		}

		if (!any_selection)
		{			
			if (node.nodeName.toLowerCase() == "div" && inst.bruktListe) {// Insert a parent after a list is finished, this is a UGLY HACK
				tinyMCE.execInstanceCommand(editor_id, 'FormatBlock', true, 'p');
				tinyMCE.triggerNodeChange();
				inst.bruktListe = false;
			}
		}
	}
	
});

function test_if_firstdiv(select){
	if(select.className=="frame" || select==null){
		return false;
	}
	try{
		while(select.className!="body"){
			select=select.parentNode;
			if(select.className=="frame"){
				return false;
			}
		}
		return true;
	}catch(err){
		return true;
	}
}


// Adds the plugin class to the list of available TinyMCE plugins
tinymce.PluginManager.add('utdanning_frames', tinymce.plugins.UtdanningObjects);
})();

