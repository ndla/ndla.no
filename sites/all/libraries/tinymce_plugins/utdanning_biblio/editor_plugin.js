(function() {
	//Initialize the plugin
	tinymce.create('tinymce.plugins.UtdanningBiblio', {
	/**
	 * Returns information about the plugin as a name/value array.
	 * The current keys are longname, author, authorurl, infourl and version.
	 *
	 * @returns Name/value array containing information about the plugin.
	 * @type Array 
	 */
	getInfo : function() {
		return {
			longname : 'Utdanning Biblio Reference',
			author : 'js',
			authorurl : 'http://www.utdanning.no',
			infourl : 'http://www.utdanning.no',
			version : "1.0"
		};
	},

	init : function(editor, url) {
		// Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('mceUtdanningBiblio');
		editor.addCommand('mceUtdanningBiblio', function() {
			editor.windowManager.open({
				file : url + '/pop.php?base='+tinyMCE.activeEditor.documentBaseURI.getURI(),
				width : 380 + parseInt(editor.getLang('example.delta_width', 0)),
				height : 250 + parseInt(editor.getLang('example.delta_height', 0)),
				inline : 1
			}, {
				plugin_url : url, // Plugin absolute URL
				some_custom_arg : 'custom arg' // Custom argument
				});
		});
			
		editor.addButton('utdanning_biblio', {
			title : 'Referanse',
			cmd : 'mceUtdanningBiblio',
			image : url + "/img/utdanning_biblio.gif",
		});
		
	},

	/**
	 * Executes a specific command, this function handles plugin commands.
	 *
	 * @param {string} editor_id TinyMCE editor instance id that issued the command.
	 * @param {HTMLElement} element Body or root element for the editor instance.
	 * @param {string} command Command name to be executed.
	 * @param {string} user_interface True/false if a user interface should be presented.
	 * @param {mixed} value Custom value argument, can be anything.
	 * @return true/false if the command was executed by this plugin or not.
	 * @type
	 */
	execCommand : function(editor_id, element, command, user_interface, value) {
		function insertDiv() {
			tinyMCE.openWindow({
					file : '../../plugins/utdanning_biblio/pop.php',
					width : 480 + tinyMCE.getLang('lang_advlink_delta_width', 0),
					height : 400 + tinyMCE.getLang('lang_advlink_delta_height', 0)
				},
				{
					editor_id : editor_id,
					inline : "yes"
				});

				return true;
		}
	},

});

// Adds the plugin class to the list of available TinyMCE plugins
tinymce.PluginManager.add('utdanning_biblio', tinymce.plugins.UtdanningBiblio);
})();
