//Inser the [bib]citekey[/bib] into the tiny editor.
function insertAction(citekey) {
	tinyMCEPopup.execCommand("mceBeginUndoLevel");
	var html = "[bib]"+citekey+"[/bib]";
	
	tinyMCE.execCommand('mceInsertContent', false, html);
	tinyMCEPopup.execCommand("mceEndUndoLevel");
	tinyMCEPopup.close();
}

//Query the database.
function doSearch() {
	var keyword = $('#keyword').val();
	var title = $('#title').val();
	var year = $('#year').val();
	var author = $('#author').val();

	var querystring = "&";
	if(keyword) {
		querystring += "keyword="+keyword+"&";
	}
	if(title) {
		querystring += "title="+title+"&";
	}
	if(year) {
		querystring += "year="+year+"&";
	}
	if(author) {
		querystring += "author="+author+"&";
	}

	$.ajax({
		type: "POST",
		url: tinyMCE.activeEditor.documentBaseURI.getURI()+"?q=utdanning_biblio_ajax_search_node"+querystring,
		dataType: "xml",
		data: "",
		success: function(xml) { 
			html = '<ul>';
			$("container",xml).each(function(i) {
				$("reference", this).each(function(j) {
					title = $("book", this).text();
					citekey = $("book", this).attr("citekey");
					year = $("book", this).attr("year");
					nid = $("book", this).attr("nid");
					html += "<li> <a href='javascript:void(0)' onclick='insertAction("+citekey+")'> " +title + ", " + year + " (Node: " + nid + ")</a>";
				});
				html += '</ul>';
				$('#nodes_div').html(html);
			});
		},
   	
   	error: function(xml) {
			alert("Error!\n\nCould not open a connection to database.\nTry again or contact system administrator.");
		}
	});	
}
