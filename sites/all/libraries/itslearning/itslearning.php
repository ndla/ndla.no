<?php
  header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title>Its learning Embed</title>

<script type="text/javascript">

    function setCookie(CookieName, CookieVal, CookieLifetime, CookiePath, CookieDomain, CookieSecure) {
            var CookieText = escape(CookieName) + '=' + escape(CookieVal); //escape() : Encodes the String
            CookieExp = new Date();
            CookieExp.setMinutes(CookieExp.getMinutes() + CookieLifetime);
            CookieText += (CookieExp ? '; EXPIRES=' + CookieExp.toGMTString() : '');
            CookieText += (CookiePath ? '; PATH=' + CookiePath : '');
            CookieText += (CookieDomain ? '; DOMAIN=' + CookieDomain : '');
            CookieText += (CookieSecure ? '; SECURE' : '');
            document.cookie = CookieText;
    }

    function getCookie(CookieName) {
            var CookieVal = null;
            if(document.cookie) {
                    var arr = document.cookie.split((escape(CookieName) + '='));
                    if(arr.length >= 2) {
                            var arr2 = arr[1].split(';');
                            CookieVal  = unescape(arr2[0]); //unescape() : Decodes the String
                    }
            }
            return CookieVal;
    }

    function deleteCookie(CookieName) {
            var tmp = getCookie(CookieName);
            if(tmp) {
                setCookie(CookieName,'',-10000,'/'); //Used for Expire
            }
    }

</script>

</head>
<body>
<?php

  if (isset($_GET['nodeid'])) {
    $embed_service_url = 'http://'.$_SERVER['SERVER_NAME'].'/ndla_utils/itslearning_embed_details/'.$_GET['nodeid'];
    $html = file_get_contents($embed_service_url);

    echo '
<form method="post" action="" id="mainForm">
        <input type="hidden" name="InsertedHtml" value="' . htmlspecialchars($html) . '">
</form>

<script type="text/javascript">
        if (getCookie("itslearningembed")) {
                document.getElementById("mainForm").action = getCookie("itslearningembed");
                deleteCookie("itslearningembed");
                document.getElementById("mainForm").submit();
        } else {
                alert("An error occurred submitting data to itslearning");
        }
</script>
';

  } else {
    if (isset($_GET['PostTo'])) {
      echo '
<script type="text/javascript">
        setCookie("itslearningembed", "' . $_GET['PostTo'] . '", 15, "/");
        document.location.href = "http://' . $_SERVER['SERVER_NAME'] . '";
</script>
';
    } else {
      echo 'An error occurred embedding things to itslearning';
    }
  }
?>
</body>
</html>
