<div class='front-header'>
  <img src='<?php echo drupal_get_path('theme','ndla_mobile'); ?>/img/logo.png' />
  <p class='slogan'><?php print t('Open Educational Resources For Secondary Schools'); ?></p>
</div>
<?php if($vars['courses']): ?>
<div class='subjects'>
  <?php print $vars['courses']; ?>
</div>
<?php endif; ?>

<?php if($vars['applications']): ?>
<div class="applications-wrapper clearfix">
  <?php print $vars['applications']; ?>
</div>
<?php endif; ?>

<?php if($vars['new_content']): ?>
<div class='subjects'>
  <?php print $vars['new_content']; ?>
</div>
<?php endif; ?>

<?php if($fb_js = variable_get('ndla_utils_facebook_js', '')) print $fb_js; ?>
<?php if($vars['facebook']): ?>
<div class='subjects'>
  <?php print $vars['facebook']; ?>
</div>
<?php endif; ?>

<?php if($vars['tweets']): ?>
<div class='subjects'>
  <?php print $vars['tweets']; ?>
</div>
<?php endif;