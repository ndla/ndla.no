<?php if (!$page): ?>
  <article id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clearfix">
<?php endif; ?>
  <?php if ($picture || $submitted || !$page): ?>
    <?php if (!$page): ?>
      <header>
	<?php endif; ?>
      <?php print $picture ?>

	  <?php if (!$page): ?>
        <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
      <?php endif; ?>

	  <?php if ($submitted): ?>
        <span class="submitted"><?php print $submitted; ?></span>
      <?php endif; ?>

    <?php if (!$page): ?>
      </header>
	<?php endif; ?>
  <?php endif;?>

  <div class='node-tabs-wrapper'>
    <ul class='node-tabs'>
      <li class='active' data-target=".tab-content">
        <span>
          <?php print t('Learning resource'); ?>
        </span>
      </li>
      <li data-target=".tab-partof">
        <span>
          <?php print t('Inngår i'); ?>
        </span>
      </li>
      <li data-target=".tab-context">
        <span>
          <?php print t('Context'); ?>
        </span>
      </li>
    </ul>
  </div>

  <div class="content tab-content node-tabs">

    <?php if ($node->field_ingress[0]['safe'] && $node->type != 'fag'): ?>
      <div class="ingress<?php print (isset($node->field_ingress_bold) && $node->field_ingress_bold[0]['value']) ? ' ingress-bold' : '' ?>">
        <?php print $ingress_image; ?>
        <?php print $node->field_ingress[0]['safe']; ?>
        <div class="clear"></div>
      </div>
    <?php endif; ?>
    
    <?php print $content; ?>
  </div>
<?php if (!$page): ?>
  </article> <!-- /.node -->
<?php endif;?>
