<!DOCTYPE html>
<!--[if lt IE 7]> <html class="ie6 ie" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <![endif]-->
<!--[if IE 7]>    <html class="ie7 ie" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <![endif]-->
<!--[if IE 8]>    <html class="ie8 ie" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <!--<![endif]-->
<head>
  <?php print $head; ?>
  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
  
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
  <title><?php print $head_title; ?></title>
  <script type="text/javascript" async src="<?php print $base_path.path_to_theme(); ?>/js/modernizr.js"></script>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
  <?php
  if(arg(0) == 'admin'):
    $css = drupal_add_css(drupal_get_path('module', 'system') . '/system.css', 'theme');
    print drupal_get_css($css);
    print drupal_get_js();
  else:
    print $styles;
    print $scripts;
  endif;
  ?>

  <!-- IE Fix for HTML5 Tags -->
  <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->


</head>

<body class="<?php print $body_classes; ?>">

  <header id="header" role="banner" class="header">
    <div data-target="sidebar-left" class="btn left open"><i class="icon-reorder"></i></div>
    <?php echo theme('ndla_mobile_login')?>

    <nav id="top-navigation" class="h-menu clearfix">
      <ul id="top-menu" class="links clearfix">
        <li class="first">
          <a href="#" class="trigger" data-target="header_search"><i class="icon-search"></i></a>
        </li>
        <li>
          <?php if(drupal_is_front_page()): ?>
            <a href="<?php print url("m/resourcemap"); ?>"><i class="icon-map-marker"></i></a>
          <?php else: ?>
            <a href="#" class="trigger" data-target="course_selector"><i class="icon-tags"></i></a>
          <?php endif; ?>
        </li>
        <li>
          <a href="#" class="trigger" data-target="language_selector"><i class="icon-globe"></i></a>
        </li>
      </ul>
    </nav>

    <?php print $header; ?>

    <?php print $ndla_mobile_course_dropdown; ?>
    <?php print $ndla_mobile_search_dropdown; ?>
    <?php print $ndla_mobile_language_dropdown; ?>
  </header>

  <aside id="sidebar-left" role="complementary" class="sidebar clearfix hidden">
    <?php print $left; ?>
  </aside> <!-- /sidebar-left -->

  <section id="main" role="main" class="clearfix hidden">
    <div class="content-wrapper">
      <?php if (!empty($messages)): print $messages; endif; ?>
      <?php if (!empty($mission)): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
      <?php if (!empty($title)): ?><h1 class="page-title" id="page-title"><?php print $title ?></h1><?php endif; ?>
      <?php if ($author): ?><div class="author"><?php print $author ?></div><?php endif ?>
      <?php if (!empty($tabs)): ?><div class="tabs-wrapper clearfix"><?php print $tabs; ?></div><?php endif; ?>
      <?php if (!empty($help)): print $help; endif; ?>


      <?php if (!empty($content_top) && arg(0) != 'search'): ?>
        <div id="content_top">
          <?php print $content_top; ?>
        </div>
      <?php endif; ?>


      <?php print $content; ?>

      <?php if($partof): ?>
        <div class='tab-partof content node-tabs'><?php print $partof; ?></div>
      <?php endif; ?>


      <?php if($context): ?>
        <div class='tab-context content node-tabs'><?php print $context; ?></div>
      <?php endif; ?>



      <?php if (!empty($content_bottom) && arg(0) != 'search'): ?>
        <div id="content_bottom">
          <?php print $content_bottom; ?>
        </div>
      <?php endif; ?>
    </div> <!-- /.content_wrapper -->

    <footer id="footer" role="contentinfo" class="clearfix">
      <?php if (!empty($footer)): print $footer; endif; ?>
    </footer>
  </section>


  <aside id="sidebar-right" role="complementary" class="sidebar">
    <div id="user-header" class="header clearfix">
      <div data-target="sidebar-right" class="btn left close"><i class="icon-remove"></i></div>
      <div class="h-title"><?php print t('User'); ?></div>
    </div>

    <?php print $right; ?>

  </aside> <!-- /sidebar-right -->

  <?php print $closure ?>

</body>
</html>
