var jq17 = jQuery.noConflict(true);

// PLUGINS!!!

/*! jQuery list 1.2.4 (c) github.com/teamdf/jquery-list | opensource.teamdf.com/license */
(function(e){var k=true,m={headerSelector:"dt",zIndex:1,fakeHeaderClass:"ui-list-fakeheader"},j={log:function(){if(k&&window.console&&window.console.log)try{window.console.log.apply(console,arguments)}catch(b){window.console.log(arguments)}},updateHeader:function(b,a){var c=b.data("list");if(c){c.fakeHeader=a.clone().removeAttr("id").addClass(c.settings.fakeHeaderClass);c.fakeHeader.css({position:"absolute",top:0,width:c.headers.width(),zIndex:c.settings.zIndex});b.data("list",c).children(0).eq(0).replaceWith(c.fakeHeader)}},
events:{resize:function(){var b=e(this),a=b.data("list");if(a){a.fakeHeader.width(a.headers.width());a.wrapper.css("maxHeight",b.css("maxHeight"))}},scroll:function(){var b=e(this),a=b.data("list");if(a){var c=null,d=a.headers.eq(a.currentHeader),i=a.currentHeader>=a.headers.length-1?null:a.headers.eq(a.currentHeader+1),f=a.currentHeader<=0?null:a.headers.eq(a.currentHeader-1),g=false;a.containerTop=b.offset().top+parseInt(b.css("marginTop"),10)+parseInt(b.css("borderTopWidth"),10);a.fakeHeader.css("top",
0);if(f!==null){var h=d.offset().top;d=d.outerHeight();if(h>a.containerTop){a.fakeHeader.css("top",h-d-a.containerTop);j.updateHeader(b,f);a.currentHeader-=1;g=true}if(h-d>a.containerTop){a.fakeHeader.css("top",0);c=a.currentHeader-1}}if(i!==null){h=i.offset().top;d=i.outerHeight();h-d<a.containerTop&&a.fakeHeader.css("top",h-d-a.containerTop);if(h<a.containerTop){a.fakeHeader.css("top",0);c=a.currentHeader+1}}if(c!==null){g=a.headers.eq(c);a.currentHeader=c;j.updateHeader(b,g);g=true}c=a.wrapper.scrollTop()>=
a.wrapper.prop("scrollHeight")-a.wrapper.height();if(g||c||a.max&&!c)b.trigger("headingChange",[a.currentHeader,a.headers.eq(a.currentHeader),c]);a.max=c;b.data("list",a)}}}},l={init:function(b){return e(this).each(function(){var a=e.extend({},m);typeof b=="object"&&e.extend(a,b);var c=e(this),d=c.data("list");if(!d){d={target:c,wrapper:c.wrapInner('<div class="ui-list" />').find(".ui-list"),settings:a,headers:[],containerTop:0,currentHeader:0,fakeHeader:null,scrolllist:[],original:{position:"",overflowX:"",
overflowY:""},max:false};c.addClass("-list-container").css({position:c.css("position")=="absolute"?"absolute":"relative",overflowY:"hidden"});d.headers=c.find(d.settings.headerSelector);d.fakeHeader=d.headers.eq(0).clone().removeAttr("id").addClass(d.settings.fakeHeaderClass);d.wrapper.bind("scroll.list",e.proxy(j.events.scroll,c)).css({height:"100%",maxHeight:c.css("maxHeight"),overflowY:"scroll",position:"relative"});d.fakeHeader.css({position:"absolute",top:0,width:d.headers.width(),zIndex:d.settings.zIndex});
e(window).bind("resize.list",e.proxy(j.events.resize,c));c.data("list",d).prepend(d.fakeHeader)}})},header:function(){var b=e(this).data("list");if(b)return b.currentHeader},scrollTo:function(b,a,c,d){return this.each(function(){var i=e(this),f=i.data("list");if(f)if(b!==undefined&&!isNaN(b)&&b>=0&&b<f.headers.length){var g=f.headers.eq(b),h=g.position().top+f.wrapper.scrollTop()+parseInt(g.css("borderTopWidth"),10)+parseInt(g.css("borderBottomWidth"),10);if(a)f.wrapper.stop().animate({scrollTop:h},
a,c,d);else{f.wrapper.stop().scrollTop(h);f.currentHeader=b;j.updateHeader(i,g);i.trigger("headingChange",[b,g]);i.data("list",f)}}})},option:function(b,a){var c=e(this),d=c.data("list");if(d){if(typeof b=="undefined")return d.settings;if(!b in d.settings)return false;if(typeof a=="undefined")return d.settings[b];else{d.settings[b]=a;return c}}},version:function(b){if(b){var a="1.2.3".split(".");b=(Number(a[0])||1)+"";var c=(Number(a[1])||0)+"";a=(Number(a[2])||0)+"";return Number("000".slice(0,3-
b.length)+b+"000".slice(0,3-c.length)+c+"000".slice(0,3-a.length)+a)}else return"list v1.2.3"},destroy:function(){return this.each(function(){var b=e(this),a=b.data("list");if(a){a.wrapper.children().unwrap();a.fakeHeader.remove();b.css(a.original).removeData("list").removeClass("-list-container").unbind(".list")}})}};e.fn.list=function(b){if(l[b])return l[b].apply(this,Array.prototype.slice.call(arguments,1));else if(typeof b==="object"||!b)return l.init.apply(this,arguments);else e.error("Method "+
b+" does not exist on jQuery.list")};e.list={};e.list.log=function(b){if(b!==undefined)k=b;return k}})(jq17, document);


// Custom
(function($, document) {
    jQuery(document).ready(function() {
      $('.contentbrowser img').each(function() {
        if($(this).attr('width')) {
          $(this).parents('.contentbrowser').first().attr('width', $(this).attr('width'));
        }
      });


      $('iframe.istribute_video').each(function() {
        $(this).attr('data-aspect', (parseInt(this.style.width, 10) / parseInt(this.style.height, 10)));
      });

      resizeIstributePlayer();

      var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
      var $sidebarLeft = $('#sidebar-left');
      var $main = $('#main');

      /*
      * Toggle the class 'hidden' to display the left sidebar.
      *
      * @param {string} toggle : CSS attribute manipulator
      */
      var leftMenuToggle = function (toggle) {
        $sidebarLeft[toggle]('hidden');
        $main[toggle]('hidden');
      };

      // Show the left sidebar if there is room for it on load.
      if (width > 960) {
        leftMenuToggle('removeClass');
      }

      // Avoid transition on page load.
      setTimeout(function () {
        $sidebarLeft.addClass('transition');
        $main.addClass('transition');
      }, 100);

      /*
      * When the window is resized, show the left sidebar if
      * there is room for it.
      */
      $(window).on("resize", function () {
        var _width = window.innerWidth;

        if(_width > 960) {
          leftMenuToggle('removeClass');
        } else {
          leftMenuToggle('addClass');
        }
      });

      $(window).on("resize", resizeIstributePlayer);

      function resizeIstributePlayer() {
        $('iframe.istribute_video').each(function() {
          var aspect = $(this).attr('data-aspect');
          $(this).height($(this).width() / aspect);
        });
      }


      if(!Modernizr.touch) {
        $('.btn.left.open').click(function () {
          leftMenuToggle('toggleClass');
        });
      } else {
        Hammer('.btn.left.open').on("tap", function () {
          leftMenuToggle('toggleClass');
        });
      }

      // Header dropdown's
        $('.trigger').click(function () {
            var target = '#'+$(this).data('target');
            if (target == undefined || target == null ) {
                //alert('no target')
            } else {
                $('.h-menu ul li a').removeClass('open');
                if ($(target).hasClass('active')) {
                    $(target).removeClass('active'); 
                } else {
                    $('.dropdown').removeClass('active');
                    $(target).addClass('active'); 
                    $(this).addClass('open');
                }
                updateDropdown();
                return false;
            };
        })

        function updateDropdown() {
            var legend_width = $('#course_selector .scrollable .ui-list fieldset:last legend').outerWidth(true);
            $('.ui-list-fakeheader').width(legend_width);
        }

        // Sticky headers
        $('#course_selector .scrollable').list({ headerSelector : 'legend' });

        if($('.tab-context').length != 0) {
          $('ul.node-tabs').show();
          $('ul.node-tabs li').click(function() {
            $('ul.node-tabs li').removeClass('active');
            $(this).addClass('active');
            $('div.node-tabs').hide();
            $($(this).data('target')).show();
          });
        }
    });
}(jq17, document));

Drupal.behaviors.user_dropdown = function(context) {
  if(typeof ndla_auth == 'undefined' || $('#user_dropdown_toggler').length == 0) {
    return;
  }

  /**
   * The behaviour is called twice. Once with
   * document as context and once with body
   * as context.
   */
  if(context == document) {
    jq17(document).click(function(event) {
      if(event.hasOwnProperty('originalEvent')) { //Check if this is a real click.
        if (jq17(event.target).closest('.right-box').get(0) == null && jq17('.right-box .login-dropdown:visible').length > 0) {
          jq17('#user_dropdown_toggler').click();
        }
      }
    });
  }

  var orig_width = 0;
  var full_width = 500;

  var clickHandler = function() {
    if (orig_width == 0) {
      // Set orig_width the first time the event is fired
      orig_width = jq17('.right-box .login').width();
    }

    // Slide to shink/expand to the left
    if(jq17('.right-box .login-dropdown:visible').length == 0) {
      jq17('.right-box .login-dropdown').animate({width: full_width + 'px'}, 100).css('overflow', 'visible');
    }
    else {
      jq17('.right-box .login-dropdown').animate({width: (orig_width + 1) + 'px'}, 100).css('overflow', 'visible');
    }

    // Slide up/down
    jq17('.right-box .login-dropdown').slideToggle(100, function() {
      if(jq17('.right-box .login-dropdown:visible').length == 1) {
        jq17('#user_dropdown_toggler i.toggler').attr('class', 'toggler icon-caret-up');
      }
      else {
        jq17('#user_dropdown_toggler i.toggler').attr('class', 'toggler icon-caret-down');
      }
    });
  };

  ndla_auth(function(auth) {
    //Set the correct login link
    jq17('#seria_login_link').attr('href', auth.login_url());
    jq17('#seria_logout_link').attr('href', auth.logout_url(window.location.href));
    jq17('#seria_settings_link').attr('href', auth.user_settings_url());
    //User is logged in
    if(auth.authenticated()) {
      jq17('.logged_in').fadeIn();
      jq17('#user_dropdown_toggler .seria_user_name').html(auth.display_name());
      var profile_pic = auth.profile_picture(45,45);
      if(profile_pic) {
        $('jq17user_dropdown_toggler').addClass('has_profile');
        $('jq17user_dropdown_toggler i.icon-user').remove();
        $('jq17user_dropdown_toggler').prepend('<img class="profile-image" src="' + profile_pic + '" />');
      }

    // Make sure click is not registered several times.
    jq17('#user_dropdown_toggler').unbind('click');
    jq17('#notification_bubble').unbind('click');
    jq17('#user_dropdown_toggler').click(clickHandler);
    jq17('#notification_bubble').click(clickHandler);
    jq17('#notification_bubble').css('cursor', 'pointer');
    }
    else {
      jq17('.not_logged_in').fadeIn();
    }

    //Link to min.ndla.no
    if(Drupal.settings.ndla_msclient_services_url) {
      jq17('#myfav_link').attr('href', Drupal.settings.ndla_msclient_services_url);
    }
    else {
      jq17('#myfav_link').remove();
    }
  });
}

Drupal.behaviors.ndla_language_switcher = function() {
  $ = jq17;
  $('.ndla_language_switcher').each(function() {
    if (!$(this).data("processed") == "1") {
      $(this).data("processed", "1");
      i=0;
      var buttons = [];
      var $container = $(this);
      var salt = $container.attr('data-salt');
      $ul = $('<ul></ul>');
      $container.prepend($ul);
      $(this).find('> div').each(function() {
        ++i;
        $li = $('<li><a href="#lang' + i + '_' + salt + '">' + Drupal.settings.ndla_utils.languages[$(this).attr('lang')] + '</a></li>');
        $ul.append($li);
      });
      $('.ndla_language_switcher').tabs();
      // remove ui-widget classes on language switchers to avoid unwanted styling
      $('.ndla_language_switcher').removeClass('ui-widget ui-widget-content');
    }
  });
}