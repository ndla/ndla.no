(function($){
    $.fn.flyin = function(settings) {
        this.each(function() {
            //hammer_settings = {
            //    drag_min_distance : 30,
            //    drag_max_touches  : 1,
            //};
            //
            //this_hammer = $(this).hammer(hammer_settings);
            var flyin_settings = settings;
            flyin_settings.drag_container = $(this);

            
            flyin_settings.left_screen_percentage = 1 - $(flyin_settings.sidebar_left_selector).width() / $(window).width();
            flyin_settings.right_screen_percentage = 1 - $(flyin_settings.sidebar_right_selector).width() / $(window).width();
            
            var flyin_variables = {
                'flyin_from_left' : false, // rollover
                'flyin_from_right' : false, // negative rollover
                'left_menu_active' : false,
                'right_menu_active' : false,
                'moving' : false,
                'touch_offset' : undefined
            };

            var animate_container = function(ev, animation_speed) {
                var base_value = 0;
                if(animation_speed === undefined) {
                    animation_speed = flyin_settings.animation_speed;
                }
                
                flyin_variables.left_menu_active = false;
                flyin_variables.right_menu_active = false;

                if(flyin_variables.flyin_from_left) {
                    base_value = $(window).width() - ($(window).width() * flyin_settings.left_screen_percentage);
                    
                    flyin_variables.left_menu_active = true;
                    if(typeof flyin_settings.sidebar_left !== 'undefined') {
                        flyin_settings.sidebar_left.activated();
                    }
                    $(flyin_settings.sidebar_left_selector).css('z-index', 2);
                    $(flyin_settings.sidebar_right_selector).css('z-index', 1);
                }

                if(flyin_variables.flyin_from_right) {
                    base_value = 0 - $(window).width() * (1 - flyin_settings.right_screen_percentage);
                    flyin_variables.right_menu_active = true;
                    if(typeof flyin_settings.sidebar_right !== 'undefined') {
                        flyin_settings.sidebar_right.activated();
                    }
                    $(flyin_settings.sidebar_left_selector).css('z-index', 1);
                    $(flyin_settings.sidebar_right_selector).css('z-index', 2);
                }
                
                flyin_settings.drag_container.animate({
                    left: base_value,
                }, animation_speed, function(){
                    if(!flyin_variables.flyin_from_left && !flyin_variables.flyin_from_right) {
                        if(flyin_settings.deactivated && {}.toString.call(flyin_settings.deactivated) == '[object Function]') {
                            flyin_settings.deactivated();
                        }
                    }
                    flyin_variables.touch_offset = undefined;
                    flyin_variables.flyin_from_left = false;
                    flyin_variables.flyin_from_right = false;
                    flyin_variables.moving = false;
                });
            }

            $(flyin_settings.open_left_button).click(function(ev) {
                flyin_variables.flyin_from_left = true;
                animate_container();
            });

            $(flyin_settings.open_right_button).click(function(ev) {
                flyin_variables.flyin_from_right = true;
                animate_container();
            });

            $(flyin_settings.close_button).click(function(ev) {
                flyin_variables.flyin_from_right = false;
                flyin_variables.flyin_from_left = false;
                animate_container();
            })

            // Listen for orientation changes
            window.addEventListener("orientationchange", function() {
                // Announce the new orientation number
                flyin_variables.flyin_from_right = false;
                flyin_variables.flyin_from_left = false;
                animate_container(0);
            }, false);

            /*
            this_hammer.on("dragend", animate_container);

            this_hammer.on("dragleft dragright", function(ev) {
                var transitionNames = {
                    'WebkitTransition' : '-webkit-transition',
                    'MozTransition'    : '-moz-transition',
                    'OTransition'      : '-o-transition',
                    'transition'       : 'transition'
                }

                trans_name = Modernizr.prefixed('transition');
                $(flyin_settings.drag_container).css(trans_name, 'all 0 ease-in-out');
                ev.gesture.preventDefault();

                var touches = ev.gesture.touches;
                var width = $(window).width();
                var breakpoint = width * flyin_settings.breakpoint;
                var negative_breakpoint = 0 - breakpoint;

                for(var t=0,len=touches.length; t<len; t++) {
                    container_offset = flyin_settings.drag_container.offset()['left'];
                    no_menu_active = (!(flyin_variables.right_menu_active || flyin_variables.left_menu_active));

                    if( container_offset < 0 && flyin_variables.moving == false ) {
                        if(no_menu_active) {
                            $(flyin_settings.sidebar_left_selector).css('z-index', 1);
                            $(flyin_settings.sidebar_right_selector).css('z-index', 2);
                        }
                        flyin_variables.moving = true;
                    }

                    if(container_offset > 0 && flyin_variables.moving == false ) {
                        if(no_menu_active) {
                            $(flyin_settings.sidebar_left_selector).css('z-index', 2);
                            $(flyin_settings.sidebar_right_selector).css('z-index', 1);
                        }
                        flyin_variables.moving = true;
                    }

                    offset = (touches[t].pageX - container_offset);
                    
                    // Is the menu moving in from the left
                    flyin_variables.flyin_from_left = (container_offset > breakpoint);
                    // or right
                    flyin_variables.flyin_from_right = (container_offset < negative_breakpoint && flyin_variables.right_menu_active == false);

                    // Is either side active
                    if(flyin_variables.left_menu_active || flyin_variables.right_menu_active) {
                        flyin_variables.flyin_from_left = false;
                        flyin_variables.flyin_from_right = false;
                    }
                    
                    if(flyin_variables.touch_offset == undefined) {
                        flyin_variables.touch_offset = offset;
                    }

                    right_max = $(window).width() - ($(window).width() * flyin_settings.left_screen_percentage - 0.01);
                    left_max = 0 - $(window).width() * (1 - flyin_settings.right_screen_percentage + 0.01) ;
                    current_left = touches[t].pageX - flyin_variables.touch_offset;

                    if(current_left <= right_max && current_left >= left_max) {
                        flyin_settings.drag_container.css({
                            left: current_left,
                        });
                    }
                }
                flyin_settings.drag_container.css(trans_name, 'all 0.4 ease-in-out');
            });
            */
        });
    }
}(jq17));