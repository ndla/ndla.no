<div id="login" class="frame">
  <?php
    $scheme = parse_url(url('<front>', array('absolute' => TRUE)), PHP_URL_SCHEME);
    $loginDestination = rawurlencode($scheme.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
  ?>
  <?php if (!variable_get('exercise_allow_anonymous_users', 0)): ?>
  <?php
  if(!variable_get('ndla_utils_auth_enable', TRUE)) {
    echo '<p>' . t('Login has been disabled for the momet.') .'</p>';
  }
  else {?>
    <p><?php print t('<a href="@url">Log in</a> to use this application.', array('@url' => htmlspecialchars(variable_get('ndla_utils_auth_server_url', 'https://auth.ndla.no')).'/login.php?continue='.htmlspecialchars($loginDestination))); ?></p>
  <?php } ?>
  <?php else: ?>
    <p><?php print t('<a href="@url">Log in</a> to preserve your training plan.', array('@url' => htmlspecialchars(variable_get('ndla_utils_auth_server_url', 'https://auth.ndla.no')).'/login.php?continue='.htmlspecialchars($loginDestination))); ?></p>
    <p><?php print t('You can also create a training plan <a href="#" class="continue">without logging on</a>. The training plan will be gone when you exit the training planner, but you can download the plan you make as a document before exiting.') ?></p>
  <?php endif ?>
</div>