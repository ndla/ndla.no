<div id="header" class="clearfix">
  <div class="center-layout clearfix">
  <?php if ($vars['logo']): ?>
    <div class="logo" role="banner">
      <a href="<?php print $vars['front_page'] ?>" title="<?php print t('To start page for'); print ' '; print $vars['site_name']?>" accesskey="1">
        <img src="<?php print $vars['logo'] ?>" alt="<?php print t('To start page for'); print ' '; print $vars['site_name'] ?>"/>
      </a>
      <!-- start fag-title -->
      <<?php print $vars['course_title_tag']?> id="fag-title">
        <?php if ($vars['fag']): ?>
          <?php if ($vars['fag']->ndla_utils_ndla2010_theme_fag_header): ?>      
            <div class="fag-title-wrap">
              <a href="<?php print url('node/' . $vars['fag']->nid, array('ndla_utils_query_set' => TRUE)) ?>" class="fag-title">
                <?php print t($vars['term']->name); ?>
              </a>
              <a href="#" class="coursebrowser_toggler arrow js" onclick='$(".coursebrowser").toggle()'><i class='big-icon coursebrowser_toggler fa fa-caret-down'></i></a>
            </div>
          <?php else: ?>
            <div class="fag-title-wrap white">
              <a href="<?php print url('node/' . $vars['context_id'], array('ndla_utils_query_set' => TRUE)) ?>" class="fag-title">
                <?php print $vars['banner_name'] ?>
              </a>
              <a href="#" class="arrow js" onclick='$(".coursebrowser").toggle()'><i class='big-icon coursebrowser_toggler fa fa-caret-down'></i></a>
            </div>
          <?php endif ?>
          <?php if ($vars['fag']->ndla_utils_under_development): ?>
            <div id="indev"><?php print t('Under development') ?></div>
          <?php endif ?>
        <?php else: ?>
          <?php $is_ndlaktuelt = (!empty($vars['node']) && $vars['node']->type == 'aktualitet') || (!empty($_GET['im_vid_100004']) && is_array($_GET['im_vid_100004']) && count($_GET['im_vid_100004']) == 1) && isset($_GET['im_vid_100004'][0]) && $_GET['im_vid_100004'][0] == variable_get('ndla_utils_ndla2010_theme_actuality_content_type', 0); 
          if ($is_ndlaktuelt): ?>
            <a href="<?php print url('search/apachesolr_search/', array('query' => 'sort_by=created&im_vid_100004%5B%5D=' . variable_get('ndla_utils_ndla2010_theme_actuality_content_type', 0) . '&language%5B%5D=und&language%5B%5D=' . $vars['language']->language)) ?>" title="<?php print t('See more actualities') ?>">
              <img src="<?php print $vars['base_path'] . drupal_get_path('theme', 'ndla2010') ?>/img/ndlaktuelt.png" alt=""/>
            </a>
          <?php endif ?>
          <a href='javascript:void(0)' onclick='$(".coursebrowser").toggle()' class="coursebrowser_toggler"><?php print t("Choose subject"); ?></a>
          <a href="#" class="coursebrowser_toggler arrow js no-subject" onclick='$(".coursebrowser").toggle()'><i class='big-icon coursebrowser_toggler fa fa-caret-down'></i></a>
        <?php endif ?>
      </<?php print $vars['course_title_tag']?>>
      <!-- end fag-title -->
    </div>
  <?php endif ?>
    <ul class="links">
      <li><?php print $vars['language_switcher'] ?></li>
      <li class='pipe'>|</li>
      <li>
        <?php print l(t('Help'), theme_get_setting('help_url'), array(
          "language" => $vars['language'],
          'attributes' => array(
            'title' => t('Help on how to use the NDLA web-site'),
            'accesskey' => '6',
            )
          ));?>
      </li>
    </ul>
    <div class='coursebrowser' id='subjects'>
      <?php print ndla2010_get_three_column_courses(); ?>
    </div>
      <?php if (!empty($vars['fag']->ndla_utils_ndla2010_theme_fag_header) && $vars['fag']->ndla_utils_ndla2010_theme_fag_header): ?>
        <img class="course_img" src="<?php print $vars['base_path'] . $vars['fag']->ndla_utils_ndla2010_theme_fag_header['filepath'] ?>" alt=""/>
      <?php endif ?>
    <?php print theme('ndla2010_black_boxes'); ?>
  </div>
</div>
<div id="navigation">
  <div class="center-layout">
  <?php if(arg(0) != 'search'): ?>
    <div id="search" role="search">
      <?php print $vars['search_box'] ?>
    </div>
        
    <a href='javascript:void(0)' class='resourcemap-button'><i class="fa fa-globe"></i><?php print t('Resource map') ?></a>
    
    <?php if (!empty($vars['grep_enabled']) && $vars['grep_enabled']): ?>
      <a id="grep" class="tray-tab" href="javascript:void(0);">
        <i class='fa fa-book'></i>
          <?php print t('Curriculum - GREP'); ?>
      </a>
    <?php endif  ?>
    
    
  <?php endif ?>
  <?php if (isset($vars['context_id'])): ?>
    <?php if ($vars['menu_enabled'] || $vars['topic_menu']): ?>
      <ul id="left-menu-tabs" class="left-menu-active">
        <?php if ($vars['menu_enabled']): ?>
          <li id="left-menu-menu-tab" class="left-menu-tab first<?php if (!$vars['topic_menu']) print ' left-menu-tab-selected' ?><?php if ($vars['default_tab'] != 'topics') print ' tab-default' ?>">
            <span class="ndla2010-tab-center"><a href="#" class="js"><?php print $vars['menu_tab_name'] ?></a></span>
          </li>
        <?php endif ?>
        <?php if ($vars['topic_menu']): ?>
          <li id="left-menu-topics-tab" class="left-menu-tab<?php if (!$vars['menu_enabled']) print ' first' ?> left-menu-tab-selected">
            <span class="ndla2010-tab-center"><a href="#" class="js"><?php print $vars['topic_menu_tab_name'] ?></a></span>
          </li>
        <?php endif ?>
      </ul>
    <?php endif ?>
    <ul id="tray-tabs"<?php if ($vars['menu_enabled'] || $vars['topic_menu']) print ' class="no-left"'; ?>>
      <?php if ($vars['topics_enabled']): ?>
        <li id="topics" class="tray-tab<?php if ($vars['default_tab'] == 'topics') print ' tab-default'; if ($vars['expanded_tab']  == 'topics') print ' click' ?>">
          <span class="ndla2010-tab-center"><a href="#" class="js"><?php print $vars['topics_tab_name'] ?></a></span>
        </li>
      <?php endif ?>
    </ul>
  <?php endif ?>
  </div>
</div>
