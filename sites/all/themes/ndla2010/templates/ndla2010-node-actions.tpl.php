<?php if ($node_id): ?>
  <ul id="node-actions">
    <?php if ($node_id): ?>
      <?php if($embed): ?>
      <li class='first'>
        <a class='embed' href="javascript:void(0)" onclick="loadEmbedCode(this);" class="embed" title="<?php print t('Embed code for this page');?>"><i class='fa fa-external-link'></i> <?php print t('Embed'); ?></a>
      </li>
      <?php endif; ?>
      <li>
        <a class="easyreader" href="<?php print url('easyreader/' . $node_id) ?>" rel='lightframe'>
          <i class='fa fa-book'></i> <?php print t('Easy Reader') ?>
        </a>
      </li>
      <?php endif ?>
    
    <li <?php print (!$embed) ? ' class="first"' : ''; ?>>
        <a href="<?php print url('print/' . $node_id) ?>" class="print" title="<?php print t('Print') ?>">
          <i class='fa fa-print'></i> <?php print t('Print') ?>
        </a>
      </li>
  </ul>
<?php endif ?>