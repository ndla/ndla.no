<div id="right-wrapper">
  <?php if($related_content): ?>
    <h2 class='tool-heading'><?php print t('Context'); ?></h2>
    <?php print $related_content; ?>
  <?php endif; ?>

  <?php if($tools): ?>
    <h2 class='tool-heading'><?php print t('Tools'); ?></h2>
    <?php print $tools; ?>
  <?php endif; ?>

  <?php if ($editor): ?>
    <div id="right-red-folder"></div>
      <?php print $editor ?>
  <?php endif ?>
</div>