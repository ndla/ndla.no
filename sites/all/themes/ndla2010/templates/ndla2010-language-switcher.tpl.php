<?php global $language; ?>
<a href="#" class="language js"><i class='fa fa-flag'></i><?php print $current_language->native ?><i class='fa fa-caret-down'></i></a>
<div id="language-switcher">
  <div class="content">
    <ul>
      <?php foreach ($languages as $lang): 
      if($language->language == $lang['options']['language']->language) { continue;} ?>
        <li><a href="<?php print url($lang['href'], $lang['options']) ?>"><?php print $lang['title'] ?></a></li>
      <?php endforeach ?>
    </ul>
  </div>
</div>