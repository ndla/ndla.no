<?php

/** \addtogroup ndla2010 */

/**
 * @file
 * @ingroup ndla2010
 * @brief
 *  The 2010 theme for NDLA.
 */

/**
 * Implementation of hook_theme().
 */
function ndla2010_theme($existing, $type, $theme, $path) {
  $path .= '/templates';
  return array(
    'ndla2010_header' => array(
      'arguments' => array(
        'vars' => array(),
      ),
      'path' => $path,
      'template' => 'ndla2010-header'
    ),
    'ndla2010_left_menu' => array(
      'arguments' => array(
        'left_menu' => NULL,
        'topic_menu' => NULL,
        'default_tab' => NULL
      ),
      'path' => $path,
      'template' => 'ndla2010-left-menu'
    ),
    'ndla2010_right' => array(
      'arguments' => array(
        'wrapper_class' => NULL,
        'node_id' => NULL,
        'related_content' => NULL,
        'tools' => NULL,
        'editor' => NULL,
        'embed' => NULL,
      ),
      'path' => $path,
      'template' => 'ndla2010-right'
    ),
    'ndla2010_node_actions' => array(
      'arguments' => array(
        'node_id' => NULL,
        'embed' => NULL,
      ),
      'path' => $path,
      'template' => 'ndla2010-node-actions'
    ),
    'ndla2010_course_category' => array(
      'arguments' => array('course_category' => '', 'courses' => array(), 'class' => '', 'render_options' => FALSE),
      'path' => $path,
      'template' => 'ndla2010-course-category'
    ),
    'ndla2010_language_switcher' => array(
      'arguments' => array('languages' => NULL, 'current_language' => NULL),
      'path' => $path,
      'template' => 'ndla2010-language-switcher'
    ),
    'ndla2010_applications' => array(
      'arguments' => array('applications' => NULL),
      'path' => $path,
      'template' => 'ndla2010-applications'
    ),
    'ndla2010_sharing' => array(
      'arguments' => array('sharing' => NULL),
      'path' => $path,
      'template' => 'ndla2010-sharing'
    ),
    'ndla2010_qrcode' => array(
      'arguments' => array('node' => NULL),
      'path' => $path,
      'template' => 'ndla2010-qrcode',
    ),
    'ndla2010_black_boxes' => array(
      'path' => $path,
      'template' => 'ndla2010-black-boxes',
    ),
  );
}

/**
 * Implementation of hook_preprocess_page().
 */
function ndla2010_preprocess_page(&$vars) {
  global $language, $base_path;

  // Add thumbnail meta tag to nodes
  $thumbnail = ndla_solr_get_thumbnail_path($vars['node']);
  if(!empty($thumbnail)) {
    drupal_set_html_head('<meta name="thumbnail" content="' . $base_path . $thumbnail . '" />');
    $vars['head'] = drupal_get_html_head();
  }

  // Set cache lifte time for 404
  if (strpos(drupal_get_headers(), '404 Not Found') !== FALSE) {
    ndla_utils_cache_lifetime(60);
  }

  if ($vars['template_files'][0] == 'page-cookbook' || $vars['template_files'][0] == 'page-exercise' || isset($vars['template_files'][1]) && $vars['template_files'][1] == 'page-treeoflife-mobile') {
    $vars['scripts'] = drupal_get_js();
    return;
  }

  $vars['display_footer'] = TRUE;
  if(arg(2) == 'ndla_editor_portal' || (arg(0) == 'node' && (arg(2) == 'edit' || arg(1) == 'add'))) {
    $vars['display_footer'] = FALSE;
  }

  $vars['course_title_tag'] = 'div';

  // Add common css
  $vars['css']['all']['theme'][$vars['directory'] . '/css/common.css'] = 1;
  drupal_add_js($vars['directory'] . '/js/header.js', 'theme');

  if ($vars['is_front']) {
    // Preprocess front page
    ndla2010_preprocess_front_page($vars);
  }
  else {
    // Stylesheets
    $vars['css']['all']['theme'][$vars['directory'] . '/css/ui-lightness/jquery-ui-1.8.1.custom.css'] = 1;
    $vars['css']['all']['theme'][$vars['directory'] . '/css/drupal_styles.css'] = 1;
    $vars['css']['all']['theme'][$vars['directory'] . '/css/ndla_tinymce_commons.css'] = 1;

    drupal_add_js(array('ndla2010' => array('coursesPath' => url('ndla-utils/ndla2010-theme/xhr/courses'))), 'setting');
  }
  if (arg(0) == 'search') {
    $vars['css']['all']['theme'][$vars['directory'] . '/css/search.css'] = 1;
  }

  // Add page template for subjects
  if (isset($vars['node']) && $vars['node']->type == 'fag' && count(explode('/', $_GET['q'])) == 2) {
    $vars['template_files'][] = 'page-course';
    // Add front page stylesheet
    $vars['css']['all']['theme'][$vars['directory'] . '/css/course-page.css'] = 1;
    ndla_utils_preprocess_course_page($vars);

    $google_plus = trim(variable_get('ndla_utils_google_badge_nid_' . $vars['node']->nid, ''));
    if(!empty($google_plus)) {
      $vars['google_plus'] = $google_plus;
    }
  }

  $vars['front_page'] = url('', array('ndla_utils_query_set' => TRUE));

  // Language switcher
  $path = drupal_is_front_page() ? '<front>' : $_GET['q'];
  $languages = language_list('enabled');
  $links = array();
  // Determine if we're searching
  $is_solr_page = FALSE;
  if (arg(0) == 'search' && arg(1) == 'apachesolr_search') {
    $filters = isset($_GET['filters']) ? check_plain($_GET['filters']) : '';
    //If a language filter is enabled enable it again
    if (preg_match('/language/', $filters, $matches)) {
      $is_solr_page = TRUE;
    }
  }
  $fag = ndla_utils_disp_get_context_value('course');
  foreach ($languages[1] as $lang) {
    $links[$lang->language] = array(
      'href' => $path,
      'title' => $lang->native,
      'options' => array(
        'language' => $lang,
        'attributes' => array('class' => 'language-link'),
        'ndla_utils_query_set' => TRUE,
        'query' => array('fag' => $fag)
      )
    );
    if ($is_solr_page) {
      $links[$lang->language]['options']['query']['filters'] = 'language:' . $lang->language;
    }
  }
  drupal_alter('translation_link', $links, $path);
  $vars['language_switcher'] = theme('ndla2010_language_switcher', $links, $vars['language']);

  // Get theme settings
  $vars['contact_url'] = url(theme_get_setting('contact_url'));
  $vars['help_url'] = url(theme_get_setting('help_url'));
  $vars['about_url'] = url(theme_get_setting('about_url'));

  $has_set_canonical = FALSE;
  // Automatically redirect the user to the correct language when trying to show a nn node with language set to nb
  // and vice versa, and a translated version of the page exists. Note! This does not apply to powerusers.
  if (isset($vars['node'])) {
    $used_by = ndla_fag_taxonomy_get_node_terms($vars['node']->nid, $vars['node']->vid);
    $used_by_nids = array();
    foreach ($used_by as $course) {
      if (isset($course->gid)) {
        $used_by_nids[] = $course->gid;
      }
    }
    if (!empty($used_by_nids)) {
      drupal_set_html_head('<meta name="fagkode" content="' . implode(',', $used_by_nids) . '" />');
    }
    if (module_exists('ndla_menu')) {
      $vars['topic_description'] = ndla_menu_topic_text();
    }
    $is_poweruser = (user_access("create fagstoff content") || user_access("view all fagstoff"));
    if (!$is_poweruser && !$vars['is_teaser'] && ($vars['node']->language != $vars['language']->language)
      && in_array($vars['node']->language, array("nb", "nn")) && in_array($vars['language']->language, array("nb", "nn"))) {
      $tniddata = translation_node_get_translations($vars['node']->tnid);
      if (is_array($tniddata) && array_key_exists($vars['language']->language, $tniddata)) {
        $languages = language_list();
        foreach($tniddata as $k => $data) {
          if($data->nid == $vars['node']->nid) {
            $wanted_language = $k;
          }
        }
        $url = url("node/" . $tniddata[$wanted_language]->nid, array('language' => $languages[$wanted_language]));
        drupal_set_header("Location: $url");
        exit();
      }
    }
    if (!arg(2)) {
      $lang_code = $language->language == 'en' ? 'en' : 'nb';
      $languages = language_list();
      $lang = $languages[$lang_code];
      $translations = translation_node_get_translations($vars['node']->tnid);

      $nid = isset($translations[$lang_code]) ? $translations[$lang_code]->nid : $vars['node']->nid;
      $url = url('node/' . $nid, array('absolute' => TRUE, 'language' => $lang, 'ndla_utils_query_set' => TRUE));
      if($nid == 1) {
        $url = url('<front>', array('absolute' => TRUE, 'language' => $lang, 'ndla_utils_query_set' => TRUE));
      }
      //This is done via the metatags module
      //drupal_set_html_head('<link rel="canonical" href="' . $url . '"/>');
      if(count($translations)) {
        foreach($translations as $lang_prefix => $data) {
          $new_lang = $languages[$lang_prefix];
          $url = url('node/' . $data->nid, array('absolute' => TRUE, 'language' => $new_lang, 'ndla_utils_query_set' => TRUE));
          drupal_set_html_head('<link rel="alternate" hreflang="' . $lang_prefix . '" href="'. ndla_utils_convert_schema($url) . '" />');
        }
      }
      $has_set_canonical = TRUE;
    }
  }
  //We are not dealing with a node. Be sure to set the canonical url
  else if(!$has_set_canonical) {
    $lang_list = language_list();
    $lang_obj = $lang_list['nb'];
    $counter = 0;
    $url = array();
    while(arg($counter)) {
      $url[] = check_plain(arg($counter));
      $counter = $counter + 1;
    }

    //Add the alternate versions.
    foreach($lang_list as $prefix => $obj) {
      if(drupal_is_front_page()) {
        drupal_set_html_head('<link rel="alternate" hreflang="' . $prefix . '" href="' . ndla_utils_convert_schema('http://ndla.no/' . $prefix) . '" />');
      }
      else {
        $lang_obj = $lang_list[$prefix];
        $hrefurl = ndla_utils_convert_schema(url(implode("/", $url), array('absolute' => TRUE, 'language' => $lang_obj)));
        drupal_set_html_head('<link rel="alternate" hreflang="' . $prefix . '" href="'. $hrefurl . '" />');
      }
    }
  }

  // Set the nodes different language links
  $vars['node_link'] = array();
  if(!empty($vars['node'])) {
    foreach (language_list() as $code => $obj) {
      $vars['node_link'][$code] = url('node/' . $vars['node']->nid, array('language' => $obj));
    }
  }
  if (function_exists("ndla_utils_disp_get_course")) {
    // Try to fetch fag context.
    $course = ndla_utils_disp_get_course();
  }
  if(empty($course)) {
    if(!empty($vars['node']->og_groups)) {
      $og_nid = array_shift($vars['node']->og_groups);
      $course = node_load($og_nid);
    }
    else if(!empty($vars['node']->nid)) {
      $g_nid = db_fetch_object(db_query("SELECT group_nid FROM {og_ancestry} WHERE nid = %d LIMIT 1", $vars['node']->nid))->group_nid;
      if(!empty($g_nid)) {
        $course = node_load($g_nid);
      }
    }
  }
  if (isset($course) && !empty($course)) {
    // Retrive course name from taxonomy
    $fag_term_id = ndla_fag_taxonomy_get_term_from_og($course->nid);
    // Make sure name is localized
    // Note! i18ntaxonomy_localize_terms needs term inside an array to work
    $fag_term = i18ntaxonomy_localize_terms(array(taxonomy_get_term($fag_term_id)));

    $vars['banner_color'] = $course->field_color[0]['value'];
    $vars['banner_name'] = $fag_term[0]->name;
    $vars['context_id'] = $course->nid;
    $vars['topics_tab_name'] = isset($course->{"topics_tab_name_$language->prefix"}) ? $course->{"topics_tab_name_$language->prefix"} : t('Topics');
    $vars['topic_menu_tab_name'] = isset($course->{"topic_menu_tab_name_$language->prefix"}) ? $course->{"topic_menu_tab_name_$language->prefix"} : t('Sequence');
    $vars['menu_tab_name'] = isset($course->{"menu_tab_name_$language->prefix"}) ? $course->{"menu_tab_name_$language->prefix"} : t('Material');

    if (isset($course->tabs_enabled)) {
      // Make sure we get the tabs in the right order...
      $num_tabs = 0;
      foreach ($course->tabs_enabled as $key => $tab) {
        $vars[$key . '_enabled'] = !empty($tab);
      }
    }
    if (isset($course->default_tab)) {
      $vars['default_tab'] = $course->default_tab;
      if (isset($vars['node']) && $vars['node']->type == 'fag'
        && isset($vars['menu_item']['path']) && $vars['menu_item']['path'] == 'node/%') {
        $vars['expanded_tab'] = $course->default_tab;
      }
    }
  }
  else {
    $vars['topics_tab_name'] = t('Topics');
    $vars['topic_menu_tab_name'] = t('Sequence');
    $vars['menu_tab_name'] = t('Material');
  }
  // Fetch content type from taxonomy
  if (isset($vars['node']) && function_exists('ndla_utils_get_node_content_type')) {
    $ct_tax = ndla_utils_get_node_content_type($vars['node']);
    if (isset($ct_tax['main']['name'])) {
      $vars['node_type'] = $ct_tax['main']['name'];
      $vars['node_type'] .= isset($ct_tax['sub']['name']) ? ": " . $ct_tax['sub']['name'] : "";
    }
  }

  $vars['copyright'] = '';
  if (!empty($vars['node']) && $vars['node'] && module_exists('ndla_authors') && ndla_authors_show_authors($vars['node']->type, $vars['node'], $course)) {
    $vars['copyright'] = theme('ndla_authors_links', $vars['node']);
  }

  if (isset($vars['node']->hide_content)) {
    $vars['hide_content'] = TRUE;
  }
  else {
    $vars['hide_content'] = FALSE;
  }

  drupal_set_html_head('<link rel="publisher" href="https://plus.google.com/+ndlano"/>');
  drupal_set_html_head("<meta property=\"fb:admins\" content=\"617822311\" />");
  drupal_set_html_head("<meta property=\"fb:admins\" content=\"623610842\" />");
  drupal_set_html_head("<meta property=\"fb:admins\" content=\"100001196703043\" />");
  drupal_set_html_head("<meta property=\"fb:admins\" content=\"849135654\" />");

  drupal_set_html_head('<meta property="article:publisher" content="https://www.facebook.com/ndla.no" />');
  if(!empty($course)) {
    $facebook_accounts = variable_get('nda_utils_facebook_fag_accounts', array());
    if(!empty($facebook_accounts[$course->nid])) {
      drupal_set_html_head('<meta property="article:author" content="https://www.facebook.com/' . $facebook_accounts[$course->nid] . '" />');
    }
  }
  if(!empty($vars['node']->authors)) {
    foreach($vars['node']->authors as $author) {
      drupal_set_html_head('<meta property="article:author" content="' . $GLOBALS['base_url'] . '/node/' . $author['person_nid'] . '" />');
    }
  }

  $vars['head'] = drupal_get_html_head();

  if (!$vars['is_front']) {
    // Header
    $vars['fag'] = $course;
    if(!empty($vars['fag']->ndla_utils_ndla2010_theme_fag_header)) {
      if(empty($vars['fag']->ndla_utils_ndla2010_theme_fag_header['filepath'])) {
        unset($vars['fag']->ndla_utils_ndla2010_theme_fag_header);
      }
    }

    if(!empty($course->nid)) {
      $term_id = ndla_fag_taxonomy_get_term_from_og($course->nid);
      $term = taxonomy_get_term($term_id);
      $vars['term'] = $term;
    }
    module_load_include('inc', 'ndla_utils', 'ndla_utils.pages');

    if($vars['display_footer'] == TRUE) {
      ndla_utils_set_about_footer($vars);
    }

    if(!(arg(0) === 'node' && (arg(1) == 'add' || arg(2) == 'edit'))) {
      // Set topic menu
      $menu_id = ndla_utils_disp_get_context_value('menu');
      $vars['topic_menu'] = $menu_id && module_exists('ndla_menu') && db_result(db_query("SELECT has_content FROM ndla_menu_translations WHERE menu_item_id = %d AND language IN ('%s', '')", $menu_id, $language->language)) != 2 ? ndla_menu(NDLA_MENU_TOPIC, $menu_id, 1, 'as_left') : '';

      // Set left menu
      $left_menu = module_exists('ndla_menu') ? ndla_menu() : '';
      $vars['menu_enabled'] = $left_menu ? TRUE : FALSE;

      $vars['left_menu'] = isset($vars['context_id']) ? theme('ndla2010_left_menu', $left_menu, $vars['topic_menu'], $vars['default_tab']) : '';
      if(isset($vars['context_id'])) {
        $menu_id = db_result(db_query("SELECT nmm.id FROM {ndla_menu_courses nmc} JOIN node n ON (nmc.nid = n.nid) JOIN {ndla_menu_menus} nmm ON (n.vid = nmm.vid) WHERE nmc.gid = %d AND nmc.type = 1", $vars['context_id']));
        drupal_add_js(array('ndla_utils' => array('left_menu' => $menu_id)), 'setting');
      }
    }


    if(!empty($vars['term']->tid)) {
      $translated_term_name = i18ntaxonomy_translate_term_name($vars['term']->tid);
      if(!empty($translated_term_name)) {
        $vars['term']->name = $translated_term_name;
      }
    }
    $vars['header'] = theme('ndla2010_header', $vars);

    // Footer
    if($vars['display_footer'] == TRUE ) {
      $indexes = array('footer_message', 'contact_url', 'help_url', 'about_url');
      if(ndla_utils_is_https()) {
        foreach($indexes as $index) {
          if(!empty($vars[$index])) {
            $vars[$index] = str_replace("http://", "https://", $vars[$index]);
          }
        }
      }

      $vars['footer'] = theme('ndla2010_footer', $vars['footer_message'], $vars['contact_url'], $vars['help_url'], $vars['about_url']);
    }
  }
  else {
    //Add <meta>-tags to frontpage
    $vars['head'] .= trim(variable_get("ndla_utils_disp_frontpage_head_metadata_" . $language->language, ""));
  }

  if (isset($vars['node']) && $vars['node']->type == 'book' || arg(2) == 'grep') {
    $vars['right'] = '';
    $vars['right_tools'] = '';
  }

  $embed_types = variable_get('ndla_utils_embed_types', array('image' => 'image', 'audio' => 'audio', 'flashnode' => 'flashnode', 'video' => 'video', 'amendor_ios' => 'amendor_ios', 'amendor_ios_task' => 'amendor_ios_task', 'amendor_electure' => 'amendor_electure', 'fagstoff' => 'fagstoff', 'oppgave' => 'oppgave', 'test' => 'test', 'veiledning' => 'veiledning'));
  $embed = 0;
  if (!empty($vars['node']) && $embed_types[$vars['node']->type]) {
    $embed = 1;
  }
  $vars['node_actions'] = arg(0) == 'contact' ? '' : theme('ndla2010_node_actions',
    isset($vars['node']->print_display) && $vars['node']->print_display && !arg(2) ? $vars['node']->nid : 0,
    $embed);

  $vars['right'] = arg(0) == 'contact' ? '' : theme('ndla2010_right',
    ndla2010_right_wrapper_class(),
    isset($vars['node']->print_display) && $vars['node']->print_display && !arg(2) ? $vars['node']->nid : 0,
    !empty($vars['right']) ? $vars['right'] : '' ,
    !empty($vars['right_tools']) ? $vars['right_tools'] : '',
    !empty($vars['right_red']) ? $vars['right_red'] : '',
    $embed);

  $subtitle = !empty($vars['node']->subtitle) ? $vars['node']->subtitle : NULL;
  if (module_exists('ndla_content_translation') && isset($vars['node']) && ndla_content_translation_enabled($vars['node']->type)) {
    $lang_map = array('nn' => 'Nynorsk', 'en' => 'English', 'nb' => 'Bokmal');
    if(!empty($vars['node']->ndla_translation->{"subtitle_".$lang_map[$language->language]})) {
      $subtitle = $vars['node']->ndla_translation->{"subtitle_".$lang_map[$language->language]};
    }
  }
  $vars['subtitle'] = $subtitle;

  // Replace jquery on safe pages
  if (!(arg(0) == 'admin' || arg(1) == 'add' || arg(2) == 'edit' || arg(2) == 'clone')) {
    // Replace legacy jquery with new jquery
    $scripts = drupal_add_js();
    $new_jquery = array(drupal_get_path('theme', 'ndla2010') . '/js/jquery-1.7.2.min.js' => $scripts['core']['misc/jquery.js']);
    $scripts['core'] = array_merge($new_jquery, $scripts['core']);
    unset($scripts['core']['misc/jquery.js']);
    $vars['scripts'] = drupal_get_js('header', $scripts);
  }
  else {
    // Keep scripts untouched
    $vars['scripts'] = drupal_get_js();
  }

  // Process styles and javascript
  $vars['styles'] = drupal_get_css($vars['css']);
  //Google web fonts
  $vars['styles'] .= "<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css' />";
  $timeline_scripts = isset($vars['timeline_scripts']) ? $vars['timeline_scripts'] : '';
  $vars['scripts'] .= $timeline_scripts;

  //KIS does not want the navigation bar while adding/editing content.
  if(arg(0) == 'node' && (arg(1) == 'add') || arg(2) == 'edit') {
    $vars['styles'] .= "<style>#navigation { display: none; }</style>";
    $vars['styles'] .= "<style>
                          #main_1 { width: 715px; }
                          .w960 .center-layout, #main_content { width: 1035px; }
                        </style>";
  }

  if(module_exists('ndla_contact')) {
    $vars['jira_button'] = ndla_contact_button();
  }

  if(module_exists('ndla_twitter')) {
    $node = NULL;
    if(!empty($vars['node']) && $vars['node']->nid == 1) {
      $node = $vars['node'];
    }
    $vars['head'] .= ndla_twitter_get_card($node, TRUE);
  }

  $vars['page_closure'] = ndla_utils_filter_links($vars['page_closure']);
  // Remove CSS for testing purposes
  //$vars['styles'] = drupal_get_css(array());
}

function ndla2010_optimize_js($scripts) {
  preg_match_all("/<script ([^>]*)src=[\"\']([^\"\']*)[\"\']([^>]*)\>\<\/script\>/i", $scripts, $matches);
  $deferrableJS = array("content_navigation.js", "utdanning_theme_functions_classes_select.js", "lightbox.js", "ui.tabs.js", "tabs.js", "utdanning_gajax.js", "audioswf.js", "bookmark_video.js", "panels.js");

  foreach ($matches[0] as $index => $match) {
    $pos = strrpos($matches[2][$index], "/");
    $pos2 = strrpos($matches[2][$index], "?");
    if (in_array(substr($matches[2][$index], ($pos + 1), ($pos2 - $pos - 1)), $deferrableJS)) {
      $scripts = str_replace($matches[0][$index], "<script " . $matches[1][$index] . "src=\"" . $matches[2][$index] . "\"" . $matches[3][$index] . " defer=\"defer\"></script>", $scripts);
    }
  }

  return $scripts;
}

function ndla2010_get_three_column_courses() {
  $hierarchical_courses = ndla_utils_get_courses_for_frontpage();

  $out = theme('ndla2010_course_category', $hierarchical_courses[0]->name, $hierarchical_courses[0]->children, '', FALSE)
                   . theme('ndla2010_course_category', $hierarchical_courses[2]->name, $hierarchical_courses[2]->children, '',  FALSE)
                   . theme('ndla2010_course_category', $hierarchical_courses[1]->name, $hierarchical_courses[1]->children, 'last',  FALSE);

  return $out;
}

/**
 * Preprocess front page.
 */
function ndla2010_preprocess_front_page(&$vars) {
  // Add front page stylesheet
  $vars['css']['all']['theme'][$vars['directory'] . '/css/front-page.css'] = 1;

  // Add courses
  $courses_cache_id = 'ndla2010_courses_' . $vars['language']->language;
  $courses_cache = cache_get($courses_cache_id);
  if ($courses_cache->data) {
    $vars['courses'] = $courses_cache->data;
  }
  else {
    $vars['courses'] = ndla2010_get_three_column_courses();
    cache_set($courses_cache_id, $vars['courses'], 'cache');
  }

  // Slogan
  $vars['front_page_slogan'] = check_plain(variable_get('ndla_utils_ndla2010_theme_front_page_' . $vars['language']->language . '_slogan', ''));

  // Applications
  $applications = array();
  for ($i = 0; $i < 9; $i++) {
    $app = variable_get('ndla_utils_ndla2010_theme_front_page_application_' . $i, array());
    if(!empty($app)) {
      $applications[$i] = (object)$app;
      $applications[$i]->href = ndla_utils_convert_ndla_url(url($applications[$i]->href));
      $applications[$i]->title = $applications[$i]->{$vars['language']->language . '_title'};
      $applications[$i]->description = $applications[$i]->{$vars['language']->language . '_description'};
    }
  }

  $vars['applications'] = theme('ndla2010_applications', $applications);


  /*
  Disable the frontpage slideshow for now: https://bug.ndla.no/browse/DEV-494
  $new_content = ndla_utils_load_slideshow();
  if(!empty($new_content)) {
    $vars['new_content'] = theme('ndla_utils_frontpage_slideshow', $new_content, $vars['base_path'], $vars['directory'], $vars['language']->language);
  }*/

  $vars['feedback_url'] = ndla_utils_convert_schema(variable_get('ndla_utils_ndla2010_theme_feedback_url', 'http://feedback.ndla.no'));
  $vars['page_closure'] = ndla_utils_filter_links($vars['page_closure']);
}

function ndla2010_preprocess_print_page(&$variables) {
  ndla2010_preprocess_page($variables);
  drupal_add_css(drupal_get_path('theme', 'ndla2010') . '/css/ndla_print_specific.css', 'theme');
  $css = drupal_add_css();
  unset($css['all']['module'][drupal_get_path('module', 'print') . '/css/print.css']);
  $variables['custom_styles'] = drupal_get_css($css);
}

function ndla2010_preprocess_print_node(&$variables) {
  ndla2010_preprocess_node($variables);
}

function ndla2010_preprocess_node(&$vars) {
  if (is_array($vars['node']->links)) {
    foreach ($vars['node']->links as $key => $value) {
      if (substr($key, 0, 17) == 'node_translation_') {
        unset($vars['node']->links[$key]);
      }
    }
  }

  if ($vars['node']->field_ingress_bilde != '') {
    $vars['image'] = node_load($vars['node']->field_ingress_bilde[0]['nid']);
    $vars['image_author'] = '';
    if (is_array($vars['image']->field_copyright)) {
      foreach ($vars['image']->field_copyright as $author_id) {
        $author = node_load($author_id);
        $vars['image_author'] .= ( $vars['image_author'] != '' ? ', ' : '') . $author->title;
      }
    }
  }
  else if ($vars['teaser']) {
    if ($vars['node']->type == 'image') {
      $vars['image'] = $vars['node'];
    }
    else if ($vars['node']->iid) {
      $vars['image'] = node_load($vars['node']->iid);
    }
  }
  $vars['links'] = !empty($vars['node']->links) ? theme('links', $vars['node']->links, array('class' => 'links inline')) : '';

  if ($vars['node']->type == 'fil' && count($vars['node']->iids) && module_exists('contentbrowser')) {
    $image = contentbrowser_get_thumbnail($vars['node']->iids[0], 'fil', TRUE);
    if (!empty($image)) {
      global $base_url;
      $vars['picture'] = l("<img src='" . $base_url . '/' . imagecache_create_path('Liten', $image) . "' />", "node/" . $vars['node']->iids[0], array('html' => TRUE));
    }
  }

  //Set up ingress image.
  $vars['ingress_image'] = '';
  if ($vars['page'] && !empty($vars['image']->images['normal'])) {
    $preset = 'fag_preset';
    $vars['ingress_image'] = "<div class=\"ingressImg\">";
    $vars['ingress_image'] .= theme('imagecache', $preset, $vars['image']->images['_original'], $vars['image']->title, '');
    $vars['ingress_image'] .= "</div>";
  }
  else if ($vars['teaser'] && isset($vars['image']->images[IMAGE_THUMBNAIL])) {
    $vars['ingress_image'] = '<img src="' . base_path() . $vars['image']->images[IMAGE_THUMBNAIL] . ' alt="" />';
  }
}

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function ndla2010_preprocess_block(&$vars, $hook) {
  if ($vars['block']->module == 'ndla_apachesolrmsf') {
    static $first_facet;
    $vars['classes_array'][] = str_replace('_', '-', $vars['block']->delta);
    if ($first_facet == NULL && $vars['block']->delta != 'current_search') {
      $vars['classes_array'][] = 'first-facet';
      $first_facet = TRUE;
    }
  }

  $vars['id'] = $vars['block']->module . '_' . $vars['block']->delta;
  $vars['class'] = 'block block-' . $vars['block']->module;
  $vars['content_class'] = 'content';

  if ($vars['id'] == 'apachesolr_sort' && ndla_utils_is_block_collapsed('ndla_utils_search_sort_header')) {
    $vars['class'] .= ' hidden';
  }
  if (isset($vars['block']->frontend_setting) && $vars['block']->frontend_setting == 'yes') {
    $vars['class'] .= ' esi-block';
  }

  if (module_exists('ndla_utils')) {
    $max_links = variable_get('blocks_max_links', array());
    if(in_array($vars['id'], array_keys($max_links)) && !empty($max_links[$vars['id']])) {
      $number = $max_links[$vars['id']];
      $show_type = !empty($vars['title']) ? $vars['title'] : $vars['block']->delta;
      $vars['content'] = ndla_utils_hide_links($vars['content'], $number, $show_type);
    }
  }
}

function ndla2010_menu_local_tasks() {
  $primary = menu_primary_local_tasks();
  if ($primary) {
    $output = '<ul class="tabs primary">' . $primary . '</ul>';
    $secondary = menu_secondary_local_tasks();
    if ($secondary) {
      $output .= '<ul class="tabs secondary">' . $secondary . '</ul>';
    }
    return $output;
  }
  return NULL;
}

function ndla2010_tinymce_theme($init, $textarea_name, $theme_name, $is_running) {
  $access_html = (in_array('code', $init['theme_advanced_buttons1']) || in_array('code', $init['theme_advanced_buttons2']) || in_array('code', $init['theme_advanced_buttons3']));
  if ($textarea_name == 'field-ingress-0-value' || $textarea_name == 'comment') {
    $init ['width'] = '735px';
    $init ['theme_advanced_buttons1'] = array('bold', 'italic', 'underline', 'separator', 'undo', 'redo', 'separator', 'sup', 'sub');
    if ($access_html) {
      $init ['theme_advanced_buttons1'][] = 'separator';
      $init ['theme_advanced_buttons1'][] = 'code';
    }
    $init ['theme_advanced_buttons2'] = array();
    $init ['theme_advanced_buttons3'] = array();
    return $init;
  }
  else if (substr($textarea_name, 0, 8) == 'answers-') {
    $init ['width'] = '442px';
    $init ['theme_advanced_buttons1'] = array('bold', 'italic', 'underline', 'separator', 'undo', 'redo', 'separator', 'sup', 'sub', 'utdanning_mediabrowser', 'tiny_mce_wiris_formulaEditor');
    $init ['theme_advanced_buttons2'] = array();
    $init ['theme_advanced_buttons3'] = array();
    if ($access_html) {
      $init ['theme_advanced_buttons1'][] = 'separator';
      $init ['theme_advanced_buttons1'][] = 'code';
    }
    return $init;
  }
  $init ['width'] = '735px';

  if ($textarea_name == 'field-embed-code-0-value') {
    return "";
  }
  return theme_tinymce_theme($init, $textarea_name, $theme_name, $is_running);
}

/* Functions for theming of Quiz results */

function ndla2010_quiz_feedback($questions, $showpoints = TRUE, $showfeedback = FALSE) {
  $header = array(format_plural(count($questions), 'Question Result', 'Question Results'));
  $rows = array();

  // Go through each of the questions.
  foreach ($questions as $question) {
    //$cols = array();
    // Ask each question to render a themed report of how the user did.
    $module = quiz_module_for_type($question->type);
    //$cols[] = array('data' => theme($module .'_report', $question, $showpoints, $showfeedback), 'class' => 'quiz_summary_qrow');
    $rows[] = theme($module . '_report', $question, $showpoints, $showfeedback);

    // Get the score result for each question only if it's a scored quiz.
    if ($showpoints) {
      $theme = ($question->correct) ? 'quiz_score_correct' : 'quiz_score_incorrect';
      //$cols[] = array('data' => theme($theme), 'class' => 'quiz_summary_qcell');
    }
    // Pack all of this into a table row.
    //$rows[] = array('data' => $cols, 'class' => 'quiz_summary_qrow');
  }

  //return theme('table', $header, $rows);
  $output = '';
  foreach ($rows as $item) {
    $output .= $item;
  }

  return $output;
}

function ndla2010_quiz_take_summary($quiz, $questions, $score, $summary) {
  // Set the title here so themers can adjust.
  drupal_set_title(check_plain($quiz->title));

  // Display overall result.
  $output = '<div id="quizResult">';

  // Only display scoring information if this is not a personality test:
  //if ($score['percentage_score']) {
  if (!empty($score['possible_score'])) {
    if (!$score['is_evaluated']) {
      $msg = t(
        'Parts of this @quiz have not been evaluated yet. The score below is not final.', array('@quiz' => QUIZ_NAME)
      );
      drupal_set_message($msg, 'error');
    }
    $output .= '<div id="quiz_score_possible">' . t('You got %num_correct of %question_count possible points.', array('%num_correct' => $score['numeric_score'], '%question_count' => $score['possible_score'])) . '</div>' . "\n";
    $output .= '<div id="quiz_score_percent">' . t('Your score: %score%', array('%score' => $score['percentage_score'])) . '</div><br />' . "\n";
  }
  $output .= '<div id="quiz_summary">' . $summary . '</div><br />' . "\n";
  // Get the feedback for all questions.
  $output .= theme('quiz_feedback', $questions, ($quiz->pass_rate > 0), TRUE);
  //$output .= '<pre>' . print_r($questions, true) . '</pre>';

  $output .= '</div>';

  return $output;
}

function ndla2010_quiz_score_correct() {
  return theme('image', drupal_get_path('theme', 'ndla') . '/images/correct.gif', t('correct'));
}

/**
 * Theme function for the recipe node form
 */
function ndla2010_cookbook_recipe_node_form() {
  drupal_add_css(drupal_get_path('module', 'cookbook') . '/theme/stylesheets/cookbook-recipe-node-form.css');

  drupal_add_js(array(
    'cookbook' => array(
      'url' => url('cookbook'),
      'path' => drupal_get_path('module', 'cookbook'),
      'imageSize' => variable_get('cookbook_image_node_thumb_size', 'thumbnail'),
      'ingredientNodes' => variable_get('cookbook_use_ingredient_nodes', 0),
    ),
    'imageUrl' => url('image')
    ), 'setting');

  drupal_add_js(drupal_get_path('module', 'cookbook') . '/theme/javascripts/jquery.mousewheel.min.js');
  drupal_add_js(drupal_get_path('module', 'cookbook') . '/theme/javascripts/jquery.jscrollpane.min.js');
  drupal_add_js(drupal_get_path('module', 'cookbook') . '/theme/javascripts/cookbook-recipe-node-form.js');

  // A small hack for the NDLA site
  if (module_exists('ole_noderef')) {
    drupal_add_js(
      "$(document).ready(function() {
        $('<a href=\"" . base_path() . drupal_get_path('module', 'ole_noderef') . "/browse.php\" rel=\"lightmodal\">" . t('Browse') . "</a>').css('color', '#00a').appendTo('#edit-cookbook-recipe-images-wrapper');
        clickedid = 'edit-cookbook-recipe-images-wrapper';
        $('<input type=\"hidden\" id=\"edit-cookbook-recipe-images-autocomplete\" value=\"field_ingress_bilde\"/>').appendTo('body');
        runkode = function(text) {
          CookbookImages.\$field.focus();
          text = text.match(/\[nid:[\d]+\]$/)[0];
          text = text.substring(5, text.length - 1);
          CookbookImages.\$field.val(text);
          CookbookImages.\$add.trigger('click');
          Lightbox.end('forceClose');
        };
        Lightbox.initList();
      });", 'inline');
  }
}

/**
 * Theme function for the exercise node form
 */
function ndla2010_exercise_node_form() {
  $module_path = drupal_get_path('module', 'exercise');
  drupal_add_js(
    array(
    'exercise' => array(
      'moduleUrl' => base_path() . $module_path,
      'thumbnailSize' => variable_get('exercise_image_thumbnail_size', 'thumbnail'),
      'viewImageUrl' => url('image/view')
    )
    ), 'setting'
  );
  drupal_add_js($module_path . '/theme/javascripts/exercise-node-form.js');
  drupal_add_js(
    "$(document).ready(function() {
      $('<a href=\"" . base_path() . drupal_get_path('module', 'ole_noderef') . "/browse.php\" rel=\"lightmodal\">" . t('Browse') . "</a>').insertAfter(Exercise.image.\$field);
      clickedid = 'edit-exercise-image-wrapper';
      $('<input type=\"hidden\" id=\"edit-exercise-image-autocomplete\" value=\"field_ingress_bilde\"/>').appendTo('body');
        var originalRunkode = runkode;
        runkode = function(text) {
          text = text.match(/\[nid:[\d]+\]$/)[0];
          text = text.substring(5, text.length - 1);
          originalRunkode(text);
          Exercise.image.\$field.change();
        };
        $('<br/>').insertAfter(Exercise.image.\$field);
        Lightbox.initList();
    })", 'inline');
}

function ndla2010_quiz_score_incorrect() {
  return theme('image', drupal_get_path('theme', 'ndla') . '/images/correct.gif', t('incorrect'));
}

function ndla2010_multichoice_report($question, $showpoints, $showfeedback) {
  // Build the question answers header (add blank space for IE).
  /* $innerheader = array(t('Answers'));
    if ($showpoints) {
    $innerheader[] = t('Correct Answer');
    }
    $innerheader[] = t('User Answer');
    if ($showfeedback) {
    $innerheader[] = '&nbsp;';
    } */

  //$output = '<div class="quiz_summary_question"><span class="quiz_question_bullet">Q:</span> '. check_markup($question->body) .'</div>';
  $output .= check_markup($question->body);

  $output .= '<ul>';
  foreach ($question->answers as $aid => $answer) {
    $cols = array();
    $output .= '<li class="' . ($answer['is_correct'] ? ($answer['user_answer'] ? 'correct' : 'correctInactive') : ($answer['user_answer'] ? 'wrong' : 'wrongInactive')) . '">';
    /* if ($showpoints) {
      $output .= $answer['is_correct'] ? theme_multichoice_selected() : theme_multichoice_unselected();
      }
      $cols[] = $answer['user_answer'] ? theme_multichoice_selected() : theme_multichoice_unselected();
      $cols[] = ($showfeedback && $answer['user_answer']) ? '<div class="quiz_answer_feedback">'. $answer['feedback'] .'</div>' : ''; */

    $rows[] = $cols;

    $output .= $answer['answer'] . '</li>';
    //$output .= '<li><pre>' . print_r($answer, TRUE) . '</pre></li>';
  }
  $output .= '</ul>';

  return $output;

  // Add the cell with the question and the answers.
  /* $q_output = '<div class="quiz_summary_question"><span class="quiz_question_bullet">Q:</span> '. check_markup($question->body) .'</div>';
    $q_output .= theme('table', $innerheader, $rows) .'<br />';
    return $q_output; */
}

/**
 * Create the HTML for insertion of Flash
 * This theme function will constrain the size of the content according to the limits
 * set on the configuration page. It then calls a secondary theme function to produce
 * the actual HTML markup.
 *
 * @param $flashnode
 *   A structured array that defines an item of Flash content. Must include
 *   keys filepath (path to the file), width, height, substitution (string containing
 *   HTML mark up for the substitution content), and optionally flashvars. Other keys
 *   may be included and they will be passed through to SWFTools.
 *   Key          Comment
 *   filepath     path to an swf file to theme
 *   width        display width of the swf file
 *   height       display height of the movie
 *   substitution substitution content to use if using JavaScript insertion
 *   flashvars    flashvars string to pass to the swf file
 *   base         base parameter to pass to the swf fil
 * @param $teaser
 *   Flag to indicate whether teaser content is being generated. Not used by flash node
 *   but could be used by other themers to provide different output depending on mode
 * @param $options
 *   Optional array of other parameters that will passed through to swf tools theme function
 * @return
 *   An HTML string for rendering the flash content
 */
function ndla2010_flashnode($flashnode, $teaser = FALSE, $options = array()) {

  if (in_array(arg(0), array('print', 'ndla_node_export')) || !empty($GLOBALS['fag_export'])) {
    $url = url('node/' . $flashnode->nid, array('absolute' => true));
    $out = "<img src='http://chart.apis.google.com/chart?chs=150x150&cht=qr&chl=$url'/><br/>$url";
    return $out;
  }

  if (module_exists("utdanning_flashpakke")) {
    utdanning_flashpakke_prepare_node($flashnode);
  }

  $max_width = $max_height = 2000;
  // Get height and width limits according to view mode, and if in teaser mode then apply teaser scaling
  if ($teaser) {
    $max_width = variable_get('flashnode_max_teaser_width', 0);
    $max_height = variable_get('flashnode_max_teaser_height', 0);
    $teaser_scale = variable_get('flashnode_teaser_scale', 1);
    $flashnode['width'] = $flashnode['width'] * $teaser_scale;
    $flashnode['height'] = $flashnode['height'] * $teaser_scale;
  }
  else if (!isset($options['no_max'])) {
    $max_width = variable_get('flashnode_max_width', 0);
    $max_height = variable_get('flashnode_max_height', 0);
  }

  // Check width does not exceed defined maximum, and scale if required
  if ($max_width) {
    if ($flashnode['width'] > $max_width) {
      $scale = $max_width / $flashnode['width'];
      $flashnode['width'] = $flashnode['width'] * $scale;
      $flashnode['height'] = $flashnode['height'] * $scale;
    }
  }

  // Now check height does not exceed defined maximum, and scale if required
  if ($max_height) {
    if ($flashnode['height'] > $max_height) {
      $scale = $max_height / $flashnode['height'];
      $flashnode['width'] = $flashnode['width'] * $scale;
      $flashnode['height'] = $flashnode['height'] * $scale;
    }
  }

  // Generate output
  $output .= theme('flashnode_markup', $flashnode, $options);

  // Return the HTML
  return $output;
}

/**
 * Render a image on a image node
 *
 * @param object $node
 * @param string $size
 * @return html
 */
function ndla2010_image_body($node, $size) {
  return image_display($node, "fullbredde");
}

/**
 * Format the audio node for display
 */
function ndla2010_audio_display($node) {

  if (in_array(arg(0), array('print', 'ndla_node_export')) || !empty($GLOBALS['fag_export'])) {
    $url = url('node/' . $node->nid, array('absolute' => true));
    $out = "<img src='http://chart.apis.google.com/chart?chs=150x150&cht=qr&chl=$url'/><br/>$url";
    return $out;
  }

  $output = '';

  $items = array();
  $player = audio_get_node_player($node);

  $output .= "<div class='audio-node block'>\n";
  // give audio_image.module (or a theme) a chance to display the images.
  if (isset($node->audio_images)) {
    $output .= theme('audio_images', $node->audio_images);
  }
  $output .= $player;
  $output .= "</div>\n";

  //Add the audio file as an attachment.
  if ($node->audio['downloadable']) {
    $file = $node->audio['file'];
    $file->list = 1;
    $node->files[$file->fid] = $file;
  }

  return $output;
}

/**
 * Implementation of hook_preprocess_search_results().
 */
function ndla2010_preprocess_search_results(&$vars) {
  if (module_exists('apachesolr')) {
    $response = apachesolr_static_response_cache();
  }
  if (isset($response) && !empty($response)) {
    // Get facet fields
    $vars['hits'] = $response->response->numFound;
  }
  else {
    $vars['hits'] = 0;
  }
  $vars['sort_bys'] = array(
    'score' => t('Relevancy'),
    'sort_title' => t('Title'),
    'type' => t('Content type'),
    'sort_name' => t('Author'),
    //'created' => t('Date'),
    'is_updated' => t('Date'),
  );
}

/**
 * Implementation of hook_preprocess_search_result().
 */
function ndla2010_preprocess_search_result(&$vars) {
  static $types;
  if ($types == NULL) {
    $result = db_query("SELECT td.tid, td.name FROM {term_data} td WHERE td.vid = 100004");
    while ($term = db_fetch_object($result)) {
      $types[$term->tid] = $term->name;
    }
  }

  $s = count($vars['result']['node']->im_vid_100004);
  if ($s) {
    $vars['result']['type'] = '';
    for ($i = 0; $i < $s; $i++) {
      if (!isset($added[$vars['result']['node']->im_vid_100004[$i]]) && isset($types[$vars['result']['node']->im_vid_100004[$i]])) {
        $vars['result']['type'] .= ($vars['result']['type'] == '' ? '' : ', ') . $types[$vars['result']['node']->im_vid_100004[$i]];
        $added[$vars['result']['node']->im_vid_100004[$i]] = TRUE;
      }
    }
  }

  if($vars['result']['node']->type == 'flashnode') {
    $vars['snippet'] = '';
  }

  /* Remove xml code in search result snippet */
  if(in_array($vars['result']['node']->type, array('amendor_electure'))) {
    $snippet = $vars['snippet'];
    $snippet = preg_replace('/&lt;\?[^\?]*\?&gt;/', '', $snippet);
    $snippet = preg_replace('/^[^?]*\?&gt;/', '', $snippet);
    $snippet = preg_replace('/&lt;\?[^?]*$/', '', $snippet);
    $vars['snippet'] = $snippet;
  }
}

/**
 * Creates a ndla2010 button
 *
 * @param string $text
 *  Text to be placed on the button
 * @param string $url
 *  For the href attribute
 * @param array $classes
 *  Classes to be added to the href. The first class should be the basis for the stying of the button.
 *  For instance  ndla-btn1 or ndla-btn2
 * @param string $id
 *  Id attribute for the button
 * @param string $tooltip
 *  title attribute for the button
 * @return string
 *  html
 */
function ndla2010_image_button($text, $url, $classes, $id, $tooltip = NULL) {
  $extra = '';
  if ($tooltip) {
    $extra .= " title=\"$tooltip\"";
  }
  return "
    <a href=\"$url\" id=\"$id\" class=\"" . implode(' ', $classes) . "\"$extra>
      <span class=\"$classes[0]-start\"></span>
      <span class=\"$classes[0]-content\">$text</span>
      <span class=\"$classes[0]-end\"></span>
    </a>
  ";
}

/**
 * Returns extra attributes to be added to #right-wrapper as a string.
 */
function ndla2010_right_wrapper_class() {
  $class = FALSE;
  $args = arg();
  if ($args[0] == 'node' && isset($args[1])) {
    if ($args[1] == 'add' && isset($args[2]) && ($args[2] == 'amendor-ios' || $args[2] == 'amendor-ios-task')) {
      $class = TRUE;
    }
    elseif (is_numeric($args[1])) {
      $node = node_load($args[1]);
      if (($node->type == 'amendor_ios' || $node->type == 'amendor_ios_task' || $node->type == 'amendor_electure') && (!isset($args[2]) || ($args[2] == 'edit' && isset($args[3]) && ($args[3] == 'amendor-ios' || $args[3] == 'amendor-ios-task')))) {
        $class = TRUE;
      }
    }
  }
  return $class ? ' class="narrow"' : '';
}

function ndla2010_authors($nid) {
  $result = db_query(
    "SELECT n2.title
      FROM {node} n
      JOIN {ndla_authors} na ON n.nid = %d AND n.vid = na.vid
      JOIN {node} n2 ON n2.nid = na.person_nid", $nid);
  $authors = '';
  while ($author = db_result($result)) {
    $authors .= $authors == '' ? $author : ', ' . $author;
  }
  return $authors;
}

/* Added extra column for node published */

function ndla2010_question_selection_table($form) {
  drupal_add_tabledrag('question-list', 'order', 'sibling', 'question-list-weight', NULL, NULL, TRUE);

  // Building headers
  $headers = array(t('Question'), t('Type'), t('Actions'), t('Update'), t('Max score'));
  if (isset($form['compulsories']))
    $headers[] = t('Compulsory');
  $headers[] = t('Weight');
  $headers[] = t('Published');

  // Building table body
  $rows = array();
  if (!empty($form['titles'])) {
    foreach (element_children($form['titles']) as $id) {
      $form['weights'][$id]['#attributes']['class'] = 'question-list-weight';

      $row = _quiz_get_question_row($form, $id);

      list($nid, $vid) = split('-', $id);
      $published = (ndla_publish_workflow_is_published(array('nid' => $nid, 'vid' => $vid))) ? 'checked' : '';
      $row['data'][] = "<input type='checkbox' disabled $published />";
      $rows[] = $row;
    }
    // Make sure the same fields aren't rendered twice
    unset($form['types'], $form['view_links'], $form['remove_links'], $form['stayers']);
    unset($form['max_scores'], $form['revision'], $form['weights'], $form['titles'], $form['compulsories']);
  }
  $html_attr = array('id' => 'question-list');

  // We hide the table if no questions have been added so that jQuery can show it the moment the first question is beeing added.
  if (isset($form['no_questions']))
    $html_attr['style'] = "display:none;";

  $table = theme('table', $headers, $rows, $html_attr);

  return drupal_render($form['random_settings'])
    . $table
    . drupal_render($form);
}

/**
 * Theme function for the search results page.
 */
function ndla2010_apachesolr_search_results_page($form, $results) {
  $params = $_GET;
  unset($params['q']);
  if(isset($params['filters'])) {
    unset($params['filters']);
  }
  if(isset($params['language'])) {
    unset($params['language']);
  }
  if ($results == '' && !empty($params)) {
    $results = '<hr/><h1>' . t('Your search yielded no results') . '</h1>' . apachesolr_search_noresults();
  }
  return $form . $results;
}

/**
 * Theme for the video player
 */
function ndla2010_istribute_player($element, $width=NULL, $height=NULL, $id=NULL, $start_time=NULL) {

  if (in_array(arg(0), array('print', 'ndla_node_export')) || !empty($GLOBALS['fag_export'])) {
    $url = url('node/' . $element->nid, array('absolute' => true));
    $out = "<img src='http://chart.apis.google.com/chart?chs=150x150&cht=qr&chl=$url'/><br/>$url";
    return $out;
  }

  _istribute_tracer();
  if (!istribute_has_video($element))
    return t("No video");
  $metadata = istribute_get_metadata($element);

  if ($metadata === NULL)
    return t("Video unavailable id: %id", array('%id' => $element->istribute_video));

  if ($width == NULL) {
    $width = variable_get('istribute_playerwidth', '426');
  }
  if ($height == NULL) {
    if (variable_get('istribute_playerheightauto', '1')) {
      if ($metadata->aspect)
        $aspect = $metadata->aspect;
      else
        $aspect = 16 / 9;
      $height = intval($width / $aspect);
    } else if ($height = variable_get('istribute_playerheight')) {

    }
    else {
      $height = intval($width / (16 / 9));
    }
  }

  if(!empty($start_time)) {
   $element->istribute_videourl .= "?startTime=" . $start_time;
  }

  return '<iframe allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" ' . ($id != NULL ? 'id="' . $id . '"' : '') . ' src="' . $element->istribute_videourl . '" style="width:' . $width . 'px;height:' . $height . 'px;margin:0px;padding:0px;border:0px solid black" class="istribute_video"></iframe>';
}

function ndla2010_preprocess_search_theme_form(&$vars) {
  if(module_exists('ndla_solr')) {
    drupal_add_js(drupal_get_path('module', 'ndla_solr') . '/js/autocomplete_fork.js', 'footer');
    $vars['autocomplete_path'] = ndla_solr_get_autocomplete_path();
    $vars['classes'] = "form-autocomplete form-text";
    if (!drupal_is_front_page()) {
      $vars['classes'] .= " small";
    }
  }
}

function ndla2010_apachesolr_image_snippet($item) {
  return theme('image', $item['node']->ss_image_relative, $item['title'], $item['title']) . $item['snippet'];
}

/**
 * Return an image with an appropriate icon for the given file.
 * NDLA: Removed alt-text
 *
 * @param $file
 *   A file object for which to make an icon.
 */
function ndla2010_filefield_icon($file) {
  if (is_object($file)) {
    $file = (array) $file;
  }
  $mime = check_plain($file['filemime']);

  $dashed_mime = strtr($mime, array('/' => '-', '+' => '-'));

  if ($icon_url = filefield_icon_url($file)) {
    return '<img class="filefield-icon field-icon-'. $dashed_mime .'"  alt="" src="'. $icon_url .'" />';
  }
}

function ndla2010_image_display($node, $label, $url, $attributes) {
  $title = isset($attributes['title']) ? $attributes['title'] : $node->title;
  if(!empty($attributes['alt'])) {
    $alt = $attributes['alt'];
  } elseif(!empty($node->field_alt_text[0]['value'])) {
    $alt = $node->field_alt_text[0]['value'];
  } else {
    $alt = $node->title;
  }
  unset($attributes['title']);
  unset($attributes['alt']);
  return theme('image', $url, $alt, $title, $attributes, FALSE);
}

/**
 * Implementation of hook_preprocess_amendor_electure().
 * Remove the 720px width - it doesnt fit on NDLA.
 */
function ndla2010_preprocess_amendor_electure(&$vars) {
  global $user;

  if($vars['nid'] == arg(1)) {
    $vars['width'] = 620;
    $vars['height'] = round((568 / 720) * $vars['width']);
  }
}

function ndla2010_preprocess_ndla_utils_disp_rights_v2(&$vars) {
  if(empty($vars['node'])) {
    $nid = check_plain(arg(1));
    if(arg(0) == 'node' && is_numeric($nid)) {
      $vars['node'] = node_load($nid);
    }
  }

  if(empty($vars['node'])) {
    return;
  }

  $path = drupal_get_path('theme', 'ndla2010');
  drupal_add_js($path . "/js/jquery.qtip.min.js");
  drupal_add_js($path . "/js/imagesloaded.pkg.min.js");

  $nids_inserted = contentbrowser_get_inserted_nodes($vars['node']->nid, array('utdanning_rdf', 'ndla_authors'), TRUE);
  $obj = new stdClass();
  $obj->nid = $vars['node']->nid;
  $nids_inserted[$vars['node']->nid] = $obj;

  /* Remove person nodes from footer */
  $result = db_query("SELECT nid FROM {node} WHERE type = 'person' AND nid IN ('" . implode("','", array_keys($nids_inserted)) . "')");
  while ($row = db_fetch_object($result)) {
    unset($nids_inserted[$row->nid]);
  }

  $data = array();
  if(!empty($nids_inserted)) {
    $result = db_query("SELECT c.nid, c.license, n.type FROM {creativecommons_lite} c INNER JOIN {node} n ON n.nid = c.nid WHERE c.nid IN(" . implode(",", array_keys($nids_inserted)) . ")  ORDER BY license ");
    while($row = db_fetch_object($result)) {
      //Dont show license information for links.
      if($nids_inserted[$row->nid] == 'link') {
        continue;
      }
      if(!empty($nids_inserted[$row->nid]) && !empty($row->license)) {
        $data[$row->license][$row->nid] = $row->nid;
      }
      else if($row->type == 'contract' || strpos($row->type, 'amendor') !== FALSE) {
        $data['by-sa'][$row->nid] = $row->nid;
      }
      else {
        $data['nolicense'][$row->nid] = $row->nid;
      }
    }
  }

  if(!empty($data['copyrighted'])) {
    $copyright = $data['copyrighted'];
    unset($data['copyrighted']);
    $data = array_merge(array('copyrighted' => $copyright), $data);
  }
  if(!empty($data['nolicense'])) {
    $nolicense = $data['nolicense'];
    unset($data['nolicense']);
    $data = array_merge($data, array('nolicense' => $nolicense));
  }

  $vars['nids_inserted'] = $data;
}
