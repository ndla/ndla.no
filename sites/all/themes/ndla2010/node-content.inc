<?php
/**
 * @file
 * @ingroup ndla2010
 */

$content = str_replace(' id="rs_read_this"', '', $content, $rs_count);

?>
<div itemscope itemtype="<?php print $node->schemaorg_itemtype;?>" id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">
  <?php if ($rs_count > 0) print '<div id="rs_read_this">'; ?>
    <?php print $picture ?>

    <?php if ($teaser): ?>
      <div class="ingressView">
        <?php if ($node_type):?>
          <div class="subtitle"><?php print $node_type; ?></div>
        <?php endif; ?>
        <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>

        <?php
        print $ingress_image;
        print $node->field_ingress[0]['safe'];
        ?>
      </div><!-- END ingressView -->
    <?php endif; ?>

    <?php if ($page): ?>
      <div class="meta">
        <?php if ($page && $links): ?>
          <div class="terms terms-inline"><?php //print $links ?></div>
        <?php endif;?>
      </div><!-- END meta -->
      <?php if ($node->field_ingress[0]['safe']): ?>
        <div class="ingress<?php print (isset($node->field_ingress_bold) && $node->field_ingress_bold[0]['value']) ? ' ingress-bold' : '' ?>">
          <?php print $ingress_image; ?>
          <?php print $node->field_ingress[0]['safe']; ?>
          <div class="clear"></div>
        </div>
      <?php endif; ?>

      <div class="node_content">
        <?php print $content ?>
      </div><!-- END node_content -->
      <?php if ($links): ?>
        <div class="terms terms-inline"><?php print $links ?></div>
      <?php endif;?>

      <div class="clear"></div>
    <?php endif; ?>
  <?php if ($rs_count > 0) print '</div>'; ?>
</div><!-- END node -->
