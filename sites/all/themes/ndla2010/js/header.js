var LanguageSwitcher = LanguageSwitcher || {}

LanguageSwitcher.initialize = function() {
  LanguageSwitcher.$switcher = $('#language-switcher').click(function() {LanguageSwitcher.doNotClose = true;});
  $('#header .language').add('#language-switcher .content .header').click(LanguageSwitcher.click);
}

LanguageSwitcher.click = function() {
  if (LanguageSwitcher.doNotClose) {
    LanguageSwitcher.doNotClose = false;
    return;
  }
  if (!$(this).is('html')) {
    LanguageSwitcher.doNotClose = true;
  }
  if (LanguageSwitcher.$switcher.is(':visible')) {
    LanguageSwitcher.$switcher.fadeOut(100);
    $('html').unbind('click', LanguageSwitcher.click);
  }
  else {
    LanguageSwitcher.$switcher.fadeIn(100);
    $('html').click(LanguageSwitcher.click);
  }
}

var Overflow = Overflow || {}

Overflow.initialize = function() {
  Overflow.$tester = $('<div></div>').appendTo('body').hide();
  $('.new-content').each(function() {
    var $this = $(this),
    width = $this.width(),
    fontSize = $this.css('fontSize');
    Overflow.$tester.css({
      fontSize: fontSize,
      width: width + 'px'
    });
    if ($.browser.msie) {
      $this.css('fontSize', fontSize);
    }
    $this.html(Overflow.check($this.html(), $this.height(), width, fontSize.substring(0, fontSize.length - 2),  $this.prev().children('a').attr('href')));
  });
}

Overflow.check = function(string, maxHeight, maxWidth, fontSize, href) {
  var ellipsis = '... <a href="' + href + '">' + Drupal.t('Read more') + '</a>';
  
  if (!Overflow.isTooLong(string + ellipsis, maxHeight)) {
    return string + ellipsis;
  }
  
  var min = 0, mid, max = Math.floor(maxWidth / 3) * Math.floor(maxHeight / fontSize);
  if (max == 0) {
    return ellipsis;
  }

  while (min + 5 < max) {
    mid = Math.floor((min + max) / 2);
    if (Overflow.isTooLong(string.substr(0, mid) + ellipsis, maxHeight)) {
      max = mid;
    }
    else {
      min = mid;
    }
  }
  return string.substr(0, max - 5) + ellipsis;  
}

Overflow.isTooLong = function(string, maxHeight) {
  Overflow.$tester.html(string);
  return (Overflow.$tester.height() > maxHeight);
}

$(document).ready(function () {
  LanguageSwitcher.initialize();
  //Overflow.initialize();
  $('a.js').click(function (e) {
    e.preventDefault();
  });
});
