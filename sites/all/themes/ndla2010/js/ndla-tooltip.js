// NDLA Tooltip
(function($) {
    $.fn.extend({
        ndlaTooltip: function(html) {
          if (typeof html == 'function') {
            $(this).each(function() {
              var $a = $(this);
              $a.ndlaTooltip(html.call($a));
            });
            return;
          }
          if (window['$ndlaTooltip'] == undefined) {
            $ndlaTooltip = $('<div id="ndla-tooltip"><div class="arrow"></div><div class="top"><div class="left-corner"></div><div class="right-corner"></div><div class="center"></div></div><div class="middle-left"><div class="middle-right"><div class="middle-center"></div></div></div><div class="bottom"><div class="left-corner"></div><div class="right-corner"></div><div class="center"></div></div></div>').css('opacity', 0).appendTo('body');
            if ($.browser.msie) {
              $ndlaTooltip.children('.arrow').css('opacity', 0);
            }
          }
          $(this).mouseover(function() {
            var $this = $(this),
            o = $this.offset(),
            c = o.left + ($this.width() / 2),
            h = $this.height() + 10,
            dw = $(document).width() / 2,
            $a = $ndlaTooltip.children('.arrow'); 
            if ($.browser.msie) {
              if (window['$ndlaCheckSize'] == undefined) {
                $ndlaCheckSize = $('<div id="ndla-check-size"></div>').appendTo('body');
              }
              var w2 = $ndlaCheckSize.show().html(html).width() + 40;
              $ndlaCheckSize.hide();
              $ndlaTooltip.css('width', w2);
              $a.stop().show().animate({opacity: 1.0}, 200)
            }
            $ndlaTooltip.stop().show().animate({opacity: 1.0}, 200).children('.middle-left').children('.middle-right').children('.middle-center').html(html);
            if (c > dw) {
              var w = $ndlaTooltip.width() - 34;
              c = c - w;
              $a.css('marginLeft', w - 16);
            }
            else {
              c = c - 35;
              $a.css('marginLeft', 18);
            }
            $ndlaTooltip.css({left: c, top: o.top + h});
          }).mouseout(function() {
            $ndlaTooltip.stop().animate({opacity: 0.0}, 200, function() {$ndlaTooltip.hide()});
            if ($.browser.msie) {
              $ndlaTooltip.children('.arrow').stop().animate({opacity: 0.0}, 200, function() {$ndlaTooltip.hide()});
            }
          });
        }
    });
})(jQuery);